<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
/**
 * First Change Commit test
 */

class Artist extends Model
{

    public static function getFeeTypes() {
        return [
            1 => '$',
            2 => '$$',
            3 => '$$$',
            4 => '$$$$'
        ];
    }

    public static function getFeeType($id) {
        $feeTypes = self::getFeeTypes();

        $keys = array_keys($feeTypes);
        $min = min($keys);
        $max = max($keys);

        $id = ($id < $min) ? $min : $id;
        $id = ($id > $max) ? $max : $id;
        return $feeTypes[$id];
    }

    public static function getFeeTypesText() {
        return [
            1 => ['sign' => '$', "spaces" => "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;", "text" => "min. $500"],
            2 => ['sign' => '$$', "spaces" => "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;", "text" => "min. $1500"],
            3 => ['sign' => '$$$', "spaces" => "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;", "text" => "min. $3000"],
            4 => ['sign' => '$$$$', "spaces" => "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;", "text" => "min. $5000"],
        ];
    }

    public static function getFeeTypeText($id) {
        $feeTypes = self::getFeeTypesText();

        $keys = array_keys($feeTypes);
        $min = min($keys);
        $max = max($keys);

        $id = ($id < $min) ? $min : $id;
        $id = ($id > $max) ? $max : $id;
        return $feeTypes[$id];
    }

    public function eventSeen()
    {
        return $this->belongsToMany('App\Event', 'artist_see_event');

    }


    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function genres() {
        return $this->belongsToMany('App\Genre', 'artist_genre');
    }

    public function eventTypes() {
        return $this->belongsToMany('App\EventType', 'artist_event_types');
    }

    public function songTypes() {
        return $this->belongsToMany('App\SongType', 'artist_song_types');
    }

    public function applyEvent()
    {
        return $this->belongsToMany(EventChange::class, 'artists_applies_changes','artist_id')->withPivot('apply');
    }

    public static function genre($id)
    {
        return static::leftJoin(
            'user_role',
            'user_role.user_id', '=', 'users.id'
        )->leftJoin(
            'roles',
            'roles.id', '=', 'user_role.role_id'
        )->where('users.id', '=', $id);
    }

    public static function active()
    {
      return static::leftJoin(
        'users',
        'users.id', '=', 'artists.user_id'
      )->where('active', '=', 1);
    }

    public static function inactive()
    {
      return static::leftJoin(
        'users',
        'users.id', '=', 'artists.user_id'
      )->where('active', '=', 0);
    }

    public static function getArtistTypes() {
        return ["Band", "Artist"];
    }

    public static function fullInfo($userId) {
        $user = DB::table('users as u')
            ->select('u.id', 'u.avatar', 'u.name',
                's.spotify_uri',
                's.instagram_user',
                's.channel_name',
                's.facebook_id',
                's.twitter_handle',
                's.website',
                'a.sounds_like', 'a.fee', 'a.description', 'a.hometown' )
            ->join('artists as a', 'a.user_id', '=', 'u.id')
            ->join('socialmedia as s', 's.user_id', '=', 'u.id')
            ->where('u.id', '=', $userId)
            ->first();

        if ($user){
            $genres = DB::table('genres')
                ->select('genres.*')
                ->join('artist_genre', 'genres.id', '=', 'artist_genre.genre_id')
                ->join('artists', 'artists.id', '=', 'artist_genre.artist_id')
                ->where('artists.user_id', $userId)
            ;
            $user->genres = $genres->get();
        }

        return $user;
    }

    public function manager()
    {
        return $this->hasOne('App\User', 'id', 'manager_user_id');
    }

    public function changes()
    {
        return $this->hasMany(EventChange::class);
    }

    public function videos()
    {
        return $this->hasMany(ArtistVideos::class);
    }

}
