<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class ArtistRequest extends Model
{
    protected $table = 'artist_requests';
    public $timestamps = false;

}
