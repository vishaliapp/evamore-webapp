<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArtistVideos extends Model
{
    protected $fillable = ['path', '*'];

    public function artist()
    {
        return $this->belongsTo(Artist::class);
    }
}
