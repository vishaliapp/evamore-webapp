<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App;


class Bid extends Model
{
    protected $fillable = [
        'id', 'type', 'artist_user_id', 'event_id', 'price',
        'status', 'is_new_event', 'created_at', 'artist_paid_at',
        'artist_fee', 'autopay', 'payment_method', 'note'
    ];

    protected $table = 'bid';
    public $timestamps = false;

    public function event() {
        return $this->belongsTo('App\Event', 'event_id');
    }

    public function token()
    {
        return $this->hasOne('App\ForwardPayment');
    }

    public function artistUser()
    {
        return $this->belongsTo('App\User', 'artist_user_id');
    }

    public function artists()
    {
        return $this->hasMany(Artist::class,  'user_id','artist_user_id');
    }

    public function contract() {
        return $this->hasOne('App\BidContract');
    }

    public function getOrder() {
        $orderService = App::make("orderService");
        return $orderService->getOrderByBid($this);
    }

    /**
     * EVENTS
     */
    public static function bidSaving($bid)
    {
        /**
         * Block changing the BID if order already has been created
         */
        $order = $bid->getOrder();
        if ($order) {
            $original_price = $bid->getOriginal('price');
            if ($original_price != $bid->price) {
                throw new \Exception('The bid price cannot be changed after order has been created');
            }
        }
        return true;
    }
}
