<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 07.03.17
 * Time: 9:08
 */

namespace App;

use App\Events\contractGenerated;
use App\ModelType\TimeZoneType;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Mpdf\Mpdf;

class BidContract extends Model {
    protected $table = 'bid_contract';
    public $timestamps = false;

    protected $events = [
        'saved' => contractGenerated::class,
    ];

    protected $casts = [
        'request_json' => 'array',
    ];

    public function generateContract($type)
    {
        $userService = \App::make("userService");
        $artistUser = $userService->getUser($this->bid->artist_user_id);
        $planerUser = $userService->getUser($this->bid->event->user_id);

        $eventDateStart = new Carbon($this->bid->event->starttime);
        $eventDateStart->setTimezone(new \DateTimeZone(\App\ModelType\TimeZoneType::getTimeZone($this->bid->event->time_zone)));
        $startDate = $eventDateStart->format("m/d/y \a\\t h:i a T");

        $request_json = $this->request_json && count($this->request_json) ? $this->request_json
                                              : $this->bid->event->splitStartDate();
        $view = \View::make('contract._contract_text', [
            'time_zone_title' => TimeZoneType::getTimeZoneTitle($this->bid->event->time_zone),
            'plannerUser' => $planerUser,
            'artistUser' => $artistUser,
            'type' => $type,
            'bid' => $this->bid,
            'event' => $this->bid->event,
            'startDate' => $startDate,
            'request_json' =>$request_json,
            'created_at' => date('m/d/y', $this->request_json['created_at']),
        ]);
        $contents = $view->render();
        $mpdf = new Mpdf();
        $mpdf->WriteHTML($contents);

        /**
         * Mkdir
         */
        $destinationPath = implode("/", [base_path(), 'uploads']);
        $destinationPath .= "/contracts";
//        $destinationPath .= "/contracts/" . date('Y') . "/" . date('m');
        if (!is_dir($destinationPath)){
            \File::makeDirectory($destinationPath, 0777, true);
        }
        $event_name = preg_replace("/[^-a-zA-Z_\d]/", '', $this->bid->event->name);
        $pdf_name = "{$event_name}_" . date("mdY") . "(#{$this->bid->id})_".$type;
        $full_path = "{$destinationPath}/{$pdf_name}.pdf";
        $mpdf->Output($full_path, 'F');
//        $mpdf->Output();
        /**
         * Saving to the Files table
         */
        $systemUser = User::getSystemUser();
        $file_id = \App\File::insertGetId([
            'user_id' => $systemUser->id,
            'entity' => 'bid_contract',
            'entity_id' => $this->id,
            'path' => $full_path,
            'name' => "{$pdf_name}.pdf",
            'bid_contract_id' => $this->bid->contract->id,
            'type' => $type
        ]);
        $file = \App\File::find($file_id);

        /**
         * Save this file to the BidContract
         */
        $this->file_id = $file_id;
        $this->save();


        /**
         * Create comment
         */
//        $commentService = \App::make("commentService");
//        $comment = $commentService->createComment($this->bid->event, $systemUser, 'Contract');
//        $fileService = \App::make("fileService");
//        $fileService->bindFile($file, 'comment', $comment->id);

        /**
         * Notification
         */
        $emailService = \App::make('emailNotificationService');
        $emailService->notifyAboutContract($this, $type);
    }

    public function bid()
    {
        return $this->belongsTo('App\Bid');
    }

    public function file()
    {
        return $this->hasMany('App\File');
    }
}