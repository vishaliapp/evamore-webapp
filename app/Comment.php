<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 13.04.17
 * Time: 10:03
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model {

    CONST ALLOWED_FILES_AMOUNT = 5;

    protected $table = 'comment';
    public $timestamps = false;

    public function event() {
        return $this->belongsTo('App\Event');
    }

    public function user() {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function files() {
        return $this->hasMany(File::class, 'entity_id');
    }
}