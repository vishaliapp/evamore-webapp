<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 26.01.17
 * Time: 12:30
 */

namespace App\Console\Commands;


use App;
use Illuminate\Console\Command;
use App\ModelType\BidType;
use App\ModelType\OrderType;
use App\ModelType\JournalType;
use App\Bid;
use DB;


class EventsCompletedToPay extends Command {
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'event:completed:to_pay';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "EV-216 Admin is Notified when an Event is Completed to Pay the Artist";

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $bidService = App::make("bidService");
        $journalService = App::make("journalService");
        $emailService = App::make('emailNotificationService');

        $bids = $bidService->getBidsForManualPaymentToArtist();
        foreach($bids as $bid) {
            $emailService->notifyAdminsAboutPaymentsToArtist($bid);
            $journalService->add("bid", $bid->id, 0, JournalType::ACT_NOTIFY_ADMIN_FOR_PAY, $description="");
        }
    }
}