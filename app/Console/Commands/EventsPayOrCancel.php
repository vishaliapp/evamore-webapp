<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 26.01.17
 * Time: 12:30
 */

namespace App\Console\Commands;

use App;
use Illuminate\Console\Command;
use App\ModelType\EventType;
use App\ModelType\OrderType;
use App\Event;
use App\Bid;
use App\ModelType\BidType;
use DB;

class EventsPayOrCancel extends Command {
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'event:pay:or:cancel';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "EV-166 Cancel Event if Event Planner Doesn't Pay in Full 48 Hours Before Event Date";

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $this->autoPaymentEvents();

//        $this->cancelNotPaidEvents();
    }

    /**
     * Automated payment for rest 50% of amount for bids
     */
    private function autoPaymentEvents(){
        $bids = Bid::select('b.*')
            ->from("bid as b")
            ->join('events as e', 'e.id', '=', 'b.event_id')
            ->leftJoin('order as o', 'o.bid_id', '=', 'b.id')
            ->whereRaw('o.status = '.OrderType::HALF_PAID)
            ->whereRaw("e.starttime > NOW()")
            ->whereRaw("NOW() > DATE_SUB(e.starttime, INTERVAL 48 HOUR)")
            ->where("e.status", "=", EventType::PUBLISHED)
            ->where("b.autopay", BidType::AUTOMATED_PAYMENT_ON)
            ->distinct()->get();

        $orderService = App::make('orderService');
        $payscapeService = App::make("payscapeService");

        foreach($bids as $bid) {
            $planerUser = $bid->event->user;

            $payscapeVault = $planerUser->payscapeVault;
            if ($payscapeVault) {
                $paymentData['vault_id'] = $payscapeVault->vault_id;

                $order = $orderService->getOrderByBid($bid);

                if (!$order) continue;

                if ($order->status != OrderType::HALF_PAID) continue;

                $payAmount = $order->price * 0.5;
                $orderItem = $orderService->createOrderItem($order, $payAmount);

                try {
                    $payscapeService->saleOrderItem($orderItem, $paymentData);

                    if ($orderItem->transaction_id > 0){
                        $orderService->setOrderStatus($order, OrderType::FULL_PAID);
                    }

                } catch ( \Exception $e ) {
                    $emailService = App::make('emailNotificationService');

                    $emailService->notifyAboutAutomatePaymentError($bid, $e->getMessage());
                }
            }
        }
    }

    private function cancelNotPaidEvents() {
        $events = Event::select('e.*')
            ->from("events as e")
            ->join('bid as b', 'e.id', '=', 'b.event_id')
            ->leftJoin('order as o', 'o.bid_id', '=', 'b.id')
            ->whereRaw('(o.status = '.OrderType::HALF_PAID.' OR o.status IS NULL)')
            ->whereRaw("e.starttime > NOW()")
            ->whereRaw("NOW() > DATE_SUB(e.starttime, INTERVAL 48 HOUR)")
            ->where("e.status", "=", EventType::PUBLISHED)
            ->distinct()->get()
        ;

        $eventService = App::make('eventsService');
        foreach($events as $event) {
            $eventService->cancelEvent($event);
        }
    }
}