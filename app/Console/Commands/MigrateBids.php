<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 26.01.17
 * Time: 12:30
 */

namespace App\Console\Commands;

use App;
use Illuminate\Console\Command;
use App\ModelType\EventType;
use App\ModelType\OrderType;
use App\Event;
use App\Bid;
use App\ModelType\BidType;
use DB;

class MigrateBids extends Command {
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bids:migrate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Migrate artists requests and planer requests as bids";

    private $bidService;
    private $eventService;
    private $userService;
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->bidService = App::make("bidService");
        $this->eventService = App::make("eventsService");
        $this->userService = App::make("userService");
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        echo "\n";
        echo "Artists requests migration\n";
        $this->migrateArtistsRequests();

        echo "\n";
        echo "Planers requests migration\n";
        $this->migratePlanersRequests();
    }

    public function migrateArtistsRequests() {
        $sql = "SELECT * FROM artist_requests";

        $requests = DB::select($sql);

        foreach ($requests as $request) {
            $artistUser = $this->userService->getUser($request->artist_user_id);
            $event = $this->eventService->getEvent($request->event_id);

            if(is_null($artistUser)){
                echo "Artist {$request->artist_user_id} not exist\n";
            }

            if(is_null($event)){
                echo "Event {$request->event_id} not exist\n";
            }

            if (!is_null($artistUser) && !is_null($event)) {
                $artistBid = $this->bidService->getArtistBidToEvent($event, $artistUser);
                if (is_null($artistBid)){
                    $this->bidService->createArtistBid($event, $artistUser);
                    echo "ok\n";
                }
            }
        }
    }

    public function migratePlanersRequests() {
        $sql = "SELECT * FROM planer_request";

        $requests = DB::select($sql);

        foreach ($requests as $request) {
            $artistUser = $this->userService->getUser($request->artist_user_id);
            $event = $this->eventService->getEvent($request->event_id);

            if(is_null($artistUser)){
                echo "Artist {$request->artist_user_id} not exist\n";
            }

            if(is_null($event)){
                echo "Event {$request->event_id} not exist\n";
            }

            if (!is_null($artistUser) && !is_null($event)) {
                $planerBid = $this->bidService->getPlanerBidToEvent($event, $artistUser);
                if (is_null($planerBid)){
                    $bid = $this->bidService->createPlanerBid($event, $artistUser, $request->is_new_event);
                    $this->bidService->setStatus($bid, $request->status);

                    echo "ok\n";
                }
            }
        }
    }

}