<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 26.01.17
 * Time: 12:30
 */

namespace App\Console\Commands;


use App;
use Illuminate\Console\Command;
use App\ModelType\BidType;
use App\ModelType\OrderType;
use App\ModelType\JournalType;
use App\Bid;
use DB;


class NotificationAboutNotFullPaidEventReminder extends Command {
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'event:not_paid:full';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "EV-165 Send Payment Reminder Notifications to Event Planner if Not Paid in Full";

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $bidService = App::make("bidService");
        $journalService = App::make("journalService");
        $emailService = App::make('emailNotificationService');

        $deadlineBeforeHours = [
            JournalType::ACT_NOTIFY_PLANER_NOT_PAID_1W, //EV-165 EVAmore sends Payment Reminder email 216 hrs before event starts to Event Planner
            JournalType::ACT_NOTIFY_PLANER_NOT_PAID_24H, //EV-232 EVAmore sends Payment Reminder email 72 hrs before event starts to Event Planner
            JournalType::ACT_NOTIFY_PLANER_NOT_PAID_12H, //EV-242 EVAmore sends Payment Reminder email 60 hrs before event starts to Event Planner
            JournalType::ACT_NOTIFY_PLANER_NOT_PAID_6H, //EV-243 EVAmore sends Payment Reminder email 54 hrs before event starts to Event Planner
            JournalType::ACT_NOTIFY_PLANER_NOT_PAID_DEADLINE, //EV-245 EVAmore should email Admin and EP 48 hrs before event start instead of cancelling event automatically
        ];

        foreach ($deadlineBeforeHours as $journalAction) {
            $bids = $bidService->getNotPaidFullBids($journalAction);
            foreach($bids as $bid) {
                $emailService->notifyPlanerAboutNotPaidBidInFull($bid, $journalAction);
                $journalService->add("bid", $bid->id, 0, $journalAction);
            }
        }
    }
}