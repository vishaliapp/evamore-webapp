<?php

namespace App\Console\Commands;

use App\ModelType\BidType;
use Illuminate\Console\Command;
use DB;
use App;
use App\Bid;
use App\ModelType\JournalType;


class NotifyArtistsByGenre extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'delayed:artists:notification';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Notify artists about event with their genres after 48 hours.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $sql = "
          SELECT b.*
          FROM bid AS b
          INNER JOIN events AS e ON e.id = b.event_id
          LEFT OUTER JOIN journal
            ON journal.entity_id = b.id
            AND journal.entity = 'bid'
            AND journal.action = :action
          WHERE b.status = :bid_status
          AND b.type = :bid_type
          AND b.is_new_event = 1
          AND e.starttime > NOW()
          AND e.status = :event_status
          AND b.created_at <= DATE_SUB(NOW(), INTERVAL 48 HOUR)
          AND journal.id IS NULL";

        $rows = DB::select($sql, [
            'action' => JournalType::ACT_NOTIFY_ARTIST_ABOUT_GENRE_EVENT_48H,
            'bid_status' => BidType::STATUS_NEW,
            'bid_type' => BidType::PLANER_TYPE,
            'event_status' => App\ModelType\EventType::PUBLISHED
        ]);

        $bids = [];
        foreach($rows as $row) {
            $bids[] = new Bid(json_decode(json_encode($row), true));
        }

        $emailService = App::make('emailNotificationService');
        $userService = App::make('userService');
        $journalService = App::make("journalService");

        foreach($bids as $bid) {
            $planerUser = $userService->getUser($bid->event->user_id);
            $emailService->notifyArtistAboutNewEvent($bid->event, $planerUser, [$bid->artist_user_id]);
            $journalService->add("bid", $bid->id, 0, JournalType::ACT_NOTIFY_ARTIST_ABOUT_GENRE_EVENT_48H, $description="");
            sleep(1);
        }
    }
}
