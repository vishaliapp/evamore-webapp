<?php

namespace App\Console\Commands;

use App\Event;
use App\ModelType\PlanerRequestType;
use App\PlanerRequest;
use Illuminate\Console\Command;
use DB;
use App;


class QueueNotifier extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email:queue:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send emails from emails queue';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $emailQueueService = App::make("emailQueueService");
        $emailQueueService->sendFromQueue(30);
    }
}
