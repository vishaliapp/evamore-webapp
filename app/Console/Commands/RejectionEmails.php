<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Mail;

class RejectionEmails extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rejection:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
   /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $data = ["domain"=>"evamore.co", "eventname" => "Super awesome show", "artist" => "Mikhail's Rock Show"];
        echo "Running rejection queue \n";
        Mail::send('emails.artist.reject_artist', $data, function ($message) {
                $message->from('mikhail@evamore.co', 'EVAmore');
                $message->to('mikhail@evamore.co')->subject("You were not selected for an event");
        });
        //
    }
}


