<?php

namespace App\Console\Commands;

use App\Repositories\PayscapeGateway;
use Illuminate\Console\Command;
use App;
use App\Event;
use App\User;
use App\ModelType\JournalType;


class test_email extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test:emails';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send emails';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {





        $bidService = App::make("bidService");
        $journalService = App::make("journalService");
        $emailService = App::make('emailNotificationService');
        $bid = $bidService->getBid(28);
        $emailService->notifyArtistAboutPaymentConfirmation($bid);


        //$emailService->notifyPlanerAboutNotPaidBidInFull($bid, JournalType::ACT_NOTIFY_PLANER_NOT_PAID_DEADLINE);

//        $emailService->notifyPlanerAboutNotPaidBidInFull($bid, App\ModelType\JournalType::ACT_NOTIFY_PLANER_NOT_PAID_6H);

    }
}
