<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 13.01.17
 * Time: 8:55
 */

namespace App;

use Illuminate\Database\Eloquent\Model;


class EmailQueue extends Model {
    protected $table = 'email_queue';
    public $timestamps = false;
}