<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    public static function getEventTypes() {
        return ['Concert', 'Show'];
    }

    public function genres() {
        return $this->belongsToMany('App\Genre', 'event_genre');
    }

    public function type() {
        return  $this->belongsTo('App\EventType', 'event_type');
    }

    public function songTypes() {
        return $this->belongsToMany('App\SongType', 'event_song_types');
    }

    public function bids()
    {
        return $this->hasOne('App\Bid');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function comments() {
        return $this->hasMany('App\Comment', 'event_id');
    }

    public function commentsByUser($user)
    {
        return $this->comments->filter(function ($comment) use ($user) {
            return $comment->user_id == $user->id;
        });
    }

    public function artistSee()
    {
        return $this->belongsToMany('App\Artist', 'artist_see_event');
    }

    public function splitStartDate()
    {
        $result = [];
        /**
         * Start Date Without Offset
         */
        $eventDateStart = new Carbon($this->starttime);
        $eventDateStart->setTimezone(new \DateTimeZone(\App\ModelType\TimeZoneType::getTimeZone($this->time_zone)));
        $result['format_startdate'] = $eventDateStart->format("m/d/y \a\\t h:i a T");
	
        /**
         * Load In Date With offset 90 minutes
         */
        $loadInTime = new Carbon($this->starttime);
        $loadInTime->modify("-90 minutes");
        $loadInTime->setTimezone(new \DateTimeZone(\App\ModelType\TimeZoneType::getTimeZone($this->time_zone)));
        $result['load_in_time_hours'] = $loadInTime->format("h");
        $result['load_in_time_minutes'] = $loadInTime->format("i");
        $result['load_in_time_ampm'] = $loadInTime->format("A");

        /**
         * Load Out Date With offset = duration
         */
        $loadOutTime = new Carbon($this->starttime);
        $loadOutTime->modify("+{$this->duration} minutes");
        $loadOutTime->setTimezone(new \DateTimeZone(\App\ModelType\TimeZoneType::getTimeZone($this->time_zone)));
        $result['load_out_time_hours'] = $loadOutTime->format("h");
        $result['load_out_time_minutes'] = $loadOutTime->format("i");
        $result['load_out_time_ampm'] = $loadOutTime->format("A");
        return $result;
    }

    public function changes()
    {
        return $this->hasMany(EventChange::class);
    }

}
