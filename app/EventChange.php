<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventChange extends Model
{
    public function event()
    {
        return $this->belongsTo(Event::class, 'event_id');
    }

    public function genres()
    {
        return $this->belongsToMany('App\Genre', 'event_genre', 'event_id');
    }

    public function type()
    {
        return  $this->belongsTo('App\EventType', 'event_type');
    }

    public function songTypes()
    {
        return $this->belongsToMany('App\SongType', 'event_song_types','event_id');
    }

    public function artistAplly()
    {
        return $this->belongsToMany(Artist::class, 'artists_applies_changes', 'event_change_id')->withPivot('apply');
    }

}
