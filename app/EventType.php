<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventType extends Model
{
    public function artists() {
        return $this->belongsToMany('App\Artist');
    }

    public function events() {
        return $this->hasMany('App\Event');
    }
}
