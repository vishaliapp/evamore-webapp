<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 13.04.17
 * Time: 10:03
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

class File extends Model {
    CONST MAX_FILE_SIZE = 20000;

    protected $table = 'file';
    public $timestamps = false;

    public function user() {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function bidContract()
    {
        return $this->belongsTo('App\BidContract');
    }
}