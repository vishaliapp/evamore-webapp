<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ForwardPayment extends Model
{
    protected $table = 'forwardPayment';
    protected $fillable = [
      'token','bidI','mail'
    ];
    public $timestamps = true;

    public function bid()
    {
        return $this->belongsTo('App\Bid','bid_id' );
    }
}
