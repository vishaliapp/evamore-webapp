<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Genre extends Model
{
    public function artists() {
        return $this->belongsToMany('App\Artist');
    }

    public function events() {
        return $this->belongsToMany('App\Event');
    }
    public function changes() {
        return $this->belongsToMany(EventChange::class);
    }
}
