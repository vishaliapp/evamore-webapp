<?php

if (! function_exists('asset')) {
    /**
     * Override standard function asset
     * Generate an asset path for the application.
     *
     * @param  string  $path
     * @param  bool    $secure
     * @param  bool    $addVersion
     * @return string
     */
    function asset($path, $secure = null, $addVersion = true)
    {
        return app('url')->asset($path, $secure, $addVersion);
    }
}
if (! function_exists('artist_budget_display')) {
    /**
     * Override standard function asset
     * Generate an asset path for the application.
     *
     * @param  string  $path
     * @param  bool    $secure
     * @param  bool    $addVersion
     * @return string
     */
    function artist_budget_display($real_price, $addVersion = true)
    {
        return $real_price * .9;
    }
}

