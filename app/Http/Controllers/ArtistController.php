<?php

namespace App\Http\Controllers;


use App\ArtistVideos;
use App\Bid;
use App\File;
use App\SongType;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\User;
use App\Artist;
use App\Social;
use App\Genre;
use App\EventType as EventTypes;
use App\Event;
use App\ModelType\BidType;
use App\ModelType\EventType;
use App;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use Mpdf\Mpdf;
use PhpOffice\PhpWord\PhpWord;
use PhpOffice\PhpWord\Shared\Html;
use Session;
use Auth;
use DB;
use Image;
use Validator;
use Instagram;
use PDF;


class ArtistController extends Controller
{

    /**
     * @return mixed
     */
    public function activeArtistsJSON()
    {
        $currentUser = Auth::user();

        $artists = Artist::active()->get();
        foreach ($artists as $artist) {
            $artist->password = null;
            $artist->remember_token = null;
            $artist->avatar = Image::url($artist->avatar, 95, 95, array('crop'));
        }

        return [
            'is_auth' => $currentUser ? true : false,
            'artists' => $artists
        ];
    }

    /**
     * @return mixed
     */
    public function activeArtistJSON(Request $request, $id)
    {
        $currentUser = Auth::user();

        $success = false;

        $response = [
            'success' => $success,
            'is_auth' => $currentUser ? true : false,
        ];
        $user = Artist::fullInfo($id);
        if ($user) {
            $user->avatar = Image::url('uploads/avatars/' . $user->avatar, 115, 115, array('crop'));
            $response['user'] = $user;

            $response['success'] = true;
        }

        return $response;
    }

    /*
    |----------------------
    |   Get select artist
    |---------------------
    */
    public function show(Request $request, $username)
    {
        $bidService = App::make("bidService");
        $eventsService = App::make("eventsService");
        $userService = App::make("userService");
        if (!$userService->getUserByUsername($username)->artist) {
            return view('errors.404');
        }
        if (!$artistUser = $userService->getUserByUsername($username)) {
            return view('errors.404');
        }

        $artistUser = $userService->getUserByUsername($username);
        $artistSocial = $artistUser->social()->first();
        $artistAbout = $artistUser->artist()->first();
        $artistInstagram = $userService->getArtistInstagram($artistAbout->id);
        $currentUser = Auth::user();
        if (!$currentUser) {
            $artists = Artist::active()->orderBy('artists.id')->get();
            $position = $artists->search(function ($person, $key) use ($username) {
                return $person->username == $username;
            });
            $page = ceil(($position + 1) / 8);

            return redirect()->route('home_artists', ["page" => $page])->with('artist', $artistUser->username);
        }

        $rolesList = $currentUser->rolesList();
        $isUserAdmin = in_array("ADMIN", $rolesList);
        $isUserCanBook = !in_array("ARTIST", $rolesList);

        $isManager = false;
        if (in_array("ARTIST_MANAGER", $rolesList) && $artistUser->artist->manager_user_id == $currentUser->id) {
            $isManager = true;
        }

        $canConfirmRequest = true;
        $bidId = $request->get('bid', 0);

        $bid = null;
        if ($bidId > 0) {
            $bid = $bidService->getBid($bidId);
            if ($bid) {
                if ($bid->event->user_id == $currentUser->id) {
                    $canConfirmRequest = true;
                }
            }
        }

        $planerEvents = [];
        $planerEventsForArtist = [];
        if ($isUserCanBook) {
            $eventsFilter['status'] = EventType::PUBLISHED;
            $planerEvents = $eventsService->getPlanerEvents($currentUser, $eventsFilter);
            foreach ($planerEvents as $event) {
                $d = new \DateTime($event->starttime);
                $d->setTimezone(new \DateTimeZone(App\ModelType\TimeZoneType::getTimeZone($event->time_zone)));
                $event->format_starttime = $d->format("d/m/Y h:i a T");
            }
            $planerEventsForArtist = $eventsService->getPlanerEventsForArtist($currentUser, $artistUser);
        }

        $artistData = Artist::where('id', $artistAbout->id)->get(['banner_directory', 'hometown', 'description', 'sounds_like', 'statetown', 'fee'])->first();
        $artistUserData = Artist::where('id', $artistAbout->id)->with('user')->select()->get()->first();
        $artistData = [
            'banner_directory' => 'Avatar',
            'hometown' => 'Hometown',
            'description' => 'Description',
            'sounds_like' => 'Sounds Like',
            'statetown' => 'Statetown',
            'fee' => 'Fee',
            'name' => 'Name'
        ];

        $artistPdfSocial = [
            'Youtube channel' => 'channel_name',
            'Twitter' => 'twitter_handle',
        ];

        return view('artists.details', [
            'artistData' => $artistData,
            'artistPdfSocial' => $artistPdfSocial,
            'artistUser' => $artistUser,
            'isUserCanBook' => $isUserCanBook,
            'isUserAdmin' => $isUserAdmin,
            'isManager' => $isManager,
            'currentUser' => $currentUser,
            'left_menu_selected' => 'artists',
            'artistSocial' => $artistSocial,
            'artistAbout' => $artistAbout,
            'artistInstagram' => $artistInstagram,
            'canConfirmRequest' => $canConfirmRequest,
            'bid' => $bid,
            'planerEvents' => $planerEvents,
            'planerEventsForArtist' => $planerEventsForArtist
        ]);
    }


    /**
     * Artist confirm planers request
     * (planer sent request earlier, artist accept or decline that request)
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|Response
     */
    public function confirmPlanerRequestAction(Request $request)
    {

        $currentUser = Auth::user();

        if (!$currentUser->hasRole('ARTIST')) {
            return response()->view('errors.403');
        }

        $validator = Validator::make($request->all(), [
            'bid_id' => 'required',
            'action' => 'required|in:accept,decline',
            'price' => 'sometimes|integer|min:0',
        ]);

        $bidService = App::make("bidService");
        $event = Event::find($request->event_id);

        if ($validator->fails()) {
            $errors = $validator->errors();
            $messages = [];
            foreach ($errors->all() as $error) {
                $messages[] = $error;
            }
            return Redirect::back()->with('confirmPlanErrors', $messages);
        }
        $bid = $bidService->getBid($request->get('bid_id'));

        try {

            if (!$bid) {
                throw new \Exception("Wrong request.");
            }

            if ($bid->status != BidType::STATUS_NEW) {
                throw new \Exception("Request to event confirmed earlier.");
            }

            $action = $request->get('action');

            if ($action == 'accept') {
                if (!$bidService->hasArtistBidsForEvent($event, $currentUser)) {
                    $artist_request = $request->get('price');
                    $artist_fee = ($artist_request * .9);
                    $bid_price = $artist_request + $artist_request * .1;
                    $bidService->setBidArtistFee($bid, $artist_fee);
                    $bidService->setBidPrice($bid, $bid_price);
                    $bidService->acceptBidByArtist($bid);

                    $emailService = App::make("emailNotificationService");
                    $emailService->notifyPlanerAboutBidPrices($event, $bid);

//                    $emailService->notifyArtistAboutNewEventFee($currentUser, $event);

                    Session::flash('message', 'Event request accepted!');
                } else {
                    throw new \Exception("Request to event accepted earlier.");
                }
            } else {
                $declineReasonText = $request->get("declineReasonText", "");
                $declineReasonCode = $request->get("declineReason", 3);
                $bidService->declineBidByArtist($bid, $declineReasonCode, $declineReasonText);
            }

            $status = ($action == "accept") ? BidType::STATUS_ACCEPTED : BidType::STATUS_DECLINED;
            Session::flash('message', BidType::getStatusName($status) . " event request");

        } catch (\Exception $e) {
            return Redirect::back()->withMessage($e->getMessage());

        }

        return redirect()->route('event_show', ['id' => $bid->event_id]);
    }


    public function addVideos(Request $request, $artistId)
    {
        try {
            $artistUser = Artist::where('id', $artistId)->first();
            $file = $request->file('file');
            if ($file) {
                $filename = $artistUser->id . Carbon::now()->toDateTimeString() . $file->getClientOriginalName();
                $path = 'uploads/videos/';
                $file->move($path, $filename);
                if ($artistUser->videos()->count() < 4) {
                    $artistUser->videos()->create([
                        'path' => $filename,
                        'artist_id' => $artistUser->id,
                        'description' => 'Artist video'
                    ]);
                } else {
                    return [
                        'success' => false,
                        'message' => 'You can have maximum 4 videos'
                    ];
                }
                return [
                    'success' => true,
                    'message' => 'You successfully uploaded videos.'
                ];
            }
        } catch (\Exception $exception) {
            return [
                'success' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     *
     * Send Email to Admin when Event Planner books an Artist
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     *
     */
    public function requestArtistBookByPlanner(Request $request)
    {
        $currentUser = Auth::user();
        $artistUser = User::find($request->get('artist_id', 0));
        $artist = Artist::where('user_id', '=', $artistUser->id)->firstOrFail();
        $eventId = $request->get('event_id', 0);
        if (!$artistUser || $eventId === 0) {
            abort(404);
        }

        if ($eventId === "new") {
            return redirect()->route('createEvent', [
                "booked_user_id" => $artistUser->id
            ]);
        }

        $event = Event::find($eventId);

        if (!$event) {
            abort(404);
        }

        $bidService = App::make("bidService");
        $bid = $bidService->getPlanerBidToEvent($event, $artistUser);
        $managerArtistsBids = Bid::where('event_id', $event->id)->get();
        $count = 0;
        if ($artistUser->artist->manager) {
            $managerId = $artistUser->artist->manager->id;
            foreach ($managerArtistsBids as $managerArtistsBid) {
                if ($managerArtistsBid->artistUser->artist->manager_user_id == $managerId) {
                    $count += 1;
                }
            }
        }

        if ($bid) {
            Session::flash('message', "You sent request to " . $artistUser->name . " for '" . $event->name . "' earlier.");
        } else {
            $bidService->createPlanerBid($event, $artistUser, false);
            $emailService = App::make("emailNotificationService");
            $emailService->notifyArtistAboutNewPersonalEvent($artistUser, $currentUser, $event, $count);

            $emailService->planerRequestsToBookArtist($currentUser, $artistUser, $event); //send email to admins

            Session::flash('message', "We got your request. Sit tight.");
        }
        return redirect()->route('artist_show', ['username' => $artistUser->username]);
    }


    /*
    |-------------------------------
    |   Get artist request response
    |-------------------------------
    */
    public function getArtistThanks()
    {
        $user = Auth::user();
        return view('artists.thanks', ['user' => $user]);
    }

    /*
    |----------------------------------
    |   Get - artist to activate page
    |----------------------------------
    */
    public function postArtistActivationChange(Request $request)
    {
        $action = $request->get('action', false);
        $currentUser = Auth::user();

        if (!$currentUser->hasRole('ADMIN')) {
            return response()->view('errors.403');
        }

        $usersIds = $request->get('artists', []);
        if (!is_array($usersIds)) {
            $usersIds = json_decode($usersIds);
        }

        if (count($usersIds) > 0) {
            $userService = App::make('userService');
            if ($action == 'activate') {
                $userService->changeUserActivation($usersIds, 1);
                Session::flash('message', count($usersIds) . " artists successfully activated.");
            } elseif ($action == 'deactivate') {
                $userService->changeUserActivation($usersIds, 0);
                Session::flash('message', count($usersIds) . " artists successfully deactivated.");
            }
        } else {
            Session::flash('message', "You should select artists.");
        }

        return redirect()->route('dashboard');
    }

    /*
    |-------------------------------------
    |   Get all artist activate requests
    |-------------------------------------
    */
    public function getAccountRequests()
    {
        return Artist::inactive()->get();
    }

    /**
     * Save information about artist by artist
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function editArtist(Request $request, $id)
    {
        $currentUser = Auth::user();
        $userService = App::make("userService");
        $artistUser = $userService->getUser($id);
        $isArtistManager = false;
        if ($currentUser != null) {
            $isArtistManager = $currentUser->hasRole('ARTIST_MANAGER');

        }
        if (!$artistUser) {
            abort("404");
        }

        $currentUser = Auth::user();
        $isArtistManager = $currentUser->hasRole('ARTIST_MANAGER');

        $canEdit = false;
        if ($currentUser->hasRole('ADMIN')) {
            $canEdit = true;
        } else if ($currentUser->hasRole('ARTIST_MANAGER') && $artistUser->artist->manager_user_id == $currentUser->id) {
            $canEdit = true;
        } else if ($currentUser->hasRole('ARTIST') && ($currentUser->id == $id)) {
            $canEdit = true;
        }

        if (!$canEdit) {
            return response()->view('errors.403');
        }
        $feeTypes = Artist::getFeeTypes();
        $feeIds = array_keys($feeTypes);
        $minFeeId = min($feeIds);
        $maxFeeId = max($feeIds);

        if ($request->getMethod() == 'POST') {


            $customAttributes = [
                'name' => 'Name',
                'email' => 'Email',
                'secondaryEmail' => 'Secondary Email',
                'artist.fee' => 'Minimum Amount',
                'artist.set_length' => 'Maximum Set Length',
                'phone' => 'Phone Number',
                'artist.hometown' => 'Current Location',
                'artist.equipments' => 'Equipments',
                'artist.description' => 'Description',
                'artist.mileage' => 'Mileage',
                'social.spotify_uri' => 'Spotify URI',
                'social.instagram_user' => 'Insagram URI',
                'social.channel_name' => 'Youtube URI',
                'social.facebook_id' => 'Facebook URI',
                'social.twitter_handle' => 'Twitter URI',
                'social.website' => 'Website',
                'genre' => 'Genre',
                'eventTypes' => 'Event Types',
                'avatar' => 'Image',
            ];
            $this->validate($request, [
                'name' => 'required|max:255',
                'email' => 'required|emailDublicateValidator:' . $artistUser->id,
                'secondaryEmail' => (($isArtistManager) ? 'email|max:255' : ''),
                'artist.fee' => 'required',
                'artist.set_length' => 'required|numeric|min:0',
                'phone' => 'required|max:255',
                'artist.hometown' => 'required|max:255',
                'artist.equipments' => 'max:255',
                'artist.description' => 'required|max:500',
                'artist.mileage' => 'integer|min:25',
                'social.spotify_uri' => 'max:255|regex:/(^([a-zA-Z0-9()&:%\/_.-?]+)?$)/u',
                'social.instagram_user' => 'max:255|regex:/(^([a-zA-Z0-9()&:%\/_.-?]+)?$)/u',
                'social.channel_name' => ((str_contains($request['social.channel_name'], 'youtube.com')) ? ['max:255', 'regex:/^(http(s)?:\/\/)?((w){3}.)?youtu(be|.be)?(\.com)?\/.+/'] : ''),
                'social.facebook_id' => 'max:255|regex:/(^([a-zA-Z0-9()&:%\/_.-?]+)?$)/u',
                'social.twitter_handle' => 'max:255|regex:/(^([a-zA-Z0-9()&:%\/_.-?]+)?$)/u',
                'social.website' => 'max:255|siteUrlValidator',
                'genre' => 'required|present|array',
                'eventTypes' => 'required|present|array',
                'songTypes' => 'required|present|array',
                'avatar' => 'image',
            ], [], $customAttributes);


            $name = $request['name'];
            $email = $request['email'];
            $secondaryEmail = $request['secondaryEmail'];
            $mileage = $request['artist.mileage'];
            $equipments = $request['artist.equipments'];
            $fee = substr($request['artist.fee'], 1);
            $set_length = $request['artist.set_length'];
            $phone = $request['phone'];
            $hometown = $request['artist.hometown'];
            $place_lat = $request['artist.place_lat'];
            $place_lng = $request['artist.place_lng'];
            $description = $request['artist.description'];
            $time_zone = (int)$request['time_zone'];

            $soundsLike = [];
            foreach ($request['soundlike'] as $soundLike) {
                $sl = trim($soundLike);
                if ($sl != '') {
                    $soundsLike[] = $sl;
                };
            }

            if (sizeof($soundsLike != 0)) {
                $soundsLike = implode(", ", $soundsLike);
            } else {
                $soundsLike = '';
            }

            $spotify = $request['social.spotify_uri'];
            $instagram = $request['social.instagram_user'];
            $youtube = $request['social.channel_name'];
            $facebook = $request['social.facebook_id'];
            $twitter = $request['social.twitter_handle'];
            $website = $request['social.website'];

            $artistUser->name = $name;
            $artistUser->email = $email;
            $artistUser->phone = $phone;
            $artistUser->time_zone = $time_zone;

            $base64Image = $request->get('upload_image');
            if ($base64Image != '') {
                $pos = strpos($base64Image, ';');
                $type = explode(':', substr($base64Image, 0, $pos))[1];
                $type = explode('/', $type)[1];
                $filename = $artistUser->id . "_" . str_random(7) . "." . $type;
                $path = public_path('uploads/avatars') . "/" . $filename;
                $dataBase64 = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $base64Image));
                file_put_contents($path, $dataBase64);
                $artistUser->avatar = $filename;
            }
            $artistUser->save();

            $artist = Artist::where('user_id', '=', $artistUser->id)->firstOrFail();
            $artist->fee = $fee;
            $artist->secondary_email = $secondaryEmail;
            $artist->set_length = $set_length;
            $artist->hometown = $hometown;
            $artist->place_lat = $place_lat;
            $artist->place_lng = $place_lng;
            $artist->mileage = $mileage;
            $artist->equipments = $equipments;
            $artist->description = $description;
            $artist->sounds_like = $soundsLike;
            $artist->save();

            $genres = (sizeof($request['genre']) != 0 ? $request['genre'] : []);
            $initialGenres = $artistUser->artist->genres;

            foreach ($initialGenres as $ig) {
                if (!in_array($ig, $genres)) {
                    $artist->genres()->detach($ig);
                }
            }

            foreach ($genres as $genre) {
                $g = Genre::where('id', $genre)->first();
                $artist->genres()->attach($g);
            }

            $eventTypes = (sizeof($request['eventTypes']) != 0 ? $request['eventTypes'] : []);
            $initialEventTypes = $artist->eventTypes;
            $songTypes = (sizeof($request['songTypes']) != 0 ? $request['songTypes'] : []);

            $initialSongTypes = $artist->songTypes;
            foreach ($initialSongTypes as $iet) {
                if (!in_array($iet, $songTypes)) {
                    $artist->songTypes()->detach($iet);
                }
            }
            foreach ($songTypes as $type) {
                $t = SongType::where('id', $type)->first();
                $artist->songTypes()->attach($t);
            }

            foreach ($initialEventTypes as $iet) {
                if (!in_array($iet, $eventTypes)) {
                    $artist->eventTypes()->detach($iet);
                }
            }

            foreach ($eventTypes as $type) {
                $t = App\EventType::where('id', $type)->first();
                $artist->eventTypes()->attach($t);
            }

            $social = Social::where('user_id', '=', $artistUser->id)->firstOrFail();
            $social->spotify_uri = $spotify;
            $social->channel_name = $youtube;
            $social->facebook_id = $facebook;
            $social->twitter_handle = $twitter;
            $social->instagram_user = $instagram;
            $social->website = $website;
            $social->save();

            Session::flash('message', 'Account updated.');

            return redirect()->route('editartist', ['id' => $id]);
        } else {

            $mileage = [
                '25' => '25 miles',
                '50' => '50 miles',
                '150' => '150 miles',
                '250' => '250 miles',
                '500' => '500 miles',
            ];

            $equipments = [
                'yes' => 'Yes',
                'no' => 'No',
            ];

            $feeTypes = App\Artist::getFeeTypesText();

            foreach ($feeTypes as $key => $value) {
                $fee[$key] = implode(' ', $value);
            };

            $isArtistManager = false;
            if ($currentUser != null) {
                $isArtistManager = $currentUser->hasRole('ARTIST_MANAGER');

            }
            return view('accounts.artist', [
                'isArtistManager' => $isArtistManager,
                'user' => $artistUser,
                'currentUser' => $currentUser,
                'genres' => Genre::all()->sortBy('name'),
                'eventTypes' => EventTypes::all()->sortBy('name'),
                'songTypes' => SongType::all()->sortBy('name'),
                'feeTypes' => $fee,
                'mileage' => $mileage,
                'equipments' => $equipments,
            ]);
        }
    }

    public function instagramCallback(Request $request)
    {
        $code = $_GET['code'];
        //get artist data
        $artist_id = (int)$_GET['state'];
        $artist = false;
        if ($artist_id) {
            $artist = Artist::find($artist_id);
        }

        if ($artist) {

            $data = Instagram::getOAuthToken($code);

            $data_insert = array('artist_id' => $artist->id, 'access_token' => $code, 'authorization_code' => $data->access_token);

            $user = User::find($artist->user->id);

            $user->social->instagram_user = $data->user->username;
            $user->push();
            DB::table('instagram_account')->insert($data_insert);

        }

        return back();

    }

    public function downloadPdf(Request $request, $id)
    {
        if (env('APP_ENV') != 'prod') {
            $userService = App::make("userService");
            $avatar = Artist::where('id', $id)->first();
            $username = $avatar->user->username;
            $artistInstagram = $userService->getArtistInstagram($avatar->id);
            $type = 'artistPdf';
            $artist = null;
            $data = $request->data;
            $socialData = $request->socialData;
            $artistUser = $userService->getUserByUsername($username);

            if ($data) {

                if ($key = array_search('name', $data)) {
                    $artist = Artist::where('id', $id)->first();
                    array_splice($data, $key, 1);
                }


                $pdfDat = Artist::where('id', $id)->first(array_except($data, 'name'));
                $pdfData = $pdfDat->toArray();
                $pdfData['banner_directory'] = $avatar->user->avatar;
                if ($artist) {
                    $pdfData['name'] = $artist->user->name;
                }
            }
            if ($socialData) {
                $pdfSocialData = $artistUser->social()->first($socialData);
            }
            $view = \View::make('artists.pdf', compact('pdfData', 'pdfSocialData'));
            $contents = $view->render();
            $mpdf = new Mpdf();
            $mpdf->WriteHTML($contents);

            $destinationPath = implode("/", [base_path(), 'uploads']);
            $destinationPath .= "/contracts";

            $event_name = preg_replace("/[^-a-zA-Z_\d]/", '', 'Artist');
            $pdf_name = "{$event_name}_" . date("mdY") . "(#{$id})_" . $type;
            $full_path = "{$destinationPath}/{$pdf_name}.pdf";
            $mpdf->Output($full_path, 'F');
// Saves file on the server as 'filename.pdf'

            $systemUser = User::getSystemUser();
            $file_id = File::insertGetId([
                'user_id' => $systemUser->id,
                'entity' => 'artistPdf',
                'entity_id' => $id,
                'path' => $full_path,
                'name' => "{$pdf_name}.pdf",
                'bid_contract_id' => '',
                'type' => $type
            ]);

            $file = File::find($file_id);

            $file->save();
            return $file_id;
        }

    }


    /**
     * Download artist's data's to pdf
     * @param Request $request
     * @return array
     * @throws \Mpdf\MpdfException
     */
    public function downloadCheckedPdf(Request $request)
    {

        if (env('APP_ENV') != 'prod') {
            $artistsDatas = $request->artists;
            $pdfDatass = $request->pdfDatas;
            $socialDatas = $request->socialDatas;
            $artistUserDatas = $request->artistUserData;
            $type = 'artistPdf';
            $filesIds = [];

            $artistsId = [];
            foreach ($artistsDatas as $id) {
                $artist = Artist::where('id', $id);
                if ($pdfDatass) {
                    $basicinfo = $artist->where('id', $id)->get($pdfDatass)->toArray();
                } else {
                    $basicinfo = [];
                }
                if ($socialDatas) {
                    $socialInfo = $artist->where('id', $id)->first()->user->social($socialDatas)->get()->toArray();
                } else {
                    $socialInfo = [];
                }
                if ($artistUserDatas) {
                    $userInfo = $artist->where('id', $id)->first()->user()->get($artistUserDatas)->toArray();
                } else {
                    $userInfo = [];
                }
                $arr = array_collapse([$basicinfo[0], $socialInfo[0], $userInfo[0]]);
                $artistsId[] = $arr;
            }

//        $artistsData = [];
//        if ($pdfDatass) {
//            foreach ($artistsId as $artist) {
//                dd($artist->get());
//                $artist->first($pdfDatass);
//
////                array_push($artistsData , $artist->get($pdfDatass));
//                dd('asd');
//            }
//            dd($artistsData);
//            $pdfData = $artistsData;
//        } elseif (!$pdfDatass) {
//            $pdfData = null;
//
//        }

//
//        if ($socialDatas) {
//            $pdfSocia = $artist->first()->user->social($artistUserDatas)->get();
//            $pdfSocialData = $pdfSocia;
//        } elseif (!$socialDatas) {
//            $pdfSocialData = null;
//        }
//        if ($artistUserDatas) {
//            $artistData = $artist->first()->user($artistUserDatas)->get();
//        } elseif (!$artistUserDatas) {
//            $artistData = null;
//        }
//Make and then save pdf file
            $view = \View::make('artists.pdfManyArtists', compact('artistsId'));
            $contents = $view->render();
            $mpdf = new Mpdf();
            $mpdf->WriteHTML($contents);

            $destinationPath = implode("/", [base_path(), 'uploads']);
            $destinationPath .= "/contracts";

            $event_name = preg_replace("/[^-a-zA-Z_\d]/", '', 'Artist');
            $pdf_name = "{$event_name}_" . date("mdY") . "(#{$artist->get()->first()->id})_" . $type . time();
            $full_path = "{$destinationPath}/{$pdf_name}.pdf";
            $mpdf->Output($full_path, 'F');
// Saves file on the server as 'filename.pdf'
            $systemUser = User::getSystemUser();
            $file_id = File::insertGetId([
                'user_id' => $systemUser->id,
                'entity' => 'artistPdf',
                'entity_id' => $artist->get()->first()->id,
                'path' => $full_path,
                'name' => "{$pdf_name}.pdf",
                'bid_contract_id' => '',
                'type' => $type
            ]);

            $file = File::find($file_id);

            $file->save();
            return $file_id;
        }


    }


}
