<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 05.01.17
 * Time: 13:07
 */

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Auth;
use App\Genre;
use App;
use App\ModelType\BidType;

class ArtistManagerController extends Controller{

    /**
     * Show the page with artists of current Artist Manager
     * @param Request $request
     */
    public function artistsAction(Request $request) {
        $currentUser = Auth::user();
        $currentUserRoles = $currentUser->rolesList();

        if (!in_array("ARTIST_MANAGER", $currentUserRoles)) {
            return response()->view('errors.403');
        }

        $artistService = App::make("artistService");
        $artistUsers = $artistService->getArtistManagerArtists($currentUser, 200);
        $artistUserIds = [];
        foreach ($artistUsers as $artistUser) {
            $artistUserIds[] = $artistUser->username;
        }

        $bidService = App::make("bidService");
        $confirmedArtistsBids = $bidService->getArtistBidsCounter($artistUserIds, [BidType::STATUS_CONFIRMED]);
        $newArtistsBids = $bidService->getArtistBidsCounter($artistUserIds, [BidType::STATUS_NEW, BidType::STATUS_ACCEPTED]);

        return view('artistManager.artists', [
            'artistUsers' => $artistUsers,
            'currentUser'=>$currentUser,
            'isAdmin' => False,
            'isArtist' => False,
            'left_menu_selected' => 'artists',
            'genres' => Genre::all()->sortBy("name"),
            'status' => '',
            'defaultSelectedGenreId' => 0,
            'confirmedArtistsBids' => $confirmedArtistsBids,
            'newArtistsBids' => $newArtistsBids
        ]);
    }
}