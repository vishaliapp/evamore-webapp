<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 13.04.17
 * Time: 11:06
 */

namespace App\Http\Controllers;


use App\Comment;
use App\User;
use Illuminate\Http\Request;
use App;
use Illuminate\Support\Facades\Response;
use View;
use Validator;
use Auth;


class CommentController extends Controller
{
    public function apiCreate(Request $request)
    {
        $res = [
            'success' => false,
            'errors' => []
        ];

        try {
            $commentService = App::make("commentService");
            $validations = [
                'content' => 'required',
                'event_id' => 'required|numeric',
                'files' => ''
            ];

            $validator = Validator::make($request->all(), $validations);

            $currentUser = Auth::user();

            if ($validator->fails()) {
                $errors = $validator->errors();
                foreach ($validations as $filed => $v) {
                    if ($errors->has($filed)) {
                        $res['errors'][$filed] = $errors->first($filed);
                    }
                }

                throw new \Exception("Error. Try later.", 1);
            }

            $eventsService = App::make("eventsService");
            $event = $eventsService->getEvent($request->get('event_id'));
            if (is_null($event))
                throw new \Exception("Event not exists.");

            if (!$commentService->checkCommentsAccess($currentUser, $event, 'write'))
                throw new \Exception("You do not have permissions for this action.");

            $validData = $validator->getData();
            $comment = $commentService->createComment($event, $currentUser, $validData['content']);

            if ($validData['files']) {
                $fileService = App::make("fileService");
                foreach (explode(",", $validData['files']) as $fileId) {
                    $file = $fileService->getFile($fileId);
                    if (!is_null($file)) {
                        $fileService->bindFile($file, 'comment', $comment->id);
                    }
                }
            }

            $res['success'] = true;

            $emailService = App::make('emailNotificationService');
//            $emailService->notifyAboutNewComment($comment);
//            dd($res);
            return $res;

        } catch (\Exception $e) {
            if ($e->getCode() != 1) {
                $res['errors'] = ['global' => $e->getMessage()];
            }
        }


    }

    public function apiUpdate(Request $request)
    {
        $res = [
            'success' => false,
            'errors' => []
        ];

        try {
            $commentService = App::make("commentService");

            $validations = [
                'content' => 'required',
                'comment_id' => 'required|numeric',
                'origin_files' => ''
            ];

            $validator = Validator::make($request->all(), $validations);

            $currentUser = Auth::user();

            if ($validator->fails()) {
                $errors = $validator->errors();
                foreach ($validations as $filed => $v) {
                    if ($errors->has($filed)) {
                        $res['errors'][$filed] = $errors->first($filed);
                    }
                }
                throw new \Exception("Error. Try later.", 1);
            }

            $validData = $validator->getData();
            $comment = $commentService->getComment($validData['comment_id']);
            $fil = $validData['files'];
            $old_fil = $validData['origin_files'];

            if (!$currentUser->hasRole("ADMIN") && $comment->user_id != $currentUser->id)
                throw new \Exception("You do not have permissions for this action");

            $comment->content = htmlspecialchars($validData['content']);
            $comment->save();

            $origin_files = explode(",", $old_fil);
//            $new_files = explode(",", $fil);
            $remove_files = array_diff($origin_files, $validData['files']);


            $fileService = App::make("fileService");
            foreach ($remove_files as $fileId) {
                $file = $fileService->getFile($fileId);
                if (!is_null($file)) {
                    $fileService->unBindFile($file);
                }
            }

            foreach ($fil as $fileId) {
                $file = $fileService->getFile($fileId);
                if (!is_null($file)) {
                    $fileService->bindFile($file, 'comment', $comment->id);
                }
            }

            $serializedComment = $this->serializeComment($comment);
            $serializedComment['can_edit'] = $commentService->checkCommentsAccess($currentUser, $comment->event, 'edit', $comment);
            $res['success'] = true;
            $res['comment'] = $serializedComment;

//            $emailService = App::make('emailNotificationService');
//            $emailService->notifyAboutNewComment($comment);

        } catch (\Exception $e) {
            if ($e->getCode() != 1) {
                $res['errors'] = ['global' => $e->getMessage()];
            }
        }

        return $res;

    }

    public function apiComment(Request $request, $id)
    {
        $res = [
            'success' => false,
            'errors' => []
        ];

        try {
            $currentUser = Auth::user();

            $commentService = App::make("commentService");

            $comment = $commentService->getComment($id);
            if (is_null($comment))
                throw new \Exception("Comment not exist");

            if (!$currentUser->hasRole("ADMIN") && $comment->user_id != $currentUser->id)
                throw new \Exception("You do not have permissions for this action");

            $serializedComment = $this->serializeComment($comment);
            $serializedComment['can_edit'] = $commentService->checkCommentsAccess($currentUser, $comment->event, 'edit', $comment);
            $res['comment'] = $serializedComment;

            $res['success'] = true;


        } catch (\Exception $e) {
            if ($e->getCode() != 1) {
                $res['errors'] = ['global' => $e->getMessage()];//. $e->getTraceAsString()];
            }
        }
        return $res;

    }

    private function serializeComment(App\Comment $comment)
    {
        $user = \Auth::user();
        $d = new \DateTime($comment->created_at);
        $d->setTimezone($user->time_zone_object);

        $fileService = App::make("fileService");
        $commentFiles = $fileService->getEntityFiles("comment", $comment->id);
        $files = [];
        foreach ($commentFiles as $file) {
            $isSystem = $file->user->isSystem();
            $files[] = [
                'id' => $file->id,
                'name' => $file->name,
                'url' => route("file_download", ['id' => $file->id]),
                'isSystem' => $isSystem,
                'downloaded_by_artist' => $isSystem && $file->bidContract ? $file->bidContract->downloaded_by_artist : false,
            ];
        }
        $username = User::with('artist')->where('id', $comment->user->id)->get();
        return [
            'id' => $comment->id,
            'created_at' => $d->format("m/d/y - h:i A"),
            'content' => $comment->content,
            'user' => [
                'id' => $comment->user->id,
                'name' => $comment->user->name,
            ],
            'username' => $username,
            'files' => $files
        ];
    }

    public function apiComments(Request $request, $eventId)
    {
        $res = [
            'success' => false,
            'errors' => []
        ];

        $validations = [
            'after_id' => 'numeric',
        ];

        $validator = Validator::make($request->all(), $validations);
        try {
            $currentUser = Auth::user();

            if ($validator->fails()) {
                $errors = $validator->errors();
                foreach ($validations as $filed => $v) {
                    if ($errors->has($filed)) {
                        $res['errors'][$filed] = $errors->first($filed);
                    }
                }

                throw new \Exception("Error. Try later.", 1);
            }

            $eventsService = App::make("eventsService");
            $event = $eventsService->getEvent($eventId);
            if (is_null($event))
                throw new \Exception("Event not exists.");

            $commentService = App::make("commentService");

            if (!$commentService->checkCommentsAccess($currentUser, $event, 'read'))
                throw new \Exception("You do not have permissions for this action.");

            $validData = $validator->getData();
            $afterId = (array_key_exists('after_id', $validData)) ? $validData['after_id'] : null;

            $comments = $commentService->getEventComments($event, $afterId);
            $serializedComments = [];
            foreach ($comments as $comment) {
                $serializedComment = $this->serializeComment($comment);
                $serializedComment['can_edit'] = $commentService->checkCommentsAccess($currentUser, $comment->event, 'edit', $comment);

                $serializedComments[] = $serializedComment;
            }
            $canEdit = $serializedComment['user']['id'];


            $res['comments'] = $serializedComments;

            $res['success'] = true;

            $html = View::make('events.comments', compact('comments', 'serializedComments', 'currentUser', 'canEdit', 'event'
            ))->
            render();
            return Response::json(['html' => $html]);
        } catch (\Exception $e) {
            if ($e->getCode() != 1) {
                $res['errors'] = ['global' => $e->getMessage()];//. $e->getTraceAsString()];
            }
            return $res;
        }

    }

    public function apiDeleteFile($id)
    {
        $file = App\File::find($id);
        $file->delete();
    }

    public function apiDelete(Request $request)
    {
        $res = [
            'success' => false,
            'errors' => []
        ];

        try {
            $commentService = App::make("commentService");

            $validations = [
                'id' => 'required',
            ];

            $validator = Validator::make($request->all(), $validations);

            $currentUser = Auth::user();

            if ($validator->fails()) {
                $errors = $validator->errors();
                foreach ($validations as $filed => $v) {
                    if ($errors->has($filed)) {
                        $res['errors'][$filed] = $errors->first($filed);
                    }
                }

                throw new \Exception("Error. Try later.");
            }

            $validData = $validator->getData();
            $comment = $commentService->getComment($validData['id']);

            if (is_null($comment))
                throw new \Exception("Comment not exist");

            if (!$commentService->checkCommentsAccess($currentUser, $comment->event, 'write'))
                throw new \Exception("You do not have permissions for this action.");

            $fileService = App::make('fileService');
            $commentFiles = $fileService->getEntityFiles('comment', $comment->id);
            foreach ($commentFiles as $commentFile) {
                if (file_exists($commentFile->path)) {
                    unlink($commentFile->path);
                }
            }

            $comment->delete();

            $res['success'] = true;

        } catch (\Exception $e) {
            if ($e->getCode() != 1) {
                $res['errors'] = ['global' => $e->getMessage()];
            }
        }

        return $res;
    }
}