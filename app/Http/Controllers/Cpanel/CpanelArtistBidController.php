<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 27.12.16
 * Time: 12:03
 */

namespace App\Http\Controllers\Cpanel;

use App\AgeDemographic;
use App\Attire;
use App\BandSetup;
use App\EnergyLevel;
use App\Genre;
use App\Http\Controllers\Controller;
use App\SongType;
use App\EventType as EventTypes;
use App\Role;
use App\User;
use Illuminate\Http\Request;
use App;
use Auth;
use Validator;

class CpanelArtistBidController extends Controller {

    public function indexAction(Request $request)
    {
        $this->checkAdminAccess();

        $currentUser = Auth::user();
        $type = $request->type;
        $eventsService = App::make("eventsService");

        // Sort
        $sorts = [];
        $sortsFields = ['location', 'max_budget', 'starttime', 'created_at', 'updated_at'];
        $viewSorts = [];
        foreach($sortsFields as $field){
            $n = "sort_" . $field;
            if ($request->has($n) && in_array(strtolower($request->get($n)), ['asc', 'desc'])){
                $sorts[$field] = $request->get($n);
            }
            $viewSorts[$n] = $request->get($n);
        }

        // Filter
        $filters['search'] = '';
        if ($request->has('search')) {
            $filters['search'] = $request->get('search');;
        }
        if($request->has('search')){
            $filters['starttime'] = [">=", date('Y-m-d H:i:s')];
        }

        //special case for location sort
        $dbSorts = [];
        foreach ($sorts as $field => $order) {
          if ($field == 'location') {
            $dbSorts['state'] = $order;
            $dbSorts['city'] = $order;
            $dbSorts['venue'] = $order;
          } else {
            $dbSorts[$field] = $order;
          }
        }

        if ($type == 'past'){
            $sign = '<';
        }else{
            $sign = '>=';
        }
        $events = $eventsService->getPagedEvents($filters, $dbSorts, 25, $sign);

        $eventsIds = [];
        foreach ($events as $event) {
            $eventsIds[] = $event->id;
        }

        $bidService = App::make("bidService");
        $eventRequestsCounter = $bidService->getCountBidsEvents($eventsIds);

        $left_menu_selected = "upcoming";
        return view('cpanel.artistBid.index',  [
            'user' => $currentUser,
            'events' => $events,
            'sorts' => $viewSorts,
            'filters' => $filters,
            'left_menu_selected' => $left_menu_selected,
            'eventRequestsCounter' => $eventRequestsCounter,
            'eventsType'=>"upcoming"
        ]);
    }

    public function apiGetPlanners()
    {
        $this->checkAdminAccess();
        $role = Role::where('name', 'FAN')->first();
        $planners = User::whereHas('roles', function ($query) use ($role) {
            $query->where('role_id', $role->id);
        })->get(['id','name']);
        return response()->json($planners);
    }

    public function apiPendingBidsAction(Request $request, $eventId) {
        $this->checkAdminAccess();

        $eventsService = App::make("eventsService");
        $userService = App::make("userService");

        $event = $eventsService->getEvent($eventId);

        if (!$event) {
            abort("404");
        }

        $bidService = App::make("bidService");
        $bids = $bidService->getBidsForGrid($event->id);

        $planerUser = $userService->getUser($event->user_id);

        return [
            'success'=>true,
            'bids' => $bids,
            'planer_user' => [
                'id' => $planerUser->id,
                'name' => $planerUser->name,
                'phone' => $planerUser->phone,
                'email' => $planerUser->email
            ]
        ];
    }

    public function apiPendingBidAction(Request $request, $bidId) {
        $this->checkAdminAccess();

        $eventsService = App::make("eventsService");
        $userService = App::make("userService");
        $bidService = App::make("bidService");

        $bid = $bidService->getBidForGrid($bidId);
        if (!$bid) {
            abort("404");
        }

        $event = $eventsService->getEvent($bid->event_id);
        if (!$event) {
            abort("404");
        }

        $planerUser = $userService->getUser($event->user_id);

        return [
            'success'=>true,
            'bid' => $bid,
            'planer_user' => [
                'id' => $planerUser->id,
                'name' => $planerUser->name,
                'phone' => $planerUser->phone,
                'email' => $planerUser->email
            ]
        ];
    }

    public function apiSetPendingBidPriceAction(Request $request) {
        $this->checkAdminAccess();

        $response = [
            "success" => false,
            "message" => ""
        ];

        $validator = Validator::make($request->all(), [
            'id'  => 'required',
            'price'  => 'required|numeric',
        ]);

        try {
            if ($validator->fails()) {
                $errors = $validator->errors()->all();
                throw new \Exception($errors[0]);
            }

            $newPrice = floatval($request->get("price"));
            if ($newPrice <=0 )
                throw new \Exception("The price must be greater then 0.");

            $bidService = App::make("bidService");
            $bid = $bidService->getBid($request->get("id"));
            if (!$bid)
                throw new \Exception("The bid does not exist.");

            if (is_null($bid->artist_fee))
                throw new \Exception("You should set the artist fee before set price.");

            if($bid->artist_fee > 0 && $newPrice < $bid->artist_fee)
                throw new \Exception("The price should be greater then artist fee.");

            $bidService->setBidPrice($bid, $newPrice);

            $response['success'] = true;
            $response['bid'] = $bid;

        } catch (\Exception $e) {
            $response['message'] = $e->getMessage();
        }

        return $response;
    }

    public function apiSetPendingBidArtistFeeAction(Request $request) {
        $this->checkAdminAccess();

        $response = [
            "success" => false,
            "message" => ""
        ];

        $validator = Validator::make($request->all(), [
            'id'  => 'required',
            'artist_fee'  => 'required|numeric|min:0'
        ]);

        try {
            if ($validator->fails()) {
                $errors = $validator->errors()->all();
                throw new \Exception($errors[0]);
            }

            $data = $validator->getData();
            $newArtistFee = floatval($data["artist_fee"]);

            $bidService = App::make("bidService");
            $bid = $bidService->getBid($request->get("id"));
            if (!$bid)
                throw new \Exception("The bid does not exist.");

            if ($bid->price > 0 && $bid->price < $newArtistFee)
                throw new \Exception("You try to set artist fee greater then price");

            $bidService->setBidArtistFee($bid, $newArtistFee);

            $emailService = App::make("emailNotificationService");
            $emailService->notifyArtistAboutNewEventFee($bid, $newArtistFee);

            $response['success'] = true;
            $response['bid'] = $bid;

        } catch (\Exception $e) {
            $response['message'] = $e->getMessage();
        }

        return $response;
    }

    public function apiSendNotificationsAboutEventBidsAction(Request $request) {
        $this->checkAdminAccess();

        $response = [
            "success" => false,
            "message" => ""
        ];

        $validator = Validator::make($request->all(), [
            'event_id' => 'required',
            'accepted' => 'required|in:0,1'
        ]);

        try {
            if ($validator->fails()) {
                $errors = $validator->errors()->all();
                throw new \Exception($errors[0]);
            }

            $eventService = App::make("eventsService");
            $bidService = App::make("bidService");

            $event = $eventService->getEvent($request->get("event_id",0));

            if (!$event)
                throw new \Exception("The event does not exist.");
            $response['event'] = $event;

            $bids = $bidService->getBidsForGrid($event->id);

            $brokenBids = [];
            foreach($bids as $bidData) {
                try{
                    if ($bidData->price == 0)
                        throw new \Exception();
                    if (is_null($bidData->bid_artist_fee))
                        throw new \Exception();
                    if(floatval($bidData->price < $bidData->bid_artist_fee))
                        throw new \Exception();
                } catch (\Exception $e) {
                    $brokenBids[] = $bidData->id;
                }
            }

            $bidsNotify = [];
            if ($request->get("accepted") == 0 && count($brokenBids) > 0){
                $response['broken_bids'] = $brokenBids;
                throw new \Exception("");
            } else {
                foreach($bids as $bidData) {
                    if(!in_array($bidData->id, $brokenBids)){
                        $bidsNotify[] = $bidService->getBid($bidData->id);
                    }
                }

                if (count($bidsNotify) > 0){
                    $bidService->sendNotificationAboutEventBidsPrices($event, $bidsNotify);
                    $response['success'] = true;
                }
            }

            if (count($brokenBids) > 0 && count($bidsNotify) == 0) {
                $response['message'] = "There are no valid bids for notify";
            }


        } catch (\Exception $e) {
            $response['message'] = $e->getMessage();
        }

        return $response;
    }
}