<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 27.01.17
 * Time: 10:58
 */

namespace App\Http\Controllers\Cpanel;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App;
use Auth;
use Validator;
use App\Bid;
use App\ModelType\BidType;
use App\ModelType\OrderType;
use App\ModelType\OrderItemType;


class CpanelArtistPaymentController extends Controller{
    public function index(){
        $this->checkAdminAccess();

        $currentUser = Auth::user();

        return view('cpanel.artistPayment.index',  [
            "currentUser" => $currentUser,
            "left_menu_selected" => 'artists_payments'
        ]);
    }

    public function apiArtistPaymentBids(Request $request, $period) {
        $this->checkAdminAccess();

        $ret = [
            'success' => False
        ];

        try {
            $bidService = App::make("bidService");

            $filters=[];
            $sorts=[];
            $perPage=10;

            $filters['period'] = $period;
            $filters['bid_status'] = [BidType::STATUS_CONFIRMED, BidType::STATUS_ARTIST_PAID, BidType::STATUS_ACCEPTED];
            $bids = $bidService->getBids($filters, $sorts, $perPage);

            $ret['paginator'] = [
                'count_pages' => ceil($bids->total()/$bids->perPage()),
                'total' => $bids->total(),
                'per_page' => $bids->perPage(),
                'current_page'  => $bids->currentPage(),
            ];

            $ret['bids'] = [];
            foreach($bids as $bid) {
                $bidService->generatePaymentBidInfo($bid);
                $ret['bids'][] = $bid;
            }

            $ret['success'] = True;

        } catch (\Exception $e) {
            $ret['message'] = $e->getMessage();
        }

        return $ret;
    }

    public function apiArtistPaymentBid($id) {
        $this->checkAdminAccess();

        $ret = [
            'success' => False
        ];

        try {
            $bidService = App::make("bidService");

            $bid = $bidService->getBid($id);

            $bidService->generatePaymentBidInfo($bid);
            $ret['bid'] = $bid;

            $ret['success'] = True;

        } catch (\Exception $e) {
            $ret['message'] = $e->getMessage();
        }

        return $ret;
    }

    public function apiArtistPaymentAmount(Request $request) {
        $this->checkAdminAccess();

        $response = [
            "success" => false,
            "message" => ""
        ];

        $validator = Validator::make($request->all(), [
            'bid_id'  => 'required',
            'amount'  => 'required|in:'.implode(",", [0, OrderType::HALF_PAID, OrderType::FULL_PAID])
        ]);

        try {
            if ($validator->fails()) {
                $errors = $validator->errors()->all();
                throw new \Exception($errors[0]);
            }

            $bidService = App::make("bidService");
            $bid = $bidService->getBid($request->get("bid_id"));
            if (!$bid)
                throw new \Exception("The bid not exists.");

            if ($bid->payment_method != BidType::PAYMENT_METHOD_ALTERNATIVE)
                throw new \Exception("You should set payment method as alternative.");

            $orderService = App::make("orderService");

            $order = $bid->getOrder();
            if(is_null($order)) {
                $order = $orderService->createOrder($bid);
            }

            switch($request->get("amount")){
                case 0:
                    $orderService->deleteOrderItems($order);
                    $orderService->setOrderStatus($order, OrderType::NEW_ORDER);
                    $bidService->acceptBidByArtist($order->bid, false);
                    break;
                case OrderType::HALF_PAID:
                    $orderService->deleteOrderItems($order);
                    $orderService->setOrderStatus($order, OrderType::NEW_ORDER); // TODO: ???
                    $orderItem = $orderService->createOrderItem($order, $bid->price * 0.5);
                    $orderItem->transaction_id = 1;
                    $orderItem->transaction_status = OrderItemType::SETTLED;
                    $orderItem->save();
                    $orderService->setOrderStatus($order, OrderType::HALF_PAID);
                    $bidService->confirmBidByPlaner($order->bid, false);
                    $bidService->singContractByPlaner($bid, $request);
                    break;
                case OrderType::FULL_PAID:
                    $orderItem = $orderService->createRestOrderItem($order);
                    if ($orderItem) {
                        $orderItem->transaction_id = 1;
                        $orderItem->transaction_status = OrderItemType::SETTLED;
                        $orderItem->save();
                        $orderService->setOrderStatus($order, OrderType::FULL_PAID);
                        $bidService->confirmBidByPlaner($order->bid, false);
                    }
                    $bidService->singContractByPlaner($bid, $request);
                    break;
            }

            $response['success'] = true;
            $response['bid'] = $bid;

        } catch (\Exception $e) {
            $response['message'] = $e->getMessage();
        }

        return $response;
    }

    public function apiArtistPaymentDate(Request $request) {
        $this->checkAdminAccess();

        $response = [
            "success" => false,
            "message" => ""
        ];

        $validator = Validator::make($request->all(), [
            'bid_id'  => 'required',
            'date'  => 'required|date_format:m/d/Y',
        ]);

        try {
            if ($validator->fails()) {
                $errors = $validator->errors()->all();
                throw new \Exception($errors[0]);
            }

            $bidService = App::make("bidService");
            $bid = $bidService->getBid($request->get("bid_id"));
            if (!$bid)
                throw new \Exception("The bid not exists.");

            $date = new \DateTime($request->get("date"));
            $bidService->setBidPaidArtistDate($bid, $date);

            $response['success'] = true;
            $response['bid'] = $bid;

        } catch (\Exception $e) {
            $response['message'] = $e->getMessage();
        }

        return $response;
    }

    public function apiBidPaymentMethod(Request $request) {
        $this->checkAdminAccess();

        $response = [
            "success" => false,
            "message" => ""
        ];

        $validator = Validator::make($request->all(), [
            'bid_id'  => 'required',
            'method'  => 'required|in:'.implode(",", [0, BidType::PAYMENT_METHOD_PAYSCAPE, BidType::PAYMENT_METHOD_ALTERNATIVE]),
        ]);

        try {
            if ($validator->fails()) {
                $errors = $validator->errors()->all();
                throw new \Exception($errors[0]);
            }

            $bidService = App::make("bidService");
            $bid = $bidService->getBid($request->get("bid_id"));
            if (!$bid)
                throw new \Exception("The bid not exists.");

            $method = ($request->get("method") == 0) ? null : $request->get("method");

            if($method == BidType::PAYMENT_METHOD_ALTERNATIVE) {
                if ($bid->price == 0) {
                    throw new \Exception("You should set the price of bid before set Alternative Payment Method.");
                }
            }

            $bidService->setBidPaidMethod($bid, $method);

            $response['success'] = true;
            $response['bid'] = $bid;

        } catch (\Exception $e) {
            $response['message'] = $e->getMessage();
        }

        return $response;
    }


    public function apiPaymentNote(Request $request) {
        $this->checkAdminAccess();

        $response = [
            "success" => false,
            "message" => ""
        ];

        $validator = Validator::make($request->all(), [
            'bid_id'  => 'required',
            'note'  => 'string',
        ]);

        try {
            if ($validator->fails()) {
                $errors = $validator->errors()->all();
                throw new \Exception($errors[0]);
            }

            $bidService = App::make("bidService");
            $bid = $bidService->getBid($request->get("bid_id"));
            if (!$bid)
                throw new \Exception("The bid not exists.");


            $bidService->setBidNote($bid, $request->get("note"));

            $response['success'] = true;
            $response['bid'] = $bid;

        } catch (\Exception $e) {
            $response['message'] = $e->getMessage();
        }

        return $response;
    }
}