<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 26.08.16
 * Time: 13:33
 */

namespace App\Http\Controllers;

use App\Artist;
use App\Role;
use App\Http\Requests;
use App\User;
use Illuminate\Http\Request;
use Auth;
use Session;
use Image;
use DB;

class CpanelArtistsController  extends Controller {

    public function allArtistsJson(Request $request) {

        $status = $request->get('status', false);

        $currentUser = Auth::user();
        $artistRole = Role::getRole("ARTIST");

        $qb = User::select('users.*')
            ->addSelect('artists.fee')
            ->join('user_role', 'user_role.user_id', '=', 'users.id')
            ->join('artists', 'artists.user_id', '=', 'users.id')
            ->where('user_role.role_id', '=', $artistRole->id)
            ;

        if( $status==false || $status=='active') {
            $qb->where('users.active', '=', 1);
        }
        elseif($status=='inactive' && $currentUser->hasRole('ADMIN')) {
            $qb->where('users.active', '=', 0);
        }

        $artistUsers = $qb->get();

        foreach ($artistUsers as $artistUser) {
            $artistUser->password = null;
            $artistUser->remember_token = null;
            $genres = [];

            foreach($artistUser->artist->genres as $g){
                $genres[] = $g->id;
            }

            $artistUser->genre = implode(",", $genres);
            $artistUser->avatar = Image::url($artistUser->avatar, 95, 95, array('crop'));
        }

        return [
            'success' => true,
            'artists' => $artistUsers,
            'status' => $status
        ];
    }
}