<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 22.08.16
 * Time: 14:39
 */

namespace App\Http\Controllers;

use App\User;
use App\Role;
use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;
use Session;
use Image;
use DB;
use App;
use App\ModelType\BidType;

class CpanelUsersController extends Controller {

    public function index(Request $request) {
        $currentUser = Auth::user();

        if (!$currentUser->hasRole('ADMIN')){
            return response()->view('errors.403');
        }

        $queryBuilder = DB::table('users')->select('users.id');

        $queryBuilder->where('users.id', '>', 0);

        $filter = [];
        $filterRole = $request->get('filter_role', 0);
        if ($filterRole) {
            $queryBuilder->join('user_role', 'user_role.user_id', '=', 'users.id');
            $queryBuilder->where('user_role.role_id', "=", $filterRole);
            $filter['filter_role'] = $filterRole;
        }

        $queryBuilder->orderBy('users.id', 'asc');

        $usersPerPage = 25;
        $users = $queryBuilder->paginate($usersPerPage);

        $usersObjects = [];
        foreach($users as $user){
            $usersObjects[] = User::find($user->id);
        }

        $roles = Role::all();
        return view('cpanel.users.index',  [
            'usersObjects' => $usersObjects,
            'users' => $users,
            'roles' => $roles,
            'left_menu_selected' => 'user_management',
            'filter' => $filter
        ]);
    }

    public function userSearchBar($search = null)
    {
        $search_text = $search;
        $data = User::whereHas('roles',function ($query) use ($search_text) {
            $query->where('name', 'like', "%$search_text%")->orWhere('username', 'like', "%$search_text%");

        })->with('artist')->get();
        return response($data);
    }

    public function edit(Request $request, $id) {
        $currentUser = Auth::user();

        if (!$currentUser->hasRole('ADMIN')){
            return response()->view('errors.403');
        }
    }

    public function accountCreate(Request $request, $role) {
        $currentUser = Auth::user();

        if(!$currentUser->hasRole('ADMIN') || !in_array($role,['ADMIN', 'FAN', 'ARTIST_MANAGER'])) {
            return response()->view('errors.403');
        }

        if ($request->method() == 'POST') {
            $this->validate($request, [
                'first_name' => 'required|max:255',
                'last_name' => 'required|max:255',
                'username' => 'required|max:255|unique:users',
                'password' => 'required|max:255',
                'phone' => 'required|integer|min:1',
                'email' => 'required|emailDublicateValidator',
                'organization' => 'max:255',
                'org_position' => 'max:255',
                'avatar' => 'image',
            ]);

            $user = new User();
            $user->username = $request['username'];
            $user->password = bcrypt($request['password']);
            $user->name = $request['first_name'] . " " . $request['laste_name'];
            $user->email = $request['email'];
            $user->organization = $request['organization'];
            $user->org_position = $request['org_position'];
            $user->phone = $request['phone'];
            $user->save();

            $base64Image = $request->get('upload_image');
            if ($base64Image != '') {
              $pos  = strpos($base64Image, ';');
              $type = explode(':', substr($base64Image, 0, $pos))[1];
              $type = explode('/',$type)[1];
              $filename = $user->id."_".str_random(7).".".$type;
              $path = public_path('uploads/avatars')."/".$filename;
              $dataBase64 = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $base64Image));
              file_put_contents($path, $dataBase64);
              $user->avatar = $filename;
            }
            $user->save();

            $role_user = Role::getRole($role);
            $user->roles()->attach($role_user);

            Session::flash('message', "New account with '{$role}' permissions created successfully.");
            return redirect()->route('cpanel_users_index');
        }


        return view('cpanel.users.account',  [
           'currentUser' => $currentUser,
            'role' => $role
        ]);
    }

    public function apiDelete($id) {
        $this->checkAdminAccess();

        $resp = [
            'success' => false,
        ];

        try {
            $currentUser = Auth::user();

            if(!$currentUser->hasRole('ADMIN')) {
                return response()->view('errors.403');
            }

            $user = User::find($id);

            if (!$user){
                throw new \Exception("Account not exists.");
            }

            $bidService = App::make("bidService");
            if ($user->artist) {
                $filters['bid_status'] = [BidType::STATUS_CONFIRMED];
                $filters['artist_user_id'] = $user->id;
                $confirmedBids = $bidService->getBids($filters, [], 1);

                if (count($confirmedBids) > 0) {
                    throw new \Exception("You can't delete an artist that booked for an event.");
                }

                if($user->active == 1)
                    throw new \Exception("You should deactivate artist before delete.");
            } else {
                $filters['bid_status'] = [BidType::STATUS_CONFIRMED];
                $filters['planer_user_id'] = $user->id;
                $confirmedBids = $bidService->getBids($filters, [], 1);

                if (count($confirmedBids) > 0) {
                    throw new \Exception("You can't delete a planer with booked events.");
                }
            }

            //delete user
            $user->delete();

            $resp['success'] = true;
            Session::flash('message', 'Account deleted successfully.');

        } catch (\Exception $e) {
            $resp['message'] = $e->getMessage();
        }

        return $resp;
    }
}