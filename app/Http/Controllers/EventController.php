<?php

namespace App\Http\Controllers;

use App\Comment;
use App\EventChange;
use App\Http\Requests\EventEditRequest;
use App\Http\Requests\EventUpdateRequest;
use App\SongType;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Artist;
use App\ArtistManager;
use App\Event;
use App\Http\Requests;
use App\Genre;
use App\EventType as EventTypes;
use App\Bid;
use Auth;
use DB;
use App;
use Session;
use Image;
use App\ModelType\TimeZoneType;
use App\ModelType\EventType;
use App\ModelType\BidType;

class EventController extends Controller
{
    /*
    |--------------------------------------------------
    |   Get selected event
    |--------------------------------------------------
    */
    public function show($id)
    {
        $currentUser = Auth::user();
        $eventsService = App::make("eventsService");
        $bidService = App::make("bidService");


        if (!$currentUser) {
            return redirect()->route('home');
        }

        $isAdmin = $currentUser->hasRole('ADMIN');
        $isArtist = $currentUser->hasRole('ARTIST');
        $isArtistManager = $currentUser->hasRole('ARTIST_MANAGER');
        $artSee = true;
        $event = $eventsService->getEvent($id);
        $artChanges = false;
        if ($isArtist && !$event->changes->isEmpty()) {
            $artSee = false;
            $artChanges = $event->changes()->where('artist_id', $currentUser->artist->id)->first();
//            $event->changes = $event->changes()->where('artist_id', $currentUser->artist->id);
        }
        if (!$event || (!$isAdmin && ($event->status != EventType::PUBLISHED && $event->status != EventType::BOOKED && $currentUser->id != $event->user_id))) {
            abort("404");
        }

        $isUpcoming = ($event->starttime >= date('Y-m-d H:i:s')) ? TRUE : FALSE;

        $canEdit = ($currentUser->id == $event->user_id || $isAdmin);


        $artistsBidsToEvent = [];
        $planerBidsToEvent = [];
        if ($canEdit) {
            $artistsBidsToEvent = $bidService->getArtistsBidsToEvent($event);
//            $planerBidsToEvent = $bidService->getPlanerBidsToEvent($event);
            $planerBidsToEvent = $bidService->getBidsByEvent($event, BidType::PLANER_TYPE);
        }
        if ($isArtist && $artSee) {
            if (count($event->artistSee()->where('artist_id', $currentUser->artist->id)->get()) == 0) {
                $event->artistSee()->attach($currentUser->artist->id);
            }
        }
        $planerBid = NULL;
        if ($artSee && $isArtist || $isArtistManager) {
            $bidService = App::make("bidService");
            $planerBid = $bidService->getPlanerBidToEvent($event, $currentUser);
            $event->min_budget = floor($event->min_budget * .90);
            $event->max_budget = floor($event->max_budget * .90);
        }

        $isEventOwner = $event->user_id == $currentUser->id;

        $commentService = App::make("commentService");
        if ($artSee) {
            $artistsHasBidsToEvent = $bidService->getArtistsHasBidsToEvent($event, $currentUser);
        }
        $artistUser = '';
        if ($isArtistManager) {
            $managerId = Auth::user()->id;
            $currentUserRoles = $currentUser->rolesList();
            $artistService = App::make("artistService");
            $artistsBidsToEvent = $bidService->getArtistManagerArtistsToEvent($event, $managerId);
            $artists = $artistService->getArtistManagerArtists($currentUser);
            $artistsHasBidsToEvent = $bidService->getArtistsHasBidsToEvent($event, $currentUser);
            $artistUsers = [];

            foreach ($artistsBidsToEvent as $artist_bid) {

                $artist_bids[$artist_bid->artist_user_id] = $artist_bid->price;
            }

            foreach ($artists as $artist) {
                if (isset($artist_bids[$artist->id])) {
                    $artist->bid = $artist_bids[$artist->id];
                }
                $artistUsers[] = $artist;
            }

            $artistsManagerArtistBidsToEvent = $bidService->getArtistsManagerArtistBidsToEvent($event);
            $artistUserIds = [];
        } else {
            $artistUsers = '';
            $artistsManagerArtistBidsToEvent = '';
        }

        //Get contract file
        if ($event->bids && $event->bids->contract) {
            if ($isArtist || $isArtistManager) {
                $bidContract = $event->bids->contract->file->where('type', 'artist');
            } elseif ($isAdmin) {
                $bidContract = $event->bids->contract->file;
            } else {
                $bidContract = $event->bids->contract->file->where('type', 'planer');
            }
        } else {
            $bidContract = '';
        }
        $comment = Comment::with('user')->where('event_id', $event->id)->get();
        return view(($isUpcoming && $canEdit) ? 'events.edit' : 'events.details', [
            'user' => $currentUser,
            'event_id' => $id,
            'event' => $event,
            'genres' => Genre::all()->sortBy('name'),
//            'eventTypes' => Event::getEventTypes(),
            'eventTypes' => EventTypes::all()->sortBy('name'),
            'selectedEventType' => $event->type,
            'artistTypes' => Artist::getArtistTypes(),
            'left_menu_selected' => $isUpcoming ? 'upcoming' : 'past',
            'canEdit' => $canEdit,
            'isArtist' => $isArtist,
            'songTypes' => SongType::all()->sortBy('name'),
            'comment' => $comment,
            'isArtistManager' => $isArtistManager,
            'artistsBidsToEvent' => $artistsBidsToEvent,
            'planerBidsToEvent' => $planerBidsToEvent,
            'planerBid' => $planerBid,
            'isEventOwner' => $isEventOwner,
            'isAdmin' => $isAdmin,
            'canReadComments' => ($artSee) ? $commentService->checkCommentsAccess($currentUser, $event, 'read') : '',
            'canWriteComments' => ($artSee) ? $commentService->checkCommentsAccess($currentUser, $event, 'write') : '',
            'artistUsers' => $artistUsers,
            'artChanges' => $artChanges,
            'artistsHasBidsToEvent' => ($artSee) ? $artistsHasBidsToEvent : '',
            'bidContract' => $bidContract,
            'artistsManagerArtistBidsToEvent' => $artistsManagerArtistBidsToEvent,
        ]);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function delete($id)
    {
        $user = Auth::user();
        $event = Event::find($id);

        if ($user->hasRole('ADMIN') || $user->id == $event->user_id) {
            if ($event->bids){
                if ($event->bids->contract){
                    $event->bids->contract()->delete();
                }
                $event->bids()->delete();
            }
            $event->delete();

            return redirect()->route('events_by_type', ['type' => 'upcoming']);
        }elseif ($event->user_id == $user->id){
            $event->delete();

            return redirect()->route('events_by_type', ['type' => 'upcoming']);
        }
        return response()->view('errors.403');
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function cancel($id)
    {
        $user = Auth::user();
        $event = Event::find($id);

        if ($user->id == $event->user_id || $user->hasRole('ADMIN')) {
            $eventService = App::make("eventsService");
            $eventService->cancelEvent($event, $user);

            return redirect()->route('events_by_type', ['type' => 'upcoming']);
        }
        return response()->view('errors.403');
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function publish($id)
    {
        $user = Auth::user();
        $event = Event::find($id);

        if ($user->hasRole('ADMIN')) {
            $eventService = App::make("eventsService");
            $eventService->publishEvent($event, $user);
            Session::flash('message', "Event '" . $event->name . "' published.");
            return redirect()->route('cpanel_pendingbids');
        }
        return response()->view('errors.403');
    }

    /*
    |--------------------------------------------------
    |   Create events
    |--------------------------------------------------
    */
    public function create(Request $request)
    {
        $user = Auth::user();

        if (is_null($user)) {
            $redirect = redirect('/#login')->with('session', 'login');

            //If unauthorized user select the artist to book, cache the artists ID and redirect to register form
            if ($request->get("booked_user_id", 0) > 0) {
                $cookie = cookie('booked_user_id', $request->get("booked_user_id"), 30);
                $redirect->withCookie($cookie);
            }
            return $redirect;
        }

        if ($user->hasRole('ARTIST')) {
            Session::flash('message', "You can't create events as artist.");
            return redirect()->route('events_by_type', ['type' => 'upcoming']);
        }

        //ID of user booked on artist details page
        $bookedArtistUserId = $request->get('booked_user_id', 0);
        $bookedArtist = NULL;
        $defaultGenres = [];
        if ($bookedArtistUserId > 0) {
            $bookedArtist = Artist::fullInfo($bookedArtistUserId);
            $bookedArtist->avatar = Image::url('uploads/avatars/' . $bookedArtist->avatar, 70, 70, array('crop'));

            foreach ($bookedArtist->genres as $genre) {
                $defaultGenres[] = $genre->id;
            }
        }
        if ($request->has('fan_id')){
            $fan = User::where('id', $request->fan_id)->first();
        }else{
            $fan = null;
        }
        return view('events.create', [
            'user' => $user,
            'genres' => Genre::all()->sortBy('name'),
            'eventTypes' => EventTypes::all()->sortBy('name'),
            'songTypes' => SongType::all()->sortBy('name'),
            'left_menu_selected' => 'upcoming',
            'bookedArtist' => $bookedArtist,
            'fan' => $fan,
            'selectedGenres' => $defaultGenres,
            'selectedEventType' => '',
        ]);
    }

    public function postSaveEvent(Request $request)
    {
        try {

            $currentUser = Auth::user();
            if ($currentUser->hasRole('ARTIST')) {
                throw new \Exception("You can't create events as artist.");
            }

            if ($request->has('event_id')) {
                $event = Event::find($request->get('event_id'));

                if ($event->user_id != $currentUser->id && !$currentUser->hasRole('ADMIN')) {
                    throw new \Exception("You can't edit this event.");
                }

                if (!$currentUser->hasRole('ADMIN')) {
                    $bidService = App::make("bidService");
                    $artistsBidsToEvent = $bidService->getArtistsBidsToEvent($event);
                    $planerBidsToEvent = $bidService->getPlanerBidsToEvent($event);

                    if (count($artistsBidsToEvent) > 0 || count($planerBidsToEvent) > 0) {
                        throw new \Exception("You can't edit event with active requests.");
                    }
                }

                $event->updated_at = date("Y-m-d H:i:s");
                $newEventFlag = FALSE;
            } else {
                $event = new Event();
                if ($request->has('fan_id')){
                    $event->user_id = $request->fan_id;
                }else{
                    $event->user_id = $currentUser->id;
                }
                $event->status = EventType::PENDING;
                $newEventFlag = TRUE;
            }


            $this->validate($request, [
                'name' => 'required|max:255',
                'date' => 'required|date|future_date|date_format:m/d/Y',
                'hours' => 'required|max:12|min:0|numeric',
                'minutes' => 'required|max:59|min:0|numeric',
                'address' => 'required|max:255',
                'venue' => 'required|max:255',
                'place_lat' => 'numeric',
                'place_lng' => 'numeric',
                'radio' => 'boolean',
                'city' => 'max:255',
                'state' => 'required|max:255',
                'content' => 'required',
                'min_budget' => 'required|integer|min:0|numeric',
                'max_budget' => 'required|integer|min:0|numeric|greater_than:min_budget',
                'duration' => 'required:integer|min:0|numeric',
                'genre' => 'required',
                'event_type' => 'required|numeric',
                'song_type' => 'required|present|array',
                'time_zone' => 'required|in:' . implode(",", array_keys(TimeZoneType::$zones))
            ]);


            $simpleFields = [
                'name',
                'address',
                'venue',
                'place_lat',
                'place_lng',
                'city',
                'state',
                'min_budget',
                'max_budget',
                'duration',
                'time_zone',
                'event_type'
            ];
            foreach ($simpleFields as $fieldName) {
                $event->{$fieldName} = $request->get($fieldName);
            }


            $date = $request['date'] . ' ' . $request['hours'] . ':' . $request['minutes'] . ':00' . ' ' . $request['period'];

            //$event_date = date("Y-m-d H:i:s A",strtotime($date));
            $event_date = new \DateTime($date, new \DateTimeZone(TimeZoneType::getTimeZone($request->get("time_zone"))));
            //$event_date->setTimezone(new \DateTimeZone("UTC"));


            $event->starttime = $event_date->format("Y-m-d H:i:s");
            $event->artist_type = '';
//            $event->event_type = '';
            $event->contact_me = 1;
            $event->sound_production = $request['radio'];
            $event->save();
            // Song Types
            $songTypes = (sizeof($request['song_type']) != 0 ? $request['song_type'] : []);
            foreach ($songTypes as $type) {
                $t = SongType::where('id', $type)->first();
                $event->songTypes()->attach($t);
            }
            // Add comment.
            if (
                (isset($request['content']) && !empty($request['content'])) ||
                (isset($request['files']) && !empty($request['files']))
            ) {
                $commentService = App::make("commentService");

                if (empty($request['content'])) {
                    $request['content'] = "Added files";
                }
                $comment = $commentService->createComment($event, $currentUser, $request['content']);
                if (isset($request['files']) && !empty($request['files'])) {
                    $fileService = App::make("fileService");
                    foreach (explode(",", $request['files']) as $fileId) {
                        $file = $fileService->getFile($fileId);
                        if (!is_null($file)) {
                            $fileService->bindFile($file, 'comment', $comment->id);
                        }
                    }
                }
            }

            $genres = (sizeof($request['genre']) != 0 ? $request['genre'] : []);
            $initialGenres = $event->genres;

            foreach ($initialGenres as $ig) {
                if (!in_array($ig, $genres)) {
                    $event->genres()->detach($ig);
                }
            }

            foreach ($genres as $genre) {
                $g = Genre::where('id', $genre)->first();
                $event->genres()->attach($g);
            }

            $emailService = App::make('emailNotificationService');
            $userService = App::make('userService');
            $bookedUserId = $request->get('booked_user_id', 0);

            if ($newEventFlag && $bookedUserId > 0) {
                $artistUser = $userService->getUser($bookedUserId);

                if ($artistUser) {
                    $bidService = App::make("bidService");
                    $bidService->createPlanerBid($event, $artistUser, TRUE);
                }
            }

            if ($newEventFlag && $currentUser->hasRole('FAN')) {
                $emailService->notifyAdminsAboutNewEventCreated($event, $currentUser);
            }

            if ($newEventFlag) {
                Session::flash('message', 'Event sent for review.');
            } else {
                Session::flash('message', 'Event saved successfully.');
            }


            return redirect()->route('events_by_type', ['type' => 'upcoming']);

        } catch (\Exception $e) {
            return redirect()->back()->withErrors($e->validator)
                ->withInput();
        }

        if (isset($event) && $event && $event->id) {
            return redirect()->route('event_show', ['id' => $event->id]);
        } else {
            $params = [];
            if ($bookedUserId = $request->get('booked_user_id', 0)) {
                $params['booked_user_id'] = $bookedUserId;
            }
            return redirect()->route('createEvent', $params);
        }
    }

//    public function artistChanges(Request $request)
//    {
//        try {
//            $emailService = App::make('emailNotificationService');
//            DB::beginTransaction();
//            $eventOld = Event::where('id', $request->event_id)->first();
//            $data = $request->except('genre', 'songType', 'duration', 'city-state', '_token');
//            $artist = Artist::where('id', $request->artists)->first();
//            $event = new EventChange();
//            $event->user_id = $eventOld->user_id;
//            $event->event_id = $eventOld->id;
//            $event->artist_id = $request->artists;
//            $event->status = EventType::PUBLISHED;
//            $simpleFields = [
//                'name',
//                'address',
//                'venue',
//                'place_lat',
//                'place_lng',
//                'city',
//                'state',
//                'min_budget',
//                'max_budget',
//                'duration',
//                'time_zone',
//                'event_type'
//            ];
//            foreach ($simpleFields as $fieldName) {
//                $event->{$fieldName} = $request->get($fieldName);
//            }
//            $date = $request['date'] . ' ' . $request['hours'] . ':' . $request['minutes'] . ':00' . ' ' . $request['period'];
//
//            //$event_date = date("Y-m-d H:i:s A",strtotime($date));
//            $event_date = new \DateTime($date, new \DateTimeZone(TimeZoneType::getTimeZone($request->get("time_zone"))));
//            //$event_date->setTimezone(new \DateTimeZone("UTC"));
//
//
//            $event->starttime = $event_date->format("Y-m-d H:i:s");
//            $event->artist_type = '';
//            $event->contact_me = 1;
//            $event->sound_production = $request['radio'];
//            // Song Types
//            $songTypes = (sizeof($request['song_type']) != 0 ? $request['song_type'] : []);
//            foreach ($songTypes as $type) {
//                $t = SongType::where('id', $type)->first();
//                $event->event->songTypes()->attach($t);
//            }
//            $genres = (sizeof($request['genre']) != 0 ? $request['genre'] : []);
//            foreach ($genres as $genre) {
//                $g = Genre::where('id', $genre)->first();
//                $event->event->genres()->attach($g);
//            }
//            $artist->event_change_id = $event->event_id;
//            $event->save();
//            $event->artistAplly()->attach($artist);
//            $emailService->notifyArtistsAboutEventChanges($event, $artist);
//            DB::commit();
//        } catch (\Exception $exception) {
//            DB::rollback();
//            return redirect()->back()->withErrors( $exception->getMessage() )
//                ->withInput();
//        }
//
//    }

//    public function applyChanges(Request $request)
//    {
//        try {
//            DB::beginTransaction();
//            $eventChanges = EventChange::where('event_id', $request->eventId)->first();
//            $artist = Artist::where('id', $request->artistId)->first();
//            $artist->event_change_id = $eventChanges->id;
//            $applyPivot = $artist->applyEvent()->where('event_change_id', $eventChanges->id)->orderBy('created_at', 'desc')->first();
//            $applyPivot->pivot->apply = 1;
//            $applyPivot->pivot->save();
//            $changes = EventChange::where('event_id', $eventChanges->event_id)->whereHas('artistAplly',function ($q){
//                $q->where('apply', 0);
//            })->exists();
//            if (!$changes) {
//                $updatedEvent = $eventChanges->event;
//                $updatedEvent->name = $eventChanges->name;
//                $updatedEvent->description = $eventChanges->description;
//                $updatedEvent->starttime = $eventChanges->starttime;
//                $updatedEvent->endtime = $eventChanges->endtime;
//                $updatedEvent->capacity = $eventChanges->capacity;
//                $updatedEvent->venue = $eventChanges->venue;
//                $updatedEvent->address = $eventChanges->address;
//                $updatedEvent->place_lat = $eventChanges->place_lat;
//                $updatedEvent->place_lng = $eventChanges->place_lng;
//                $updatedEvent->artist_type = $eventChanges->artist_type;
//                $updatedEvent->min_budget = $eventChanges->min_budget;
//                $updatedEvent->max_budget = $eventChanges->max_budget;
//                $updatedEvent->event_type = $eventChanges->event_type;
//                $updatedEvent->contact_me = $eventChanges->contact_me;
//                $updatedEvent->duration = $eventChanges->duration;
//                $updatedEvent->status = $eventChanges->status;
//                $updatedEvent->time_zone = $eventChanges->time_zone;
//                $updatedEvent->city = $eventChanges->city;
//                $updatedEvent->state = $eventChanges->state;
//                $updatedEvent->sound_production = $eventChanges->sound_production;
//                $updatedEvent->save();
//                $updatedEvent->changes()->delete();
//                $eventChanges->artistAplly()->delete();
//                DB::commit();
//            }else{
//                DB::commit();
//            }
//        } catch (\Exception $exception) {
//            DB::rollBack();
//            return response()->json(['error' => $exception->getMessage()]);
//        }
//    }

    public function postUpdateEvent(EventUpdateRequest $request)
    {
        try {
            $currentUser = Auth::user();
            $updatedEvent = Event::where('id', $request->get('event_id'))->first();
            $updatedBid = Bid::with('artists', 'event', 'artistUser')->where('event_id', $request->get('event_id'))->get();
            $emailService = App::make('emailNotificationService');

//            if ($updatedEvent->bids) {
//                $startTime = explode(' ', $updatedEvent->starttime);
//                $hours = $request['hours'];
//                $minutes = $request['minutes'];
//                $period = $request['period'];
//                $newStartTime = $startTime[0] . ' ' . $hours . ':' . $minutes . ':00' . ' ' . $period;
//                $updatedEvent->starttime = $newStartTime;
//                $updatedEvent->save();
//            }
            if ($currentUser->hasRole('ARTIST')) {
                throw new \Exception("You can't create events as artist.");
            }

            if ($request->has('event_id')) {
                $event = Event::find($request->get('event_id'));

                if ($event->user_id != $currentUser->id && !$currentUser->hasRole('ADMIN')) {
                    throw new \Exception("You can't edit this event.");
                }

                if (!$currentUser->hasRole('ADMIN')) {
                    $bidService = App::make("bidService");
                    $artistsBidsToEvent = $bidService->getArtistsBidsToEvent($event);
                    $planerBidsToEvent = $bidService->getPlanerBidsToEvent($event);

                    if (count($artistsBidsToEvent) > 0 || count($planerBidsToEvent) > 0) {
                        throw new \Exception("You can't edit event with active requests.");
                    }
                }
                $songTypes = (sizeof($request['songType']) != 0 ? $request['songType'] : []);

                $initialSongTypes = $event->songTypes;
                foreach ($initialSongTypes as $iet) {
                    if (!in_array($iet, $songTypes)) {
                        $event->songTypes()->detach($iet);
                    }
                }
                foreach ($songTypes as $type) {
                    $t = SongType::where('id', $type)->first();
                    $event->songTypes()->attach($t);
                }
                $event->updated_at = date("Y-m-d H:i:s");
                $newEventFlag = FALSE;
            } else {
                $event = new Event();
                $event->user_id = $currentUser->id;
                $event->status = EventType::PENDING;
                $newEventFlag = TRUE;
            }


            $simpleFields = [
                'name',
                'address',
                'venue',
                'place_lat',
                'place_lng',
                'city',
                'state',
                'min_budget',
                'max_budget',
                'duration',
                'time_zone',
                'event_type'
            ];
            foreach ($simpleFields as $fieldName) {
                $event->{$fieldName} = $request->get($fieldName);
            }


            $date = $request['date'] . ' ' . $request['hours'] . ':' . $request['minutes'] . ':00' . ' ' . $request['period'];

            //$event_date = date("Y-m-d H:i:s A",strtotime($date));
            $event_date = new \DateTime($date, new \DateTimeZone(TimeZoneType::getTimeZone($request->get("time_zone"))));
            //$event_date->setTimezone(new \DateTimeZone("UTC"));


            $event->starttime = $event_date->format("Y-m-d H:i:s");
            $event->artist_type = '';
//            $event->event_type = '';
            $event->contact_me = 1;
            $event->sound_production = $request['radio'];
            $event->save();


            // Add comment.
            if (
                (isset($request['content']) && !empty($request['content'])) ||
                (isset($request['files']) && !empty($request['files']))
            ) {
                $commentService = App::make("commentService");

                if (empty($request['content'])) {
                    $request['content'] = "Added files";
                }
                $comment = $commentService->createComment($event, $currentUser, $request['content']);
                if (isset($request['files']) && !empty($request['files'])) {
                    $fileService = App::make("fileService");
                    foreach (explode(",", $request['files']) as $fileId) {
                        $file = $fileService->getFile($fileId);
                        if (!is_null($file)) {
                            $fileService->bindFile($file, 'comment', $comment->id);
                        }
                    }
                }
            }

            $genres = (sizeof($request['genre']) != 0 ? $request['genre'] : []);
            $initialGenres = $event->genres;

            foreach ($initialGenres as $ig) {
                if (!in_array($ig, $genres)) {
                    $event->genres()->detach($ig);
                }
            }

            foreach ($genres as $genre) {
                $g = Genre::where('id', $genre)->first();
                $event->genres()->attach($g);
            }

            $emailService = App::make('emailNotificationService');
            $userService = App::make('userService');
            $bookedUserId = $request->get('booked_user_id', 0);
            $bidService = App::make("bidService");

            if ($newEventFlag && $bookedUserId > 0) {
                $artistUser = $userService->getUser($bookedUserId);

                if ($artistUser) {
                    $bidService->createPlanerBid($event, $artistUser, TRUE);
                }
            }

            if ($newEventFlag && $currentUser->hasRole('FAN')) {
                $emailService->notifyAdminsAboutNewEventCreated($event, $currentUser);
            }

            if ($newEventFlag) {
                Session::flash('message', 'Event sent for review.');
            } else {
                Session::flash('message', 'Event saved successfully.');
            }
            $bids = $bidService->getBidsByEvent($event);
            foreach ($bids as $bid){
                $emailService->notifyArtistsAboutEventChanges($event, $bid->artistUser->artist);
            }

            return redirect()->route('events_by_type', ['type' => 'upcoming']);

        } catch (\Exception $e) {
            Session::flash('message', $e->getMessage());
        }

        if (isset($event) && $event && $event->id) {
            return redirect()->route('event_show', ['id' => $event->id]);
        } else {
            $params = [];
            if ($bookedUserId = $request->get('booked_user_id', 0)) {
                $params['booked_user_id'] = $bookedUserId;
            }
            return redirect()->route('createEvent', $params);
        }
    }

    /*
    |--------------------------------------------------
    |   Edit event
    |--------------------------------------------------
    */

    public function postEditEvent(EventEditRequest $request)
    {
        try {
            $currentUser = Auth::user();
            $event = Event::find($request->get('event_id'));
            $genres = (sizeof($request['genre']) != 0 ? $request['genre'] : []);
            $initialGenres = $event->genres;
            foreach ($initialGenres as $ig) {
                if (!in_array($ig, $genres)) {
                    $event->genres()->detach($ig);
                }
            }
            foreach ($genres as $genre) {
                $g = Genre::where('id', $genre)->first();
                $event->genres()->attach($g);
            }


            $date = $request['date'] . ' ' . $request['hours'] . ':' . $request['minutes'] . ':00' . ' ' . $request['period'];
            $event_date = new \DateTime($date, new \DateTimeZone(TimeZoneType::getTimeZone($request->get("time_zone"))));


            $simpleFields = [
                'name',
                'address',
                'venue',
                'place_lat',
                'place_lng',
                'city',
                'state',
                'min_budget',
                'max_budget',
                'duration',
                'time_zone',
                'event_type'
            ];
            foreach ($simpleFields as $fieldName) {
                $event->{$fieldName} = $request->get($fieldName);
            }

            $event->starttime = $event_date->format("Y-m-d H:i:s");
            $event->artist_type = '';
//        $event->name = $request->name;
            $event->description = $request->content;
            $event->contact_me = 1;
            $event->sound_production = $request['radio'];
//        $event->min_bu
            $event->save();

            $bids = Bid::with('artists', 'event', 'artistUser')->where('event_id', $event->id)->get();

            $emailService = App::make('emailNotificationService');
            if ($bids) {
                foreach ($bids as $bid) {
                    $emailService->notifyArtistsAboutEventChanges($bid);
                }

            }
        } catch (\Exception $e) {
            Session::flash('message', $e->getMessage());
        }
        if (isset($event) && $event && $event->id) {
            return redirect()->route('event_show', ['id' => $event->id]);
        } else {
            $params = [];
            if ($bookedUserId = $request->get('booked_user_id', 0)) {
                $params['booked_user_id'] = $bookedUserId;
            }
            return redirect()->route('createEvent', $params);
        }

    }


    /*
    |--------------------------------------------------
    |   Get upcoming events
    |--------------------------------------------------
    */
    public function events(Request $request, $type = 'upcoming')
    {
        $user = Auth::user();
        $isFan = $user->hasRole("FAN");
        $isAdmin = $user->hasRole('ADMIN');
        $isArtist = $user->hasRole('ARTIST');
        $isArtistManager = $user->hasRole('ARTIST_MANAGER');
        $currentUser = Auth::user()->id;


        if ($isAdmin && $type == 'upcoming') {
            return redirect()->route("cpanel_pendingbids");
        }

        if ($isFan) {
            if (intval($request->cookie("booked_user_id")) > 0) {
                $bookedUserId = intval($request->cookie("booked_user_id"));
                $cookie = cookie('booked_user_id', NULL, 0);
                $redirect = redirect();
                return $redirect->route('createEvent', [
                    "booked_user_id" => $bookedUserId
                ])->withCookie($cookie);

            }
        }

        if ($type == 'past') {
            $left_menu_selected = "past";
            $sign = "<";
        } else {
            $sign = ">=";
            $left_menu_selected = "upcoming";
        }

        $bidArtist = Bid::select('bid.*')
            ->join('events', 'events.id', '=', 'bid.event_id')
            ->where('bid.artist_user_id', $currentUser)
            ->where('events.starttime', $sign, date('Y-m-d H:i:s'))
            ->get();

        if ($isArtist || $isArtistManager) {
            $queryBuilder = Event::where('starttime', $sign, date('Y-m-d H:i:s'))
                ->select('events.*')
                ->addSelect(DB::raw('floor(events.max_budget * .90) as max_budget, floor(events.min_budget * .90) as min_budget'))
                ->groupBy('events.id');

        } else {
            $queryBuilder = Event::where('starttime', $sign, date('Y-m-d H:i:s'))
                ->select('events.*')
                ->groupBy('events.id');
        }

        $queryBuilder->leftJoin('users', 'users.id', '=', 'events.user_id');

        $isArtist = $user->hasRole('ARTIST');

        if (!$isAdmin && !$isArtist && !$isArtistManager) {
            $queryBuilder->where('user_id', $user->id);
        }
        if ($isArtist) {
            $bidTypes = [5, 8];
            $bids = Bid::select('bid.event_id')
//                ->where('bid.created_at', '>=', Carbon::now()->subDays(2)->toDateTimeString())
                ->where('bid.artist_user_id', '<>', $user->artist->user_id)
                ->whereIn('bid.status', $bidTypes)
                ->get();

            if ($type == 'upcoming') {
                $queryBuilder->join('event_genre', 'event_genre.event_id', '=', 'events.id')
                    ->join('artist_genre', 'artist_genre.genre_id', '=', 'event_genre.genre_id')
                    ->where('artist_genre.artist_id', $user->artist()->first()->id)
                    ->whereNotIn('events.id', $bids);
            } elseif ($type == 'past') {
                $queryBuilder->whereIn('events.id', function ($query) use ($user) {
                    $query->select('events.id')->from('events')
                        ->join('bid', function ($join) use ($user) {
                            $join->on('bid.event_id', '=', 'events.id')
                                ->where('bid.artist_user_id', '<>', $user->artist()->first()->id);
                        })
                        ->join('bid_contract', function ($join) {
                            $join->on('bid_contract.bid_id', '=', 'bid.id');
                        });
                });
            }
        }

        if ($isArtistManager) {
            $queryBuilder->join('event_genre', 'event_genre.event_id', '=', 'events.id')
                ->join('artist_genre', 'artist_genre.genre_id', '=', 'event_genre.genre_id')
                ->join('artists', 'artists.id', '=', 'artist_genre.artist_id')
                ->where('artists.manager_user_id', $user->id);
            /*  $queryBuilder->whereNotIn('events.id',function($query) use($user)
          {
            $query->select('id')->from('events')
            ->join('bid', function ($join) use($user) {
                $join->on('bid.event_id', '=', 'events.id')
                     ->whereNotIn('bid.artist_user_id',function($query2) use($user)
                 {
                   $query2->select('user_id')->from('artists')->where('artists.manager_user_id', $user->id);


                 });
            })
            ->join('bid_contract', function ($join) {
                $join->on('bid_contract.bid_id', '=', 'bid.id');
            });


          });*/
            /*
                $queryBuilder->leftJoin('bid', function ($join) use($user) {
                    $join->on('bid.event_id', '=', 'events.id')
                         ->where('bid.artist_user_id', '<>', 'artists.user_id');
                })
                ->leftJoin('bid_contract', function ($join) {
                    $join->on('bid_contract.bid_id', '=', 'bid.id');
                })->whereNull('bid_contract.id');

                ->leftJoin('bid', function ($join) use($user) {
                    $join->on('bid.event_id', '=', 'events.id')
                         ->where('bid.artist_user_id', '<>', $user->id);
                })
                ->leftJoin('bid_contract', function ($join) {
                    $join->on('bid_contract.bid_id', '=', 'bid.id');
                })*/
        }


        // Sorting events
        $sortsFields = [
            'location',
            'max_budget',
            'starttime',
            'created_at',
            'updated_at'
        ];
        $sorts = [];
        foreach ($sortsFields as $field) {
            $n = "sort_" . $field;
            $sorts[$n] = '';
            if ($request->has($n) && in_array($request->get($n), ['asc', 'desc'])) {
                $sortLocation = $request->get($n);
                $sorts[$n] = $sortLocation;
                if ($field == 'location') {//special case for sorting locations
                    $queryBuilder->orderBy('state', $sortLocation);
                    $queryBuilder->orderBy('city', $sortLocation);
                    $queryBuilder->orderBy('venue', $sortLocation);
                } else {
                    $queryBuilder->orderBy($field, $sortLocation);
                }
            }
        }
        // Filtering events
        $filters = [];
        $filters['search'] = "";
        if ($request->has('search')) {
            $searchText = $request->get('search');
            $filters['search'] = $searchText;

            $queryBuilder
                ->where(DB::raw('CONCAT(events.state, " ", events.city, " ", events.venue, " ", events.name, " ", users.username)'), 'like', '%' . $searchText . '%');
        }

        $queryBuilder->whereIn("events.status", [EventType::PUBLISHED, EventType::BOOKED]);


        $artUser = Auth::user()->artist;

        $eventByRel = Event::whereHas('artistSee', function ($query) use ($sign, $artUser) {
            $query->where('starttime', $sign, date('Y-m-d H:i:s'))
                ->select('events.*')
                ->addSelect(DB::raw('floor(events.max_budget * .90) as max_budget, floor(events.min_budget * .90) as min_budget'))
                ->groupBy('events.id');
        })->get();
        $managerArtists = null;
        if ($isArtistManager) {
            $manager = User::whereHas('roles', function ($qb) {
                $qb->where('users.id', Auth::user()->id)
                    ->where('roles.name', 'ARTIST_MANAGER');
            })->first();

            $managerArtists = Artist::where('manager_user_id', $manager->id)->get()->all();
        }
        $eventsPerPage = 25;
        $events = $queryBuilder->paginate($eventsPerPage);
        $myEvents = Event::where('user_id', $currentUser)
            ->where('starttime', $sign, date('Y-m-d H:i:s'))
            ->get();

        $eventsIds = [];
        foreach ($events as $event) {
            $eventsIds[] = $event->id;
        }

        $bidService = App::make("bidService");

        $eventRequestsCounter = $bidService->getCountBidsEvents($eventsIds);
        if ($isAdmin) {
            return view('cpanel.artistBid.adminPastEvents', [
                'bidArtist' => $bidArtist,
                'user' => $user,
                'artUser' => $artUser,
                'events' => $events,
                'sorts' => $sorts,
                'eventRequestsCounter' => $eventRequestsCounter,
                'filters' => $filters,
                'left_menu_selected' => $left_menu_selected,
                'eventsType' => $type,
                'isAdmin' => $isAdmin,
                'managerArtists' => $managerArtists,
                'isArtist' => $isArtist,
                'isFan' => $isFan,
                'isArtistManager' => $isArtistManager,
                'myEvents' => $myEvents,
                'currentUser' => $currentUser,
                'eventSeen' => $eventByRel,
            ]);
        } else {
            return view('events.upcoming', [
                'bidArtist' => $bidArtist,
                'user' => $user,
                'artUser' => $artUser,
                'events' => $events,
                'sorts' => $sorts,
                'eventRequestsCounter' => $eventRequestsCounter,
                'filters' => $filters,
                'left_menu_selected' => $left_menu_selected,
                'eventsType' => $type,
                'isAdmin' => $isAdmin,
                'managerArtists' => $managerArtists,
                'isArtist' => $isArtist,
                'isFan' => $isFan,
                'isArtistManager' => $isArtistManager,
                'myEvents' => $myEvents,
                'currentUser' => $currentUser,
                'eventSeen' => $eventByRel,
            ]);
        }

    }

    /**
     * @param Request $request
     * @param $eventId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function interestedEvent(Request $request, $eventId)
    {

        $currentUser = Auth::user();

        $isArtist = $currentUser->hasRole('ARTIST');
        $isArtistManager = $currentUser->hasRole('ARTIST_MANAGER');

        if (!$currentUser->hasRole('ARTIST') AND (!$currentUser->hasRole('ARTIST_MANAGER') && !$request->input('artistId'))) {
            return response()->view('errors.403');
        }

        if ($currentUser->hasRole('ARTIST_MANAGER')) {
            $currentUser->id = $request->input('artistId');
        }

        $bidService = App::make("bidService");

        $event = Event::find($eventId);

        if (!$bidService->hasArtistBidsForEvent($event, $currentUser)) {
            $bid = $bidService->createArtistBid($event, $currentUser);
            $artist_request = $request->get('price');
            $artist_fee = ($artist_request * .9);
            $bid_price = $artist_request + $artist_request * .1;
            $bidService->setBidArtistFee($bid, $artist_fee);
            $bidService->setBidPrice($bid, $bid_price);
            $emailService = App::make('emailNotificationService');

            $emailService->notifyAdminsAboutArtistRequestNewEvent($event, $currentUser);
            $bidService->sendNotificationAboutEventBidsPrices($event, array($bid));

            Session::flash('message', 'We got your request. Sit tight.');
        } else {
            Session::flash('message', 'You sent request to this event earlier.');
        }

        return redirect()->route('event_show', ['id' => $eventId]);
    }

    public function apiBidDetails(Request $request, $id)
    {

        $resp = [
            'success' => TRUE,
        ];
        try {
            $currentUser = Auth::user();
            $currentUserRoles = $currentUser->rolesList();

            $isAdmin = in_array("ADMIN", $currentUserRoles);
            $bidService = App::make("bidService");
            $bid = $bidService->getBid($id);

            if (!$bid) {
                throw new \Exception("Selected bid not exist");
            }

            if ($bid->event->user_id != $currentUser->id) {
                if (!$isAdmin){
                    throw new \Exception("You do not have permissions fot this actions");
                }
            }

            foreach ($bid->event->splitStartDate() as $key => $value) {
                $bid->event->$key = $value;
            }

            $userService = App::make("userService");
            $artistUser = $userService->getUser($bid->artist_user_id);
            $planerUser = $userService->getUser($bid->event->user_id);


            $resp['success'] = TRUE;
            $resp['bid'] = $bid;
            $resp['bid']->order = $bid->getOrder();

            $resp['artistUser'] = [
                'id' => $artistUser->id,
                'name' => $artistUser->name,
                'sounds_like' => $artistUser->artist->sounds_like,
                'avatar' => Image::url('uploads/avatars/' . $artistUser->avatar, 115, 115, array('crop')),
                'hometown' => $artistUser->artist->hometown,
                'statetown' => $artistUser->artist->statetown,
                'username' => $artistUser->username,
            ];
            $resp['planerUser'] = [
                'id' => $planerUser->id,
                'name' => $planerUser->name,
                'avatar' => Image::url('uploads/avatars/' . $planerUser->avatar, 115, 115, array('crop')),
                'payscapeVault' => NULL
            ];

            if ($planerUser->payscapeVault) {
                $resp['planerUser']['payscapeVault'] = [
                    'card_tail' => $planerUser->payscapeVault->card_tail
                ];
            }

        } catch (\Exception $e) {
            $resp['message'] = $e->getMessage();
        }

        return $resp;
    }
}
