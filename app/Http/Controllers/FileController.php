<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 14.04.17
 * Time: 9:54
 */

namespace App\Http\Controllers;

use App\File;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Validator;
use Auth;
use App;


class FileController extends Controller {

    function apiUpload(Request $request) {
        $max_size = File::MAX_FILE_SIZE / 1000;
        $message_max = " The file may not be greater than {$max_size} megabytes.";
        if ($request->offsetGet('file_size_exceeded')) {
            return response()->json([
                'success' => false,
                'errors' => [$message_max],
            ]);
        }

        $currentUser = Auth::user();
        $requestData = $request->all();
        $max_size = File::MAX_FILE_SIZE;
        $file = $request->file('file');
        $requestData['is_not_exe'] = (int) ! preg_match('/^.*\.exe$/i', $file->getClientOriginalName());


        $validator = Validator::make($requestData, [
            'entity'  => 'required',
            'entity_id'  => 'required|numeric',
            'file' => "required|max:{$max_size}",
            'is_not_exe' => 'accepted'
        ], ['accepted' => ' Invalid file format', 'max' => $message_max]);

        $res = ['success' => false];
        if (!$validator->fails()) {
            //Move Uploaded File
            $tmp = md5(implode("_", [
                time(),
                $file->getClientOriginalName(),
                $file->getRealPath()
            ]));

            $subDir1 = substr($tmp, 0, 4);
            $subDir2 = substr($tmp, 4, 4);

            $destinationPath = implode("/", [base_path(), 'uploads']);
            foreach([$subDir1, $subDir2] as $subDir) {
                $destinationPath .= "/" . $subDir;
                if (!is_dir($destinationPath)){
                    mkdir($destinationPath);
                }
            }
            try {
                $newFilePath = $file->move($destinationPath, $tmp.".".$file->getClientOriginalExtension());
                $fileService = App::make("fileService");

                $validData = $validator->getData();

                $fileInfo = $fileService->saveFileInfo($currentUser, $validData['entity'], $validData['entity_id'], $newFilePath, $file->getClientOriginalName());

                $res['success'] = true;
                $res['file'] = [
                    'id' => $fileInfo->id,
                    'name' => $fileInfo->name,
                    'url' => route("file_download", ['id' => $fileInfo->id])
                ];
            } catch (FileException $e) {

            }
        } else {
            $res['errors'] = $validator->errors();
        }

        return $res;
    }

    public function download(Request $request, $id) {
        $fileService = App::make('fileService');

        $file = $fileService->getFile($id);
        $currentUser = Auth::user();
        $fileUser = \App\User::find($file->user_id);
        if ($fileUser->isSystem() && $currentUser->artist) {
            $bidContract = \App\BidContract::where('file_id', $file->id)->first();
            if ($bidContract->bid->artistUser->id == $currentUser->id) {
                $bidContract->downloaded_by_artist = 1;
                $bidContract->save();
            }
        }

        return response()->download($file->path, $file->name);
    }

    public function getFileData(Request $request, $ids) {
      $fileService = App::make('fileService');

      $ids_arr = explode(",", $ids);
      $ids_arr = array_map("intval", $ids_arr);
      $ids_arr = array_filter($ids_arr);

      $res['files'] = [];
      foreach($ids_arr as $item) {
        $file = $fileService->getFile($item);
        if ($file) {
          $file['url'] = route("file_download", ['id' => $file['id']]);
          $res['files'][] = $file;
        }

      }
      $res['success'] = true;

      return $res;
    }
}