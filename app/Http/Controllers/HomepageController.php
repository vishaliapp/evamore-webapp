<?php

namespace App\Http\Controllers;

use App\Genre;
use App\EventType;
use App\SongType;
use App\User;
use Illuminate\Cookie\CookieJar;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Auth;
use DB;
use App;
use Session;
use App\Event;
use Debugbar;



class HomepageController extends Controller
{
    public function home(Request $request)
    {
        
        if (isset(back()->withInput()->getSession()->all()['_previous']['url'])){
            \Session::put('backUrl', back()->withInput()->getSession()->all()['_previous']['url']);
        }

        /*  if (Auth::user()){
              return redirect('dashboard');
      } */
    //  $request->session()->put('url.intended', url()->previous());
    $loginRedirect = false;
        self::getBrowserInfo();
        if ($request->query('login')) {
          $loginRedirect = true;
        }
        $loginModal = null;
        if (session()->get('session')){
            $loginModal = session()->get('session');
        }
    	return view('homeviews.landing',[
            'loginRedirect' =>$loginRedirect, 
            'loginModal' => $loginModal 
        ]);
    }

    public function home_logout(Request $request)
    {
        self::getBrowserInfo();
        if (Auth::user()){
            return redirect('dashboard');
        }

        // Clear redirector.
      $request->session()->put('url.intended', null);
      $request->headers->remove('referer');
      $loginRedirect = false;
      return view('homeviews.landing',['loginRedirect' =>$loginRedirect]);
    }

    public function events()
    {
    	return view('homeviews.events');
    }

    public function artists(CookieJar $cookieJar, Request $request)
    {


        $req_fee_string = $request->get('fee');
        $req_genre_string = $request->get('genres');
        $request_fee = $req_fee_string ? explode(',', $req_fee_string) : [];
        $request_genres = $req_genre_string ? explode(',', $req_genre_string) : [];
        $req_location = $request->get('location');
        $appends = [];

        if (!empty($req_fee_string)) {
            $appends['fee'] = $request->get('fee');
        }
        if (!empty($req_genre_string)) {
            $appends['genres'] = $request->get('genres', null);
        }
        if (!empty($req_location)) {
            $appends['location'] = $req_location;
        }


        $featured_artists_set = App\Services\ArtistService::getFeaturedArtists();
        $featured_artists = $featured_artists_set->chunk(6);
        $genres = Genre::all()->sortBy('name');
        $artists_query = App\Artist::active()->select([
            'artists.id',
            'artists.created_at',
            'users.id as user_id',
            'users.username as user_name',
            'users.name',
            'users.avatar',
        ]);
        if (!empty($request_fee)) {
            $artists_query->whereIn('fee', $request_fee);
        }
        $artists_query->join('artist_genre', 'artist_genre.artist_id', '=', 'artists.id');
        $artists_query->join('genres', 'artist_genre.genre_id', '=', 'genres.id');
        if ($request_genres) {
            $artists_query->whereIn('genres.name', $request_genres)->orderBy('name');
        }
        if (!empty($req_location)) {
            $artists_query->where('hometown', 'like', $req_location.'%');
        }

        $artists = $artists_query->groupBy('artists.id')->orderBy('created_at', 'asc')->paginate(8);
        $locations = [
            'New York City',
            'Chicago',
            'Atlanta',
            'Austin',
            'Nashville'
        ];

        \View::share('genres', $genres);
        \View::share('request_genres', $request_genres);
    	return view('homeviews.artists', compact( 'featured_artists', 'artists', 'appends', 'request_fee', 'locations', 'req_location'));
    }

    public function about()
    {
    	return view('homeviews.about');
    }

    public function support()
    {
        $user = Auth::user();

        $view = $user ? "homeviews.support_auth" : "homeviews.support";

    	return view($view, [
            'left_menu_selected' => 'support'
        ]);
    }

    public function sendSupport(Request $request){
        $validator = Validator::make($request->all(), [
            'message'              => 'required',
            'first_name'              => 'required',
            'last_name'              => 'required',
            'email'         => 'required|email'
        ]);

        if ($validator->fails()) {
            return redirect()->route('support')
                ->withErrors($validator)
                ->withInput();
        }else{
            $emailService = App::make("emailNotificationService");
            $first_name = $request->get('first_name');
            $last_name = $request->get('last_name');
            $email = $request->get('email');
            $message = $request->get('message');
            $emailService->notifyAboutSupportMessage($first_name, $last_name, $email, $message);

            Session::flash('message', "Thanks! We'll respond shortly.");

            return redirect()->route('support');
        }
        return view('homeviews.support');
    }

    public function artistsSignUp()
    {
        $currentUser = Auth::user();

        $mileage = [
            '25' => '25 miles',
            '50' => '50 miles',
            '150' => '150 miles',
            '250' => '250 miles',
            '500' => '500 miles',
        ];

        $equipments = [
            'yes' => 'Yes',
            'no' => 'No',
        ];
            
        $feeTypes = App\Artist::getFeeTypesText();

        foreach ($feeTypes as $key => $value) {
            $fee[$key] = implode(' ', $value);
        };
        $songTypes = SongType::all();
        
        return view('homeviews.artistRegistration', [
            'genres' => Genre::all()->sortBy('name'),
            'eventTypes' => EventType::all()->sortBy('name'),
            'feeTypes' => $fee,
            'songTypes' => $songTypes,
            'currentUser' => $currentUser,
            'mileage' => $mileage,
            'equipments' => $equipments,
        ]);
    }

    public static function getBrowserInfo()
    {
        /**
         * In order to show GIF instead of video on the IPAD IOS <= 9
         */
        $agent = new \Jenssegers\Agent\Agent();
        $browser = $agent->browser();
        $version = (int) $agent->version($browser);
        $isIpad = $agent->is('iPad');
        $isMobile = $agent->isMobile() && ! $isIpad;
        //$version = 9;
        $isIOSEqualOrLess_9 = $agent->isTablet()
            && $agent->isSafari()
            && $version >= 5
            && $version <= 9;
        \View::share('isIOSEqualOrLess_9', $isIOSEqualOrLess_9);
        \View::share('agent', $agent);
        \View::share('isMobile', $isMobile);
    }
}
