<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 04.01.17
 * Time: 13:04
 */

namespace App\Http\Controllers;


use App\Artist;
use App\Bid;
use App\Event;
use App\ForwardPayment;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Auth;
use App;
use Illuminate\Support\Facades\Log;
use Validator;
use App\ModelType\BidType;
use App\ModelType\OrderType;
use App\ModelType\PaymentType;

class PaymentController extends Controller
{

    public function payBidAction(Request $request, $id)
    {

        $currentUser = Auth::user();

        $eventsService = App::make("eventsService");
        $bidService = App::make("bidService");

        $bid = $bidService->getBid($id);

        if (!$bid) {
            abort(404, "The bid does not exist.");
        }

        if (!$bid->event) {
            abort(404, "The event does not exist.");
        }

        if ($bid->event->user_id != $currentUser->id) {
            return response()->view('errors.403', [
                'message' => "You are not owner of this event"
            ]);
        }

        $userService = App::make('userService');
        $artistUser = $userService->getUser($bid->artist_user_id);


//        $orderService = App::make('orderService');
//        $orderService = $orderService->getOrderByPlanerRequest($planerRequest);


        return view('payment.pay_bid', [
            'bid' => $bid,
            'currentUser' => $currentUser,
            'artistUser' => $artistUser,
        ]);
    }

    public function apiForwardPayBidAction(Request $request)
    {
//        dd($request->all());
        $token = $request->paymentToken;
        $active = ForwardPayment::where('token', $token)->first();
        $paymentMethod = $request['paymentMethod'];
        $currentUser = Auth::user();
        $bidService = App::make("bidService");

        $res = [
            'success' => false,
            'errors' => []
        ];
        if ($active->isActive) {

            try {
                $now = new \DateTime();

                $validations = [
                    'bid_id' => 'required',
                    'payAmount' => 'required|numeric:1|in:1,2',
                    'payMethod' => 'required|numeric:1|in:1,2',  // 1 - existed card, 2 - credit card
                    'expirationMonth' => 'required_if:payMethod,2|numeric|digits_between:1,12',
                    'expirationYear' => 'required_if:payMethod,2|numeric|digits:4|min:' . $now->format("Y"),
                    //'saveCardCheckbox' => 'required_if:cardNumber,expirationMonth,expirationYear,cvc '

                ];
                if ($paymentMethod == 'amex') {
                    $validations['cardNumber'] = 'required_if:payMethod,2|digits:15';
                    $validations['cvv'] = 'required_if:payMethod,amex|digits:4';
                } else {
                    $validations['cardNumber'] = 'required_if:payMethod,2|digits:16';
                    $validations['cvc'] = 'required_if:payMethod,other|digits:3';
                }

                //      automaticalyPayment
                //      saveCard

                $validator = Validator::make($request->all(), $validations);

                if ($validator->fails()) {
                    $errors = $validator->errors();
                    foreach ($validations as $filed => $v) {
                        if ($errors->has($filed)) {
                            $res['errors'][$filed] = $errors->first($filed);
                        }
                    }

                    throw new \Exception("Error in confirmation process. Try later.", 1);
                }

                $id = $request->get("bid_id", 0);
                $bid = $bidService->getBid($id);

                if (!$bid)
                    throw new \Exception("Bid to invite not exist.");

                if (!$bid->event)
                    throw new \Exception("The event does not exist.");


//            if ($bid->event->user_id != $currentUser->id) {
//                throw new \Exception("You are not owner of this event");
//            }

                $orderService = App::make('orderService');
                $order = $orderService->getOrderByBid($bid);

                if (!$order) {
                    $order = $orderService->createOrder($bid);
                }

                if ($order->status == OrderType::FULL_PAID) {
                    throw new \Exception("The bid already paid.");
                }

                $payscapeService = App::make("payscapeService");

                $payMethod = $request->get("payMethod");

                //=====================

                $paymentData = [
                    'ipAddress' => $request->ip()
                ];

                if ($payMethod == PaymentType::PAY_BY_VAULT) {
                    if ($currentUser->payscapeVault) {
                        $paymentData['vault_id'] = $currentUser->payscapeVault->vault_id;
                    } else {
//                    throw \Exception("You do not have stored credit card.");
                    }
                } else {
                    $expirationMonth = $request->get("expirationMonth");
                    $expirationMonth = (strlen($expirationMonth) == 1) ? "0" . $expirationMonth : $expirationMonth;
                    $expirationYear = substr($request->get("expirationYear"), 2, 2);
                    $paymentData = [
                        'ccnumber' => trim($request->get("cardNumber")),
                        'ccexp' => $expirationMonth . $expirationYear,
                    ];

                    if ($paymentMethod == 'amex') {
                        $paymentData['cvv'] = trim($request->get("cvv"));
                    } else {
                        $paymentData['cvv'] = trim($request->get("cvc"));
                    }

                    $saveCardFlag = $request->get("saveCard", 0);
                    if ($saveCardFlag == 1) {
                        try {
                            $payscapeService->deleteVault($currentUser);
                            $payscapeService->addVault($currentUser, $paymentData['ccnumber'], $paymentData['ccexp'], $currentUser->username);
                        } catch (\Exception $e) {
                            throw new \Exception($e->getMessage() . " Try later.");
                        }
                    }
                }
                //=====================

                if ($order->status == OrderType::HALF_PAID) {
                    $orderItem = $orderService->createRestOrderItem($order);

                    $newOrderStatus = OrderType::FULL_PAID;
                    $isFirsPayment = false;
                } else {
                    $isFirsPayment = true;
                    switch ($request->get("payAmount")) {
                        case BidType::HALF_AMOUNT :
                            $payAmount = $order->price * 0.5;
                            $newOrderStatus = OrderType::HALF_PAID;
                            break;
                        case BidType::FULL_AMOUNT :
                            $payAmount = $order->price;
                            $newOrderStatus = OrderType::FULL_PAID;
                            break;
                        default:
                            $payAmount = 0;
                            break;
                    }

                    if ($payAmount == 0) {
                        throw new \Exception("You should pay 50% or full amount.");
                    }

                    $orderItem = $orderService->createOrderItem($order, $payAmount);
                }

                if ($orderItem) {
                    try {
                        $payscapeService->saleOrderItem($orderItem, $paymentData);

                        if ($orderItem->transaction_id > 0) {
                            $order = $orderService->getOrder($orderItem->order_id);

                            $orderService->setOrderStatus($order, $newOrderStatus);
                            $bidService->setBidPaidMethod($bid, BidType::PAYMENT_METHOD_PAYSCAPE);

                            if ($isFirsPayment) {
                                $bidService->confirmBidByPlaner($bid);
                            }

                            //TODO: should be some better solution
                            if ($request->get("payAmount") == BidType::HALF_AMOUNT && $request->get("automaticalyPayment")) {
                                $bid->autopay = 1;
                                $bid->save();
                            }

                            $res['message'] = "Paid successfully";
                        }

                    } catch (\Exception $e) {
                        Log::info($e->getMessage());
                        throw new \Exception($e->getMessage());
                    }
                }

                $res['success'] = true;
            } catch (\Exception $e) {

                if ($e->getCode() == 0) {
                    $res['errors'] = ['global' => $e->getMessage()];
                }
            }
            if ($res['success'] === true) {
                //Email function there
            }
            $active->isActive = 0;
            $active->save();
            return $res;
        }


    }


    public function apiPayBidAction(Request $request)
    {
        $paymentMethod = $request['paymentMethod'];
        $currentUser = Auth::user();
        $currentUserRoles = $currentUser->rolesList();

        $isAdmin = in_array("ADMIN", $currentUserRoles);
        $bidService = App::make("bidService");
        $res = [
            'success' => false,
            'errors' => []
        ];
        try {
            $now = new \DateTime();

            $validations = [
                'bid_id' => 'required',
                'payAmount' => 'required|numeric:1|in:1,2',
                'payMethod' => 'required|numeric:1|in:1,2',  // 1 - existed card, 2 - credit card
                'expirationMonth' => 'required_if:payMethod,2|numeric|digits_between:1,12',
                'expirationYear' => 'required_if:payMethod,2|numeric|digits:4|min:' . $now->format("Y"),
                //'saveCardCheckbox' => 'required_if:cardNumber,expirationMonth,expirationYear,cvc '

            ];

            if ($paymentMethod === 'amex') {
                $validations['cardNumber'] = 'required_if:payMethod,2|digits:15';
                $validations['cvv'] = 'required_if:payMethod,2|digits:4';
            } else {
                $validations['cardNumber'] = 'required_if:payMethod,2|digits:16';
                $validations['cvc'] = 'required_if:payMethod,2|digits:3';
            }

            //      automaticalyPayment
            //      saveCard

            $validator = Validator::make($request->all(), $validations);

            if ($validator->fails()) {
                $errors = $validator->errors();
                foreach ($validations as $filed => $v) {
                    if ($errors->has($filed)) {
                        $res['errors'][$filed] = $errors->first($filed);
                    }
                }

                throw new \Exception("Error in confirmation process. Try later.", 1);
            }

            $id = $request->get("bid_id", 0);
            $bid = $bidService->getBid($id);

            if (!$bid)
                throw new \Exception("Bid to invite not exist.");

            if (!$bid->event)
                throw new \Exception("The event does not exist.");


            if ($bid->event->user_id != $currentUser->id) {
                if (!$isAdmin) {
                    throw new \Exception("You are not owner of this event");
                }
            }

            $orderService = App::make('orderService');
            $order = $orderService->getOrderByBid($bid);

            if (!$order) {
                $order = $orderService->createOrder($bid);
            }

            if ($order->status == OrderType::FULL_PAID) {
                throw new \Exception("The bid already paid.");
            }

            $payscapeService = App::make("payscapeService");

            $payMethod = $request->get("payMethod");

            //=====================

            $paymentData = [
                'ipAddress' => $request->ip()
            ];

            if ($payMethod == PaymentType::PAY_BY_VAULT) {
                if ($currentUser->payscapeVault) {
                    $paymentData['vault_id'] = $currentUser->payscapeVault->vault_id;
                } else {
                    throw \Exception("You do not have stored credit card.");
                }
            } else {
                $expirationMonth = $request->get("expirationMonth");
                $expirationMonth = (strlen($expirationMonth) == 1) ? "0" . $expirationMonth : $expirationMonth;
                $expirationYear = substr($request->get("expirationYear"), 2, 2);
                $paymentData = [
                    'ccnumber' => trim($request->get("cardNumber")),
                    'ccexp' => $expirationMonth . $expirationYear,
                ];

                if ($paymentMethod == 'amex') {
                    $paymentData['cvv'] = trim($request->get("cvv"));
                } else {
                    $paymentData['cvv'] = trim($request->get("cvc"));
                }

                $saveCardFlag = $request->get("saveCard", 0);
                if ($saveCardFlag == 1) {
                    try {
                        $payscapeService->deleteVault($currentUser);
                        $payscapeService->addVault($currentUser, $paymentData['ccnumber'], $paymentData['ccexp'], $currentUser->username);
                    } catch (\Exception $e) {
                        throw new \Exception($e->getMessage() . " Try later.");
                    }
                }
            }
            //=====================

            if ($order->status == OrderType::HALF_PAID) {
                $orderItem = $orderService->createRestOrderItem($order);

                $newOrderStatus = OrderType::FULL_PAID;
                $isFirsPayment = false;
            } else {
                $isFirsPayment = true;
                switch ($request->get("payAmount")) {
                    case BidType::HALF_AMOUNT :
                        $payAmount = $order->price * 0.5;
                        $newOrderStatus = OrderType::HALF_PAID;
                        break;
                    case BidType::FULL_AMOUNT :
                        $payAmount = $order->price;
                        $newOrderStatus = OrderType::FULL_PAID;
                        break;
                    default:
                        $payAmount = 0;
                        break;
                }

                if ($payAmount == 0) {
                    throw new \Exception("You should pay 50% or full amount.");
                }

                $orderItem = $orderService->createOrderItem($order, $payAmount);
            }

            if ($orderItem) {
                try {
                    $payscapeService->saleOrderItem($orderItem, $paymentData);

                    if ($orderItem->transaction_id > 0) {
                        $order = $orderService->getOrder($orderItem->order_id);

                        $orderService->setOrderStatus($order, $newOrderStatus);
                        $bidService->setBidPaidMethod($bid, BidType::PAYMENT_METHOD_PAYSCAPE);

                        if ($isFirsPayment) {
                            $bidService->confirmBidByPlaner($bid);
                        }

                        //TODO: should be some better solution
                        if ($request->get("payAmount") == BidType::HALF_AMOUNT && $request->get("automaticalyPayment")) {
                            $bid->autopay = 1;
                            $bid->save();
                        }

                        $res['message'] = "Paid successfully";
                    }

                } catch (\Exception $e) {
                    Log::info($e->getMessage());
                    throw new \Exception($e->getMessage());
                }
            }

            $res['success'] = true;
        } catch (\Exception $e) {

            if ($e->getCode() == 0) {
                $res['errors'] = ['global' => $e->getMessage()];
            }
        }
        if ($res['success'] === true) {
            //Email function there
        }
        return $res;
    }

    public function sendForwardPay(Request $request)
    {
        try {

            $emailService = App::make('emailNotificationService');
//        dd($request->all());
            $mail = $request->mail;
            $bidId = $request->bidId;
            $token = str_random(36);
            $payerName = $request->payerName;

            $forwardPayment = new ForwardPayment();
            $forwardPayment->mail = $mail;
            $forwardPayment->bidId = $bidId;
            $forwardPayment->token = $token;
            $forwardPayment->save();
            $bid = Bid::where('id', $bidId)->first();
            $event = $bid->event;
            $emailService->forwardPaymentToMail($bid, $event, $payerName, $mail, $token);

        } catch (\Exception $e) {
            dd($e);
        }


    }


    public function forwardPay($token)
    {

        $forward = ForwardPayment::where('token', $token)->first();
        $bid = Bid::where('id', $forward->bidId)->first();
        $event = Event::where('id', $bid->event_id)->first();
        $artist = $bid->artists->first();
        if (Carbon::parse($forward->created_at)->diffInHours() >= 24) {
            return view('errors.tokenError');
        } else {
            if ($forward->isActive != 0) {
                return view('artists.forwardPaymentModal', compact('bid', 'event', 'artist', 'token'));
            } else {
                return view('errors.tokenError');
            }
        }

    }

    public function apiConfirmContractAction(Request $request)
    {
        $currentUser = Auth::user();
        $currentUserRoles = $currentUser->rolesList();

        $isAdmin = in_array("ADMIN", $currentUserRoles);
        $bidService = App::make("bidService");
        $eventsService = App::make("eventsService");
        $res = [
            'success' => false,
            'errors' => []
        ];
        try {
            $validations = [
                'bid_id' => 'required',
                'signature' => 'required|max:255',
            ];

            $validator = Validator::make($request->all(), $validations);

            if ($validator->fails()) {
                $errors = $validator->errors();
                foreach ($validations as $filed => $v) {
                    if ($errors->has($filed)) {
                        $res['errors'][$filed] = $errors->first($filed);
                    }
                }

                throw new \Exception("Error in confirmation process. Try later.", 1);
            }

            $id = $request->get("bid_id", 0);
            $bid = $bidService->getBid($id);

            if (!$bid)
                throw new \Exception("Bid to invite not exist.");

            if (!$bid->event)
                throw new \Exception("The event does not exist.");

            if ($currentUser->artist) {
                if ($bid->artist_user_id != $currentUser->id) {
                    throw new \Exception("You are not invited to this event");
                }
                $bidService->singContractByArtist($bid, $request->get('signature'));
            } else {
                if ($bid->event->user_id != $currentUser->id) {
                    if (!$isAdmin) {
                        throw new \Exception("You are not owner of this event");

                    }
                }
                //sign contract
                $bidService->singContractByPlaner($bid, $request);

                //change bid Status
                $bidService->selectBidByPlaner($bid);

                //switch event status
                $eventsService->bookEvent($bid->event, $bid->artist);

                //decline other bids
//                $bidService->declineOtherBids($bid);

                /**
                 * Create Order on the STEP #2
                 */
                $orderService = App::make('orderService');
                $order = $orderService->getOrderByBid($bid);
                if (!$order) {
                    $order = $orderService->createOrder($bid);
                }
            }

            $res['success'] = true;
        } catch (\Exception $e) {

            if ($e->getCode() == 0) {
                $res['errors'] = ['global' => $e->getMessage()];
            }
        }

        return $res;
    }

    public function apiForwardConfirmContractAction(Request $request)
    {
        $currentUser = Auth::user();
        $bidService = App::make("bidService");
        $eventsService = App::make("eventsService");
        $res = [
            'success' => false,
            'errors' => []
        ];
        try {
            $validations = [
                'bid_id' => 'required',
                'signature' => 'required|max:255',
            ];

            $validator = Validator::make($request->all(), $validations);

            if ($validator->fails()) {
                $errors = $validator->errors();
                foreach ($validations as $filed => $v) {
                    if ($errors->has($filed)) {
                        $res['errors'][$filed] = $errors->first($filed);
                    }
                }

                throw new \Exception("Error in confirmation process. Try later.", 1);
            }

            $id = $request->get("bid_id", 0);
            $bid = $bidService->getBid($id);

            if (!$bid)
                throw new \Exception("Bid to invite not exist.");

            if (!$bid->event)
                throw new \Exception("The event does not exist.");

            if ($currentUser->artist) {
                if ($bid->artist_user_id != $currentUser->id) {
                    throw new \Exception("You are not invited to this event");
                }
//                $bidService->singContractByArtist($bid, $request->get('signature'));
            } else {
                //sign contract
                $bidService->singContractByPlaner($bid, $request);

                //change bid Status
                $bidService->selectBidByPlaner($bid);

                //switch event status
                $eventsService->bookEvent($bid->event, $bid->artist);

                //decline other bids
//                $bidService->declineOtherBids($bid);

                /**
                 * Create Order on the STEP #2
                 */
                $orderService = App::make('orderService');
                $order = $orderService->getOrderByBid($bid);
                if (!$order) {
                    $order = $orderService->createOrder($bid);
                }
            }

            $res['success'] = true;
        } catch (\Exception $e) {

            if ($e->getCode() == 0) {
                $res['errors'] = ['global' => $e->getMessage()];
            }
        }

        return $res;
    }

}

