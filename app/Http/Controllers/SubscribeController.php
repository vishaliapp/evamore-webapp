<?php

namespace App\Http\Controllers;

use App\Social;
use App\User;
use App\Role;
use App\Genre;
use App\Event;
use App\Artist;
use Illuminate\Contracts\Validation\ValidationException;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use DB;
use Image;
use App;
use Validator;
use Session;
use Debugbar;


class SubscribeController extends Controller {
  /*
|--------------------------------------------------
|   Post login parameters
|--------------------------------------------------
*/
  public function ajaxSubscribe(Request $request) {
    $responseData = [
      'success' => FALSE
    ];

    $validator = Validator::make($request->all(), [
      'email' => 'required|unique:subscribers|max:255|email',
    ], [
        'unique' => 'This email is already signed up to our newsletter',
    ]);

    if ($validator->fails()) {
      $responseData['message'] = $validator->errors()->first();
    }
    else {
        $subscribe =  \App\Subscriber::create(['email' => $request->get('email')]);
        $responseData['success'] = TRUE;
        $responseData['message'] = 'Your email has been added, please check your email to complete the newsletter signup';
    }
    return $responseData;
  }
}
