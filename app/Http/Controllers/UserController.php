<?php

namespace App\Http\Controllers;

use App\EventType;
use App\Social;
use App\SongType;
use App\User;
use App\Role;
use App\Genre;
use App\Event;
use App\Artist;
use Carbon\Carbon;
use Illuminate\Contracts\Validation\ValidationException;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use DB;
use Image;
use App;
use Validator;
use Session;
use Debugbar;


class UserController extends Controller
{

    public function artistsearch(Request $request)
    {
        $defaultSelectedGenreId = $request->get('genre_id', 0);

        $currentUser = Auth::user();

        $currentUserRoles = $currentUser->rolesList();

        $isArtist = in_array("ARTIST", $currentUserRoles);
        $isAdmin = in_array("ADMIN", $currentUserRoles);

        if ($isArtist) {
            return redirect()->route('events_by_type', ['type' => 'upcoming']);
        }

        $status = $request->get('status', null);
        $req_fee_string = $request->get('fee', false);
        $req_genre_string = $request->get('genres', false);
        $request_fee = $req_fee_string ? explode(',', $req_fee_string) : [];
        $request_genres = $req_genre_string ? explode(',', $req_genre_string) : [];
        $req_location = $request->get('location');

        $appends = [];

        if ($status) {
            $appends['status'] = $status;
        }
        if (!empty($req_fee_string)) {
            $appends['fee'] = $request->get('fee');
        }
        if (!empty($req_genre_string)) {
            $appends['genres'] = $request->get('genres', null);
        }
        if (!empty($req_location)) {
            $appends['location'] = $req_location;
        }

        $featured_artists_set = App\Services\ArtistService::getFeaturedArtists();
        $featured_artists = $featured_artists_set->chunk(4);
        $genres = Genre::all()->sortBy('name');

        $select = [
            'artists.id',
            'artists.created_at',
            'users.id as user_id',
            'users.name',
            'users.username as artist_username',
            'users.avatar',
        ];
        $artists_query = $status === 'inactive' && $isAdmin ? App\Artist::inactive() : App\Artist::active();
//      $artists_query = App\Artist::active()->select([
//          'artists.id',
//          'users.id as user_id',
//          'users.username as user_name',
//          'users.name',
//          'users.avatar',
//      ]);
        $artists_query->select($select)->orderBy('created_at', 'asc');
        if (count($request_fee)) {
            $artists_query->whereIn('fee', $request_fee);
        }
        $artists_query->join('artist_genre', 'artist_genre.artist_id', '=', 'artists.id');
        $artists_query->join('genres', 'artist_genre.genre_id', '=', 'genres.id');

//       if ($status !== 'inactive' && ! $isAdmin || count($request_genres)) {
//           $artists_query->whereIn('genres.name', $request_genres);
//       }

        if ($status !== 'inactive' && $request_genres) {
            $artists_query->whereIn('genres.name', $request_genres)->orderBy('name');
        }
        if (!empty($req_location)) {
            $artists_query->where('hometown', 'like', $req_location . '%');
        }

        $locations = [
            'New York City',
            'Chicago',
            'Austin',
            'Atlanta',
            'Nashville'
        ];

        $artists_query->groupBy('artists.id')->orderBy('created_at', 'asc');
        $artists = $artists_query->paginate(8);
        $view_data = compact('genres', 'featured_artists', 'artists', 'appends', 'request_genres', 'request_fee', 'locations', 'req_location');
        $view_data2 = [
            'currentUser' => $currentUser,
            'left_menu_selected' => 'artists',
            'genres' => Genre::all()->sortBy("name"),
            'status' => $status,
            'defaultSelectedGenreId' => $defaultSelectedGenreId,
            'checkbox' => $isAdmin
        ];
        $isArtistManager = $currentUser->hasRole('ARTIST_MANAGER');

        \View::share('isArtistManager', $isArtistManager);
        \View::share('isAdmin', $isAdmin);
        \View::share('isArtist', $isArtist);


        $artistData = [
            'hometown' => 'Hometown',
            'description' => 'Description',
            'sounds_like' => 'Sounds Like',
            'statetown' => 'Statetown',
            'fee' => 'Fee',
        ];
        $artistUserData = [
            'name' => 'Name',
            'avatar' => 'Avatar',
        ];

        $artistPdfSocial = [
            'Youtube channel' => 'channel_name',
            'Twitter' => 'twitter_handle',
        ];

        return view('artists.index', $view_data + $view_data2, compact('artistPdfSocial', 'artistUserData', 'artistData'));
    }

    public function artistSearchBar($search = null)
    {

        $search_text = $search;
        $data = User::where('active', 1)->whereHas('artist', function ($query) use ($search_text) {
            $query->where('name', 'like', "%$search_text%")->orWhere('username', 'like', "%$search_text%");
        })->get();

        return response($data);

    }


    public function dashboard()
    {
        $currentUser = Auth::user();
        $currentUserRoles = $currentUser->rolesList();

        $isArtist = in_array("ARTIST", $currentUserRoles);
        $isAdmin = in_array("ADMIN", $currentUserRoles);
        $isArtistManager = in_array("ARTIST_MANAGER", $currentUserRoles);

        if ($isArtist) {
            return redirect()->route('events_by_type', ['type' => 'upcoming']);
        }
        if ($isAdmin) {
            return redirect()->route('events_by_type', ['type' => 'upcoming']);

        }
        if ($isArtistManager) {
            return redirect()->route('events_by_type', ['type' => 'upcoming']);

        }
        \View::share('isArtistManager', $isArtistManager);
        \View::share('isAdmin', $isAdmin);
        \View::share('isArtist', $isArtist);

        return view('members.dashboard', ["currentUser" => $currentUser, "left_menu_selected" => "dashboard"]);
    }

    public function forgotPassword()
    {
        return view('accounts.forgot');
    }

    public function resetPasswordByEmail(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email|max:255',
        ]);

        $user = User::whereEmail($request['email'])->first();
        if (isset($user)) {
            $emailService = App::make('emailNotificationService');
            $emailService->sendEmailtoUserForPasswordReset($user);
            return redirect()
                ->route('sent_email')
                ->with('email', $user->email);
        } else {
            return redirect()
                ->route('account_forgot')
                ->with('status', 'There is no user with such email');
        }
    }

    public function sentEmail()
    {
        return view('accounts.sentRecoveryEmail');
    }

    public function setNewPassword($secretKey)
    {
        $resetPasswordService = App::make('resetPasswordService');
        $reset = $resetPasswordService->keyExists($secretKey);
        if (isset($reset)) {
            $user = User::find($reset->user_id);
            return view('accounts.newPassword', [
                'key' => $secretKey,
                'user' => $user,
            ]);
        } else {
            return view('accounts.linkIsExpired');
        }
    }

    public function saveNewPassword(Request $request)
    {

        $this->validate($request, [
            'password' => 'required|min:8|confirmed',
            'user_id' => 'required',
        ]);

        $user = User::find($request['user_id']);
        $resetPasswordService = App::make('resetPasswordService');
        $userExists = $resetPasswordService->keyWithThisUserIdExists($request['key'], $user->id);
        if (isset($userExists)) {
            $user->password = bcrypt($request['password']);
            $user->save();
            $resetPasswordService->deletePasswordResetRecord($user->id);
            return redirect()->route('password_changed');
        }
    }

    public function passwordChanged()
    {
        return view('accounts.passwordChanged');
    }

    /*
    |--------------------------------------------------
    |   Post data for User-(fan) signup
    |--------------------------------------------------
    */
    public function postSignUp(Request $request)
    {
        $currentUser = Auth::user();
        $this->validate($request, [
            'fname' => 'required|max:255',
            'lname' => 'required|max:255',
            'username' => 'required|max:255|unique:users',
            'email' => 'required|email|max:120|unique:users',
            'password' => 'required|min:8|confirmed',
            'phone' => 'max:255',
            'instagram_user' => 'max:255',
        ]);


        $firstname = $request['fname'];
        $lastname = $request['lname'];
        $username = $request['username'];;
        $email = $request['email'];
        $password = bcrypt($request['password']);
        $phone = $request['phone'];

        $role_user = Role::getRole("FAN");

        $user = new User();
        $user->name = $firstname . ' ' . $lastname;
        $user->username = $username;
        $user->email = $email;
        $user->password = $password;
        $user->phone = $phone;
        $user->save();
        $user->roles()->attach($role_user);


        //Send emails only when new planner registered
        if (!$currentUser) {
            $emailService = App::make('emailNotificationService');
            $emailService->newPlanerSignUp($user);
        }

        Auth::login($user);
        $currentUser = Auth::user();
        $isAdmin = $currentUser->hasRole('ADMIN');
        $isArtistManager = $currentUser->hasRole('ARTIST_MANAGER');
        $isArtist = $currentUser->hasRole('ARTIST');

        if (!$isArtist && !$isArtistManager && !$isAdmin) {
            return redirect()->route('dashboard');
        } else {
            return redirect()->route('events');
        }
    }

    /*
    |--------------------------------------------------
    |   Post data for artist signup
    |--------------------------------------------------
    */
    public function postArtistSignUp(Request $request)
    {
        $currentUser = Auth::user();
        $isAdmin = FALSE;
        $isArtistManager = FALSE;

        if ($currentUser) {
            $isAdmin = $currentUser->hasRole('ADMIN');
            $isArtistManager = $currentUser->hasRole('ARTIST_MANAGER');
        }

        //New artist can be created by Admin or user(artist) can register yourself
        if ($currentUser && !($isAdmin || $isArtistManager)) {
            return response()->view('errors.403');
        }
        try 
        {
            DB::beginTransaction();

            $feeTypes = Artist::getFeeTypes();
            $feeIds = array_keys($feeTypes);
            $minFeeId = min($feeIds);
            $maxFeeId = max($feeIds);
            $customAttributes = [
                'name' => 'Name',
                'username' => 'Username',
                'email' => 'Email',
                'secondaryEmail' => 'Secondary Email',
                'password' => 'Password',
                'artist.fee' => 'Minimum Amount',
                'artist.set_length' => 'Maximum Set Length',
                'phone' => 'Phone Number',
                'artist.hometown' => 'Current Location',
                'artist.description' => 'Description',
                'artist.mileage' => 'Mileage',
                'artist.equipments' => 'Equipments',
                'social.spotify_uri' => 'Spotify URI',
                'social.instagram_user' => 'Insagram URI',
                'social.channel_name' => 'Youtube URI',
                'social.facebook_id' => 'Facebook URI',
                'social.twitter_handle' => 'Twitter URI',
                'social.website' => 'Website',
                'genre' => 'Genre',
                'eventTypes' => 'Event Type',
                'avatar' => 'Image',
            ];
            $this->validate($request, [
                'name' => 'required|max:255',
                'username' => 'required|max:255|unique:users',
                'email' => (($isArtistManager || $isAdmin) ? '' : 'required|') . 'email|max:120|unique:users',
                'secondaryEmail' => (($isArtistManager) ? 'email|max:255' : ''),
                'password' => 'required|min:8',
                'artist.fee' => 'required',
                'artist.set_length' => 'required|numeric|min:0',
                'phone' => 'required|integer|min:1',
                'artist.hometown' => 'required|max:255',
                'artist.description' => 'required|max:500',
                'artist.mileage' => 'integer|min:25',
                'artist.equipments' => 'max:255',
                'social.spotify_uri' => 'max:255|regex:/(^([a-zA-Z0-9()&:%\/_.-?]+)?$)/u',
                'social.instagram_user' => 'max:255|regex:/(^([a-zA-Z0-9()&:%\/_.-?]+)?$)/u',
                'social.channel_name' => ((str_contains($request['social.channel_name'], 'youtube.com')) ? ['max:255','regex:/^(http(s)?:\/\/)?((w){3}.)?youtu(be|.be)?(\.com)?\/.+/'] : ''),
                'social.facebook_id' => 'max:255|regex:/(^([a-zA-Z0-9()&:%\/_.-?]+)?$)/u',
                'social.twitter_handle' => 'max:255|regex:/(^([a-zA-Z0-9()&:%\/_.-?]+)?$)/u',
                'social.website' => 'max:255|siteUrlValidator',
                'genre' => 'required|present|array',
                'songTypes' => 'required|present|array',
                'eventTypes' => 'required|present|array',
                'avatar' => 'image',
            ], [], $customAttributes);

            $name = $request['name'];
            $username = $request['username'];
            $email = $request['email'];
            $secondaryEmail = $request['secondaryEmail'];
            $password = bcrypt($request['password']);
            $fee = substr($request['artist.fee'], 1);
            $set_length = $request['artist.set_length'];
            $phone = $request['phone'];
            $time_zone = (int)$request['time_zone'];
            $mood = $request['mood'];
            $hometown = $request['artist.hometown'];
            $place_lat = $request['artist.place_lat'];
            $place_lng = $request['artist.place_lng'];
            $mileage = $request['artist.mileage'];
            $equipments = $request['artist.equipments'];
            $description = $request['artist.description'];
            $facebook_id = $request['social.facebook_id'];
            $aboutUs= $request['artist.hear_about_us'];

            $soundsLike = [];

            foreach ($request['soundlike'] as $soundLike) {
                $sl = trim($soundLike);
                if ($sl != '') {
                    $soundsLike[] = $sl;
                };
            }

            if (!empty($soundsLike)) {
                $soundsLike = implode(", ", $soundsLike);
            } else {
                $soundsLike = '';
            }

            $spotify_uri = $request['social.spotify_uri'];
            $instagram_user = $request['social.instagram_user'];
            $channel_name = $request['social.channel_name'];
            $facebook_id = $request['social.facebook_id'];
            $twitter_handle = $request['social.twitter_handle'];
            $website = $request['social.website'];

            $role_artist = Role::getRole("ARTIST");

            $user = new User();
            $user->name = $name;
            $user->username = $username;
            $user->email = $email;
            $user->active = 'FALSE';
            $user->password = $password;
            $user->phone = $phone;
            $user->avatar = 'eva_profilepicture.png';
            $user->time_zone = $time_zone;
            $user->save();

            $base64Image = $request->get('upload_image');
            if ($base64Image != '') {
                $pos = strpos($base64Image, ';');
                $type = explode(':', substr($base64Image, 0, $pos))[1];
                $type = explode('/', $type)[1];
                $filename = $user->id . "_" . str_random(7) . "." . $type;
                $path = public_path('uploads/avatars') . "/" . $filename;
                $dataBase64 = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $base64Image));
                file_put_contents($path, $dataBase64);
                $user->avatar = $filename;
            }

            $user->save();

            $user->roles()->attach($role_artist);

            $artist = new Artist();
            $artist->user_id = $user->id;
            $artist->secondary_email = $secondaryEmail;
            $artist->mood = $mood;
            $artist->hometown = $hometown;
            $artist->place_lat = $place_lat;
            $artist->place_lng = $place_lng;
            $artist->fee = $fee;
            $artist->set_length = $set_length;
            $artist->mileage = $mileage;
            $artist->equipments = $equipments;
            $artist->description = $description;
            $artist->sounds_like = $soundsLike;
            $artist->hear_about_us = $aboutUs;

            if ($isArtistManager) {
                $artist->manager_user_id = $currentUser->id;
            }
            $artist->save();


            $genres = (sizeof($request['genre']) != 0 ? $request['genre'] : []);
            foreach ($genres as $genre) {
                $g = Genre::where('id', $genre)->first();
                $artist->genres()->attach($g);
            }

            $eventTypes = (sizeof($request['eventTypes']) != 0 ? $request['eventTypes'] : []);
            foreach ($eventTypes as $type) {
                $t = EventType::where('id', $type)->first();
                $artist->eventTypes()->attach($t);
            }
            // Song Types
            $songTypes = (sizeof($request['songTypes']) != 0 ? $request['songTypes'] : []);
            foreach ($songTypes as $type) {
                $t = SongType::where('id', $type)->first();
                $artist->songTypes()->attach($t);
            }

            $social = new Social();
            $social->user_id = $user->id;
            $social->spotify_uri = $spotify_uri;
            $social->instagram_user = $instagram_user;
            $social->channel_name = $channel_name;
            $social->facebook_id = $facebook_id;
            $social->twitter_handle = $twitter_handle;
            $social->website = $website;
            $social->save();

            //Send emails only when new artist registered
            if (!$currentUser) {
                $emailService = App::make('emailNotificationService');
                $emailService->newArtistApplies($user);
                $emailService->newArtistApplies($artist);
            }

            if ($isArtistManager) {
                Session::flash('message', 'New artist created successfully!');
                DB::commit();
                return redirect()->route('manager_artists');
            }
            if ($isAdmin) {
                Session::flash('message', 'New artist created successfully!');
                DB::commit();
                return redirect()->route('cpanel_users_index');
            }
            DB::commit();

            return redirect()->route('artistthanks');
        } 
        catch (\Exception $exception) {
            DB::rollBack();
            return redirect()->back()->withErrors($exception->getMessage())
                ->withInput();
        }

    }

    /*
    |--------------------------------------------------
    |   Post login parameters
    |--------------------------------------------------
    */
    public function postSignIn(Request $request)
    {
        $this->validate($request, [
            'username' => 'required',
            'password' => 'required',
        ]);

        if (Auth::attempt([
            'username' => $request['username'],
            'password' => $request['password']
        ])
        ) {
            //Check if user is active
            $user = Auth::getLastAttempted();
            if ($user->active) {
                Auth::login($user, $request->has('remember'));
                $prev = $request->session()->get('backUrl');

                if (empty($prev)) {
                    return redirect()->route('dashboard');
                } else {
                    return redirect()->route($prev);
                }
            } else {
                Auth::logout();
                return redirect()->back()
                    ->withErrors([
                        'active' => 'You must be active to login.'
                    ]);
            }
        }
        return redirect()->back();
    }

    /*
  |--------------------------------------------------
  |   Post login parameters
  |--------------------------------------------------
  */
    public function ajaxSignIn(Request $request)
    {

        $responseData = [
            'success' => FALSE
        ];

        $validator = Validator::make($request->all(), [
            'username' => 'required',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            $responseData['message'] = 'Username or password is incorrect';
        } else {


            if (Auth::attempt([
                'username' => $request['username'],
                'password' => $request['password']
            ])
            ) {
                //Check if user is active
                $user = Auth::getLastAttempted();
                if ($user->active) {
                    Auth::login($user, $request->has('remember'));

                    $currentUserRoles = $user->rolesList();

                    $isArtist = in_array("ARTIST", $currentUserRoles);
                    $isAdmin = in_array("ADMIN", $currentUserRoles);
                    $isArtistManager = in_array("ARTIST_MANAGER", $currentUserRoles);

                    $responseData['success'] = TRUE;
                    $prev = $request->session()->get('backUrl');

                    if (empty($prev)) {
                        // If user logout.
                        if ($isArtist || $isAdmin || $isArtistManager) {
                            $responseData['redirect_to'] = route('events_by_type', ['type' => 'upcoming']);
                        } else {
                            $responseData['redirect_to'] = route('dashboard');
                        }
                    } else {
                        $responseData['redirect_to'] = $request->session()
                            ->get('backUrl');
                        \Session::put('backUrl', '');

                    }
                } else {
                    Auth::logout();
                    $responseData['message'] = 'You must be active to login.';
                }
            } else {
                $responseData['message'] = 'Username or password is incorrect';
            }
        }
        return $responseData;
    }

    /*
    |--------------------------------------------------
    |   Logout users
    |--------------------------------------------------
    */
    public function getLogout(Request $request)
    {

        Auth::logout();
        return redirect()->route('home_logout');
    }

    /*
    |--------------------------------------------------
    |   Get logged in user/artist profile page
    |--------------------------------------------------
    */
    public function getProfile(Request $request, $id)
    {
        $currentUser = Auth::user();
        if ($currentUser->hasRole('ADMIN')) {
            $user = User::find($id);

            return view('accounts.fan', [
                'user' => $user,
                'currentUser' => $currentUser,
            ]);
        }

        if ($currentUser->hasRole('FAN') || $currentUser->hasRole('ARTIST_MANAGER')) {
            if ($id != $currentUser->id) {
                return response()->view('errors.403');
            }

            $user = User::find($id);
            return view('accounts.fan', [
                'user' => $user,
                'currentUser' => $currentUser
            ]);
        }

        return response()->view('errors.403');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function postEditUser(Request $request)
    {

        //TODO: add check permissions for save data
        $currentUser = Auth::user();

        $userId = $request->get('id', 0);

        if (!$currentUser->hasRole('ADMIN') && $userId != $currentUser->id) {
            return response()->view('errors.403');
        }

        $user = User::find($userId);

        $this->validate($request, [
            'name' => 'required|max:255',
            'phone' => 'max:255',
            'email' => 'required|email|emailDublicateValidator:' . $user->id,
            'organization' => 'max:255',
            'org_position' => 'max:255',
        ]);

        $name = $request['name'];
        $email = $request['email'];
        $org = $request['organization'];
        $org_pos = $request['org_position'];
        $phone = $request['phone'];
        $time_zone = (int)$request['time_zone'];

        $user->name = $name;
        $user->email = $email;
        $user->organization = $org;
        $user->org_position = $org_pos;
        $user->phone = $phone;
        $user->time_zone = $time_zone;
        $user->save();

        $base64Image = $request->get('upload_image');

        if ($base64Image != '') {

            $pos = strpos($base64Image, ';');
            $type = explode(':', substr($base64Image, 0, $pos))[1];
            $type = explode('/', $type)[1];

            $filename = $user->id . "_" . str_random(7) . "." . $type;
            $path = public_path('uploads/avatars') . "/" . $filename;

            $dataBase64 = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $base64Image));
            file_put_contents($path, $dataBase64);

            $user->avatar = $filename;

        }

        $user->save();

        Session::flash('message', "Account updated.");
        return redirect()->route('edit_account', ['id' => $user->id]);
    }

    public function ajaxSignUp(Request $request)
    {
        $responseData = [
            'success' => FALSE,
            'errors' => ''
        ];

        $validator = Validator::make($request->all(), [
            'fname' => 'required|max:255',
            'lname' => 'required|max:255',
            'username' => 'required|max:255|unique:users',
            'email' => 'required|email|max:120|unique:users',
            'password' => 'required|min:8|confirmed',
            'phone' => 'required|max:255',
            'instagram_user' => 'max:255',
        ]);

        if ($validator->fails()) {
            return json_encode([
                'success' => FALSE,
                'errors' => $validator->errors()->getMessages()
            ]);
        } else {
            $currentUser = Auth::user();
            $firstname = $request['fname'];
            $lastname = $request['lname'];
            $username = $request['username'];;
            $email = $request['email'];
            $password = bcrypt($request['password']);
            $phone = $request['phone'];

            $role_user = Role::getRole("FAN");

            $user = new User();
            $user->name = $firstname . ' ' . $lastname;
            $user->username = $username;
            $user->email = $email;
            $user->password = $password;
            $user->phone = $phone;
            $user->active = 1;
            $user->locked = 0;
            $user->save();
            $user->roles()->attach($role_user);

            //Send emails only when new planner registered
            if (!$currentUser) {
                $emailService = App::make('emailNotificationService');
                $emailService->newPlanerSignUp($user);
            }

            Auth::login($user);

            $responseData['success'] = TRUE;

            if (intval($request->cookie("booked_user_id")) > 0) {
                $responseData['redirect_to'] = route('createEvent', [
                    "booked_user_id" => $request->cookie("booked_user_id")
                ]);
                \Cookie::queue(\Cookie::forget('booked_user_id'));
            } else {
                $responseData['redirect_to'] = route('dashboard');
            }
        }
        return $responseData;
    }

    public function apiProfileAction($id)
    {
        $currentUser = Auth::user();

        if (!$currentUser) {
            abort("403");
        }

        $userService = App::make("userService");

        $user = $userService->getUser($id);

        if (!$user) {
            abort("404");
        }

        $userData = [
            'id' => $user->id,
            'name' => $user->name,
            'active' => $user->active,
            'artist' => NULL,
            'avatar' => Image::url('/uploads/avatars/' . $user->avatar, 115, 115, array('crop'))
        ];

        if ($user->artist) {
            $user->artist->genres;
            $user->artist->social = $user->social;

            $userData['artist'] = $user->artist;
        }

        return [
            'success' => TRUE,
            'user' => $userData
        ];
    }
}
