<?php

namespace App\Http\Middleware;

use App\File;
use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;
use Closure;

class CheckFileSize extends  BaseVerifier
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $proceed = $request->input('proceed');
        $issetFile = $request->hasFile('file');
        if ($proceed && ! $issetFile) {
            /**
             * If file is greater then upload_max_filesize or post_max_size
             * the $file will be NULL.
             */
            $request->offsetSet('file_size_exceeded', 1);
            return $next($request);
        } else {
            if ( ! $this->tokensMatch($request) ) {
                throw new TokenMismatchException;
            }
            return $next($request);
        }
    }
}
