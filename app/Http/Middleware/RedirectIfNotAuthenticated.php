<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfNotAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
            echo 3; exit;
            return redirect('/');
        }

        if ($request->has("prev")){
            return $next($request);
        }
        return redirect()->route('home', ['prev'=>$request->getRequestUri()]);
    }
}
