<?php

namespace App\Http\Middleware;

use App\File;
use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;
use Closure;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        /**
         * It will be checked in \App\Http\Middleware\CheckFileSize::class
         */
        'api/file/upload',
    ];
}
