<?php

namespace App\Http\Requests;

use App\Event;
use App\Http\Requests\Request;
use App\ModelType\EventType;
use App\ModelType\TimeZoneType;

class EventUpdateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules =  [
            'name' => 'required|max:255',
            'date' => 'required|date|future_date|date_format:m/d/Y',
            'hours' => 'required|max:12|min:0|numeric',
            'minutes' => 'required|max:59|min:0|numeric',
            'address' => 'required|max:255',
            'venue' => 'required|max:255',
            'place_lat' => 'numeric',
            'place_lng' => 'numeric',
            'radio' => 'boolean',
            'city' => 'max:255',
            'state' => 'max:255',
            'min_budget' => 'required|integer|min:0|numeric',
            'max_budget' => 'required|integer|min:0|numeric|greater_than:min_budget',
            'duration' => 'required:integer|min:0|numeric',
            'genre' => 'required',
            'event_type' => 'required|numeric',
            'time_zone' => 'required|in:' . implode(",", array_keys(TimeZoneType::$zones))
        ];
        return $rules;
    }
}
