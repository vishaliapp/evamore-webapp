<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*
|-------------------	
|	Landing Pages
|-------------------
*/
Route::get('/', 'HomepageController@home')->name('home');
Route::get('/home-logout', 'HomepageController@home_logout')->name('home_logout');
Route::get('more', 'HomepageController@events')->name('home_events');

Route::get('/artists', 'HomepageController@artists')->name('home_artists');
Route::get('/artists/registration', ['uses'=>'HomepageController@artistsSignUp', 'as'=>'artists_registration']);
Route::get('/about', 'HomepageController@about')->name('home_about');
Route::get('/support',  ['uses'=>'HomepageController@support', 'as'=>'support']);
Route::post('/support', ['uses' => 'HomepageController@sendSupport','as' => 'sendsupport']);

/*
|---------------------------	
|	Logged In Landing Pages
|---------------------------
*/
Route::get('/dashboard', ['uses' => 'UserController@dashboard','as' => 'dashboard', 'middleware' => 'auth']);
Route::get('/artistsearch', ['uses' => 'UserController@artistsearch','as' => 'artistsearch', 'middleware' => 'auth']);
Route::post('/artist/search-bar/{search}', ['uses' => 'UserController@artistSearchBar','as' => 'artist-search-bar', 'middleware' => 'auth']);

Route::post('/users/search-bar/{search}', ['uses' => 'CpanelUsersController@userSearchBar','as' => 'artist-search-bar', 'middleware' => 'auth']);

/*
|------------------------------	
| Post for User Signup/Login
|------------------------------
*/
Route::post('/signup', ['uses' => 'UserController@postSignUp','as' => 'signup']);	
Route::post('/signin', ['uses' => 'UserController@postSignIn','as' => 'signin']);	
Route::post('/ajax/signin', ['uses' => 'UserController@ajaxSignIn','as' => 'ajax_signin']);
Route::post('/ajax/signup', ['uses' => 'UserController@ajaxSignUp','as' => 'ajax_signup']);

/*
|-------------------	
|	User Logout
|-------------------
*/
Route::get('/logout', ['uses' => 'UserController@getLogout','as' => 'logout']);

/*
|------------------------------
| Post for Subscribe
|------------------------------
*/
Route::get('/ajax/subscribe', ['uses' => 'SubscribeController@ajaxSubscribe','as' => 'ajax_subscribe']);
/*
|-------------------	
|	Artist Routes
|-------------------
*/
/* Artist get */
Route::get('artists/active/json/get', 'ArtistController@activeArtistsJSON');
Route::get('api/v1/artists/{username}', 'ArtistController@activeArtistJSON');

Route::get('artist/application/thanks', ['uses' => 'ArtistController@getArtistThanks','as' => 'artistthanks']);
Route::get('artists/{username}', ['uses' => 'ArtistController@show','as' => 'artist_show'] )->where('username', '(.*)');

/* Artist post*/
Route::post('artistsignup', ['uses' => 'UserController@postArtistSignUp','as' => 'artistsignup']);	
Route::post('artist/activation/change', ['uses' => 'ArtistController@postArtistActivationChange','as' => 'artists_activation_change']);

Route::post('artist/confirm/booking', ['middleware' => 'auth','uses' => 'ArtistController@confirmArtistRequest','as' => 'confirm_artist_request']); //!!

Route::post('artist/bookrequest', ['middleware' => 'auth', 'uses' => 'ArtistController@requestArtistBookByPlanner','as' => 'artist_bookrequest']);
Route::post('artists/active', ['middleware' => 'auth', 'uses'=>'ArtistController@artistsActivate','as'=>'artists_activate']);

Route::post('artists/confirm/planer/request', ['middleware' => 'auth', 'uses'=>'ArtistController@confirmPlanerRequestAction','as'=>'artists_confirm_planer_request']);

Route::get('instagram', 'ArtistController@getArtistInstagram');
Route::get('instagramcallback', 'ArtistController@instagramCallback');

/*
|-------------------	
|	Events Routes
|-------------------
*/
Route::get('/events/get', ['middleware' => 'auth', 'uses'=>'EventController@index']);
Route::get('events/create', ['uses'=>'EventController@create', 'as' => 'createEvent' ]);
Route::get('events/{type}', ['middleware' => 'auth', 'uses' => 'EventController@events', 'as' => 'events_by_type']);
Route::get('events/past/get', 'EventController@getPastEvents');
Route::get('events/{id}/display', ['uses' => 'EventController@show', 'as' => 'event_show']);
Route::post('events/{id}/change', 'EventController@artistChanges')->name('event_update_artist');
Route::get('events/{event_id}/change', 'EventController@showChanges')->name('show_event_changes');
Route::post('events/apply', 'EventController@applyChanges')->name('apply_event');
Route::get('events/{id}/delete', ['middleware' => 'auth', 'uses' => 'EventController@delete','as' => 'deleteEvent']);
Route::get('events/{id}/cancel', ['middleware' => 'auth', 'uses' => 'EventController@cancel','as' => 'cancelEvent']);
Route::post('events/{id}/publish', ['middleware' => 'auth', 'uses' => 'EventController@publish','as' => 'publishEvent']);

Route::post('events/save', ['middleware' => 'auth', 'uses' => 'EventController@postSaveEvent','as' => 'saveEvent']);

Route::post('events/update', ['middleware' => 'auth', 'uses' => 'EventController@postUpdateEvent','as' => 'updateEvent']);

Route::get('events/interested/{eventId}', ['middleware' => 'auth', 'uses' => 'EventController@interestedEvent','as' => 'event_interested']);

Route::get('api/bid/details/{id}', ['middleware' => 'auth', 'uses' => 'EventController@apiBidDetails','as' => 'api_bid_details']);


/*
|-------------------
|	Pdf and Word Routes
|-------------------
*/

Route::post('download/artist-profil/pdf/{id}/{data?}/{socialData?}','ArtistController@downloadPdf')->name('download.pdf');
Route::post('download/checked/pdf/{data?}','ArtistController@downloadCheckedPdf')->name('dowload.checked.pdf');
/*
|-------------------	
|	Account Routes
|-------------------
*/

Route::get('/artist/edit/{id}',  ['middleware' => 'auth', 'uses'=>'ArtistController@editArtist', 'as'=>'editartist']);
Route::post('/artist/save/{id}', ['middleware' => 'auth', 'uses'=>'ArtistController@editArtist', 'as'=>'editartist_post']);

Route::get('account/{id}/edit', ['middleware' => 'auth', 'uses'=>'UserController@getProfile', 'as'=>'edit_account']);
Route::get('api/account/{id}', ['middleware' => 'auth', 'uses'=>'UserController@apiProfileAction', 'as'=>'api_account']);
Route::get('/account/forgot', ['uses'=>'UserController@forgotPassword', 'as'=>'account_forgot']);
Route::get('/account/reset/{secretKey}',['uses'=>'UserController@setNewPassword', 'as'=>'reset_password_with_key']);
Route::get('/account/password/changed', ['uses'=>'UserController@passwordChanged', 'as'=> 'password_changed']);
Route::post('/account/send/email', ['uses'=>'UserController@resetPasswordByEmail', 'as'=>'reset_password_by_email']);
Route::get('/account/send/email', ['uses'=>'UserController@sentEmail', 'as'=>'sent_email']);
Route::post('/account/new/password', ['uses'=>'UserController@saveNewPassword', 'as'=>'save_new_password']);
Route::post('/artist/videos/{artistId}','ArtistController@addVideos');
//Route::post('/artist/video/remove/{id}','ArtistController@removeVideo');
Route::post('edit/user', ['middleware' => 'auth', 'uses'=>'UserController@postEditUser', 'as'=>'edituser']);

/*
|-------------------
|	Cpanel Routes
|-------------------
*/

Route::get('cpanel/users', ['middleware' => 'auth', 'uses'=>'CpanelUsersController@index', 'as'=>'cpanel_users_index']);
Route::get('cpanel/api/planners', ['middleware' => 'auth', 'uses'=>'Cpanel\CpanelArtistBidController@apiGetPlanners', 'as'=>'cpanel_get_planners']);
Route::get('cpanel/users/edit/{id}', ['middleware' => 'auth', 'uses'=>'CpanelUsersController@edit', 'as'=>'cpanel_users_edit']);
Route::get('cpanel/account/create/{role}', ['middleware' => 'auth', 'uses'=>'CpanelUsersController@accountCreate', 'as'=>'cpanel_account_create']);
Route::post('cpanel/account/create/{role}', ['middleware' => 'auth', 'uses'=>'CpanelUsersController@accountCreate', 'as'=>'cpanel_account_create_save']);
Route::get('cpanel/account/delete/{id}', ['middleware' => 'auth', 'uses'=>'CpanelUsersController@accountDelete', 'as'=>'cpanel_account_delete']);
Route::get('cpanel/artists/json', ['middleware' => 'auth', 'uses'=>'CpanelArtistsController@allArtistsJson', 'as'=>'cpanel_all_artists_json']);
Route::post('/cpanel/api/account/delete/{id}', ['middleware' => 'auth', 'uses'=>'CpanelUsersController@apiDelete', 'as'=>'api_account_delete']);

Route::get('cpanel/pendingbids', ['middleware' => 'auth', 'uses'=>'Cpanel\CpanelArtistBidController@indexAction', 'as'=>'cpanel_pendingbids']);

Route::get('cpanel/api/pendingbids/{eventId}', ['middleware' => 'auth', 'uses'=>'Cpanel\CpanelArtistBidController@apiPendingBidsAction', 'as'=>'cpanel_api_pendingbids']);
Route::get('cpanel/api/pendingbid/{bidId}', ['middleware' => 'auth', 'uses'=>'Cpanel\CpanelArtistBidController@apiPendingBidAction', 'as'=>'cpanel_api_pendingbid']);
Route::post('cpanel/api/pendingbid/price', ['middleware' => 'auth', 'uses'=>'Cpanel\CpanelArtistBidController@apiSetPendingBidPriceAction', 'as'=>'cpanel_api_pendingbid_price']);
Route::post('cpanel/api/pendingbid/artistfee', ['middleware' => 'auth', 'uses'=>'Cpanel\CpanelArtistBidController@apiSetPendingBidArtistFeeAction', 'as'=>'cpanel_api_pendingbid_artistfee']);
Route::post('cpanel/api/pendingbid/notifyplaner', ['middleware' => 'auth', 'uses'=>'Cpanel\CpanelArtistBidController@apiSendNotificationsAboutEventBidsAction', 'as'=>'cpanel_api_pendingbid_notifyplaner']);

Route::get('manager/artists', ['middleware' => 'auth', 'uses'=>'ArtistManagerController@artistsAction', 'as'=>'manager_artists']);

/*
|-------------------
|	Payment Routes
|-------------------
*/

Route::post('forward/pay/{bidId}/{mail}',['middleware' => 'auth', 'uses'=>'PaymentController@sendForwardPay', 'as'=>'forward.payment']);
Route::get  ('forward/pay/to-mail/{data}',[ 'uses'=>'PaymentController@forwardPay', 'as'=>'forwardPay']);
Route::get('pay/bid/{id}', ['middleware' => 'auth', 'uses'=>'PaymentController@payBidAction', 'as'=>'pay_bid']);
Route::post('api/pay/bid', ['middleware' => 'auth', 'uses'=>'PaymentController@apiPayBidAction', 'as'=>'api_pay_bid']);
Route::post('api/contract/confirm', ['middleware' => 'auth', 'uses'=>'PaymentController@apiConfirmContractAction', 'as'=>'api_confirm_contract']);


Route::post('api/contract/confirm/forward', ['uses'=>'PaymentController@apiForwardConfirmContractAction', 'as'=>'api_forward_confirm_contract']);
Route::post('api/forward/pay/bid', ['uses'=>'PaymentController@apiForwardPayBidAction', 'as'=>'api_forward_pay_bid']);

/*
|-------------------
|	Artists Payments
|-------------------
*/
Route::get('cpanel/payments', ['middleware' => 'auth', 'uses'=>'Cpanel\CpanelArtistPaymentController@index', 'as'=>'cpanel_payments_page']);
Route::get('cpanel/api/payments/{period}', ['middleware' => 'auth', 'uses'=>'Cpanel\CpanelArtistPaymentController@apiArtistPaymentBids', 'as'=>'cpanel_api_artist_payment_bids']);
Route::get('cpanel/api/payments/bid/{id}', ['middleware' => 'auth', 'uses'=>'Cpanel\CpanelArtistPaymentController@apiArtistPaymentBid', 'as'=>'cpanel_api_artist_payment_bid']);
Route::post('cpanel/api/payments/amount', ['middleware' => 'auth', 'uses'=>'Cpanel\CpanelArtistPaymentController@apiArtistPaymentAmount', 'as'=>'cpanel_api_artist_payment_amount']);
Route::post('cpanel/api/payments/date', ['middleware' => 'auth', 'uses'=>'Cpanel\CpanelArtistPaymentController@apiArtistPaymentDate', 'as'=>'cpanel_api_artist_payment_date']);
Route::post('cpanel/api/payments/method', ['middleware' => 'auth', 'uses'=>'Cpanel\CpanelArtistPaymentController@apiBidPaymentMethod', 'as'=>'cpanel_api_bid_payment_method']);
Route::post('cpanel/api/payments/note', ['middleware' => 'auth', 'uses'=>'Cpanel\CpanelArtistPaymentController@apiPaymentNote', 'as'=>'cpanel_api_bid_payment_note']);

/*
|-------------------
|	Event Comments
|-------------------
*/

Route::get('api/comment/{id}', ['middleware' => 'auth', 'uses'=>'CommentController@apiComment', 'as'=>'api_comment']);
Route::post('api/comment/create', ['middleware' => 'auth', 'uses'=>'CommentController@apiCreate', 'as'=>'api_create_comment']);
Route::post('api/comment/update', ['middleware' => 'auth', 'uses'=>'CommentController@apiUpdate', 'as'=>'api_update_comment']);
Route::post('api/comment/delete', ['middleware' => 'auth', 'uses'=>'CommentController@apiDelete', 'as'=>'api_delete_comment']);
Route::post('api/files/delete/{id}', ['middleware' => 'auth', 'uses'=>'CommentController@apiDeleteFile', 'as'=>'api_delete_file']);
Route::get('api/comments/{eventId}', ['middleware' => 'auth', 'uses'=>'CommentController@apiComments', 'as'=>'api_comments']);

/*
|-------------------
|	Files
|-------------------
*/

Route::post('api/file/upload', ['middleware' => ['checkFileSize', 'auth'], 'uses'=>'FileController@apiUpload', 'as'=>'api_file_upload']);
Route::get('file/{id}', ['middleware' => 'auth', 'uses'=>'FileController@download', 'as'=>'file_download']);
Route::get('api/file/get-list/{ids}', ['middleware' => 'auth', 'uses'=>'FileController@getFileData', 'as'=>'file_get_file_data']);

/*
|-------------------
|       Redirects
|-------------------
*/

Route::get('/events', [function () {
        return redirect()->route('home_events');
}]);
