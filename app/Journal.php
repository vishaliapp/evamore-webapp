<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 26.01.17
 * Time: 14:26
 */

namespace App;

use Illuminate\Database\Eloquent\Model;


class Journal extends Model {
    protected $table = 'journal';
    public $timestamps = false;
}