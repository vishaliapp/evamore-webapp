<?php

namespace App\Libraries;

use Illuminate\Filesystem\Filesystem;
use Illuminate\Http\Request;
use Illuminate\Routing\RouteCollection;
use Illuminate\Routing\UrlGenerator;
use Illuminate\Support\Str;

class CustomUrlGenerator extends UrlGenerator
{
    public function __construct(RouteCollection $routes, Request $request)
    {
        parent::__construct($routes, $request);
    }

    /**
     * Generate an asset path for the application.
     *
     * @param  string  $path
     * @param  bool    $secure
     * @param  bool    $addVersion
     * @return string
     */
    public function asset($path, $secure = NULL, $addVersion = true) {
        if ($this->isValidUrl($path)) {
            return $path;
        }

        // Once we get the root URL, we will check to see if it contains an index.php
        // file in the paths. If it does, we will remove it since it is not needed
        // for asset paths, but only for routes to endpoints in the application.
        $root = $this->getRootUrl($this->getScheme($secure));
        $url = $root;
        /**
         * Modified this file due to upgrade from 5.2 version to 5.8 laravel version
         * 
         * File : CustomUrlGenerator
         *        
         */

        $result = $this->removeIndex($root).'/'.trim($path, '/');

        /**
         * You can switch if off in the config file
         * config/assets.php
         */
        if ( config('assets.verEnabled') && $addVersion ) {
            $full_path = public_path( ltrim($path, "\\/") );
            $last_mod = @filemtime($full_path);
            $result .= "?$last_mod";
        }
        return $result;
    }
}