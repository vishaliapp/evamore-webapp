<?php

namespace App\Listeners;

use App\Events\contractGenerated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class contractGeneratedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  contractGenerated  $event
     * @return void
     */
    public function handle(contractGenerated $event)
    {
        $bidContract = $event->model;
    }
}
