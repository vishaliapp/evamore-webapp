<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 26.12.16
 * Time: 10:25
 */

namespace App\ModelType;


class BidType {

    const ARTIST_TYPE = 1;
    const PLANER_TYPE = 2;

    const STATUS_NEW = 1;
    const STATUS_ACCEPTED = 2;
    const STATUS_DECLINED = 3;
    const STATUS_CANCELLED_BY_SYSTEM = 4;
    const STATUS_OTHER_BID_SELECTED = 7;
    const STATUS_SELECTED = 8;
    /**
     * Status set for after first payment(for full or part payment)
     */
    const STATUS_CONFIRMED = 5;
    const STATUS_ARTIST_PAID = 6; //Set by admin after manual pay money to artist

    // Available payment amounts
    const HALF_AMOUNT = 1;
    const FULL_AMOUNT = 2;

    const AUTOMATED_PAYMENT_OFF = 0;
    const AUTOMATED_PAYMENT_ON = 1;

    // Available payment methods
    const PAYMENT_METHOD_PAYSCAPE = 1;
    const PAYMENT_METHOD_ALTERNATIVE = 2;

    private static $statusNames = [
        self::STATUS_NEW => "New",
        self::STATUS_ACCEPTED => "Accepted",
        self::STATUS_DECLINED => "Declined",
        self::STATUS_CONFIRMED => "Confirmed",
        self::STATUS_CANCELLED_BY_SYSTEM => "Cancelled by system",
        self::STATUS_OTHER_BID_SELECTED => "Other Bid Selected",
        self::STATUS_SELECTED => "Bid Selected"
    ];

    public static function getStatusName($status) {
        if (array_key_exists($status, self::$statusNames))
            return self::$statusNames[$status];
        return "None status";
    }
}
