<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 30.01.17
 * Time: 10:16
 */

namespace App\ModelType;


class EventType {
    const PENDING = 0;
    const PUBLISHED = 1;
    const CANCELLED = 2;
    const BOOKED = 3;

    private static $statuses = [
        self::PENDING => "Pending",
        self::PUBLISHED => "Published",
        self::CANCELLED => "Cancelled",
        self::BOOKED => "Booked"
    ];

    public static function getStatusName($status) {
        return array_key_exists($status, self::$statuses) ? self::$statuses[$status] : "None";
    }
}
