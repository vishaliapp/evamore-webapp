<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 26.01.17
 * Time: 14:37
 */

namespace App\ModelType;


class JournalType {
    // base actions
    const ACT_CREATE = 1;
    const ACT_UPDATE = 2;
    const ACT_DELETE = 3;
    const ACT_CANCEL = 4;
    const ACT_PUBLISH = 5;
    const ACT_BOOK = 6;

    // notify admins actions
    const ACT_NOTIFY_ADMIN_FOR_PAY = 101;

    //notify planers actions
    const ACT_NOTIFY_PLANER_NOT_PAID_1W = 201;  // 1 week before deadline
    const ACT_NOTIFY_PLANER_NOT_PAID_24H = 202;  // 24 hours before deadline
    const ACT_NOTIFY_PLANER_NOT_PAID_12H = 203;  // 12 hours before deadline
    const ACT_NOTIFY_PLANER_NOT_PAID_6H = 204;  // 6 hours before deadline
    const ACT_NOTIFY_PLANER_NOT_PAID_DEADLINE = 205;  // not paid before deadline

    const ACT_NOTIFY_ARTIST_ABOUT_GENRE_EVENT_48H = 301; //

    private static $notPaidActions = [
        self::ACT_NOTIFY_PLANER_NOT_PAID_1W,
        self::ACT_NOTIFY_PLANER_NOT_PAID_24H,
        self::ACT_NOTIFY_PLANER_NOT_PAID_12H,
        self::ACT_NOTIFY_PLANER_NOT_PAID_6H,
        self::ACT_NOTIFY_PLANER_NOT_PAID_DEADLINE
    ];

    public static function getNotPaidActions() {
        return self::$notPaidActions;
    }

}
