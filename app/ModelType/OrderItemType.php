<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 17.01.17
 * Time: 12:07
 */

namespace App\ModelType;


class OrderItemType {
    const PENDING_SETTLEMENT = 1;
    const AUTHORIZED = 2;
    const SETTLED = 3;
}