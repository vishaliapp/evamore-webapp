<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 25.01.17
 * Time: 15:10
 */

namespace App\ModelType;


class OrderType {
    const NEW_ORDER = 0;
    const HALF_PAID = 1;
    const FULL_PAID = 2;
}