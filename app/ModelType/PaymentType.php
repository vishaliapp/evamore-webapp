<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 10.02.17
 * Time: 11:51
 */

namespace App\ModelType;


class PaymentType {
    const PAY_BY_VAULT = 1;
    const PAY_BY_CARD = 2;
}