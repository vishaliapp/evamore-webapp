<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 10.02.17
 * Time: 15:29
 */

namespace App\ModelType;


class TimeZoneType {

    const EASTERN = 1;
    const CENTRAL = 2;
    const MOUNTAIN = 3;
    const MOUNTAIN_NO_DST = 4;
    const PACIFIC = 5;
    const ALASKA = 6;
    const HAWAII = 7;
    const HAWAII_NO_DST = 8;

    public static $zones = [
        self::CENTRAL => "America/Chicago",
        self::EASTERN => "America/New_York",
        self::MOUNTAIN => "America/Denver",
        self::MOUNTAIN_NO_DST => "America/Phoenix",
        self::PACIFIC => "America/Los_Angeles",
        self::ALASKA => "America/Anchorage",
        self::HAWAII => "America/Adak",
        self::HAWAII_NO_DST => "Pacific/Honolulu"
    ];

    public static $zonesTitles = [
        self::CENTRAL => "Central",
        self::EASTERN => "Eastern",
        self::MOUNTAIN => "Mountain",
        self::MOUNTAIN_NO_DST => "Mountain no DST",
        self::PACIFIC => "Pacific",
        self::ALASKA => "Alaska",
        self::HAWAII => "Hawaii",
        self::HAWAII_NO_DST => "Hawaii no DST"
    ];

    public static function getTimeZones() {
        return self::$zones;
    }

    public static function getTimeZonesTitles() {
        return self::$zones;
    }

    public static function getTimeZone($id) {
        return array_key_exists($id, self::$zones) ? self::$zones[$id] : null;
    }

    public static function getTimeZoneTitle($id) {
        return array_key_exists($id, self::$zonesTitles) ? self::$zonesTitles[$id] : null;
    }

}