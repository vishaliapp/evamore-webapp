<?php

namespace App\Observers;


use Advisantgroup;
use App\Subscriber;

class SubscriberObserver {

  public function created(Subscriber $subscriber) {
    global $app;
    /** @var \Advisantgroup\Emma $emma */
    $emma = $app->make(Advisantgroup\Emma::class);
    $emma->membersSignup([
      'email' => $subscriber['email'],
      'group_ids' => [
        config('emma.groups.newsletter'),
      ],
      'opt_in_confirmation' => true
    ]);
  }

}