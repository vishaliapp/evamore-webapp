<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 11.01.17
 * Time: 11:18
 */

namespace App;

use Illuminate\Database\Eloquent\Model;


class Order extends Model {
    protected $table = 'order';
    public $timestamps = false;

    public function items() {
        return $this->hasMany('App\OrderItem');
    }

    public function bid() {
        return $this->belongsTo('App\Bid', 'bid_id');
    }
}