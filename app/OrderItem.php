<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 11.01.17
 * Time: 11:18
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model {
    protected $table = 'order_item';
    public $timestamps = false;

    public function order() {
        return $this->belongsTo('App\Order', 'order_id');
    }
}