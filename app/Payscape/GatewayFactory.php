<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 11.01.17
 * Time: 16:03
 */

namespace App\Payscape;


class GatewayFactory {

    public static function getPayscapeGateway() {
        $payscapeGateway = new PayscapeGateway();

        if (env("PAYSCAPE_USERNAME") && env("PAYSCAPE_PASSWORD")) {
            $payscapeGateway->setLogin(env("PAYSCAPE_USERNAME"), env("PAYSCAPE_PASSWORD"));
        }
        return $payscapeGateway;
    }
}