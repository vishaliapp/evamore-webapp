<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 03.02.17
 * Time: 13:35
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

class PayscapeVault extends Model {
    protected $table = 'payscape_vault';
    public $timestamps = false;
}