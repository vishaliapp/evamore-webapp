<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlanerRequest extends Model {

    protected $table = 'planer_request';
    public $timestamps = false;

}
