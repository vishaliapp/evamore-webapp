<?php

namespace App\Providers;

use App\Libraries\CustomUrlGenerator;
use Illuminate\Queue\Events\JobFailed;
use Queue;
use Illuminate\Support\Facades\DB;
use Validator;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Input;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Queue::failing(function (JobFailed $event) {

        });
        Blade::directive('currentUserEventsCount', function () {
            return '<?php
                $evSer = App::make("eventsService");
                $user = Auth::user();

                if ($user) {
                    echo $evSer->getCountEvents($user->id);
                } else {
                    echo false;
                }
            ?>';
        });

        Blade::directive('upcomingEventsCount', function () {
            return '<?php
                $evSer = App::make("eventsService");
                echo $evSer->getUpcomingEventsCount();
            ?>';
        });

        Blade::directive('pastEvents', function () {
            return '<?php
                $evSer = App::make("eventsService");
                $count = $evSer->getCountUserPastEvents(Auth::user()->id);
                if ($count > 0) :
            ?>';
        });

        Blade::directive('endPastEvents', function () {
            return '<?php endif; ?>';
        });

        //==============
        Validator::extend('greater_than', function($attribute, $value, $parameters, $validator) {
            $other = Input::get($parameters[0]);
            return intval($value) > intval($other);

        });

        Validator::replacer('greater_than', function($message, $attribute, $rule, $params) {
            return str_replace('_', ' ' , 'The '. $attribute .' must be greater than the ' .$params[0]);
        });

        //==============
        Validator::extend('emailDublicateValidator', function($attribute, $value, $parameters, $validator) {
            $qb = DB::table('users')
                ->where('email', $value);

            if (count($parameters) == 1){
                $qb->where('id', '!=', $parameters[0] );
            }

            $c= $qb->count();

            return $c == 0;
        });

        Validator::replacer('emailDublicateValidator', function($message, $attribute, $rule, $parameters) {
            return "This email address already exist in database.";
        });

        //==============

        Validator::extend('future_date', function($attribute, $value, $parameters, $validator) {
            $now = new \DateTime();
            $now->setTime(0,0,0);

            $date = new \DateTime($value);

            return $date >= $now;
        });

        Validator::replacer('future_date', function($message, $attribute, $rule, $params) {
            return "You can't set the date earlier than the current date.";
        });

        Validator::extend('siteUrlValidator', function($attribute, $value, $parameters, $validator) {


            if (!preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i",$value)) {
                return false;
            }

            return true;
        });

        Validator::replacer('siteUrlValidator', function($message, $attribute, $rule, $params) {
            return "Invalid site URL. It should start with 'http://' or 'https://' prefix.";
        });

        /**
         * Bid Model Events
         */
        \App\Bid::saving(function($bid) {
            return \App\Bid::bidSaving($bid);
        });
    }
}
