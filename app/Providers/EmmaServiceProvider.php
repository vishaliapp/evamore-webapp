<?php

namespace App\Providers;

use App\Observers\SubscriberObserver;
use App\Subscriber;
use Illuminate\Support\ServiceProvider;
use Advisantgroup;

class EmmaServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        Subscriber::observe(SubscriberObserver::class);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(Advisantgroup\Emma::class, function($app) {
          return new Advisantgroup\Emma(
            config('emma.account_id'),
            config('emma.public_key'),
            config('emma.private_key')
          );
        });
    }

  /**
   * Get the services provided by the provider.
   *
   * @return array
   */
  public function provides()
  {
    return [Advisantgroup\Emma::class];
  }

}
