<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 12.01.17
 * Time: 9:59
 */

namespace App\Repositories;

use App\Artist;
use App\Bid;
use App\Console\Commands\test_email;
use App\User;
use App\Event;
use App\ModelType\BidType;
use DB;

class BidRepository {

    public function getBid($id) {
        return Bid::find($id);
    }

    public function saveBid(Bid $bid) {
        $bid->save();
    }

    public function getBidsByEvent(Event $event, $type=null) {

        $qb = Bid::where("event_id", "=", $event->id)
            ->where('status', '<>', 7);
        if (!is_null($type)) {
            $qb->where("type", "=", $type);
        }

        return $qb->get();
    }


    public function getCountArtistBidsForEvent(Event $event, User $artistUser=null) {
        $qb = DB::table('bid')
            ->where('event_id', $event->id)
            ->where("type", "=", BidType::ARTIST_TYPE);


        if (!is_null($artistUser)) {
            $qb->where('artist_user_id', $artistUser->id);
        }

        return $qb->count() >0;
    }

    /**
     * Return bid to event sent by artist
     * @param Event $event
     * @param User $artistUser
     * @return mixed
     */
    public function getArtistBidToEvent(Event $event, User $artistUser) {
        $qb = Bid::where('event_id', $event->id)
            ->where("type", "=", BidType::ARTIST_TYPE)
            ->where('artist_user_id', $artistUser->id);
        return $qb->first();
    }

    /**
     * Return list of bis sent by artists
     * @param Event $event
     * @return mixed
     */
    public function getArtistsBidsToEvent(Event $event) {
        $qb = Bid::where('event_id', $event->id)
            ->where("type", "=", BidType::ARTIST_TYPE)
            ->where("price", ">", 0)
            ->where('status', '<>', 7);
        return $qb->get();
    }

    /**
     * @param Event $event
     * @return mixed
     */
    public function getSelectedArtistsBidsToEvent($event) {
        $qb = Bid::where('event_id', $event->id)
            ->where("type", ">", 0)
            ->where("price", ">=", 0)
            ->where('status', '=', 1);
        return $qb->get();
    }

    /**
     *
     * @param Event $event
     * @param $managerId
     * @return mixed
     */
    public function getArtistManagerArtistsToEvent(Event $event , $managerId)
    {
        $qb = Bid::where('event_id', $event->id)
            ->where("type", "=", BidType::ARTIST_TYPE)
            ->where("price", ">", 0)
            ->whereHas('artists', function ($q) use ($managerId) {
                $q->where('manager_user_id', $managerId);
            })
            ->where('status', '<>', 7)
            ->with('artists');
        return $qb->get();
    }

    /**
     * Return array of events with count artists requests
     * @param array $eventIds
     * @return array
     */
    public function getCountBidsToEvents($eventIds = []) {
        $res = [];
        if(count($eventIds) > 0){
            $res = DB::table("bid")
                ->select("event_id", DB::raw('count(*) AS count_bids'))
                ->whereIn('event_id', $eventIds)
                ->groupBy("event_id")
                ->get();
        }
        return $res;
    }

    //===== Planer Method

    /**
     * Return bid to event sent by planer
     * @param Event $event
     * @param User $artistUser
     * @return
     */
    public function getPlanerBidToEvent(Event $event, User $artistUser) {
        $qb = Bid::where('event_id', $event->id)
            ->where("type", "=", BidType::PLANER_TYPE)
            ->where('artist_user_id', $artistUser->id);
        return $qb->first();
    }

    /**
     * Return bid to event sent by planer
     * @param Event $event
     * @param User $artistUser
     * @return
     */
    public function getPlanerBidsToEvent(Event $event) {
        $qb = Bid::where('event_id', $event->id)
            ->where("type", "=", BidType::PLANER_TYPE)
            ->where("price", ">", 0);
        return $qb->get();
    }

    /**
     * Return list of bids where planer invite artist on create new event
     * @param Event $event
     * @return mixed
     */
    public function getPlanerBidsToNewEvent(Event $event) {
        $qb = Bid::where('event_id', $event->id)
            ->where("type", "=", BidType::PLANER_TYPE)
            ->where("is_new_event", "=", 1);
        return $qb->get();
    }

    //=====

    private function getBidsForGridQueryBuilder() {
        return DB::table("bid as b")
            ->select("b.*", "b.artist_fee as bid_artist_fee",
                "e.min_budget as min_budget", "e.max_budget as max_budget", "e.name as event_name",
                "art.fee as artist_fee",
                "u.name as artist_name", "u.email as artist_email", "u.phone as artist_phone")
            ->join("events as e", "e.id", "=", "b.event_id")
            ->join("users as u", "u.id", "=", "b.artist_user_id")
            ->join("artists as art", "art.user_id", "=", "u.id");
    }

    public function getBidsForGrid($eventId) {
        $qb = $this->getBidsForGridQueryBuilder();
        $res = $qb->where('event_id', $eventId)->get();
        foreach ($res as $bid) {
            $bid->status = BidType::getStatusName($bid->status);
        }
        return $res;
    }

    public function getBidForGrid($bidId) {
        $qb = $this->getBidsForGridQueryBuilder();
        $res = $qb->where('b.id', $bidId)->first();
        $res->status = BidType::getStatusName($res->status);
        return $res;
    }

    /**
     * Return list of bids for artist's manager artists
     * @param User $user - user with ARTIST_MANAGER role
     * @param Event $event
     * @return
     */
    public function getBidsForArtistManager(User $user, Event $event) {
        $qb = Bid::where('event_id', $event->id)
            ->join('artists a', 'a.manager_user_id', "=", $user->id);
        return $qb->get();
    }

}