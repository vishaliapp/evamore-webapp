<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 13.04.17
 * Time: 10:24
 */

namespace App\Repositories;

use App\Artist;
use App\Comment;
use App\User;
use App\Event;
use DB;
use App;
use Illuminate\Support\Facades\Auth;

class CommentRepository
{

    public function getComment($id)
    {
        return Comment::find($id);
    }

    public function createComment(Event $event, User $user, $content)
    {
        $comment = new Comment();
        $comment->event_id = $event->id;
        $comment->user_id = $user->id;
        $comment->content = htmlspecialchars($content);
        $comment->save();
        return $comment;
    }

    public function getEventComments(Event $event, $afterId = null)
    {
        $currentUser = Auth::user();
        if ($currentUser->hasRole('ARTIST')) {
            //If Artist
            $qb = Comment::where("event_id", $event->id)->where(function ($query) use ($event, $currentUser) {
                $query->where("user_id", $event->user_id)
                    ->orWhere("user_id", $currentUser->id)
                    ->orWhere("user_id", $currentUser->artist->manager_user_id)
                    ->orderBy("created_at", "ASC");
            });
        } elseif ($currentUser->hasRole('FAN') || $currentUser->hasRole('ADMIN')) {
            //If Event planner
            $qb = Comment::where(function ($query) use ($event, $currentUser) {
                $query->where("event_id", $event->id)
                    ->orderBy("created_at", "ASC");
            });
        } elseif ($currentUser->hasRole('ARTIST_MANAGER')) {
            //If Artist-manager
            $artists = Artist::where('manager_user_id', $currentUser->id)->pluck('user_id');
            $qb = Comment::where("event_id", $event->id)->where(function ($query) use ($event, $currentUser, $artists) {
                $query->whereIn("user_id", $artists)
                    ->orWhere("user_id", $currentUser->id)
                    ->orWhere("user_id", $event->user_id)
                    ->orderBy("created_at", "ASC");
            });
        }
        if (!is_null($afterId)) {
            $qb->where('id', '>', $afterId);
        }

        return $qb->get();
    }
}