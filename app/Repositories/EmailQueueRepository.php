<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 13.01.17
 * Time: 8:56
 */

namespace App\Repositories;

use App\EmailQueue;

class EmailQueueRepository {

    public function createQueueItem($subject, $toEmail, $toName, $message, $attachment = null) {
        $q = new EmailQueue();
        $q->subject = $subject;
        $q->to_email = $toEmail;
        $q->to_name = $toName;
        $q->message = $message;
        $q->attachment = $attachment;
        $q->status = 0;
        $this->save($q);
        return $q;
    }

    public function save(EmailQueue $emailQueue) {
        $emailQueue->save();
    }


    public function getFromQueue($limit) {
        return EmailQueue::where('status', '=', 0)
            ->orderBy('created_at', 'desc')
            ->limit($limit)
            ->get();

    }


}