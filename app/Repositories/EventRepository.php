<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 23.12.16
 * Time: 9:13
 */

namespace App\Repositories;


use App\Bid;
use App\ModelType\BidType;
use App\User;
use App\Event;
use DB;

class EventRepository {

    public function getEvent($id) {
        return Event::with('artistSee')->find($id);
    }

    public function save(Event $event) {
        $event->save();
    }

    public function getUserEvents(User $user, $filter=[]) {
        $queryBuilder = Event::where('starttime', ">", date('Y-m-d H:i:s'))
            ->select('events.*')
            ->where('user_id', $user->id)
            ->orderBy('created_at', 'desc')
        ;

        if (array_key_exists("status", $filter)){
            If(is_array($filter['status'])){
                $queryBuilder->whereIn("status", $filter['status']);
            } else {
                $queryBuilder->where("status", $filter['status']);
            }
        }

        return $queryBuilder->get();
    }

    /**
     * return the list of events witch planner sent request to artist
     * @param User $planerUser
     * @param User $artistUser
     * @param bool $futureEvents
     * @return mixed
     */
    public function getPlanerEventsForArtist(User $planerUser, User $artistUser, $futureEvents=true) {
        $queryBuilder = DB::table('events as e')
            ->select('e.*')
            ->join('bid as b', 'b.event_id', '=', 'e.id')
            ->orderBy('e.created_at', 'desc')
            ->where('e.user_id', $planerUser->id)
            ->where("b.artist_user_id", "=", $artistUser->id)
            ->where("b.type", BidType::PLANER_TYPE)
        ;

        if ($futureEvents) {
            $queryBuilder->where('starttime', ">", date('Y-m-d H:i:s'));
        }
        return $queryBuilder->get();
    }
}
