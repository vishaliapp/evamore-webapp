<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 14.04.17
 * Time: 9:51
 */

namespace App\Repositories;

use App\File;
use App\User;

class FileRepository {

    public function getFile($id) {
        return File::find($id);
    }

    public function create(User $user, $entity, $entityId, $filePath, $fileName) {
        $file = new File();
        $file->user_id = $user->id;
        $file->entity = $entity;
        $file->entity_id = $entityId;
        $file->path = $filePath;
        $file->name = $fileName;
        $file->save();
        return $file;
    }


}