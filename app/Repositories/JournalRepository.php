<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 26.01.17
 * Time: 14:29
 */

namespace App\Repositories;

use App\Journal;

class JournalRepository {

    public function save(Journal $journal) {
        $journal->save();
    }
}