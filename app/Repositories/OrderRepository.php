<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 11.01.17
 * Time: 11:24
 */

namespace App\Repositories;

use App\Order;

class OrderRepository {

    public function getOrder($id) {
        return Order::find($id);
    }

    public function save(Order $order) {
        $order->save();
    }


    public function getOrderByBidId($id) {
        return Order::where("bid_id", "=", $id)->first();
    }
}