<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 23.12.16
 * Time: 8:51
 */

namespace App\Repositories;

use App\Role;
use App\User;
use DB;

class UserRepository {
    public function getUser($id) {
        return User::find($id);
    }

    public function getUserByUsername($username) {
        return User::where("username", "=", $username)->first();
    }

    public function getUsersByRole(Role $role) {
        return DB::table('users')->select("users.*")
            ->join('user_role', 'users.id', '=', 'user_role.user_id')
            ->where('user_role.role_id', '=', $role->id)
            ->get();
    }

}