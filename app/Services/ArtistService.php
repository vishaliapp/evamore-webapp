<?php
/**
 * Created by PhpStorm.
 * User: dasha
 * Date: 23.08.16
 * Time: 11:21
 */

namespace App\Services;

use Illuminate\Support\Facades\DB;

use App\User;

class ArtistService
{

    /**
     * Get active Artists by their Genres
     * @param array $genreIds
     * @return array
     */
    public function getArtists($genreIds = [])
    {
        if (count($genreIds) != 0) {
            return DB::table('artists')
                ->select('artists.*')
                ->join('artist_genre', 'artists.id', '=', 'artist_genre.artist_id')
                ->join('users', 'users.id', '=', 'artists.user_id')
                ->whereIn('artist_genre.genre_id', $genreIds)
                ->where('users.active', 1)
                ->distinct()->get();
        } else
            return [];
    }

    public function getUserGenres($userId){
        $qb = DB::table('genres')
            ->select('genres.*')
            ->join('artist_genre', 'genres.id', '=', 'artist_genre.genre_id')
            ->join('artists', 'artists.id', '=', 'artist_genre.artist_id')

            ->where('artists.user_id', $userId)
        ;

        return $qb->get();
    }

    /**
     * Return list of User objects for manager's artists
     * @param User $artistManager
     * @param $page
     * @param $perPage
     */
    public function getArtistManagerArtists (User $artistManager, $perPage = null) {
        $qb = User::select("users.*")
            ->where("a.manager_user_id", $artistManager->id)
            ->join("artists as a", "a.user_id", "=", "users.id")
            ->orderBy("users.id", "DESC");;

        if (!is_null($perPage)) {
            return $qb->paginate($perPage);
        }

        return $qb->get();
    }

    public static function getManagerByArtist($artistUser)
    {
        if ($artistUser instanceof \App\Artist) {
            return $artistUser->manager;
        } else if ($artistUser instanceof \App\User) {
            return $artistUser->artist->manager;
        } else {
            if ($artistUser->id) {
                $artistUserModel = \App\Artist::find($artistUser->id);
                return $artistUserModel->manager;
            } else {
                return new \Exception('Artist User Error. ');
            }
        }
    }

    public static function getFeaturedArtists()
    {
        $featured_artists_set = \App\Artist::active()->select([
            'artists.id',
            'users.id as user_id',
            'users.name',
            'users.avatar',
        ]);
        $featured_artists_set->inRandomOrder()->limit(8);
        $result = $featured_artists_set->orderBy('users.id')->get();

        return $result;
    }
}
