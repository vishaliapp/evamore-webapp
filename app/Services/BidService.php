<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 12.01.17
 * Time: 10:00
 */

namespace App\Services;

use DB;
use App;
use App\Bid;
use App\BidContract;
use App\ModelType\BidType;
use App\ModelType\JournalType;
use App\ModelType\OrderType;
use App\Event;
use App\User;

class BidService
{
    private $bidRepository;
    private $emailService;

    public function __construct()
    {
        $this->bidRepository = App::make("bidRepository");
        $this->emailService = App::make('emailNotificationService');
    }

    public function getBid($id)
    {
        return $this->bidRepository->getBid($id);
    }

    public function getBidsByEvent($event, $type = null)
    {
        return $this->bidRepository->getBidsByEvent($event, $type);
    }

    public function hasArtistBidsForEvent(Event $event, $artistUser)
    {
        $count = $this->bidRepository->getCountArtistBidsForEvent($event, $artistUser);

        return ($count > 0) ? TRUE : FALSE;
    }

    /**
     * Return bid for event sent by artist
     * @param $eventId
     * @param $userId
     * @return null
     */
    public function getArtistBidToEvent(Event $event, User $artistUser)
    {
        return $this->bidRepository->getArtistBidToEvent($event, $artistUser);
    }

    public function getArtistManagerArtistsToEvent(Event $event ,$managerId)
    {
        return $this->bidRepository->getArtistManagerArtistsToEvent($event,$managerId);
    }


    /**
     * Return list of bis sent by artists
     * @param Event $event
     * @return mixed
     */
    public function getArtistsBidsToEvent(Event $event)
    {
        return $this->bidRepository->getArtistsBidsToEvent($event);
    }

    /**
     * Artists hwo just selected to event not applies
     * @param Event $event
     * @return mixed
     */
    public function getSelectedArtistsBidsToEvent(Event $event)
    {
        return $this->bidRepository->getSelectedArtistsBidsToEvent($event);
    }

    public function getArtistsHasBidsToEvent(Event $event, User $artistUser) {
        $qb = Bid::where('event_id', $event->id)
            ->where('artist_user_id', $artistUser->id)
            ->where('status', '<>', 1);
        return $qb->first();
    }

    public function getArtistsManagerArtistBidsToEvent(Event $event) {

        $qb = DB::table('bid')
            ->select('*')
            ->leftJoin('users', 'users.id', '=', 'bid.artist_user_id')
            ->where('event_id', '=', $event->id);
    return $qb->get();
}

    /**
     * @param array $eventIds
     * @return array
     */
    public function getCountBidsEvents($eventIds = [])
    {
        $counts = [];
        $objects = $this->bidRepository->getCountBidsToEvents($eventIds);
        foreach ($objects as $obj) {
            $counts[$obj->event_id] = $obj->count_bids;
        }
        return $counts;
    }

    public function createArtistBid(Event $event, $artistUser)
    {
        $bid = new Bid();
        $bid->type = BidType::ARTIST_TYPE;
        $bid->artist_user_id = $artistUser->id;
        $bid->event_id = $event->id;
        $bid->is_new_event = TRUE;
        $bid->status = BidType::STATUS_ACCEPTED;
        $bid->price = 0.00;
        $this->bidRepository->saveBid($bid);
        return $bid;
    }

    public function createPlanerBid(Event $event, $artistUser, $isNewEvent = FALSE)
    {
        $bid = new Bid();
        $bid->type = BidType::PLANER_TYPE;
        $bid->artist_user_id = $artistUser->id;
        $bid->event_id = $event->id;
        $bid->is_new_event = $isNewEvent;
        $bid->status = BidType::STATUS_NEW;
        $bid->price = 0.00;
        $this->bidRepository->saveBid($bid);
        return $bid;
    }

    public function getHalfPrice($price) {
      return round(($price/2),2);
    }
    //======= Planer Methods

    /**
     * Return bid to event where planer invite artist
     * @param Event $event
     * @param User $artistUser
     * @return mixed
     */
    public function getPlanerBidToEvent(Event $event, User $artistUser)
    {
        return $this->bidRepository->getPlanerBidToEvent($event, $artistUser);
    }

    /**
     * Return list of bids to event where planer invite artists
     * @param Event $event
     * @param User $artistUser
     * @return mixed
     */
    public function getPlanerBidsToEvent(Event $event)
    {
        return $this->bidRepository->getPlanerBidsToEvent($event);
    }

    /**
     * Return list of bids where planer invite artist on create new event
     * @param Event $event
     * @return mixed
     */
    public function getPlanerBidsToNewEvent(Event $event)
    {
        return $this->bidRepository->getPlanerBidsToNewEvent($event);
    }


    //===============

    public function setBidPrice(Bid $bid, $new_price)
    {
        $bid->price = $new_price;
        $this->bidRepository->saveBid($bid);
    }

    public function setBidArtistFee(Bid $bid, $new_artist_fee)
    {
        $bid->artist_fee = $new_artist_fee;
        $this->bidRepository->saveBid($bid);
    }

    public function sendNotificationAboutEventBidsPrices(Event $event, $bids = [])
    {
        $this->emailService->notifyPlanerAboutBidPrices($event, $bids);
    }

    /**
     * Set date of paid to artist by admin manually
     * @param Bid $bid
     * @param $amount
     */
    public function setBidPaidArtistDate(Bid $bid, \DateTime $date)
    {
        $bid->artist_paid_at = $date->format("Y-m-d");
        $bid->status = BidType::STATUS_ARTIST_PAID;
        $this->bidRepository->saveBid($bid);
    }

    /**
     * Set method of payment by planer
     * @param Bid $bid
     * @param $amount
     */
    public function setBidPaidMethod(Bid $bid, $method)
    {
        $bid->payment_method = $method;
        $this->bidRepository->saveBid($bid);
    }

    public function setBidNote(Bid $bid, $note)
    {
        $bid->note = $note;
        $this->bidRepository->saveBid($bid);
    }


    public function getBidsForGrid($eventId)
    {
        $rows = $this->bidRepository->getBidsForGrid($eventId);
        return $rows;
    }

    public function getBidForGrid($requestId)
    {
        $rows = $this->bidRepository->getBidForGrid($requestId);
        return $rows;
    }

    public function setStatus(Bid $bid, $status)
    {
        $bid->status = $status;
        $this->bidRepository->saveBid($bid);
    }

    public function acceptBidByArtist(Bid $bid, $notify = TRUE)
    {
        $this->setStatus($bid, BidType::STATUS_ACCEPTED);

        if ($notify) {
            $this->emailService->notifyAdminsAboutArtistRequestNewEvent($bid->event, $bid->artistUser);
        }
    }

    public function declineBidByArtist(Bid $bid, $declineReasonCode, $declineReasonText = "")
    {
        $this->setStatus($bid, BidType::STATUS_DECLINED);

        $userService = App::make('userService');
        $planerUser = $userService->getUser($bid->event->user_id);

        switch ($declineReasonCode) {
            case 1:
                $reasonDeclineMessage = "The budget is too low";
                break;
            case 2:
                $reasonDeclineMessage = "There is scheduling conflict";
                break;
            default:
                $reasonDeclineMessage = "Other. " . $declineReasonText;
                break;
        }

        if (in_array($declineReasonCode, [1, 2])) {
            if ($planerUser) {
                $this->emailService->notifyPlanerAboutDeclineRequest($planerUser, $bid->artistUser, $bid->event, $reasonDeclineMessage);
            }
        }

        $this->emailService->notifyAdminsAboutDeclineRequest($planerUser, $bid->artistUser, $bid->event, $reasonDeclineMessage);

        if ($bid->is_new_event) {
            // if event with pre-attached user Send email to all other Artists.
            $this->emailService->notifyArtistAboutNewEvent($bid->event, $planerUser, [$bid->artist_user_id]);
        }

    }

    public function cancelBidBySystem(Bid $bid)
    {
        $this->setStatus($bid, BidType::STATUS_CANCELLED_BY_SYSTEM);
    }
    public function otherBidSelected(Bid $bid)
    {
        $this->setStatus($bid, BidType::STATUS_OTHER_BID_SELECTED);
    }
    public function selectBidByPlaner(Bid $bid)
    {
        $this->setStatus($bid, BidType::STATUS_SELECTED);
    }
    public function declineOtherBids(Bid $bid, $notify = TRUE)
    {
        $event = $bid->event;
        //get all bids that are not this bids
        $bids = $this->getBidsByEvent($event);

        //loop through bids
        for ($i=0; $i < count($bids); $i++) {
          //check if not current bid
          if ($bids[$i]->id != $bid->id) {
            //cancel other bids
            $this->otherBidSelected($bids[$i]);
            //notify
            if ($notify) {
              $this->emailService->notifyArtistOtherBidSelected($bids[$i]->artist_user_id,$bids[$i]->event_id);
            }
          }
        }
    }

    public function confirmBidByPlaner(Bid $bid, $notify = TRUE)
    {
        $this->setStatus($bid, BidType::STATUS_CONFIRMED);

        if ($notify) {
            $this->emailService->notifyArtistAboutPaymentConfirmation($bid);
        }
    }


    public function getArtistBidsCounter($ids, $status = [])
    {
        $counts = Bid::select("artist_user_id", DB::raw('count(*) as bids_count'))
            ->whereIn("artist_user_id", $ids)
            ->join("events as e", "e.id", "=", "bid.event_id")
            ->whereIN("bid.status", $status)
            ->where("e.starttime", ">", DB::raw('NOW()'))
            ->groupBy("artist_user_id")
            ->get();

        $ret = [];
        foreach ($counts as $c) {
            $ret[$c->artist_user_id] = $c->bids_count;
        }
        return $ret;
    }

    /**
     *
     */
    public function getBidsForManualPaymentToArtist()
    {
        $sql = "
          select b.*
          from bid as b
          inner join `order` as o on o.bid_id = b.id
          inner join events as e on e.id = b.event_id
          left outer join journal
            on journal.entity_id = b.id
            and journal.entity = 'bid'
            and journal.action = :action
          where o.status = :order_status
          and b.status = :bid_status
          and e.status = :event_status
          and NOW() > DATE_ADD(e.starttime, INTERVAL e.duration MINUTE)
          and journal.id is NULL";

        $bids = DB::select($sql, [
            'action' => JournalType::ACT_NOTIFY_ADMIN_FOR_PAY,
            'order_status' => OrderType::FULL_PAID,
            'bid_status' => BidType::STATUS_CONFIRMED,
            'event_status' => App\ModelType\EventType::PUBLISHED
        ]);

        $ret = [];
        foreach ($bids as $bid) {
            $ret[] = new Bid(json_decode(json_encode($bid), TRUE));
        }
        return $ret;
    }

    public function getNotPaidFullBids($journalAction)
    {
        if (!in_array($journalAction, JournalType::getNotPaidActions())) {
            return FALSE;
        }

        $ret = [];

        $deadlineHours = 48;
        switch ($journalAction) {
            case JournalType::ACT_NOTIFY_PLANER_NOT_PAID_1W:
                $hoursInterval = $deadlineHours + 24 * 7;
                break;
            case JournalType::ACT_NOTIFY_PLANER_NOT_PAID_24H:
                $hoursInterval = $deadlineHours + 24;
                break;
            case JournalType::ACT_NOTIFY_PLANER_NOT_PAID_12H:
                $hoursInterval = $deadlineHours + 12;
                break;
            case JournalType::ACT_NOTIFY_PLANER_NOT_PAID_6H:
                $hoursInterval = $deadlineHours + 6;
                break;
            case JournalType::ACT_NOTIFY_PLANER_NOT_PAID_DEADLINE:
                $hoursInterval = $deadlineHours;
                break;
        }

        $sql = "
          select b.*
          from bid as b
          inner join `order` as o on o.bid_id = b.id
          inner join events as e on e.id = b.event_id
          left outer join journal
            on journal.entity_id = b.id
            and journal.entity = 'bid'
            and journal.action = :journal_action
          where (o.status = :order_status and b.status = :bid_status_1)
          AND e.status = :event_status
          and e.starttime > NOW()
          and NOW() > DATE_SUB(e.starttime, INTERVAL :hours_interval HOUR)
          and journal.id is NULL";

        if ($hoursInterval > 0) {
            $bids = DB::select($sql, [
                'journal_action' => $journalAction,
                'order_status' => OrderType::HALF_PAID,
                'bid_status_1' => BidType::STATUS_CONFIRMED,
                'event_status' => App\ModelType\EventType::PUBLISHED,
                'hours_interval' => $hoursInterval
            ]);

            foreach ($bids as $bid) {
                $ret[] = new Bid(json_decode(json_encode($bid), TRUE));
            }
        }

        return $ret;
    }

    public function getBids($filters = [], $sorts = [], $perPage)
    {

        $queryBuilder = Bid::select('bid.*');

        $queryBuilder->join('events as e', 'e.id', '=', 'bid.event_id');

        // Sorting events
        foreach ($sorts as $field => $value) {
            $queryBuilder->orderBy($field, $value);
        }

        // Filtering events
        if (array_key_exists("period", $filters)) {
            switch ($filters['period']) {
                case "upcoming":
                    $queryBuilder->where("e.starttime", ">=", DB::raw('NOW()'));
                    break;
                case "ended":
                    $queryBuilder->whereRaw("DATE_ADD(e.starttime, INTERVAL e.duration MINUTE) < NOW()");
                    break;
            }
        }

        if (array_key_exists("bid_status", $filters)) {
            if (is_array($filters['bid_status'])) {
                $queryBuilder->whereIn("bid.status", $filters['bid_status']);
            } else {
                $queryBuilder->where("bid.status", $filters['bid_status']);
            }
        }

        if (array_key_exists("artist_user_id", $filters)) {
            $queryBuilder->where("bid.artist_user_id", $filters['artist_user_id']);
        }

        if (array_key_exists("planer_user_id", $filters)) {
            $queryBuilder->where("e.user_id", $filters['planer_user_id']);
        }

        return $queryBuilder->paginate($perPage);
    }

    public function generatePaymentBidInfo($bid)
    {
        $userService = App::make("userService");
        $orderService = App::make("orderService");

        $oBid = new Bid(json_decode(json_encode($bid), TRUE));

        $planerUser = $userService->getUser($oBid->event->user_id);
        $bid->planerUser = [
            'id' => $planerUser->id,
            'name' => $planerUser->name,
            'email' => $planerUser->email,
            'phone' => $planerUser->phone
        ];

        $bid->artistUser = [
            'id' => $oBid->artistUser->id,
            'name' => $oBid->artistUser->name,
            'email' => $oBid->artistUser->email,
            'phone' => $oBid->artistUser->phone
        ];

        $bid->event = $oBid->event;
        $bid->payment_method = $oBid->payment_method;

        $bid->format_artist_paid_at = "";
        if (!is_null($bid->artist_paid_at)) {
            $artistPaidAt = new \DateTime($bid->artist_paid_at);
            $bid->format_artist_paid_at = $artistPaidAt->format("m/d/Y");
        }

        $order = $bid->getOrder();
        $bid->order_info = NULL;
        if ($order) {
            $bid->order_info = [
                "status" => $order->status,
                "paid_amount" => $orderService->getOrderPaidSum($order),
            ];
        }
        $bid->note = $oBid->note;
    }

    public function signContractByArtist(Bid $bid, $artistUserSignature)
    {
        $contract = $bid->contract;
        if (is_null($contract)) {
            $contract = $this->createContract($bid);
        }

        $contract->artist_signature = $artistUserSignature;
        $contract->save();
    }

    public function singContractByPlaner(Bid $bid, $request)
    {
        $time_data = $bid->event->splitStartDate();
        $planerUserSignature = $request->get('signature', '');
        $request_json = array_merge($time_data, $request->all());
        $request_json['created_at'] = time();
        $contract = $bid->contract;
        if (is_null($contract)) {
            $contract = $this->createContract($bid);
        }

        $contract->planer_signature = $planerUserSignature;
        $contract->request_json = $request_json;
        $contract->save();

        if (!$contract->file->count()) {
            //Generate contract for planer
            $contract->generateContract('planer');

            //Generate contract for artist
            $contract->generateContract('artist');

            //Generate contract for artist
        }
    }

    public function createContract(Bid $bid)
    {
        $contract = new BidContract();
        $contract->bid_id = $bid->id;
        $contract->save();
        return $contract;
    }

    /**
     * Return list of bids for artist's manager artists
     * @param User $user - user with ARTIST_MANAGER role
     * @param Event $event
     */
    public function getBidsForArtistManager(User $user, Event $event) {

    }
}
