<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 13.04.17
 * Time: 10:24
 */

namespace App\Services;

use App;
use App\Bid;
use App\Event;
use App\Comment;
use App\ModelType\BidType;
use App\ModelType\EventType;
use App\User;
use Faker\Provider\zh_CN\DateTime;


class CommentService {

    private $commentRepository;

    public function __construct() {
        $this->commentRepository = App::make("commentRepository");
    }

    public function getComment($id) {
        return $this->commentRepository->getComment($id);
    }

    public function createComment(Event $event, User $user, $content, $files=[]) {
        return $this->commentRepository->createComment($event, $user, $content);

    }

    public function getEventComments(Event $event, $afterId=null) {
        return $this->commentRepository->getEventComments($event, $afterId);
    }

    /**
     * @param User $user
     * @param Event $event
     * @param $action read | write
     */
    public function checkCommentsAccess(User $user, Event $event, $action, Comment $comment = null){

        //Admin
        if($user->hasRole("ADMIN")){
            return true;
        }

        $now = new \DateTime(); // UTC timezone
        $eventStartAt = new \DateTime($event->starttime);


        //Event owner
        if($user->id == $event->user_id) {
            switch ($action) {
                case "read": return true; break;
                case "write": if($event->status == EventType::CANCELLED) return false;
                              if($now->getTimestamp() > $eventStartAt->getTimestamp()) return false;
                              return true;
                              break;
            }
        }

        // Comment owner.
        if ($comment) {
          if ($action == "edit") {
            return (intval($comment->user_id) == intval($user->id)) ? true: false;
          }
        }

        //Artist
        if($user->hasRole("ARTIST")) {
            $artistBid = Bid::where('event_id', $event->id)
                ->where("artist_user_id", $user->id)
                ->first();

            if ($artistBid && $action == 'read' && $event->status == EventType::BOOKED || $event->status == EventType::PUBLISHED) {
                return true;
            }
            if ($artistBid && $action == 'write') {
                return false;
            }

            if ($artistBid && $now->getTimestamp() > $eventStartAt->getTimestamp() && $action == 'read') {
                return true;
            }

            if(!is_null($artistBid)) {
              //if(in_array($artistBid->status, [BidType::STATUS_ACCEPTED, BidType::STATUS_CONFIRMED, BidType::STATUS_ARTIST_PAID]) ) {
              // Allow write access only for paid artists.
              if(in_array($artistBid->status, [BidType::STATUS_CONFIRMED, BidType::STATUS_ARTIST_PAID]) ) {
                    return true;
                }
            }
        }

        if ($user->hasRole("ARTIST_MANAGER")) {
            $artistService = App::make("artistService");
            $managerArtists = $artistService->getArtistManagerArtists($user);

            foreach($managerArtists as $artist) {
                if($this->checkCommentsAccess($artist, $event, $action)) {
                    return true;
                }
            }
        }

        return false;

    }
}