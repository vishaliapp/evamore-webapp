<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 30.08.16
 * Time: 14:22
 */

namespace App\Services;


class EmailAvatarGeneratorService {
    function thumbnail($image, $new_w, $new_h, $focus = 'center')
    {
        $w = $image->getImageWidth();
        $h = $image->getImageHeight();

        if ($w > $h) {
            $resize_w = $w * $new_h / $h;
            $resize_h = $new_h;
        }
        else {
            $resize_w = $new_w;
            $resize_h = $h * $new_w / $w;
        }
        $image->resizeImage($resize_w, $resize_h, \Imagick::FILTER_LANCZOS, 0.9);

        switch ($focus) {
            case 'northwest':
                $image->cropImage($new_w, $new_h, 0, 0);
                break;

            case 'center':
                $image->cropImage($new_w, $new_h, ($resize_w - $new_w) / 2, ($resize_h - $new_h) / 2);
                break;

            case 'northeast':
                $image->cropImage($new_w, $new_h, $resize_w - $new_w, 0);
                break;

            case 'southwest':
                $image->cropImage($new_w, $new_h, 0, $resize_h - $new_h);
                break;

            case 'southeast':
                $image->cropImage($new_w, $new_h, $resize_w - $new_w, $resize_h - $new_h);
                break;
        }
    }

    public function generateEmailAvatar($originalImagePath, $destinationsImagePath, $bgColor = "white") {

        $resImage = new \Imagick();
        $resImage->newImage(250, 250, new \ImagickPixel('white'));
        $resImage->setImageFormat("png");

        $bg = new \Imagick();
        $bg->newImage(250, 125, new \ImagickPixel($bgColor));
        $resImage->compositeImage($bg, \Imagick::COMPOSITE_DEFAULT, 0, 0);


        $image = new \Imagick($originalImagePath);
        $image->setImageFormat("png");
        $this->thumbnail($image, 250, 250, 'center');
        if ( method_exists($image, 'roundCorners') ) {
            $image->roundCorners(125,125);
        }


        $resImage->compositeImage($image, \Imagick::COMPOSITE_DEFAULT, 0, 0);

        return $resImage->writeImage($destinationsImagePath);
    }
}