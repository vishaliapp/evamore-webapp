<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 15.08.16
 * Time: 12:09
 */

namespace App\Services;

use App;
use Mail;
use DB;
use App\User;
use App\Event;
use App\Bid;
use App\ModelType\JournalType;
use App\File;


class EmailNotificationService
{
    private $emailQueueService;
    private $userService;

    public function __construct()
    {
        $this->emailQueueService = App::make("emailQueueService");
        $this->userService = App::make('userService');
    }

    private function getEmailAvatarUrl($user, $bgColor = "white")
    {
        try {
            $originalImagePath = public_path('uploads/avatars/eva_profilepicture.png');
            $p = 'uploads/avatars/email_' . str_replace("#", '', $bgColor) . '_' . $user->avatar;
            $destinationsImagePath = public_path($p);
            $emailUserAvatarUrl = "/" . $p;
            $srv = App::make('emailAvatarGeneratorService');

            $srv->generateEmailAvatar($originalImagePath, $destinationsImagePath, $bgColor);
            return $emailUserAvatarUrl;
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function forwardPaymentToMail($bid, $event, $payerName, $mail, $token)
    {
        try {

            $data = [
                'token' => $token,
                'bid' => $bid,
                'domain' => env('APP_URL'),
                'event' => $event,
                'payerName' => $payerName,
                'artist' => $bid->artistUser,
                'planner' =>$event->user
            ];

            $message = 'emails.planer.forward_pay';
            $emailTo = $mail;
            $emailToName = 'bid planer name';
            if ($event->user->organization) {
                $subject = 'Event Payment Request from ' . $event->user->name . ' of ' . $event->user->organization . ' .';
            } else {
                $subject = 'Event Payment Request from ' . $event->user->name . ' .';
            }


            $this->emailQueueService->addEmailToQueue($subject, $emailTo, $emailToName, $message, $data);
        } catch (\Exception $e) {
            dd($e);
        }
    }


    /**
     * Send email to admins when new artist applies
     * @param $artist
     */

    public function newArtistApplies($artist)
    {

        $emailArtistAvatarUrl = $this->getEmailAvatarUrl($artist, "#30b5ba");

        $data = [
            'artist' => $artist,
            'domain' => env('APP_URL'),
            'emailArtistAvatarUrl' => $emailArtistAvatarUrl
        ];

        $userService = App::make('userService');
        $admins = $userService->getAdmins();

        $message = 'emails.admin.new_artist';

        foreach ($admins as $admin) {
            $emailTo = $admin->email;
            $emailToName = $admin->username;

            $this->emailQueueService->addEmailToQueue('New artist', $emailTo, $emailToName, $message, $data);
        }

        // Send email to Artist.
        $message = 'emails.artist.new_artist';
        $emailTo = $artist->email;
        $emailToName = $artist->username;
        $this->emailQueueService->addEmailToQueue("Your EVAmore application has been submitted!", $emailTo, $emailToName, $message, $data);
        /**
         * Notify Manager.
         * Text and Name are the same.
         * Only email was changed.
         */
        $manager = ArtistService::getManagerByArtist($artist);
        if ($manager) {
            $m = $manager;
            $this->emailQueueService->addEmailToQueue("Your EVAmore application has been submitted!", $m->email, $emailToName, $message, $data);
        }
    }

    /**
     * Send email to admins and to p[laner when new planer signup
     * @param $planer
     */
    public function newPlanerSignUp($planer)
    {
        // Reload user for attach avatar.
        $planer = User::find($planer->id);

        $emailPlannerAvatarUrl = $this->getEmailAvatarUrl($planer, "#30b5ba");

        $data = [
            'planner' => $planer,
            'domain' => env('APP_URL'),
            'emailPlannerAvatarUrl' => $emailPlannerAvatarUrl
        ];

        $userService = App::make('userService');
        $admins = $userService->getAdmins();

        $message = 'emails.admin.new_planner';

        foreach ($admins as $admin) {
            $emailTo = $admin->email;
            $emailToName = $admin->username;

            $this->emailQueueService->addEmailToQueue('New planner', $emailTo, $emailToName, $message, $data);
        }

        // Send email to Planner.
        $message = 'emails.planer.new_planner';
        $emailTo = $planer->email;
        $emailToName = $planer->username;
        $this->emailQueueService->addEmailToQueue("Welcome to the EVAmore team!", $emailTo, $emailToName, $message, $data);
    }

    /**
     * Notify Event Planners by Email When an Artists Indicates Interest in Playing Their Event
     * @param $event
     * @param $artistUser
     * @param $planerUser
     */
    public function requestToPlayEventByArtist(Event $event, User $artistUser, User $planerUser, Bid $bid)
    {
        $data = [
            'event' => $event,
            'artistUser' => $artistUser,
            'eventOwner' => $planerUser,
            'bid' => $bid,
            'emailUserAvatarUrl' => $this->getEmailAvatarUrl($artistUser, "#30b5ba"),
            'domain' => env('APP_URL')
        ];

        $emailTo = $planerUser->email;
        $emailToName = $planerUser->username;

        $message = 'emails.user.request_to_play';

        $this->emailQueueService->addEmailToQueue('Request to Play Event', $emailTo, $emailToName, $message, $data);
    }

    /**
     * Notify Admins by Email When an Artists Indicates Interest in Playing Some Event
     * @param $event
     * @param $artistUser
     */

    public function notifyAdminsAboutArtistRequestNewEvent($event, $artistUser)
    {
        $data = [
            'event' => $event,
            'artistUser' => $artistUser,
            'emailUserAvatarUrl' => $this->getEmailAvatarUrl($artistUser, "#30b5ba"),
            'domain' => env('APP_URL')
        ];

        $userService = App::make('userService');
        $admins = $userService->getAdmins();


        foreach ($admins as $admin) {
            $emailTo = $admin->email;
            $emailToName = $admin->username;

            $message = 'emails.admin.artist_request_to_play';
            $subject = 'Request to Play Event';

            $this->emailQueueService->addEmailToQueue($subject, $emailTo, $emailToName, $message, $data);
        }
    }

    /**
     * send emails to admins when user create new event
     * @param $event
     * @param $user
     */
    public function notifyAdminsAboutNewEventCreated($event, $user)
    {

        $emailUserAvatarUrl = $this->getEmailAvatarUrl($user, "#30b5ba");
        $data = [
            'event' => $event,
            'user' => $user,
            'domain' => env('APP_URL'),
            'emailUserAvatarUrl' => $emailUserAvatarUrl
        ];

        $admins = $this->userService->getAdmins();

        $message = 'emails.admin.new_event';

        foreach ($admins as $admin) {
            $emailTo = $admin->email;
            $emailToName = $admin->username;

            $this->emailQueueService->addEmailToQueue('New event', $emailTo, $emailToName, $message, $data);
        }

        // Send email to planner.
        $message = 'emails.planer.new_event';
        $emailTo = $user->email;
        $emailToName = $user->username;
        $this->emailQueueService->addEmailToQueue("Nice work, your EVAmore event is being reviewed by our team!", $emailTo, $emailToName, $message, $data);
    }

    /**
     * Send email to Planer when event was published.
     * @param $event
     * @param $userPlaner
     */
    public function notifyPlannerAboutNewEventPublished($event, $userPlaner)
    {
        $data = [
            'event' => $event,
            'userPlaner' => $userPlaner,
            'emailUserAvatarUrl' => $this->getEmailAvatarUrl($userPlaner, '#FF6666'),
            'artist' => '',
            'domain' => env('APP_URL')
        ];

        $view = 'emails.planer.notify_planer_about_new_event_published';

        $this->emailQueueService->addEmailToQueue("Your event is live on EVAmore! Congratulations!", $userPlaner->email, $userPlaner->username, $view, $data);

        return FALSE;
    }

    /**
     * Send email to Artist when User creates Event with Artist's Genre
     * @param $event
     * @param $userPlaner
     */
    public function notifyArtistAboutNewEvent($event, $userPlaner, $excludeArtists = [])
    {
        $data = [
            'event' => $event,
            'userPlaner' => $userPlaner,
            'emailUserAvatarUrl' => $this->getEmailAvatarUrl($userPlaner, '#FF6666'),
            'artist' => '',
            'domain' => env('APP_URL')
        ];

        $genres = $event->genres()->get();
        $artists = [];

        if (count($genres) != 0) {
            foreach ($genres as $genre) {
                $genreIds[] = $genre->id;
            }
            $artistService = App::make('artistService');
            $artists = $artistService->getArtists($genreIds);
        }

        if (count($artists) != 0) {
            foreach ($artists as $artist) {
                if (in_array($artist->user_id, $excludeArtists)) {
                    continue;
                }

                $au = App\User::find($artist->user_id);
                $emailTo = $au->email;
                $emailToName = $au->username;

                $data['artist'] = $emailToName;
                $message = 'emails.artist.notify_artist_about_new_event';
                $this->emailQueueService->addEmailToQueue("User Creates New Event", $emailTo, $emailToName, $message, $data);


                /**
                 * Notify manager.
                 * Text and Name are the same.
                 * Only email was changed.
                 */
                $manager = ArtistService::getManagerByArtist($artist);
                if ($manager) {
                    $this->emailQueueService->addEmailToQueue("User Creates New Event", $manager->email, $emailToName, $message, $data);
                }
            }
        }
        return FALSE;
    }

    /**
     * Notify Artist by Email When an Admin Activates Their Account
     *
     */
    public function notifyArtistAboutActivation($user)
    {
        $data = [
            'user' => $user,
            'domain' => env('APP_URL')
        ];

        $emailTo = $user->email;
        $emailToName = $user->username;

        $message = 'emails.artist.notify_about_activation';

        $this->emailQueueService->addEmailToQueue("Your account activated", $emailTo, $emailToName, $message, $data);

        /**
         * Notify artist manager.
         * Text and Name are the same.
         * Only email was changed.
         */
        $artist = $user->artist;
        $manager = ArtistService::getManagerByArtist($artist);
        if ($artist && $manager) {
            $m = $manager;
            $this->emailQueueService->addEmailToQueue("Your account activated", $m->email, $emailToName, $message, $data);
        }
    }


    public function sendEmailtoUserForPasswordReset($user)
    {
        $date = new \DateTime(date("Y-m-d h:i:sa"));
        $date->add(new \DateInterval('P1D'));

        $arrayForSecretKey = [
            time(),
            $user->email,
            $this->generateRandomString()
        ];

        $secretKey = md5(implode("_", $arrayForSecretKey));

        DB::table('reset_password')->insert(
            array(
                'user_id' => $user->id,
                'secret_key' => $secretKey,
                'expires_at' => $date
            )
        );

        $emailTo = $user->email;
        $emailToName = $user->username;

        $data = [
            'user' => $user,
            'emailUserAvatarUrl' => $this->getEmailAvatarUrl($user, '#30b5ba'),
            'link' => route("reset_password_with_key", $secretKey),
            'domain' => env('APP_URL')
        ];

        $message = 'emails.user.password_reset';

        $this->emailQueueService->addEmailToQueue("EVAmore - Password Reset", $emailTo, $emailToName, $message, $data);
    }

    public function generateRandomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    /**
     * Notify support about new message. Send copy of message to user.
     * @param $name
     * @param $email
     * @param $textMessage
     */
    public function notifyAboutSupportMessage($first_name, $last_name, $email, $textMessage)
    {
        $data = [
            'first_name' => $first_name,
            'last_name' => $last_name,
            'email' => $email,
            'textMessage' => $textMessage,
            'domain' => env('APP_URL')
        ];

        //Send email to support administrator
        $emailTo = config('mail.support_email');
        $emailToName = 'Support';

        $message = 'emails.admin.support_email';
        $this->emailQueueService->addEmailToQueue("Question to support", $emailTo, $emailToName, $message, $data);

        //Send copy of email to message owner
        $emailTo = $email;
        $emailToName = "{$first_name} {$last_name}";

        $message = 'emails.user.copy_support_email';
        $this->emailQueueService->addEmailToQueue("Copy of your question to support", $emailTo, $emailToName, $message, $data);
    }

    /**
     * Notify Artist about new event after planer requested to play on his event.
     *
     * @param $artistUser
     * @param $planerUser
     * @param $event
     * @param int $count
     * @throws \Exception
     */
    public function notifyArtistAboutNewPersonalEvent($artistUser, $planerUser, $event, $count = 0)
    {
        $data = [
            'event' => $event,
            'artistUser' => $artistUser,
            'planerUser' => $planerUser,
            'emailUserAvatarUrl' => $this->getEmailAvatarUrl($artistUser, "#30b5ba"),
            'domain' => env('APP_URL')
        ];

        $emailTo = $artistUser->email;
        $emailToName = $artistUser->username;

        $message = 'emails.artist.request_to_play_event';
        $this->emailQueueService->addEmailToQueue('Request to Play Event', $emailTo, $emailToName, $message, $data);

        /**
         * Notify Artists Manager.
         * Text and Name are the same.
         * Only email was changed.
         */
        $manager = ArtistService::getManagerByArtist($artistUser);
        if ($manager && $count == 0) {
            $this->emailQueueService->addEmailToQueue('Request to Play Event', $manager->email, $emailToName, $message, $data);
        }
    }


    /**
     * Send email to Admin when planer requests to book an artist
     * @param $planer
     * @param $artist
     * @throws \Exception
     */
    public function planerRequestsToBookArtist($planer, $artist, $event)
    {
        $data = [
            'artist' => $artist,
            'user' => $planer,
            'emailUserAvatarUrl' => $this->getEmailAvatarUrl($planer, '#30b5ba'),
            'domain' => env('APP_URL'),
            'event' => $event
        ];

        $userService = App::make('userService');
        $admins = $userService->getAdmins();

        foreach ($admins as $admin) {
            $emailTo = $admin->email;
            $emailToName = $admin->username;

            $message = 'emails.admin.planer_request_artist_to_play';
            $this->emailQueueService->addEmailToQueue("New Request to Book for artist", $emailTo, $emailToName, $message, $data);
        }
    }

    /**
     * Send email to planer about decline the request by artist
     * @param $planerUser
     * @param $artistUser
     * @param $event
     * @throws \Exception
     */
    public function notifyPlanerAboutDeclineRequest($planerUser, $artistUser, $event)
    {
        $data = [
            'artistUser' => $artistUser,
            'planerUser' => $planerUser,
            'event' => $event,
            'emailArtistUserAvatarUrl' => $this->getEmailAvatarUrl($artistUser, '#30b5ba'),
            'domain' => env('APP_URL')
        ];

        $emailTo = $planerUser->email;
        $emailToName = $planerUser->username;

        $message = 'emails.planer.artist_decline_planers_request';
        $this->emailQueueService->addEmailToQueue("Artist declined your request", $emailTo, $emailToName, $message, $data);
    }

    /**
     * Send email to admins about decline the request by artist
     * @param $planerUser
     * @param $artistUser
     * @param $event
     */
    public function notifyAdminsAboutDeclineRequest($planerUser, $artistUser, $event, $reasonDeclineMessage)
    {
        $data = [
            'reasonDeclineMessage' => $reasonDeclineMessage,
            'artistUser' => $artistUser,
            'planerUser' => $planerUser,
            'event' => $event,
            'emailArtistUserAvatarUrl' => $this->getEmailAvatarUrl($artistUser, '#30b5ba'),
            'domain' => env('APP_URL')
        ];

        $admins = $this->userService->getAdmins();

        $message = 'emails.admin.artist_decline_planers_request';

        foreach ($admins as $admin) {
            $emailTo = $admin->email;
            $emailToName = $admin->username;

            $this->emailQueueService->addEmailToQueue("Artist declined planer's request", $emailTo, $emailToName, $message, $data);
        }
    }


    /**
     * Send email to planer about price for event bids
     * @param $planerUser
     * @param $artistUser
     * @param $event
     */
    public function notifyPlanerAboutBidPrices(Event $event, $bids)
    {
        $planerUser = $this->userService->getUser($event->user_id);
        $data = [
            'planerUser' => $planerUser,
            'event' => $event,
            'bids' => $bids,
            'domain' => env('APP_URL')
        ];
//        dd($data);

        $emailTo = $planerUser->email;
        $emailToName = $planerUser->username;

        $message = 'emails.planer.event_bids_prices';
        $this->emailQueueService->addEmailToQueue("Available prices for requests", $emailTo, $emailToName, $message, $data);
    }

    /**
     * Send Artist Notification When Event Planner Payment is Confirmed
     * @param $planerUser
     * @param $artistUser
     * @param $event
     */
    public function notifyArtistAboutPaymentConfirmation($bid)
    {
        $planerUser = $this->userService->getUser($bid->event->user_id);

        $data = [
            'artistUser' => $bid->artistUser,
            'planerUser' => $planerUser,
            'event' => $bid->event,
            'bid' => $bid,
            'emailPlanerUserAvatarUrl' => $this->getEmailAvatarUrl($planerUser, '#FF6666'),
            'domain' => env('APP_URL')
        ];

        $emailTo = $bid->artistUser->email;
        $emailToName = $bid->artistUser->username;

        $message = 'emails.artist.notify_artist_about_payment_confirmation';
        $this->emailQueueService->addEmailToQueue("Event Planner Payment is Confirmed", $emailTo, $emailToName, $message, $data);

        /**
         * Notify artist manager.
         * Text and Name are the same.
         * Only email was changed.
         */
        $manager = ArtistService::getManagerByArtist($bid->artistUser);
        if ($manager) {
            $subject = "Event Planner Payment is Confirmed";
            $this->emailQueueService->addEmailToQueue($subject, $manager->email, $emailToName, $message, $data);
        }
    }


    /**
     * Admin is Notified when an Event is Completed to Pay the Artist
     * @param Bid $bid
     */
    public function notifyAdminsAboutPaymentsToArtist(Bid $bid)
    {

        $artistUserAvatarUrl = $this->getEmailAvatarUrl($bid->artistUser, "#30b5ba");
        $data = [
            'event' => $bid->event,
            'artistUser' => $bid->artistUser,
            'domain' => env('APP_URL'),
            'artistUserAvatarUrl' => $artistUserAvatarUrl
        ];

        $admins = $this->userService->getAdmins();

        $message = 'emails.admin.notify_for_artist_payment';

        foreach ($admins as $admin) {
            $emailTo = $admin->email;
            $emailToName = $admin->username;

            $this->emailQueueService->addEmailToQueue("Don't Forget to pay the Artist", $emailTo, $emailToName, $message, $data);
        }
    }

    /**
     * Send Payment Reminder Notifications to Event Planner if Not Paid in Full
     * @param Bid $bid
     * @param $subject
     * @param $emailTitle
     */
    public function notifyPlanerAboutNotPaidBidInFull(Bid $bid, $journalAction)
    {
        $planerUser = $bid->event->user;

        $data = [
            'planerUser' => $planerUser,
            'event' => $bid->event,
            'domain' => env('APP_URL'),
        ];

        $emailTo = $planerUser->email;
        $emailToName = $planerUser->username;

        $message = NULL;
        if ($journalAction == JournalType::ACT_NOTIFY_PLANER_NOT_PAID_1W) {
            if ($bid->autopay == App\ModelType\BidType::AUTOMATED_PAYMENT_OFF) {
                $subject = "Heads up. One week left to pay for your EVAmore event!";
                $message = 'emails.planer.rest_payment_manual_reminder';
            }
        } else {
            if ($journalAction == JournalType::ACT_NOTIFY_PLANER_NOT_PAID_24H) {
                if ($bid->autopay == App\ModelType\BidType::AUTOMATED_PAYMENT_OFF) {
                    $subject = "24 hours till event payment deadline! Don't forget to pay the remaining 50%";
                    $message = 'emails.planer.rest_payment_manual_reminder';
                } else {
                    $subject = "Heads up - we will be processing your automated payment in 24 hours";
                    $data['hours'] = 24;
                    $message = 'emails.planer.rest_payment_auto_reminder';
                }
            } else {
                if ($journalAction == JournalType::ACT_NOTIFY_PLANER_NOT_PAID_12H) {
                    if ($bid->autopay == App\ModelType\BidType::AUTOMATED_PAYMENT_OFF) {
                        $subject = "12 hours till event payment deadline! Don't forget to pay the remaining 50%";
                        $message = 'emails.planer.rest_payment_manual_reminder';
                    } else {
                        $subject = "Heads up - we will be processing your automated payment in 12 hours";
                        $data['hours'] = 12;
                        $message = 'emails.planer.rest_payment_auto_reminder';
                    }
                } else {
                    if ($journalAction == JournalType::ACT_NOTIFY_PLANER_NOT_PAID_6H) {
                        if ($bid->autopay == App\ModelType\BidType::AUTOMATED_PAYMENT_OFF) {
                            $subject = "6 hours till event payment deadline! Don't forget to pay the remaining 50%";
                            $message = 'emails.planer.rest_payment_manual_reminder';
                        } else {
                            $subject = "Heads up - we will be processing your automated payment in 6 hours";
                            $data['hours'] = 6;
                            $message = 'emails.planer.rest_payment_auto_reminder';
                        }
                    } else {
                        if ($journalAction == JournalType::ACT_NOTIFY_PLANER_NOT_PAID_DEADLINE) {

                            //notify admins
                            $adminsUsers = $this->userService->getAdmins();
                            foreach ($adminsUsers as $adminUser) {
                                $to = $adminUser->email;
                                $toName = $adminUser->username;
                                $subject = "48-hr deadline warning - event wasn't paid in full";
                                $data['text'] = 'The event "' . $bid->event->name . '" reached 48-hr deadline without being paid in full';
                                $data['isAdmin'] = TRUE;
                                $message = 'emails.general.rest_payment_deadline_reminder';
                                $this->emailQueueService->addEmailToQueue($subject, $to, $toName, $message, $data);
                            }

                            //Notify planer
                            $subject = "48-hr deadline warning - you missed payment for your event!";
                            $data['text'] = 'Your event "' . $bid->event->name . '" reached 48-hr deadline without being paid in full';
                            $data['isAdmin'] = FALSE;
                            $message = 'emails.general.rest_payment_deadline_reminder';
                        }
                    }
                }
            }
        }

        if (!is_null($message)) {
            $this->emailQueueService->addEmailToQueue($subject, $emailTo, $emailToName, $message, $data);
        }
    }

    /**
     * @param Event $event
     * @param array $artists
     */
    public function notifyAboutCancelEvent($event, $artistsUsers = [])
    {
        $planerUser = $this->userService->getUser($event->user_id);

        $data = [
            'planerUser' => $planerUser,
            'event' => $event,
            'domain' => env('APP_URL'),
        ];

        $subject = "Event cancelled";
        $message = 'emails.general.cancellation_event';

        //notify artists
        foreach ($artistsUsers as $artistUser) {
            $emailTo = $artistUser->email;
            $emailToName = $artistUser->username;

            $this->emailQueueService->addEmailToQueue($subject, $emailTo, $emailToName, $message, $data);

            /**
             * Notify Manager.
             * Text and Name are the same.
             * Only email was changed.
             */
            $manager = ArtistService::getManagerByArtist($artistUser);
            if ($manager) {
                $this->emailQueueService->addEmailToQueue($subject, $manager->email, $emailToName, $message, $data);
            }
        }

        //notify planer
        $emailTo = $planerUser->email;
        $emailToName = $planerUser->username;

        $this->emailQueueService->addEmailToQueue($subject, $emailTo, $emailToName, $message, $data);

        //notify admins
        $adminsUsers = $this->userService->getAdmins();
        foreach ($adminsUsers as $adminUser) {
            $emailTo = $adminUser->email;
            $emailToName = $adminUser->username;

            $this->emailQueueService->addEmailToQueue($subject, $emailTo, $emailToName, $message, $data);
        }
    }

    public function notifyAboutAutomatePaymentError(Bid $bid, $message)
    {
        $planerUser = $bid->event->user;

        $data = [
            'planerUser' => $planerUser,
            'event' => $bid->event,
            'domain' => env('APP_URL'),
        ];

        $subject = "Automate payment error";

        //notify artist
        $artistUser = $bid->artistUser;
        $data['message'] = "Event planners payment didn't go through.";
        $message = 'emails.general.automate_payment_error';
        $this->emailQueueService->addEmailToQueue($subject, $artistUser->email, $artistUser->username, $message, $data);

        /**
         * Notify artist manager.
         * Text and Name are the same.
         * Only email was changed.
         */
        $manager = ArtistService::getManagerByArtist($bid->artistUser);
        if ($manager) {
            $this->emailQueueService->addEmailToQueue($subject, $manager->email, $artistUser->username, $message, $data);
        }

        //notify planer
        $emailTo = $planerUser->email;
        $emailToName = $planerUser->username;
        $data['message'] = "Oh no! Your payment did not work!";
        $message = 'emails.general.automate_payment_error';
        $this->emailQueueService->addEmailToQueue($subject, $emailTo, $emailToName, $message, $data);

        //notify admins
        $adminsUsers = $this->userService->getAdmins();
        $data['message'] = "Event planners payment didn't go through.";
        $message = 'emails.general.automate_payment_error';
        foreach ($adminsUsers as $adminUser) {
            $emailTo = $adminUser->email;
            $emailToName = $adminUser->username;

            $this->emailQueueService->addEmailToQueue($subject, $emailTo, $emailToName, $message, $data);
        }
    }

    /**
     * Notify artist about set new fee for artist's event by Admin
     * @param Bid $bid
     * @param $changes
     * @throws \Exception
     */

    Public function notifyArtistsAboutEventChanges($event, $artist)
    {
        $emailArtistAvatarUrl = $this->getEmailAvatarUrl($artist->user, "#30b5ba");

        $data = [
            'event' => $event,
            'artistUser' => $artist->user,
            'domain' => env('APP_URL'),
            'emailArtistAvatarUrl' => $emailArtistAvatarUrl,
        ];
        $emailTo = $artist->user->email;
        $emailToName = $artist->user->username;

        $message = 'emails.artist.notifyAboutChanges';

        $this->emailQueueService->addEmailToQueue("Event {$artist->user->name} has been changed", $emailTo, $emailToName, $message, $data);

//        $emailArtistAvatarUrl = $this->getEmailAvatarUrl($bid->artistUser, "#30b5ba");
//
//        $data = [
//            'event' => $bid->event,
//            'artistUser' => $bid->artistUser,
//            'domain' => env('APP_URL'),
//            'emailArtistAvatarUrl' => $emailArtistAvatarUrl,
//            'bid' => $bid,
//        ];
//        $emailTo = $bid->artistUser->email;
//        $emailToName = $bid->artistUser->username;
//
//        $message = 'emails.artist.notifyAboutChanges';
//
//        $this->emailQueueService->addEmailToQueue("Event {$bid->event->name} has been changed", $emailTo, $emailToName, $message, $data);

    }

    public function notifyArtistAboutNewEventFee(Bid $bid, $newArtistFee)
    {
        $data = [
            'artistUser' => $bid->artistUser,
            'event' => $bid->event,
            'bid' => $bid,
            'newArtistFee' => $newArtistFee,
            'domain' => env('APP_URL')
        ];

        $emailTo = $bid->artistUser->email;
        $emailToName = $bid->artistUser->username;

        $message = 'emails.artist.notify_artist_about_new_event_fee';

        $this->emailQueueService->addEmailToQueue("Your fee for {$bid->event->name} has been set", $emailTo, $emailToName, $message, $data);

        /**
         * Notify artist manager.
         * Text and Name are the same.
         * Only email was changed.
         */
        $subject = "Your fee for {$bid->event->name} has been set";
        $manager = ArtistService::getManagerByArtist($bid->artistUser);
        if ($manager) {
            $this->emailQueueService->addEmailToQueue($subject, $manager->email, $emailToName, $message, $data);
        }
    }

    /**
     * Notify Admins, EP, Artists about new comment for event
     * @param $comment
     */
    public function notifyAboutNewComment($comment)
    {

        $event = $comment->event;

        $data = [
            'event' => $event,
            'comment' => $comment,
            'domain' => env('APP_URL')
        ];

        $subject = "Someone posted a comment on {$event->name}!";

        //notify admins
        $adminsUsers = $this->userService->getAdmins();
        $message = 'emails.general.new_event_comment';

        foreach ($adminsUsers as $adminUser) {
            if ($adminUser->id != $comment->user_id) {
                $emailTo = $adminUser->email;
                $emailToName = $adminUser->username;
                $this->emailQueueService->addEmailToQueue($subject, $emailTo, $emailToName, $message, $data);
            }
        }

        //notify event planer
        if ($event->user->id != $comment->user_id) {
            $this->emailQueueService->addEmailToQueue($subject, $event->user->email, $event->user->username, $message, $data);
        }

        //notify artists
        $commentService = App::make("commentService");
        $qb = Bid::where('event_id', $event->id)
            ->where("price", ">", 0)->whereIn("status", [1, 2, 5, 6]);

        $allEventBids = $qb->get();
        foreach ($allEventBids as $bid) {
            if ($commentService->checkCommentsAccess($bid->artistUser, $event, 'read')) {
                if ($bid->artistUser->id != $comment->user_id) {
                    $this->emailQueueService->addEmailToQueue($subject, $bid->artistUser->email, $bid->artistUser->username, $message, $data);
                }

                /**
                 * Anyway Manager gets an email.
                 * Text and Name are the same.
                 * Only email was changed.
                 */
                $manager = ArtistService::getManagerByArtist($bid->artistUser);
                if ($manager) {
                    $this->emailQueueService->addEmailToQueue($subject, $manager->email, $bid->artistUser->username, $message, $data);
                }
            }
        }

    }

    public function notifyAboutContract($bidContract, $type)
    {

        $event = $bidContract->bid->event;

        $data = [
            'event' => $event,
            'domain' => env('APP_URL')
        ];

        $subject = "The contract has been posted on {$event->name}";
        $file = File::where('bid_contract_id', $bidContract->id)->where('type', $type)->first();
        $attachment = $file->path;

        //notify admins
        $adminsUsers = $this->userService->getAdmins();
        $message = 'emails.general.contract_has_been_created';

        foreach ($adminsUsers as $adminUser) {
            $emailTo = $adminUser->email;
            $emailToName = $adminUser->username;
            $adminFile = File::where('bid_contract_id', $bidContract->id)->first();
            $this->emailQueueService->addEmailToQueue($subject, $emailTo, $emailToName, $message, $data, $adminFile);
        }

        if ($type == 'planer') {
            //notify event planer
            $this->emailQueueService->addEmailToQueue($subject, $event->user->email, $event->user->username, $message, $data, $attachment);
        }

        if ($type == 'artist') {
            //notify artists
            $this->emailQueueService->addEmailToQueue($subject, $bidContract->bid->artistUser->email, $bidContract->bid->artistUser->username, $message, $data, $attachment);

            //Notify artist manager
            $manager = ArtistService::getManagerByArtist($bidContract->bid->artistUser);
            if ($manager) {
                $this->emailQueueService->addEmailToQueue($subject, $manager->email, $bidContract->bid->artistUser->username, $message, $data, $attachment);
            }
        }

    }

    public function notifyArtistOtherBidSelected($artist_user_id, $event_id)
    {
        $eventsService = App::make("eventsService");
        $event = $eventsService->getEvent($event_id);
        $artist = $this->userService->getUser($artist_user_id);

        $data = ["domain" => env('APP_URL'), "eventname" => $event->name, "artist" => $artist->name];
        $message = 'emails.artist.reject_artist';
        $subject = "You were not selected for an event";

        //notify artist of rejection
        $this->emailQueueService->addEmailToQueue($subject, $artist->email, $artist->username, $message, $data);

        /**
         * Notify artist manager
         */
        $manager = ArtistService::getManagerByArtist($artist);
        if ($manager) {
            $this->emailQueueService->addEmailToQueue($subject, $manager->email, $artist->username, $message, $data);
        }

    }
}
