<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 13.01.17
 * Time: 8:56
 */

namespace App\Services;

use App;
use App\EmailQueue;
use Illuminate\Support\Facades\Mail;
use Swift_Mailer;
use Swift_MailTransport;
use Swift_Message;
use Swift_SmtpTransport;
//use Illuminate\Support\Facades\Mail;

class EmailQueueService {
    private $emailQueueRepository;

    public function __construct() {
        $this->emailQueueRepository = App::make("emailQueueRepository");
    }

    public function addEmailToQueue($subject, $toEmail, $toName, $view, $data,  $attachment = null) {
        try {
            Mail::queue($view, $data, function ($message) use ($subject, $toEmail, $toName, $attachment) {
                $message->from(env("MAIL_FROM_EMAIL"), env("MAIL_FROM_NAME"));
                if ($attachment) {
                    $message->attach($attachment);
                }
                $message->to($toEmail, $toName)->subject($subject);
            });
            if (env('MAIL_HOST', false) == 'smtp.mailtrap.io') {
                sleep(1); //use usleep(500000) for half a second or less
            }
        } catch (\Exception $e) {
            \Log::info($e);
        }
        
        //support for artist accounts without email

    }

    public function getFromQueue($limit=10) {
        return $this->emailQueueRepository->getFromQueue($limit);
    }

    public function markAsSent(EmailQueue $emailQueue) {
        $emailQueue->status = 1;
        $this->emailQueueRepository->save($emailQueue);
    }

    public function sendFromQueue($limit) {
        $emails = $this->getFromQueue($limit);
        foreach($emails as $email) {
            $this->sendEmail($email);
            $this->markAsSent($email);
        }
    }

    public function sendEmail(EmailQueue $email)
    {
        try {
            Mail::queue($email->message, function ($message) use ($email) {
                $message->from(env("MAIL_FROM_EMAIL"), env("MAIL_FROM_NAME"));
                $message->setBody($email->message, 'text/html');
                if ($email->attachment != null) {
                    $message->attach($email->attachment);
                }
                $message->to($email->to_email, $email->to_name)->subject($email->subject);
            });
            if (env('MAIL_HOST', false) == 'smtp.mailtrap.io') {
                sleep(1); //use usleep(500000) for half a second or less
            }
        } catch (\Exception $e) {
            \Log::info($e);
        }
        

//        // Create the mail transport configuration
//        $transport = Swift_SmtpTransport::newInstance(env("MAIL_HOST"), env("MAIL_PORT"))
//            ->setUsername(env("MAIL_USERNAME"))
//            ->setPassword(env("MAIL_PASSWORD"));
//
//        // Create the message
//        $message = Swift_Message::newInstance();
//        $message->setTo(array(
//            $email->to_email => $email->to_name,
//        ));
//        $message->setSubject($email->subject);
//        $message->setBody($email->message, 'text/html');
//        $message->setFrom(env("MAIL_FROM_EMAIL"), env("MAIL_FROM_NAME"));
//
//        // Send the email
//        $mailer = Swift_Mailer::newInstance($transport);
//        $mailer->send($message);
    }
}
