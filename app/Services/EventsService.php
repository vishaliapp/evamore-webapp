<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 12.08.16
 * Time: 11:39
 */
namespace App\Services;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Event;
use App;
use App\ModelType\BidType;
use App\ModelType\EventType;
use App\ModelType\JournalType;

class EventsService {

    private $eventRepository;
    private $userService;

    public function __construct() {
        $this->eventRepository = App::make("eventRepository");
        $this->notificationService = App::make("emailNotificationService");
        $this->userService= App::make("userService");
    }

    public function getEvent($id) {
        return $this->eventRepository->getEvent($id);
    }

    public function getPagedEvents($filters=[], $sorts=[], $perPage, $sign) {
//        $sign = ">=";
//        $queryBuilder  = Event::where('starttime', '<', Carbon::now()->format('Y-m-d H:i:s')) ->select('events.*')->groupBy('events.id');
        $queryBuilder = Event::where('starttime', $sign, Carbon::now()->format('Y-m-d H:i:s'))
            ->select('events.*')
            ->groupBy('events.id');

        $queryBuilder->leftJoin('users', 'users.id', '=', 'events.user_id');


        // Sorting events
        foreach($sorts as $field=>$value){
            $queryBuilder->orderBy($field, $value);
        }

        // Filtering events
        if (array_key_exists('search', $filters) && $filters['search'] != '') {
            $queryBuilder
                ->where(DB::raw('CONCAT(events.state, " ", events.city, " ", events.venue, " ", events.name, " ", users.username)'), 'like', '%'.$filters['search'].'%');
        }

        if (array_key_exists('starttime', $filters)) {
            $queryBuilder->where('starttime', $filters['starttime'][0], $filters['starttime'][1]);
        }

        if (array_key_exists('status', $filters)) {
            if (is_array($filters['status'])) {
                $queryBuilder->whereIn("status", $filters['status']);
            } else {
                $queryBuilder->where("status", $filters['status']);
            }
        }

        return $queryBuilder->paginate($perPage);
    }

    public function getCountEvents($userId) {
        return DB::table('events')
            ->where('user_id', $userId)
            ->where('starttime', '>=', date("Y-m-d H:i:s"))
            ->count();
    }

    public function getUpcomingEventsCount() {
        return DB::table('events')
            ->where('starttime', '>=', date("Y-m-d H:i:s"))
            ->count();
    }

    public function getCountUserPastEvents($user) {
        $user = User::find($user);
        if ($user->hasRole('ARTIST')) {
            $c = DB::table('bid')
                ->join('events', 'events.id', '=', 'bid.event_id')
                ->where('bid.artist_user_id', $user->id)
                ->where('events.starttime', '<', date('Y-m-d H:i:s'))
                ->count();
        } else {
            $c = DB::table('events')
                ->where('events.user_id', $user->id)
                ->where('events.starttime', '<', date("Y-m-d H:i:s"))
                ->count();
        }
        return $c;
    }

    public function getPlanerEvents($planerUser, $filter=[]) {
        return $this->eventRepository->getUserEvents($planerUser, $filter);
    }

    /**
     * return the list of future events witch planner sent request to artist
     * @param User $planerUser
     * @param $artistUser
     */
    public function getPlanerEventsForArtist(User $planerUser, $artistUser) {
        return $this->eventRepository->getPlanerEventsForArtist($planerUser, $artistUser);
    }

    /**
     * @param Event $event
     * @param User $user - an user that cancel event or null if event cancel by system
     */
    public function cancelEvent(Event $event, User $user=null) {
        $event->status = EventType::CANCELLED;
        $this->eventRepository->save($event);

        $journalService = App::make("journalService");

        $userId = is_null($user) ? 0 : $user->id;

        $journalService->add("event", $event->id, $userId, JournalType::ACT_CANCEL);

        $bidService = App::make("bidService");
        $artistsUsers = [];

//        $artistsBidsToEvent = $bidService->getArtistsBidsToEvent($event);
        $artistsBidsToEvent = $bidService->getSelectedArtistsBidsToEvent($event);
        foreach($artistsBidsToEvent as $bid){
            $artistsUsers[] = $bid->artistUser;
        }

        //Planner bids to event
//        $planerBidsToEvent = $bidService->getPlanerBidsToEvent($event);
//        foreach($planerBidsToEvent as $bid){
//            $artistsUsers[] = $bid->artistUser;
//        }
        $this->notificationService->notifyAboutCancelEvent($event, $artistsUsers);
    }

    /**
     * @param Event $event
     * @param User $user - an user that cancel event or null if event cancel by system
     */
    public function publishEvent(Event $event, User $user=null) {

        $oldStatus = $event->status;

        $event->status = EventType::PUBLISHED;
        $this->eventRepository->save($event);

        $journalService = App::make("journalService");

        $userId = is_null($user) ? 0 : $user->id;

        $journalService->add("event", $event->id, $userId, JournalType::ACT_PUBLISH);

        //notify artist about personal invite to event
        if ($oldStatus == EventType::PENDING) {
            $bidService = App::make("bidService");
            //the bids created on create new event with request artist to this new event
            $planerBids = $bidService->getPlanerBidsToNewEvent($event);
            if (count($planerBids) == 1) {
                $this->notificationService->notifyArtistAboutNewPersonalEvent($planerBids[0]->artistUser, $event->user, $event);
            } else {
                $this->notificationService->notifyArtistAboutNewEvent($event, $event->user);
            }

            // Notify Planer about published event.
          $this->notificationService->notifyPlannerAboutNewEventPublished($event, $event->user);
        }
    }
    public function bookEvent(Event $event, User $user=null) {

        $event->status = EventType::BOOKED;
        $this->eventRepository->save($event);

        $journalService = App::make("journalService");

        $userId = is_null($user) ? 0 : $user->id;

        //add journal entry
        $journalService->add("event", $event->id, $userId, JournalType::ACT_BOOK);

    }

}
