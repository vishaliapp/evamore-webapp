<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 14.04.17
 * Time: 9:52
 */

namespace App\Services;

use App;
use App\User;
use App\File;

class FileService {
    private $fileRepository;

    public function __construct() {
        $this->fileRepository = App::make('fileRepository');
    }

    public function getFile($id){
//        dd($id);
        return $this->fileRepository->getFile($id);
    }
    public function saveFileInfo(User $user, $entity, $entityId, $filePath, $fileName) {
        return $this->fileRepository->create($user, $entity, $entityId, $filePath, $fileName);
    }

    public function getUserFiles(User $user) {
        $qb = File::select("file.*");
        $qb->where('user_id', $user->id);
        return $qb->get();
    }

    public function getEntityFiles($entity, $entityId, User $user=null) {
        $qb = File::select("file.*");

        $qb->where('entity', $entity);
        $qb->where('entity_id', $entityId);
        if(!is_null($user)) {
            $qb->where('user_id', $user->id);
        }
        return $qb->get();
    }

    public function bindFile(File $file, $entity, $entityId) {
//        dd($file,$entity,$entityId);
        $file->entity = $entity;
        $file->entity_id = $entityId;
        $file->save();
    }

    public function unBindFile(File $file) {
      $file->entity = "none";
      $file->entity_id = 0;
      $file->save();
    }
}