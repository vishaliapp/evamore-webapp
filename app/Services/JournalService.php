<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 26.01.17
 * Time: 14:27
 */

namespace App\Services;

use App;
use App\Journal;

class JournalService {

    private $journalRepository;

    public function __construct() {
        $this->journalRepository = App::make("journalRepository");
    }

    public function add($entityName, $entityId, $userId, $action, $description="") {
        $j = new Journal();
        $j->user_id  = $userId;
        $j->entity = $entityName;
        $j->entity_id = $entityId;
        $j->action = $action;
        $j->description = $description;

        $this->journalRepository->save($j);
        return $j;
    }

}