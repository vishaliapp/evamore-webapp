<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 11.01.17
 * Time: 11:23
 */

namespace App\Services;

use App;
use App\Order;
use App\OrderItem;
use App\Bid;
use App\ModelType\OrderType;


class OrderService {
    private $orderRepository;

    public function __construct() {
        $this->orderRepository = App::make("orderRepository");
    }

    public function getOrder($id) {
        return $this->orderRepository->getOrder($id);
    }

    public function getOrderByBid(Bid $bid) {
        return $this->orderRepository->getOrderByBidId($bid->id);
    }

    public function createOrder(Bid $bid) {
        $order = new Order();
        $order->bid_id = $bid->id;
        $order->price = $bid->price;
        $order->status = OrderType::NEW_ORDER;
        $this->orderRepository->save($order);
        return $order;
    }

    public function deleteOrder(Order $order) {
        $order->delete();
    }

    public function deleteOrderItems(Order $order) {
        foreach($order->items as $orderItem){
            $orderItem->delete();
        }
    }

    public function createOrderItem(Order $order, $amount) {
        if ($order->status == OrderType::FULL_PAID) return null;

        $orderItem = new OrderItem();
        $orderItem->order_id = $order->id;
        $orderItem->amount = $amount;

        $orderItem->save();
        return $orderItem;
    }

    //============\

    public function createPrepaymentOrderItem(Order $order, $coefficient=0.5) {
        return $this->createOrderItem($order, $order->price * $coefficient);
    }

    public function createRestOrderItem(Order $order) {
        $paid = $this->getOrderPaidSum($order);
        $rest = $order->price - $paid;
        if ($rest > 0) {
            return $this->createOrderItem($order, $rest);
        }
        return null;
    }

    public function getOrderPaidSum(Order $order) {
        $amount = OrderItem::where("order_id", "=", $order->id)
            ->where('transaction_id', ">", 0)
            ->sum('amount');
        return is_null($amount) ? 0 : $amount;
    }

    public function setOrderStatus(Order $order, $newStatus) {
        $order->status = $newStatus;
        $this->orderRepository->save($order);
    }
}