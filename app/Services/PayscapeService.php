<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 17.01.17
 * Time: 12:11
 */

namespace App\Services;

use App\Payscape\GatewayFactory;
use App;
use App\OrderItem;
use App\ModelType\OrderItemType;
use App\User;
use App\PayscapeVault;


class PayscapeService {

//    public function saleOrderItem(OrderItem $orderItem, $ccnumber, $ccexp, $ipAddress){
    public function saleOrderItem(OrderItem $orderItem, $data=[]){

        $eventService = App::make("eventsService");
        $userService = App::make("userService");

        $bid = $orderItem->order->bid;

        $event = $eventService->getEvent($bid->event_id);

        $planerUser = $userService->getUser($event->user_id);

        $payscapeGateway = GatewayFactory::getPayscapeGateway();

        $payscapeGateway->setBilling(
            array_key_exists("billing_user_name", $data) ? $data["billing_user_name"] : $planerUser->name,
            "",
            $planerUser->organization,
            "", //$address1,
            "", //$address2,
            "", //$city,
            "", //$state,
            array_key_exists("zipCode", $data) ? $data["zipCode"] : "",
            "", //$country,
            $planerUser->phone,
            "", //$fax,
            $planerUser->email,
            "");

        $orderDescription = "Order #".$orderItem->order_id.". Order item #".$orderItem->id;
        $payscapeGateway->setOrder(App::environment().$orderItem->id,
            $orderDescription,
            0,
            0,
            0,
            array_key_exists("ipAddress", $data) ? $data['ipAddress'] : "" );

        if (array_key_exists('vault_id', $data)){
            $result = $payscapeGateway->doSaleByVault($data['vault_id'], $orderItem->amount);
        } else {
            $ccnumber = array_key_exists("ccnumber", $data) ? $data['ccnumber'] : "" ;
            $ccexp = array_key_exists("ccexp", $data) ? $data['ccexp'] : "" ;
            $cvv  = array_key_exists("cvv", $data) ? $data['cvv'] : "" ;
            $result = $payscapeGateway->doSale($orderItem->amount, $ccnumber, $ccexp, $cvv);
        }


        if ($result['response'] == 1) {
            $orderItem->transaction_id = $result['transactionid'];
            $orderItem->transaction_status = OrderItemType::PENDING_SETTLEMENT;
            $orderItem->save();
            return $orderItem;
        } else if ($result['response'] == 2) {
            throw new \Exception("Transaction declined. " . $result['responsetext']);
        } else if ($result['response'] == 3) {
            throw new \Exception("Error in transaction data or system error ".$result['responsetext']);
        }

        throw new \Exception("Error in transaction data or system error");
    }

    public function addVault(User $user, $ccnumber, $ccexp, $firstName="", $lastName="") {

        if ($user->payscapeVault){
            return false;
        }

        $payscapeGateway = GatewayFactory::getPayscapeGateway();
        $result = $payscapeGateway->addVault($ccnumber, $ccexp, $firstName, $lastName);

        if ($result['response'] == 1) {

            $cardTail = substr ( $ccnumber , 12, 4 );
            //TODO: Replace to PayscapeService !!!
            $payscapeVault = new PayscapeVault();
            $payscapeVault->vault_id = $result['customer_vault_id'];
            $payscapeVault->user_id = $user->id;
            $payscapeVault->card_tail = $cardTail;
            $payscapeVault->save();
            return true;
        } else if ($result['response'] == 2) {
            throw new \Exception("Error on create vault. " . $result['responsetext']);
        }
        throw new \Exception("Error on create vault.");
    }

    public function deleteVault(User $user){
        if (!is_null($user->payscapeVault)) {
            $payscapeGateway = GatewayFactory::getPayscapeGateway();
            $result = $payscapeGateway->deleteVault($user->payscapeVault->vault_id);

            if ($result['response'] == 1) {
                //TODO: Replace to PayscapeService !!!
                $user->payscapeVault->delete();
                $user->payscapeVault = null;
                return true;
            } else if ($result['response'] == 2) {
                throw new \Exception("Error on delete vault. " . $result['responsetext']);
            }
            throw new \Exception("Error on delete vault.");
        }
    }
}