<?php
/**
 * Created by PhpStorm.
 * User: dasha
 * Date: 26.08.16
 * Time: 9:10
 */
namespace App\Services;

use DB;


class ResetPasswordService {

    public function keyExists($secretKey) {
        return DB::table('reset_password')
            ->where('secret_key', '=', $secretKey)
            ->where('expires_at', '>', $this->getCurrentTime())
            ->first();
    }

    public function keyWithThisUserIdExists($secretKey, $userId) {
        return DB::table('reset_password')
            ->where('secret_key', '=', $secretKey)
            ->where('user_id', '=', $userId)
            ->where('expires_at', '>', $this->getCurrentTime())
            ->first();
    }

    public function deletePasswordResetRecord($userId) {
        DB::table('reset_password')->where('user_id', '=', $userId)->delete();
    }

    public function getCurrentTime() {
        $now = new \DateTime();
        $now->format('Y-m-d H:i:s');
        return $now;
    }
}