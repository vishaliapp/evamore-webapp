<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 17.08.16
 * Time: 10:23
 */

namespace App\Services;

use DB;
use App;
use App\Role;
use App\User;
use Instagram;


class UserService
{

    private $userRepository;

    public function __construct()
    {
        $this->userRepository = App::make('userRepository');
    }

    public function getAdmins()
    {
        $role = Role::getRole('ADMIN');
        if ($role) {
            return $this->userRepository->getUsersByRole($role);
        }
        return [];
    }

    public function getUser($id)
    {
        return $this->userRepository->getUser($id);
    }

    public function getUserByUsername($username)
    {
        return $this->userRepository->getUserByUsername($username);
    }

    public function changeUserActivation($ids = [], $status)
    {
        if (count($ids) > 0) {
            DB::table('users')
                ->whereIn('id', $ids)
                ->update(['active' => $status]);

            $users = User::select("*")
                ->whereIn('id', $ids)
                ->get();

            //Send email notifications only on activation users
            if ($status == 1) {
                foreach ($users as $user) {
                    $emailService = App::make('emailNotificationService');
                    $emailService->notifyArtistAboutActivation($user);
                }
            }
        }
    }

    public function getArtistInstagram($artistUser)
    {
        $result = [];
        $instaToken = DB::table('instagram_account')->where('artist_id', $artistUser)->value('authorization_code');
        Instagram::setAccessToken($instaToken);

        if ($instaToken != null) {
            $result['media'] = Instagram::getUserMedia('self', 8);
            $result['user'] = Instagram::getUser();
        }
        return $result;
    }
}