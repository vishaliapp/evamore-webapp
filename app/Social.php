<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Social extends Model
{
    protected $table = 'socialmedia';
    public $timestamps = false;


    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
