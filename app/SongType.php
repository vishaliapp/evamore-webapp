<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SongType extends Model
{
    protected $table = 'song_types';

    public function artists()
    {
        return $this->belongsToMany('App\Artist');
    }

    public function event()
    {
        return $this->belongsToMany('App\Artist');
    }
    public function changes()
    {
        return $this->belongsToMany(EventChange::class);
    }
}
