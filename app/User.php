<?php

namespace App;

use App\ModelType\TimeZoneType;
use Auth;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cookie;

class User extends Authenticatable
{
    CONST SYSTEM_USER = 'SYSTEM';
    CONST SYSTEM_USER_NAME = 'EVA';

    private $roles_list = null;

	public function roles()
	{
		return $this->belongsToMany('App\Role', 'user_role', 'user_id', 'role_id');
	}

	public function artist()
    {
        return $this->hasOne('App\Artist');
    }

    public function files()
    {
        return $this->hasMany('\App\File', 'user_id');
    }

    public function social()
    {
        return $this->hasOne('App\Social');
    }

    public function payscapeVault()
    {
        return $this->hasOne('App\PayscapeVault');
    }

    public function rolesList(){
        $list = [];
        foreach ($this->roles()->get() as $role) {
            
            $list[] = $role->name;
           
        }
        return $list;
    }

    public static function role($id)
    {
      return static::leftJoin(
        'user_role',
        'user_role.user_id', '=', 'users.id'
      )->leftJoin(
        'roles',
        'roles.id', '=', 'user_role.role_id'
      )->where('users.id', '=', $id);
    }

    public function hasRole($roleName)
    {
        if (is_null($this->roles_list)){
            $this->roles_list = $this->rolesList();
        }

        return in_array($roleName, $this->roles_list);

    }

    public static function getSystemUser()
    {
        return self::where('username', self::SYSTEM_USER)->first();
    }

    public function isSystem()
    {
        return $this->hasRole(self::SYSTEM_USER);
    }

    /**
     * Mutators $user->time_zone_object
     *
     *    *** WARNING ***
     * timestump is stored to the DB using system DB.
     * It DOES NOT depend on config('app.timezone');
     * Moreover it is stored  considering system timezone.
     *
     * === ENV TIMEZONE MUST equal SYSTEM TIMEZONE ===
     */
    public function getTimeZoneObjectAttribute()
    {
        if ($this->time_zone) {
            $tz = TimeZoneType::getTimeZone($this->time_zone);
            return $tz ? new \DateTimeZone($tz) : new \DateTimeZone( config('app.timezone') );
        } else {
            /**
             * Trying to get offset from COOKIES
             */
            $cookie_time_zone = isset($_COOKIE['time_zone']) && $_COOKIE['time_zone'] ? urldecode($_COOKIE['time_zone']): null;
            $zones = TimeZoneType::getTimeZones();
            if (in_array($cookie_time_zone, $zones)) {
                return new \DateTimeZone($cookie_time_zone);
            } else {
                return new \DateTimeZone( config('app.timezone') );
            }
        }
    }
}
