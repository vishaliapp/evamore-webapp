<?php

/*
|--------------------------------------------------------------------------
| Create The Application
|--------------------------------------------------------------------------
|
| The first thing we will do is create a new Laravel application instance
| which serves as the "glue" for all the components of Laravel, and is
| the IoC container for the system binding all of the various parts.
|
*/

$app = new Illuminate\Foundation\Application(
    realpath(__DIR__.'/../')
);

/*
|--------------------------------------------------------------------------
| Bind Important Interfaces
|--------------------------------------------------------------------------
|
| Next, we need to bind some important interfaces into the container so
| we will be able to resolve them when needed. The kernels serve the
| incoming requests to this application from both the web and CLI.
|
*/

$app->singleton(
    Illuminate\Contracts\Http\Kernel::class,
    App\Http\Kernel::class
);

$app->singleton(
    Illuminate\Contracts\Console\Kernel::class,
    App\Console\Kernel::class
);

$app->singleton(
    Illuminate\Contracts\Debug\ExceptionHandler::class,
    App\Exceptions\Handler::class
);

$app->bind('userService', App\Services\UserService::class);
$app->bind('eventsService', App\Services\EventsService::class);
$app->bind('emailNotificationService', App\Services\EmailNotificationService::class);
$app->bind('artistService', App\Services\ArtistService::class);
$app->bind('resetPasswordService', App\Services\ResetPasswordService::class);
$app->bind('emailAvatarGeneratorService', App\Services\EmailAvatarGeneratorService::class);
$app->bind('orderService', App\Services\OrderService::class);
$app->bind('bidService', App\Services\BidService::class);
$app->bind('emailQueueService', App\Services\EmailQueueService::class);
$app->bind('journalService', App\Services\JournalService::class);
$app->bind('commentService', App\Services\CommentService::class);
$app->bind('fileService', App\Services\FileService::class);


$app->bind('userRepository', App\Repositories\UserRepository::class);
$app->bind('eventRepository', App\Repositories\EventRepository::class);
$app->bind('orderRepository', App\Repositories\OrderRepository::class);
$app->bind('bidRepository', App\Repositories\BidRepository::class);
$app->bind('emailQueueRepository', App\Repositories\EmailQueueRepository::class);
$app->bind('journalRepository', App\Repositories\JournalRepository::class);
$app->bind('commentRepository', App\Repositories\CommentRepository::class);
$app->bind('fileRepository', App\Repositories\FileRepository::class);

$app->bind('payscapeService', App\Services\PayscapeService::class);

/*
|--------------------------------------------------------------------------
| Return The Application
|--------------------------------------------------------------------------
|
| This script returns the application instance. The instance is given to
| the calling script so we can separate the building of the instances
| from the actual running of the application and sending responses.
|
*/

return $app;
