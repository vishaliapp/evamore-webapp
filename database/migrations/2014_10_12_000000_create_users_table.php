<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /**
        Add a comment to this line
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });*/

        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->passthru('text', 'username', 'varchar(255)')->unique();
            $table->passthru('text', 'email', 'varchar(255)')->unique();
            $table->string('password');
            $table->boolean('active')->default(true);
            $table->boolean('locked')->default(false);
            $table->nullableTimestamps('last_login');            
            $table->string('phone')->nullable();
            $table->string('organization')->nullable();
            $table->string('org_position')->nullable();
            $table->string('avatar')->default('anonymous_artist.gif');
            $table->rememberToken();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
