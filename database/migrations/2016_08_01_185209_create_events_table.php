<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');  
            $table->string('name');
            $table->string('description')->nullable();
            $table->dateTime('starttime');
            $table->dateTime('endtime')->nullable();
            $table->integer('capacity')->unsigned()->nullable();;
            $table->string('location')->nullable();
            $table->string('artist_type')->nullable();
            $table->integer('min_budget')->unsigned();
            $table->integer('max_budget')->unsigned();
            $table->string('genres')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('events');
    }
}
