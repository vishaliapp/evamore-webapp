<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreatePlanerRequestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('planer_request', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('planer_user_id', false, true);
            $table->integer('artist_user_id', false, true);
            $table->integer('event_id', false, true);
            $table->timestamp('created_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('planer_request');
    }
}
