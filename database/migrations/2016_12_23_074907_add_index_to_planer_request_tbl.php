<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexToPlanerRequestTbl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('planer_request', function ($table) {
            $table->index('planer_user_id');
            $table->index('artist_user_id');
            $table->index('event_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('planer_request', function ($table) {
            $table->dropIndex('planer_user_id');
            $table->dropIndex('artist_user_id');
            $table->dropIndex('event_id');
        });
    }
}
