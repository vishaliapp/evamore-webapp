<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\ModelType\BidType;


class AddStatusToPlanerRequestTbl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('planer_request', function ($table) {
            $table->tinyInteger('status')
            ->default(BidType::STATUS_NEW);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('planer_request', function ($table) {
            $table->dropColumn('status');
        });
    }
}
