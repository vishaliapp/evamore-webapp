<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewEventFieldToPlanerRequestTbl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('planer_request', function ($table) {
            $table->boolean('is_new_event')
                ->default(false);
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('planer_request', function ($table) {
            $table->dropColumn('is_new_event');
        });
    }
}
