<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameUserIdFieldInArtistRequestsTbl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('artist_requests', function (Blueprint $table) {
            $table->renameColumn('user_id', 'artist_user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('artist_requests', function (Blueprint $table) {
            $table->renameColumn('artist_user_id', 'user_id');
        });
    }
}
