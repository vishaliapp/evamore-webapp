<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropPlanerRequestIdFieldInOrderTbl extends Migration
{
    public function up()
    {
        Schema::table('order', function (Blueprint $table) {
            $table->dropForeign('order_planer_request_id_foreign');
            $table->dropColumn('planer_request_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('order', function (Blueprint $table) {
            $table->unsignedInteger('planer_request_id');
            $table->foreign('planer_request_id')->references('id')->on('planer_request');
        });
    }
}
