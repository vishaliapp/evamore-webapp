<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCardTailFieldToPayscapeVaultTbl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('payscape_vault', function (Blueprint $table) {
            $table->char('card_tail', 4)->default(null)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('payscape_vault', function (Blueprint $table) {
            $table->dropColumn('card_tail');
        });
    }
}
