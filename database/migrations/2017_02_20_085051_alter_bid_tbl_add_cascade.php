<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterBidTblAddCascade extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bid', function (Blueprint $table) {
            $table->dropForeign('bid_artist_user_id_foreign');
            $table->foreign('artist_user_id')->references('id')->on('users')
                ->onDelete('cascade');

            $table->dropForeign('bid_event_id_foreign');
            $table->foreign('event_id')->references('id')->on('events')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bid', function (Blueprint $table) {
            $table->dropForeign('bid_artist_user_id_foreign');
            $table->foreign('artist_user_id')->references('id')->on('users');

            $table->dropForeign('bid_event_id_foreign');
            $table->foreign('event_id')->references('id')->on('events');
        });
    }
}
