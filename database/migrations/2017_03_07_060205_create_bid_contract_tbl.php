<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBidContractTbl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bid_contract', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('bid_id');
            $table->string('planer_signature')->default(null);
            $table->string('artist_signature')->default(null);
            $table->text('data')->default(null);
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));

            $table->foreign('bid_id')->references('id')->on('bid');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('bid_contract');
    }
}
