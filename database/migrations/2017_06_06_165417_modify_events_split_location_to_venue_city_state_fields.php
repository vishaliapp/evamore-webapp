<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyEventsSplitLocationToVenueCityStateFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('events', function (Blueprint $table) {
        $table->renameColumn('location', 'venue');
        $table->string('city')->nullable();
        $table->string('state')->nullable();

      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('events', function (Blueprint $table) {
        $table->renameColumn('venue', 'location');
        $table->dropColumn('city');
        $table->dropColumn('state');

      });
    }
}
