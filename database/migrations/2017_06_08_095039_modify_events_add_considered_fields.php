<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyEventsAddConsideredFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('events', function (Blueprint $table) {
        $table->boolean('considered_sound')->default(false);
        $table->boolean('considered_stage')->default(false);
        $table->boolean('considered_lights')->default(false);
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('events', function (Blueprint $table) {
        $table->dropColumn(['considered_sound', 'considered_stage', 'considered_lights']);

      });
    }
}
