<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRequestJsonToTheBidContract extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bid_contract', function (Blueprint $table) {
            $table->text('request_json')->nullable();
            $table->string('file_id')->nullable();
            $table->string('link')->nullable();
            $table->tinyInteger('downloaded_by_artist')->nullable();
            $table->index('link');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bid_contract', function (Blueprint $table) {
            $table->dropColumn('request_json');
            $table->dropColumn('file_id');
            $table->dropColumn('link');
            $table->dropColumn('downloaded_by_artist');
        });
    }
}
