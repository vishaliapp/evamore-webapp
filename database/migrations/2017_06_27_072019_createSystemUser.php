<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSystemUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $system = App\Role::where('name', \App\User::SYSTEM_USER)->first();
        if ( ! $system ) {
            $systemRoleId = \App\Role::insertGetId([
                'name' => \App\User::SYSTEM_USER,
                'description' => 'Evamore System User',
                'title' => 'System',
            ]);
            $systemUser = App\User::where('username', \App\User::SYSTEM_USER)->first();
            if ( ! $systemUser ) {
                $systemUserId = \App\User::insertGetId([
                    'name' => \App\User::SYSTEM_USER_NAME,
                    'username' => \App\User::SYSTEM_USER,
                    'email' => \App\User::SYSTEM_USER . '@evamore.co',
                    'password' => bcrypt( uniqid() ),
                    'active' => 1,
                ]);
                $systemUser = App\User::find($systemUserId);
                $systemUser->roles()->attach($systemRoleId);
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $system = App\Role::where('name', \App\User::SYSTEM_USER)->first();
        $systemUser = App\User::where('username', \App\User::SYSTEM_USER)->first();
        if ($system) {
            if ($systemUser) {
                if ( $systemUser->files && $systemUser->files->count() ) {
                    foreach ($systemUser->files() as $f) {
                        $f->delete();
                    }
                }
                $systemUser->roles()->detach($system->id);
                $systemUser->delete();
            }
            $system->delete();
        }
    }
}
