<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterUsersRemoveUniqueEmail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
	Schema::table('users', function(Blueprint $table)
        {
            $table->dropUnique(['email']);
	    $table->index('email');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //revert index removal
	Schema::table('users', function(Blueprint $table)
        {
	    $table->dropIndex(['email']);	
            $table->unique('email');

        });
    }
}

