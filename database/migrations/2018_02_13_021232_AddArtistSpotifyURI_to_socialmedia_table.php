<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddArtistSpotifyURIToSocialmediaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('socialmedia', function(Blueprint $table) {
            $table->string('spotify_uri')->nullable();
            $table->string('instagram_user')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('socialmedia', function(Blueprint $table) {
            $table->dropColumn('spotify_uri');
            $table->dropColumn('instagram_user');
        });
    }
}
