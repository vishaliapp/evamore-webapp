<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\User;
use App\Social;

class ArtistPopulateSocialmediaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
            $artists = User::all();
            foreach ($artists as $artist) {
                $social = Social::where('user_id', '=', $artist->id)->first();
                if (!$social) {
                   $social = new Social;
                   $social->user_id = $artist->id;
                   $social->save();
                }
            }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
