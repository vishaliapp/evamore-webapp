<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStateToUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
            Schema::table('artists', function (Blueprint $table) {
                $table->string('statetown')->after('hometown');;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
            Schema::tables('artists', function (Blueprint $table) {
                $table->string('statetown');
        });
    }
}
