<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropConsideredColums extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::table('events', function ($table) {
		    $table->dropColumn('considered_sound');
		    $table->dropColumn('considered_lights');
		    $table->dropColumn('considered_stage');
	    });
    }
}
