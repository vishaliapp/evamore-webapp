<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddArtistMileageColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::table('artists', function (Blueprint $table) {
		    $table->string('mileage')->default(null);
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::tables('artists', function (Blueprint $table) {
		    $table->dropColumn('mileage');
	    });
    }
}
