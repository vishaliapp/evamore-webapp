<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLatlngToArtists extends Migration
{
    public function up()
    {
        Schema::table('artists', function($table) {
            $table->decimal('place_lng', 11, 8)->after('hometown');
            $table->decimal('place_lat', 10, 8)->after('hometown');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('artists', function($table) {
            $table->dropColumn('place_lat');
            $table->dropColumn('place_lng');
        });
    }
}
