<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ForwardPaymentToMail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('forwardPayment', function (Blueprint $table) {
            $table->increments('id');
            $table->string('token');
            $table->integer('bidId')->unsigned();
            $table->string('mail');
            $table->boolean('isActive')->default(1);
            $table->timestamps();
        });
        Schema::table('forwardPayment', function($table) {
            $table->foreign('bidId')->references('id')->on('bid');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('forwardPayment');
    }
}
