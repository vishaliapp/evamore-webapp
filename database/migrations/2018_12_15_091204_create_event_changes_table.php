<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventChangesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_changes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('event_id')->unsigned();
            $table->foreign('event_id')->references('id')->on('events');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->integer('artist_id')->unsigned();
            $table->foreign('artist_id')->references('id')->on('artists');
            $table->string('name');
            $table->string('description')->nullable();
            $table->dateTime('starttime');
            $table->dateTime('endtime')->nullable();
            $table->integer('capacity')->unsigned()->nullable();
            $table->string('venue')->default(null);
            $table->string('address')->default(null);
            $table->decimal('place_lng', 11, 8);
            $table->decimal('place_lat', 10, 8);
            $table->string('artist_type')->nullable();
            $table->integer('min_budget')->unsigned();
            $table->integer('max_budget')->unsigned();
            $table->string('event_type');
            $table->boolean('contact_me');
            $table->integer('duration');
            $table->boolean('sound_production')->default(0);
            $table->unsignedTinyInteger('status')->default(0);
            $table->unsignedTinyInteger('time_zone');
            $table->string('city')->nullable();
            $table->string('state')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('event_changes');
    }
}
