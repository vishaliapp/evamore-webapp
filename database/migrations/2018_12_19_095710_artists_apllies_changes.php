<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ArtistsAplliesChanges extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('artists_applies_changes',function (Blueprint $table){
            $table->increments('id');
            $table->boolean('apply')->default(0);

            $table->integer('artist_id')->unsigned()->index();
            $table->foreign('artist_id')->references('id')->on('artists')->onDelete('cascade');


            $table->integer('event_change_id')->unsigned()->index();
            $table->foreign('event_change_id')->references('id')->on('event_changes')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('artists_applies_changes');
    }
}
