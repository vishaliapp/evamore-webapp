<?php

use Illuminate\Database\Seeder;
use App\EventType;

class EventTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $types = ['Wedding', 'Corporate Event', 'Private Party', 'College Event'];

        foreach($types as $typeName) {
            $type = EventType::where('name', $typeName)->first();
            if ($type) continue;

            $type = new EventType();
            $type->name = $typeName;
            $type->save();
        }

        $existedEventTypes = EventType::all();

        foreach($existedEventTypes as $existedEventType) {
            if (!in_array($existedEventType->name, $types)) {
                $existedEventType->delete();
            }
        }
    }
}
