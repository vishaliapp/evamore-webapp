<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Genre;


class GenreTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $genres = ["DJ", "Acoustic", "Country", "Dance", "Electronic",
            "Folk", "Funk", "Hip-Hop", "Jazz", "Pop", "Rock",
            "R&B","Reggae","Bluegrass","Specialty Acts","Singer-Songwriter", "Latin", "Classical", "Instrumental"];

        foreach($genres as $genreName) {
            $g = Genre::where('name', $genreName)->first();
            if ($g) continue;

            $g = new Genre();
            $g->name = $genreName;
            $g->save();
        }

        $existedGenres = Genre::all();

        foreach($existedGenres as $existedGenre) {
            if (!in_array($existedGenre->name, $genres)) {
                $existedGenre->delete();
            }
        }
    }
}
