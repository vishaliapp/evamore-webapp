<?php

use Illuminate\Database\Seeder;
use App\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            [
                'name' => 'FAN',
                'title' => 'Fan',
                'description' => 'EVAmore planer user'
            ],[
                'name' => 'ARTIST',
                'title' => 'Artist',
                'description' => 'EVAmore artist user'
            ],[
                'name' => 'ADMIN',
                'title' => 'Admin',
                'description' => 'Admin'
            ],[
                'name' => 'ARTIST_MANAGER',
                'title' => 'Artist Manager',
                'description' => 'Artist Manager'
            ],
        ];

        foreach($roles as $roleData) {
            $role = Role::getRole($roleData['name']);
            if (!$role) {
                $role = new Role();
                $role->name = $roleData['name'];
            }
            $role->title = $roleData['title'];
            $role->description = $roleData['description'];
            $role->save();
        }
    }

}
