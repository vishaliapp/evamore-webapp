<?php

use Illuminate\Database\Seeder;
use App\SongType;

class SongTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $types = ['All cover set', 'Some covers', 'No covers'];

        foreach($types as $typeName) {
            $type = SongType::where('name', $typeName)->first();
            if ($type) continue;

            $type = new SongType();
            $type->name = $typeName;
            $type->save();
        }

        $existedSongTypes = SongType::all();

        foreach($existedSongTypes as $existedSongType) {
            if (!in_array($existedSongType->name, $types)) {
                $existedSongType->delete();
            }
        }
    }
}
