<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;


class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $userService = App::make("userService");
        $user_admin = $userService->getUserByUsername("eva");
        if(!$user_admin){
            $user_admin = new User();
            $user_admin->name  = 'Eva More';
            $user_admin->username = 'eva';
            $user_admin->email = 'booleanoperator@gmail.com';
            $user_admin->password = bcrypt('test');
            $user_admin->phone = '6154170024';
            $user_admin->save();

            $role_admin = Role::where('name', 'Admin')->first();
            $user_admin->roles()->attach($role_admin);
        }
	
	$user_admin = $userService->getUserByUsername("mikhail");
        if(!$user_admin){
            $user_admin = new User();
            $user_admin->name  = 'Mikhail Kozorovitskiy';
            $user_admin->username = 'mikhail';
            $user_admin->email = 'mikhail@thecrazyrussian.com';
            $user_admin->password = bcrypt('testtest');
            $user_admin->phone = '6154170024';
            $user_admin->save();

            $role_admin = Role::where('name', 'Admin')->first();
            $user_admin->roles()->attach($role_admin);
        }

    }
}
