
var Demo = (function() {

    function popupResult(result) {
        console.log(result);
        $('#myModal').modal('hide');
        var imageSrc = result.src;

        $('#user-avatar').attr('src', imageSrc);
        $('#image_base_64').val(imageSrc);


        // var html;
        // if (result.html) {
        //     html = result.html;
        // }
        // if (result.src) {
        //     html = ' \'<img src="\' + result.src + \'" />\'';
        // }
        // swal({
        //     title: '',
        //     content: false,
        //     text: html,
        //     allowOutsideClick: true
        // });
        // setTimeout(function(){
        //     $('.sweet-alert').css('margin', function() {
        //         var top = -1 * ($(this).height() / 2),
        //             left = -1 * ($(this).width() / 2);
        //
        //         return top + 'px 0 0 ' + left + 'px';
        //     });
        // }, 1);
    }

    function demoUpload() {
        var $uploadCrop;

        function readFile(input) {
            if (input.files && input.files[0]) {

                var reader = new FileReader();
                $('#myModal').modal('show');

                reader.onload = function (e) {
                    $('#myModal').on('shown.bs.modal', function(){
                        $('.upload-demo').addClass('ready');

                        $uploadCrop.croppie('bind', {
                            url: e.target.result
                        })

                        var x = $uploadCrop.croppie('result', 'html').then(function(html) {
                            // html is div (overflow hidden)
                            // with img positioned inside.
                            console.log(html);
                        });
                        console.log(x);
                    });
                }

                reader.readAsDataURL(input.files[0]);
            }
            else {
                swal("Sorry - you're browser doesn't support the FileReader API");
            }
        }
        $('#myModal').on('shown.bs.modal', function(){
            $("#upload").val('')
        })
        $('.js-main-image').on('click', function (ev) {
            mc.croppie('result', {
                type: 'rawcanvas',
                circle: true,
                // size: { width: 300, height: 300 },
                format: 'png'
            }).then(function (canvas) {
                popupResult({
                    src: canvas.toDataURL()
                });
            });
        });
        var $avatarUploader = $("#avatarUploader");

        $('#avatarUploaderImageClean').on('click', function () {
            if ($avatarUploader !== undefined) {
                // console.log(12312);
                $('input[name=image_]', $avatarUploader).val("");
                $('img.picture-element-image').attr('src', '/images/drag_drop_white.png')
            }
            return false;
        });

        $uploadCrop = $('#upload-demo').croppie({
            enableExif: true,
            viewport: {
                width: 300,
                height: 300,
                type: 'circle'
            },
            boundary: {
                width: 824,
                height: 700
            },
            enableZoom:true
        });

        $('#upload').on('change', function () { readFile(this); });
        $('.upload-result').on('click', function (ev) {
            $uploadCrop.croppie('result', {
                type: 'canvas',
                size: 'viewport'
            }).then(function (resp) {
                popupResult({
                    src: resp
                });
            });
        });
    }

    function init() {
        demoUpload();
    }

    return {
        init: init
    };
})();


// Full version of `log` that:
//  * Prevents errors on console methods when no console present.
//  * Exposes a global 'log' function that preserves line numbering and formatting.
(function () {
    var method;
    var noop = function () { };
    var methods = [
        'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
        'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
        'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
        'timeStamp', 'trace', 'warn'
    ];
    var length = methods.length;
    var console = (window.console = window.console || {});

    while (length--) {
        method = methods[length];

        // Only stub undefined methods.
        if (!console[method]) {
            console[method] = noop;
        }
    }


    if (Function.prototype.bind) {
        window.log = Function.prototype.bind.call(console.log, console);
    }
    else {
        window.log = function() {
            Function.prototype.apply.call(console.log, console, arguments);
        };
    }
})();
