<style>
#JtuyoshiCrop #Painel{font-size:12px;padding-bottom:10px;margin:0}
#JtuyoshiCrop #Principal{position:relative;margin:0}
#JtuyoshiCrop #SelecaoRecorte{position:absolute;background-color:#FFF;border:2px #333 dotted;opacity:0.5;
    -webkit-border-radius: 50%;
    -moz-border-radius: 50%;
    border-radius: 50%;
}
#JtuyoshiCrop .row{
  padding: 0.5em 1em;
}
</style>
<script>

</script>
<div id="Painel">

<div class="row">
  <div class="col-xs-2">

  	<select id="SelectOrientacao" title="Choose the Orientation of the cutout area"  class="form-control">    	
    </select>  	

  </div>
  <div class="col-xs-2">
  	<select id="SelectProporcao" title="Choose the Aspect Ratio of selection"  class="form-control">    	
    </select>
  </div>   
   <div class="col-xs-12 col-md-10">
		<div class="btn-group">
<!--			<button id="button_crop_recortar" class="btn btn-default">Crop</button>-->
			<button id="button_crop_original" class="btn btn-primary btn-invert">Set My Picture</button>
		</div>
   </div>
   
</div>

</div>
<div id="Principal">
	<div id='SelecaoRecorte'></div>
</div>