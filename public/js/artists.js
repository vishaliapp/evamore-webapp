window.artists = {}; //GLOBAL ARTIST ARRAY
window.log = false;

//GET ARTISTS
(function(){

    $.fn.artistsDetails = function(){

        var self = this;
        var $artistInfoModal = $('#artistInfo');
        self.$nextBtn = $(".modal-button-next", $artistInfoModal);
        self.$prevBtn = $(".modal-button-prev", $artistInfoModal);

        self.usersIdsOrder = [];

        var init = function() {
            loadArtists();
            initModals();

            self.$nextBtn.hide();
            self.$prevBtn.hide();
            $.each([self.$nextBtn, self.$prevBtn], function(){
                var $this = $(this);
                $this.on('click', function(event){
                    var $button = $(event.target);
                    selectArtist($button);
                });
                $('span', $this).on('click', function(e){
                    e.stopPropagation();
                })
            })
        };

        var loadArtists = function() {
            $.ajax({
                url: "artists/active/json/get",
                type: "GET",
                dataType: "json",
                success: function (response) {
                    window.log = response.is_auth;

                    for (var i=0; i<response.artists.length; i++) {
                        self.usersIdsOrder.push(response.artists[i].id);
                    }

                    var artistData = artistHTML(response.artists, [], response.is_auth);

                    $("#artistData").html(artistData);
                },
                error: function (response) {
                }
            });
        };

        var loadArtistData = function (id, callback) {
            $.ajax({
                url: "api/v1/artists/"+id,
                type: "GET",
                dataType: "json",
                success: function(response) {
                    callback(response);
                },
                error: function(response) {
                }
            });
        };

        var renderArtist = function(artist_id, callback) {
            loadArtistData(artist_id, function(response){
                if (response.success) {
                    var template = _.template($('#artist-in-modal').text());
                    var content = template({
                        auth: response.auth,
                        user: response.user
                    });
                    var $b = $('.modal-body', $artistInfoModal);
                    $b.html(content);
                    if (response.user.channel_name) {
                        renderYoutubeVideosList(response.user.channel_name, $("#modal-artist-"+response.user.id+" .youtube_videos ul"), 2);
                    }
                    callback();
                }
            });
        };

        var selectArtist = function($clickedElement) {
            var artist_id = $clickedElement.attr('data-artist_id');
            if (!!artist_id) {
                renderArtist(artist_id, function(){
                    self.enableNavBtns(artist_id);
                });
            }
        };

        var initModals = function(){

            $artistInfoModal.on('shown.bs.modal', function (event) {
                var $button = $(event.relatedTarget) // Button that triggered the modal
                selectArtist($button);
            });

            $artistInfoModal.on('hide.bs.modal', function(){
                $('.modal-body', $artistInfoModal).html("");
            });
        };

        self.enableNavBtns = function(current_artist_id) {
            self.enableNextBtn(current_artist_id);
            self.enablePrevBtn(current_artist_id);

            var $b = $('.modal-body', $artistInfoModal);
            var $buttons = $('.modal-button', $artistInfoModal);
            $buttons.height($b.height());
            $('span', $buttons).css("margin-top", ($b.height()-20)/2);
        };

        self.enableNextBtn = function(current_artist_id) {
            var artist_index = self.usersIdsOrder.indexOf(parseInt(current_artist_id));
            if (artist_index >=0 && artist_index < self.usersIdsOrder.length-1){
                self.$nextBtn.attr('data-artist_id', self.usersIdsOrder[artist_index+1]);
                self.$nextBtn.show();
            } else {
                self.$nextBtn.hide();
            }
        };

        self.enablePrevBtn = function(current_artist_id) {
            var artist_index = self.usersIdsOrder.indexOf(parseInt(current_artist_id));
            if (artist_index > 0 && artist_index <= self.usersIdsOrder.length){
                self.$prevBtn.attr('data-artist_id', self.usersIdsOrder[artist_index-1]);
                self.$prevBtn.show();
            } else {
                self.$prevBtn.hide();
            }
        };

        init();
    };

    $("#artistData").artistsDetails();
    //return false;
}());



/* Update this function to work with and without a filter
 * If filter is [] or the artist has a genre in the filter array (array intersect genres/filters and result size > 0), append to artist card
 * Else continue	
 */
function artistHTML(data, filter, auth){
    if (data.hasOwnProperty('artists')){
        artists = data.artists;
    } else {
        artists = data;
    }

    var artistCards = "";

    var loginWindowData =  auth ? "" : 'data-toggle="modal" data-target="#popup-login"';
    for (i in artists){
        var url = auth ? "/artists/" + artists[i].id : "#"

        if (filter.length == 0 || array_intersect(artists[i].genre.split(","), filter)){
            artistCards += '<li class="artist">';
            artistCards += '<div class="artist-image"><img src="/uploads/avatars/' + artists[i].avatar + '" alt=""  data-toggle="modal" data-target="#artistInfo" data-artist_id="'+artists[i].id+'"/></div>';
            artistCards += '<a href="#" data-toggle="modal" data-target="#artistInfo" data-artist_id="'+artists[i].id+'" class="artist-name" >' + artists[i].name.toUpperCase()+ '</a>';

            if (artists[i].sounds_like){
                artistCards += '<p>Sounds like ' + artists[i].sounds_like + '</p>';
            }
            artistCards += '</li>';
        }
    }
    return artistCards;
}




