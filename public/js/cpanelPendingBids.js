/**
 * Created by ruslan on 09.01.17.
 */

(function($){
    $.fn.pendingBids = function(options){
        var self = this;

        self.settings = {
            bids_selector: '.bids',
            bids_container: $(".event-bids"),
            bibsGridTemplate: '',
            bibPriceFormTemplate: '',
            bibArtistFeeFormTemplate: '',
            form_modal: null
        };
        $.extend(self.settings, options);

        self.currentBidsButtonPosition = {left: 0, top:0}

        var activateBidButtons = function(){
            var $bidsButtons = $(self.settings.bids_selector, self);
            $bidsButtons.on('click', function(){
                var $btn = $(this);
                self.currentBidsButtonPosition = $btn.offset();
                var eventId = $btn.attr("data-event-id");
                if (eventId > 0) {

                    $(".event-box").css({opacity: 0.5});
                    $btn.parents('.event-box').css({opacity: 1});

                    loadArtistsRequests(eventId, renderBidsGrid);
                }
            });
        };

        var loadArtistsRequests = function(eventId, callback) {
            $.ajax({
                url: "/cpanel/api/pendingbids/"+eventId,
                type: "GET",
                dataType: "json",
                data: {},
                success: function(response)
                {
                    if (response.success) {
                        callback(response.bids, response.planer_user);
                    }
                }
            })
        };

        var loadArtistRequest = function(bidId, callback) {
            $.ajax({
                url: "/cpanel/api/pendingbid/"+bidId,
                type: "GET",
                dataType: "json",
                data: {},
                success: function(response)
                {
                    if (response.success) {
                        callback(response.bid, response.planer_user);
                    }
                }
            })
        };

        var renderBidsGrid = function(bids, planer, data){
            var $gridHtml = $(self.settings.bibsGridTemplate({
                bids: bids,
                planer: planer
            }));

            //var $m = $("#baseModal").clone().addClass('event-bids').modal({show:false});
            //$(".modal-dialog", $m).addClass("modal-lg")
            //
            //$(".modal-content", $m).html($gridHtml);
            //
            //$m.modal('show');

            var browserDimensions = getBrowserDimensions();

            var topPos = self.currentBidsButtonPosition.top + 50;
            self.settings.bids_container.hide();
            self.settings.bids_container.html($gridHtml)
            var m_width = self.settings.bids_container.width();

            $('.main').css('margin-bottom', self.settings.bids_container.height())

            m_width = browserDimensions.window.width*0.9;
            $gridHtml.css('width', m_width);
            setTimeout(function(){
                self.settings.bids_container.css({
                    'position': 'absolute',
                    'top': topPos + "px",
                    'left': ((browserDimensions.window.width - m_width) / 2) + "px",
                    'z-index': 1000,
                    'width': m_width
                });
                self.settings.bids_container.show();
            }, 0);

            var toolbox = $('.bids-grid-window', $gridHtml),
                height = self.settings.bids_container.height();//toolbox.height(),
                scrollHeight = toolbox.get(0).scrollHeight;

            toolbox.bind('mousewheel', function(e, d) {
                if((this.scrollTop === (scrollHeight - height) && d < 0) || (this.scrollTop === 0 && d > 0)) {
                    e.preventDefault();
                }
            });

            var $subWin = $('<div></div>').addClass("modal-backdrop fade in").css({'opacity': '0', 'z-index':900});
            $('body').append($subWin);

            $(".price-form-btn", $gridHtml).on('click', function(){
                var $this = $(this);
                var bidId = $this.attr('data-bid-id');
                loadArtistRequest(bidId, showBidPriceForm);
                return false;
            });

            $(".artist-fee-form-btn", $gridHtml).on('click', function(){
                var $this = $(this);
                var bidId = $this.attr('data-bid-id');
                loadArtistRequest(bidId, showBidArtistFeeForm);
                return false;
            });

            $("button.close", $gridHtml).on('click', function(){
                clearContainer();
                $('.main').css('margin-bottom', 0)
                $subWin.remove()
                return false;
            });

            $subWin.on('click', function(){
                clearContainer();
                $('.main').css('margin-bottom', 0)
                $subWin.remove()
                return false;
            });

            $('.notifyEventPlanerForm', $gridHtml).submit(function() {
                $("tr", $gridHtml).removeClass('broken');
                notifyEventPlaner($(this), 0);
                return false;
            });
        };

        var notifyEventPlaner = function($form, isAccepted){
            var options = {
                url: "/cpanel/api/pendingbid/notifyplaner",
                dataType: 'json',
                type: 'post',
                beforeSubmit:  function(formData, jqForm, options){
                    formData.push({
                        name: "accepted",
                        required: false,
                        type: "text",
                        value: isAccepted
                    });
                    return true;
                },
                success: function(response, statusText, xhr, $form){
                    if (response.success) {
                        var tpl = _.template($("#success-bids-alert-1").text());
                        var $content = $(tpl());

                        $(".modal-content", self.settings.form_modal).html($content);

                        $('form', $content).submit(function() {});

                        self.settings.form_modal.modal('show');

                    } else {
                        var tpl = _.template($("#broken-bids-alert-1").text());
                        var $content = $(tpl({
                            event: response.event
                        }));

                        $(".modal-content", self.settings.form_modal).html($content);

                        $('form', $content).submit(function() {
                            notifyEventPlaner($(this), 1);
                            return false;
                        });

                        if (response.message != ''){
                            $('.error-message', $content).html(response.message);
                        }

                        self.settings.form_modal.modal('show');

                        _.each(response.broken_bids, function(id){
                            $("#bid-"+id).addClass('broken');
                        });
                    }
                }  // post-submit callback
            };


            $form.ajaxSubmit(options);
        };

        var showBidPriceForm = function(bid, planer) {
            var $formHtml = $(self.settings.bibPriceFormTemplate({
                bid: bid,
                planer: planer
            }));

            $(".modal-content", self.settings.form_modal).html($formHtml);

            var options = {
                //target:        '#output1',   // target element(s) to be updated with server response
                beforeSubmit:  function(){
                    $(".error-message", self.settings.form_modal).text("")
                },
                url: '/cpanel/api/pendingbid/price',
                type: 'post',
                dataType: 'json',
                success:       function(response, statusText, xhr, $form){
                    if (response.success) {
                        loadArtistsRequests(response.bid.event_id, renderBidsGrid);
                        self.settings.form_modal.modal('hide');

                    } else {
                        $(".error-message", self.settings.form_modal).text(response.message)
                    }
                }
            };

            self.settings.form_modal.modal('show');

            $("form", self.settings.form_modal).ajaxForm(options);;
        };

        var showBidArtistFeeForm = function(bid, planer) {
            var $formHtml = $(self.settings.bibArtistFeeFormTemplate({
                bid: bid,
                planer: planer
            }));

            $(".modal-content", self.settings.form_modal).html($formHtml);

            var options = {
                //target:        '#output1',   // target element(s) to be updated with server response
                beforeSubmit:  function(){
                    $(".error-message", self.settings.form_modal).text("")
                },
                url: '/cpanel/api/pendingbid/artistfee',
                type: 'post',
                dataType: 'json',
                success:       function(response, statusText, xhr, $form){
                    if (response.success) {
                        loadArtistsRequests(response.bid.event_id, renderBidsGrid);
                        self.settings.form_modal.modal('hide');
                    } else {
                        $(".error-message", self.settings.form_modal).text(response.message)
                    }
                }
            };

            self.settings.form_modal.modal('show');

            $("form", self.settings.form_modal).ajaxForm(options);;
        };

        var clearContainer = function() {
            self.settings.bids_container.html("");
            $(".event-box").css({opacity: 1});
        };

        var init = function(){
            activateBidButtons();
        };

        init();
    }
})(jQuery);
