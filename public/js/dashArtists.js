(function($){

    $.fn.artists_20 = function(options){
        var self = this;

        self.settings = {};
        $.extend(self.settings, options);

        self.artists = [];
        self.genresFilters = [];
        self.feeFilter = null;

        var renderArtists = function() {
            var $artistsContainer = $("ul.artists", self);
            $artistsContainer.html("");

            var template = _.template($('#artist-details-1').text());

            for (i in self.artists){
                if ((self.feeFilter==null || parseInt(self.artists[i].fee)==self.feeFilter) && (self.genresFilters.length == 0 || array_intersect(self.artists[i].genre.split(","), self.genresFilters))) {
                    var content = template({
                        artist: self.artists[i],
                        status: self.settings.status
                    });
                    var $li = $('<li class="artist"></li>').append(content)
                    $artistsContainer.append($li)
                }
            }
        };

        var uploadArtists = function(){
            $.ajax({
                url: "/cpanel/artists/json",
                type: "GET",
                dataType: "json",
                data: {
                    status: self.settings.status
                },
                success: function(response)
                {
                    if (response.success) {
                        $("#genre-"+self.settings.defaultSelectedGenreId).prop( "checked", true );

                        self.artists = response.artists;
                        renderArtists();
                    }
                }
            });
        };

        var initGenreFilter = function() {

            var $items = $("input[type=checkbox]", self);

            $items.on('change',function(){
                self.genresFilters = [];

                $items.each(function(){
                    if(this.checked){
                        self.genresFilters.push($(this).val());
                    }
                });

                renderArtists();
            });
        };

        var initFeeFilter = function(){
            //FILTER ARTIST BY FEE
            if (self.settings.feeSelector) {
                self.settings.feeSelector.on('change', function () {
                    var fee = $('option:selected', $(this)).val();

                    self.feeFilter = (fee > 0) ? fee : null;
                    renderArtists();
                });
            }
        };

        var init = function(){
            uploadArtists();
            initGenreFilter();
            initFeeFilter();
        };

        init();
    };

})(jQuery);