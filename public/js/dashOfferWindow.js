(function($){

    $.fn.offerWindow = function(options){
        var self = this;

        self.settings = {
            'modal': null,
            'offerDetailsTemplate': '',
            'afterShowModal': function(modal, bid, artistUser) {}
        };
        $.extend(self.settings, options);

        var activateOfferButtons = function() {
            $(self).on('click', function(){
                var bidId = $(this).attr("data-bid-id");
                getOfferInfo(bidId, showOfferModal);
            })
        };

        var getOfferInfo = function(bidId, callback){
            $.ajax({
                url: "/api/bid/details/"+bidId,
                type: "GET",
                dataType: "json",
                data: {},
                success: function(response)
                {
                    if (response.success) {
                        callback(response.bid, response.artistUser, response.planerUser);
                    }
                }
            })
        };

        var showOfferModal = function(bid, artistUser, planerUser) {
            var $html = self.settings.offerDetailsTemplate({
                bid: bid,
                artistUser: artistUser
            });
            $(".modal-content", self.settings.modal).html($html);

            self.settings.afterShowModal(self.settings.modal, bid, artistUser, planerUser);

            self.settings.modal.modal('show');
            return false;
        };


        var init = function(){
            activateOfferButtons();
        };
        init();
    };

    $.fn.contractForm = function(options) {
        var self = this;

        self.settings = {
            'bid': null,
            'artistUser': null,
            'planerUser': null,
            'afterShowModal': function(modal, bid, artistUser) {},
            'modal': null,
            confirmContractCallback: function(){}

        };
        $.extend(self.settings, options);

        var showPerformanceContract = function($m){
            var tpl = _.template($('#performance-contract-1').text());

            var $html = tpl({
                bid: self.settings.bid,
                artistUser: self.settings.artistUser,
                planerUser: self.settings.planerUser
            });

            //$(".modal-dialog", $m).addClass('modal-lg');
            $(".modal-content", $m).html($html);

            $m.modal('show');

            $m.on('shown.bs.modal', function (e) {
                $("body").addClass("modal-open");
            });

            $m.on('hidden.bs.modal', function (e) {
                $(".btn-agree", $m).unbind("click");
                $(".btn-cancel", $m).unbind("click");
                $("body").removeClass("modal-open");
            });

            $(".btn-agree", $m).on('click', function(){
                sendContractConfirmation($m, self.settings.confirmContractCallback);
                //$("form", self.settings.modal).submit();
                //$m.modal('hide');
                $(".btn-agree", $m).unbind("click");
            });

            $(".btn-cancel").on('click', function(){
                $m.modal('hide');
                //self.settings.modal.modal('hide');
                //$(".modal-body", self.simleModal).html("Your payment was cancelled.");
                //self.simleModal.modal('show');
            });

            return false;
        };

        var sendContractConfirmation = function($m, successCallback){
            var options = {
                //target:        '#output1',   // target element(s) to be updated with server response
                beforeSubmit:  function(formData, jqForm, options){
                    $(".error-message", $m).html("");
                },
                url: '/api/contract/confirm',
                type: 'post',
                dataType: 'json',
                success:       function(response, statusText, xhr, $form){
                    if (response.success) {
                        $m.modal('hide');
                        //$(".modal-body", self.simleModal).html("Payment processed successfully.");
                        //self.simleModal.modal('show');
                        successCallback();
                    } else {
                        var keys = Object.keys(response.errors)
                        for (var i=0; i<keys.length; i++) {
                            $(".error-"+keys[i], $m).append(response.errors[keys[i]]);
                            if (keys[i] == "global"){
                                $(".error-"+keys[i], $m).css({"background": "#FFaaaa"});
                            }
                        }
                    }
                }
            };

            $("form", $m).submit(function() {
                $(this).ajaxSubmit(options);
                return false;
            });
        };

        var init = function() {

            $("a",  self).on('click', function(e){
                e.preventDefault();
                swal({
                    title: 'Are you sure you want to book this artist for your event?',
                    text: 'By clicking yes, you are confirming this artist and all other bids will be removed.',
                    icon: "warning",
                    buttons: [true, 'Yes'],
                    dangerMode: true,
                    iconColor: '#333'
                })
                    .then(function(agree)  {
                        if (agree) {
                            var $m = $("#baseModal").clone().addClass('modal-terms').modal({show: false});

                            showPerformanceContract($m);
                            self.settings.afterShowModal($m, self.settings.bid, self.settings.artistUser);

                            return false;
                        } else {
                            // swal("Your imaginary file is safe!");
                            $('.decline').click();
                        }
                    });
            });
        };

        init();
    };

    $.fn.paymentForm = function(options) {
        var self = this;

        self.settings = {
            template: '',
            bid: null,
            artistUser: null,
            planerUser: null,
            afterInitCallback: function(){},
            'afterShowModal': function(modal, bid, artistUser) {}
        };
        $.extend(self.settings, options);

        self.simleModal = $("#popup-finish-payment").modal({show:false});

        var selectors;

        var showPaymentForm = function(){
            var $html = self.settings.template({
                bid: self.settings.bid,
                artistUser: self.settings.artistUser,
                planerUser: self.settings.planerUser
            });
            $(".modal-content", self).html($html);

            selectors = $("select", self).selectpicker({
                with: '100%'
            });
            self.modal('show');
            self.settings.afterShowModal(self, self.settings.bid, self.settings.artistUser);
            $("input[name=cardNumber]", self).mask("9999 9999 9999 9999");
            $("input[name=amexCardNumber]", self).mask('9999 999999 99999');

            enableFields();

            $("input[name=payMethod]", self).on("change", function(){
                enableFields();
            });

            var $saveCardCheckbox = $("#saveCardCheckbox");
            var $automaticalyPayment = $("#automaticalyPayment");
            $("input[type=text]", self).keyup(function(){
                if(isCardInfoFill()) {
                    enableFields();
                } else {
                    $saveCardCheckbox.prop('checked', false).prop( "disabled", true);
                    $automaticalyPayment.prop('checked', false).prop( "disabled", true);
                }
            });

            $("input[name=payAmount]:radio", self).change(function () {
                if ($(this).val() == 2) { //  App\ModelType\BidType::FULL_AMOUNT
                    $automaticalyPayment.prop('checked', false).prop( "disabled", true);
                } else {
                    enableFields();
                }
            });

            $saveCardCheckbox.on("change", function(){
                enableFields()
            });
            $('[data-toggle="tooltip"]', self).tooltip();
            return false;
        };

        var enableFields = function() {
            var $usedCardRadio = $("#payMethod-1", self);
            var $newCardRadio = $("#payMethod-2", self);
            var $cardInfoFields = $(".card-info", self);
            console.log($usedCardRadio, $newCardRadio);

            var $automaticalyPayment = $("#automaticalyPayment");
            var $saveCardCheckbox = $("#saveCardCheckbox");

            //existed card selected
            if ($usedCardRadio.is(':checked')) {
                //Hiding fields
                $('.amex').css({display: 'none'});
                $('.other').css({display: 'none'});
                $('.exp-date').css({display: 'none'});
                $('.payment_method').css({display: 'none'});
                $("#payment-type-change").val('default').selectpicker("refresh");

                _.each($cardInfoFields, function(e){
                    var $e = $(e);
                    var type = $e.prop("type");
                    if (type == "text"){
                        $e.val("")
                    }
                    if (type == "checkbox"){  // Save this to your account
                        $e.prop( "checked", false)
                    }
                });

                $("#expirationMonth", self).selectpicker('val',1);
                var now = new Date(); var yearFrom = now.getFullYear();
                $("#expirationYear", self).selectpicker('val',yearFrom);
                $cardInfoFields.prop( "disabled", true);
                $("button[data-id=expirationMonth], button[data-id=expirationYear]", self)
                    .css({'background':"#EEEEEE"});

                $automaticalyPayment.prop( "disabled", false)
            }

            //new card selected
            if ($newCardRadio.is(':checked')) {
                $('.payment_method').css({display: 'table-row'});

                $("button[data-id=expirationMonth], button[data-id=expirationYear]", self)
                    .css({'background':"#FFFFFF"});
                $cardInfoFields.prop( "disabled", false );
                $automaticalyPayment.prop("disabled", false);
                if(!isCardInfoFill()) {
                    $saveCardCheckbox.prop('checked', false).prop( "disabled", true);
                    $automaticalyPayment.prop('checked', false).prop("disabled", true);
                }
                if(isCardInfoFill() && $saveCardCheckbox.is(':checked')) {
                    $automaticalyPayment.prop('checked', false).prop( "disabled", false);
                } else {
                    $automaticalyPayment.prop('checked', false).prop( "disabled", true);
                }
            }
        };
        var isCardInfoFill = function() {
            //card number
            let vCardNumber;
            let vCvc;
            if (paymentMethod == 'amex') {
                //Card Number
                vCardNumber = $('#amexCardNumber').val();
                //CVC
                vCvc = ($("#cvv", self).val().length == 4) ? true : false;
            } else {
                //Card Number
                vCardNumber = $('#cardNumber').val();

                //CVC
                vCvc = ($("#cvc", self).val().length == 3) ? true : false;
            }

            return vCardNumber && vCvc;
        };

        var sendFormConfig = function() {
            var options = {
                target:        '#output1',   // target element(s) to be updated with server response
                beforeSubmit:  function(formData, jqForm, options){
                    var parts = [];
                    var stringCardNumber;
                    if (paymentMethod == 'amex') {
                        stringCardNumber = $('#amexCardNumber').val()
                    } else {
                        stringCardNumber = $('#cardNumber').val();
                    }

                    if (stringCardNumber != ''){
                        stringCardNumber = stringCardNumber.replace(/\s/g, '');
                        formData.push({
                            name: "cardNumber",
                            required: false,
                            type: "text",
                            value: stringCardNumber
                        });
                    }
                    formData.push({
                        name: 'paymentMethod',
                        required: true,
                        type: 'text',
                        value: paymentMethod
                    })

                    $(".error-message", self).text("");
                    $(".error-message.error-global", self).css({'background': "none"});

                    return true;
                },
                url: '/api/pay/bid',
                type: 'post',
                dataType: 'json',
                beforeSend: function () {
                    $('.loading').css({display: 'block'});
                },
                success: function(response, statusText, xhr, $form){
                    $('.loading').css({display: 'none'});
                    if (response.success) {
                        swal({
                            title: 'Payment processed successfully.',
                            icon: "success",
                        })
                            .then(function success () {
                                self.modal('hide');
                            });
                        // $(".modal-body", self.simleModal).html("Payment processed successfully.");
                        // self.simleModal.modal('show');
                    } else {
                        var keys = Object.keys(response.errors)
                        for (var i=0; i<keys.length; i++) {
                            $(".error-"+keys[i], self).append(response.errors[keys[i]]);
                            if (keys[i] == "global"){
                                $(".error-"+keys[i], self).css({"background": "#FFaaaa"});
                            }
                        }
                    }
                }
            };
            $("form", self).ajaxForm(options);
        };

        var init = function() {
            showPaymentForm();
            sendFormConfig();
        };

        init();

    }


    //Change Payment Method
    var paymentMethod = 'other';
    $(document).on('change','#payment-type-change',function(){
        if ($(this).val() == 'amex') {
            $('.amex').css({display: 'table-row'});
            $('.other').css({display: 'none'});
            $('.exp-date').css({display: 'table-row'});
            paymentMethod = 'amex';
        } else {
            $('.amex').css({display: 'none'});
            $('.other').css({display: 'table-row'});
            $('.exp-date').css({display: 'table-row'});
            paymentMethod = 'other';
        }
    })
})(jQuery);