window.artists = {}; //GLOBAL ARTIST ARRAY
window.log = false;

//GET ARTISTS
(function(){
  $.ajax({
    url: "artists/active/json/gett",
    type: "GET",
    dataType: "json",
    success: function(result)
    {
      log = true;
      artistData = artistHTML(result, [], log);

      $("#requestedArtistData").html(artistData);
			$("#upcomingArtists").html(result.number);
    },
    error: function(result)
    {
    }
  });
  return false;
}());

$(document).ready(function() {
	"use strict";

	//FILTER ARTISTS
	$("input[name='genre[]']").on('change',function(){
		var genres = [];
		$("input[name='genre[]']").each(function(){
			if(this.checked){
				genres.push(this.id);
			}
  	    });

		var artistData = artistHTML(artists, genres, log);
		$("#requestedArtistData").html(artistData);

	});

	$('#deactivateArtist').on('submit', function() {
		var deactivation = [];
		$("#requestedArtistData input[type='checkbox']").each(function(){
			if (this.checked){
				deactivation.push(this.id);
			}
		});

		if (deactivation.length > 0){
			$('#deactivateArtist').append("<input type='hidden' name='artistIds' value='" + deactivation.toString() + "'>");
		}
		else{
			return false;
		}
	});
});

/* Update this function to work with and without a filter
 * If filter is [] or the artist has a genre in the filter array (array intersect genres/filters and result size > 0), append to artist card
 * Else continue	
 */
function artistHTML(data, filter, log){
	
	if (data.hasOwnProperty('artists')){
		artists = data.artists;
	}
	else{
		artists = data;
	}

	var artistCards = "";

	for (i in artists){

		var url = log ? "/artists/" + artists[i].id : "#artistData"
		if (filter.length == 0 || array_intersect(artists[i].genre.split(","), filter)){
			artistCards += " \
				<li class='artist'> \
        	<div class='artist-image'><img src='" + artists[i].profile_directory + "' alt=''></div> <!-- artist-image --> \
        	<a href=" + url + " class='artist-name' target='_blank'>" + artists[i].name.toUpperCase()+ "</a><!-- artist-name -->";
			if (artists[i].sounds_like){
				artistCards += " \
        	<p>Sounds like " + artists[i].sounds_like + "</p> \
      		<!-- artist -->"; 
			}
			else{
				artistCards += " \
					<!-- artist -->";
			}
			artistCards += "<br/><center><input type='checkbox' id ='" + artists[i].id + "'></center></li>";
		}
	}

	return artistCards;
}

function array_intersect(needle, haystack){
	if (haystack.length == 0){
		return true;
	}

	var len = needle.length;
	for (var i=0; i<len; i++){
		if (haystack.indexOf(needle[i]) > -1){
			return true;
		}
	}	
	
	return false;
}
