window.events = {};

(function(){
	"use strict";

	$.ajax({
		url: '/events/get',
		method: 'GET',
		dataType: "json",
    success: function(result)
    {
      var eventData = eventHTML(result, false);
	  $(".sidebar-nav-count").html(result.number);
      $("#upcomingEvents").html(result.number);
      $("#eventData").html(eventData);
      alert("Here");
    },
    error: function(result)
    {
			result = result.responseJSON;
			$('#error_msg').text(result.error);
			if (result.addEvent) {
				$('#eventData').html(createEvent());
			}
    }
  });
  return false;		
}());

function createEvent() {
	return " \
			<li class='event-box active' style='width:25%'> \
        <a href='/events/create' class='event-box-add'>Add an event <span><i class='ico-plus'></i></span></a> \
      </li><!-- event-box -->";
}

function eventHTML(data, filter){
  events = data.events;
  var eventCards = "";
	var current_user_id = data.current_user_id;
	var current_role = data.current_role;
	//if (data.addEvent != null){
		eventCards += " \
			<li class='event-box active' style='width:25%'> \
				<a href='/events/create' class='event-box-add'>Add an event <span><i class='ico-plus'></i></span></a> \
			</li><!-- event-box -->";
	//}

  for (i in events){
		//if index is not numeric (ie. if its metadata about the resulting array)
		if (!(!jQuery.isArray(i) && (i - parseFloat(i) + 1) >= 0)){
			continue;
		}

		var date = events[i].starttime.split(" ")[0].split("-");
		var day = date[2]; var month = date[1]; var year=date[0];
		var genres = events[i].genres.split(",").join(" & ");
		var url = "/events/" + events[i].id + "/details";
		var status ="";
		
		if (typeof events[i].artist_request[current_user_id] !== 'undefined' && current_role == 'artist'){
			if(events[i].artist_request[current_user_id] == 0)
				status = "<label class='label label-default'>Play Requested</label>";
			else if(events[i].artist_request[current_user_id] == 1)	
				status = "<label class='label label-primary'>In Booking</label>";
			else if(events[i].artist_request[current_user_id] == 2)
				status = "<label class='label label-success'>Confirm Play</label>";
		}
		
		if (current_role != 'artist' && events[i].artist_request !== 'undefined'){
			jQuery.each(events[i].artist_request, function( i, val ) {
				if(val == 2){
					status = "<center><label class='label label-success'>Artist confirmed</label></center>";
					return false;		
				}					
			});	
		}
		eventCards += " \
			<li class='filter-item event-box event-box-fill'> \
				<h4 class='event-box-title'>" + events[i].name +"</h4><!-- event-box-title --> \
				<p class='event-box-meta'>" + month +"/" + day + "/" + year + " – " + events[i].location + "</p><!-- event-box-meta --> \
				<ul class='list-event-details'> \
					<li> \
						<p>Open to</p> \
						<h6>" + genres + " Bands</h6> \
						<h6 class='event-details-price'>$" + events[i].min_budget + " – $" + events[i].max_budget + "</h6><!-- event-details-price --> \
					</li> \
				</ul><!-- list-event-details --> \
				<div class='actions'> \
					<a href=" + url +" class='btn btn-fill btn-invert'>View</a> \
					<div class='pull-right'>"+ status +"</div>\
				</div><!-- event-box-actions --> \
			</li><!-- filter-item -->";
  }

  return eventCards;
}

