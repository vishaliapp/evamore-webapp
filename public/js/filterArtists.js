(function($) {
    $(function () {
        var isFilteredActive = ~location.href.indexOf('status=active'),
             isFilteredInActive = ~location.href.indexOf('status=inactive'),
             url = location.pathname + '?'
                + (isFilteredActive ? 'status=active&' : '')
                + (isFilteredInActive ? 'status=inactive&' : ''),
            $artistFilterPrice = $('.artist-filter-price'),
            $selectFilterFee = $('#select-filter-fee'),
            $selectFilterGenre = $('#select-filter-genre'),
            location_string = '',
            $artistFilterGenre = $('.artist-filter-genre');
            $selectFilterLocation = $('#select-filter-location');
            $artistFilterLocation = $('.artist-filter-location');

        function filterClick(e) {
            var $this = $(this),
                $parent = $this.closest('.filter-wrapper');
            e.preventDefault();
            if ( ! $this.hasClass('active') && $parent.find('.active').length >2 ) {
                return false;
            }
            $(this).toggleClass('active');
            applyFilters();
        }

        function applyFilters() {
            var genres = [],
                prices = [],
                fee_string = '',
                genres_string = '',
                params = {};

            $artistFilterGenre.filter('.active').each(function() {
                genres.push( $(this).data('genre_name') );
            });
            $artistFilterPrice.filter('.active').each(function() {
                prices.push( $(this).data('fee_value') );
            });

            genres_string = genres.join(',');
            fee_string = prices.join(',');

            if (fee_string) {
                params.fee = fee_string;
            }
            if (genres_string) {
                params.genres = genres_string;
            }
            if(location_string != '') {
                params.location = location_string;
            }
            window.location = url + $.param(params);
        }

        $artistFilterGenre.click(filterClick);
        $artistFilterPrice.click(filterClick);

        $selectFilterFee.on('change', function (e) {
            var $this = $(this);
            $artistFilterPrice.removeClass('active');
            if ( $this.val() ) {
                $( $this.val() ).each(function (key, item) {
                    $artistFilterPrice.filter('[data-fee_value=' + item + ']').addClass('active');
                });
            }
            applyFilters();
        });

        $selectFilterGenre.on('change', function () {
            var $this = $(this);
            $artistFilterGenre.removeClass('active');
            if ( $this.val() ) {
                $( $this.val() ).each(function (key, item) {
                    $artistFilterGenre.filter('[data-genre_name="' + item + '"]').addClass('active');
                });
            }
            applyFilters();
        });

        $selectFilterLocation.on('change', function () {
            location_string = $(this).val();
            applyFilters();
        });
    })
})(window.jQuery);