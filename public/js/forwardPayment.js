$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
$(document).on('click', '#forwardPayment', function (e) {
    e.preventDefault()
    var bidId = $('#forwardPayment').attr('data-bid-id');
    var mail = $('#email').val();
    var payerName = $('#payerName').val();
    $.ajax({
        url: '/forward/pay/' + bidId + '/' + mail + '',
        method: 'post',
        data: {
            mail: mail,
            bidId: bidId,
            payerName: payerName
        }
    })
})

$(document).on('click','#firstStepButton', function () {

    $('#firstStepDiv').delay("slow").fadeOut();

    $('#secondStepContract').delay(1000).fadeIn();
})

var paymentMethod = 'other';
$(document).on('change','#payment-type-change', function () {
    if ($(this).val() == 'amex') {
        $('.amex').css({display: 'table-row'});
        $('.other').css({display: 'none'});
        $('.exp-date').css({display: 'table-row'});
        paymentMethod = 'amex';
    } else {
        $('.amex').css({display: 'none'});
        $('.other').css({display: 'table-row'});
        $('.exp-date').css({display: 'table-row'});
        paymentMethod = 'other';
    }
})
$("input[name=payAmount]:radio").change(function () {
    if ($(this).val() == 2) { //  App\ModelType\BidType::FULL_AMOUNT
        $automaticalyPayment.prop('checked', false).prop("disabled", true);
    } else {
        enableFields();
    }
});


var $usedCardRadio = $("#payMethod-1");
var $newCardRadio = $("#payMethod-2");
var $cardInfoFields = $(".card-info");

var $automaticalyPayment = $("#automaticalyPayment");
var $saveCardCheckbox = $("#saveCardCheckbox");

//existed card selected
if ($usedCardRadio.is(':checked')) {
    console.log($("#payment-type-change").val())

    //Hiding fields
    $('.amex').css({display: 'none'});
    $('.other').css({display: 'none'});
    $('.exp-date').css({display: 'none'});
    $('.payment_method').css({display: 'none'});
    $("#payment-type-change").val('default').selectpicker("refresh");

    _.each($cardInfoFields, function (e) {
        var $e = $(e);
        console.log($e);
        var type = $e.prop("type");
        if (type == "text") {
            $e.val("")
        }

    });

    $("#expirationMonth").selectpicker('val', 1);
    console.log($("#expirationMonth").val());
    var now = new Date();
    var yearFrom = now.getFullYear();
    console.log($("#expirationYear").val())
    $("#expirationYear").selectpicker('val', yearFrom);
    $cardInfoFields.prop("disabled", true);
    $("button[data-id=expirationMonth], button[data-id=expirationYear]",)
        .css({'background': "#EEEEEE"});

    $automaticalyPayment.prop("disabled", false)
}

//new card selected
if ($newCardRadio.is(':checked')) {
    $('.payment_method').css({display: 'table-row'});

    $("button[data-id=expirationMonth], button[data-id=expirationYear]",)
        .css({'background': "#FFFFFF"});
    $cardInfoFields.prop("disabled", false);
    $automaticalyPayment.prop("disabled", false);
    if (!isCardInfoFill()) {
        $saveCardCheckbox.prop('checked', false).prop("disabled", true);
        $automaticalyPayment.prop('checked', false).prop("disabled", true);
    }
    if (isCardInfoFill() && $saveCardCheckbox.is(':checked')) {
        $automaticalyPayment.prop('checked', false).prop("disabled", false);
    } else {
        $automaticalyPayment.prop('checked', false).prop("disabled", true);
    }

};

var isCardInfoFill = function () {
    //card number
    let vCardNumber;
    let vCvc;
    if (paymentMethod == 'amex') {
        //Card Number
        vCardNumber = $('#amexCardNumber').val();
        //CVC
        vCvc = ($("#cvv").val().length == 4) ? true : false;
    } else {
        //Card Number
        vCardNumber = $('#cardNumber').val();

        //CVC
        vCvc = ($("#cvc").val().length == 3) ? true : false;
    }
    return vCardNumber && vCvc;
};
$(document).ready(function($){
    $("input[name=cardNumber]").mask("9999 9999 9999 9999");
    $("input[name=amexCardNumber]").mask('9999 999999 99999');
})

$(document).on('click','#thirdStepPayButton', function () {
    console.log(isCardInfoFill())
    var stringCardNumber;
    var vcvc;
    if (paymentMethod == 'amex') {
        stringCardNumber = $('#amexCardNumber').val()
        vcvc = $('#cvv').val();
    } else {
        stringCardNumber = $('#cardNumber').val();
        vcvc = $('#cvc').val();
    }

    if (stringCardNumber != '') {
        stringCardNumber = stringCardNumber.replace(/\s/g, '');

        $.ajax({
            url: "/api/forward/pay/bid",
            type: 'post',
            data: {
                _token: $('input[name=_token]').val(),
                bid_id: $('.bidId').val(),
                payAmount: $("input[name=payAmount]:checked").val(),
                cardNumber: stringCardNumber,
                expirationMonth: $("#expirationMonth").val(),
                cvc: vcvc,
                paymentToken:$('input[name=paymentToken]').val(),
                expirationYear: $("#expirationYear").val(),
                payMethod: $("input[name=payMethod]:checked").val(),
                paymentMethod: paymentMethod
            },
            beforeSend: function () {
                $('.loading').css({display: 'block'});
            },
            success: function (response, statusText, xhr, $form) {
                $('.loading').css({display: 'none'});
                if (response.success) {
                    swal({
                        title: 'Payment processed successfully.',
                        icon: "success",
                    })
                        .then(success => {
                            window.location.href = '/'
                        });
                } else {
                    var keys = Object.keys(response.errors)
                    for (var i = 0; i < keys.length; i++) {
                        $(".error-" + keys[i]).append(response.errors[keys[i]]);
                        if (keys[i] == "global") {
                            $(".error-" + keys[i]).css({"background": "#FFaaaa"});
                        }
                    }
                }
            }
        })
    }


})
$(document).on('click','#contractAgree', function () {

    var $automaticalyPayment = $("#automaticalyPayment");
    $("input[type=text]").keyup(function () {
        if (isCardInfoFill()) {
            enableFields();
        } else {
            $saveCardCheckbox.prop('checked', false).prop("disabled", true);
            $automaticalyPayment.prop('checked', false).prop("disabled", true);
        }
    });
})
enableFields = function() {
    var $usedCardRadio = $("#payMethod-1");
    var $newCardRadio = $("#payMethod-2");
    var $cardInfoFields = $(".card-info");
    console.log($cardInfoFields)

    var $automaticalyPayment = $("#automaticalyPayment");
    var $saveCardCheckbox = $("#saveCardCheckbox");

    //existed card selected
    if ($usedCardRadio.is(':checked')) {
        //Hiding fields
        $('.amex').css({display: 'none'});
        $('.other').css({display: 'none'});
        $('.exp-date').css({display: 'none'});
        $('.payment_method').css({display: 'none'});
        $("#payment-type-change").val('default').selectpicker("refresh");

        _.each($cardInfoFields, function(e){
            var $e = $(e);
            var type = $e.attr("type");
            if (type == "text"){
                $e.val("")
            }
        });

        $("#expirationMonth").selectpicker('val',1);
        var now = new Date(); var yearFrom = now.getFullYear();
        $("#expirationYear").selectpicker('val',yearFrom);
        $cardInfoFields.prop( "disabled", true);
        $("button[data-id=expirationMonth], button[data-id=expirationYear]", )
            .css({'background':"#EEEEEE"});

        $automaticalyPayment.prop( "disabled", false)
    }

    //new card selected
    if ($newCardRadio.is(':checked')) {
        $('.payment_method').css({display: 'table-row'});

        $("button[data-id=expirationMonth], button[data-id=expirationYear]", )
            .css({'background':"#FFFFFF"});
        $cardInfoFields.prop( "disabled", false );
        $automaticalyPayment.prop("disabled", false);
        if(!isCardInfoFill()) {
            $saveCardCheckbox.prop('checked', false).prop( "disabled", true);
            $automaticalyPayment.prop('checked', false).prop("disabled", true);
        }
        if(isCardInfoFill() && $saveCardCheckbox.is(':checked')) {
            $automaticalyPayment.prop('checked', false).prop( "disabled", false);
        } else {
            $automaticalyPayment.prop('checked', false).prop( "disabled", true);
        }
    }
};

//
// var isCardInfoFill = function() {
//     //card number
//     let vCardNumber;
//     let vCvc;
//     if (paymentMethod == 'amex') {
//         //Card Number
//         vCardNumber = $('#amexCardNumber').val();
//         //CVC
//         vCvc = ($("#cvv").val().length == 4) ? true : false;
//     } else {
//         //Card Number
//         vCardNumber = $('#cardNumber').val();
//
//         //CVC
//         vCvc = ($("#cvc").val().length == 3) ? true : false;
//     }
//
//     return vCardNumber && vCvc;
// };

$("input[name=payMethod]").on("change", function(){
    $("#cardNumber").mask("9999 9999 9999 9999");
    $("#amexCardNumber").mask('9999 999999 99999');
    enableFields();
});
