;
(function ($, window, document, undefined) {

    $( document ).ajaxError(function(event, jqXHR, ajaxSettings, thrownError) {
        if (jqXHR.status == 401){
            document.location = "/";
        }
    });

    var $win = $(window);
    var $doc = $(document);

    $doc.ready(function () {

        var $mobileNav = $('#bs-example-navbar-collapse-1');
        var $navAccess = $('.nav-access', $mobileNav);

        $mobileNav.on('show.bs.collapse', function () {
            $mobileNav.addClass("mobileMenu");
            $navAccess.removeClass("nav-access");
        });
        $mobileNav.on('hidden.bs.collapse', function () {
            $mobileNav.removeClass("mobileMenu");
            $mobileNav.addClass('nav-access');
        });

        var $avatarUploader = $("#avatarUploader");
        if ($avatarUploader.length) {
            var defaultImagePath = $avatarUploader.attr("data-default-image");
            var oldImage = $avatarUploader.attr("data-old-image");

                // $avatarUploader.PictureCut({
                //     InputOfImageDirectory: "image",
                //     PluginFolderOnServer: "/addons/picture-cut-master/",
                //     FolderOnServer: "/uploads/",
                //     EnableCrop: true,
                //     CropWindowStyle: "Bootstrap",
                //     Extensions: ["jpeg", "jpg", "png", "gif"],
                //     CropOrientation: false,
                //     CropModes: {
                //         widescreen: false,
                //         letterbox: false,
                //         free: false,
                //         squarebox: true
                //     },
                //     ImageButtonCSS: {
                //         width: "139px",
                //         height: "139px"
                //     },
                //     DefaultImageButton: (defaultImagePath) ? defaultImagePath : '',//'/addons/picture-cut-master/src/img/icon_add_image2.png',
                //     OldImage: (oldImage) ? oldImage : '',
                //     UploadedCallback: function (data) {
                //
                //     }
                // });

            $('#avatarUploaderImageClean').on('click', function () {
                if ($avatarUploader !== undefined) {
                    $('input[name=image]', $avatarUploader).val("");
                    $('img.picture-element-image').attr('src', '/images/drag_drop_white.png')
                }
                return false;
            });
        }
        //Fullsize Background Image
        $('.fullsize').each(function () {
            var $img = $(this);

            $img.addClass('hidden').parent().addClass('fullsize-container').css('background-image', 'url(' + $img.attr('src') + ')');
        });

        $('.select').dropdown();

        var $car = $('.section-sky .section-bg');

        if ($('.section-sky').hasClass('intro')) {
            $('.section-bg-car').addClass('active');
        }

        if ($car.length) {
            $win.on('scroll', function () {
                if ($win.scrollTop() + $win.height() > $car.offset().top) {
                    $('.section-bg-car').addClass('active');
                }
            });
        }

        //Tabs
        var $tabsContainer = $('.tabs-body');

        $('.tabs-nav a').on('click', function (e) {
            e.preventDefault();

            var $tabLink = $(this);
            var tabTarget = $tabLink.attr('href');

            $tabLink
                .addClass('active')
                .parent()
                .siblings()
                .find('a')
                .removeClass('active');

            $(tabTarget)
                .addClass('active')
                .siblings()
                .removeClass('active');

            setTabHeight();
            tabCallback($tabLink);
        });

        setTabHeight();

        function tabCallback($tabLink) {
            var isCond = + $tabLink.data('is-conditions');
            if (isCond) {
                setTimeout(function() {
                    jQuery('#conditions-link').click();
                }, 0);
            }
        }

        function setTabHeight() {
            var activeTabHeight = $('.tab.active').outerHeight();
            $tabsContainer.height(activeTabHeight);
        };

        $win.on('resize', function () {
            setTabHeight();
        });

        //Datepicker
        var $datePicker = $('#datepicker-inline');
        $datePicker.datepicker({
            todayHighlight: true
        });
        if ($datePicker.attr('data-year') !== undefined) {
            var y = $datePicker.attr('data-year');
            var m = $datePicker.attr('data-month') - 1;
            var d = $datePicker.attr('data-day');
            $datePicker.datepicker('update', new Date(y, m, d));
            $("#my_hidden_input").val(
                $("#datepicker").datepicker('getFormattedDate')
            )
            $('#date').val($("#datepicker-inline").datepicker('getFormattedDate'));
        }

        $datePicker.on('changeDate', function (event) {
            $('#date').val($("#datepicker-inline").datepicker('getFormattedDate'));
        });

        $("#date").on("changeDate", function (event) {
            $("#my_hidden_input").val(
                $("#datepicker").datepicker('getFormattedDate')
            )
        });

        //Mobile Menu
        if ($('#trigger-overlay').length) {
            var triggerBttn = document.getElementById('trigger-overlay'),
            overlay = document.querySelector('div.overlay');
            if (overlay) {
                var closeBttn = overlay.querySelector('button.overlay-close');
                transEndEventNames = {
                    'WebkitTransition': 'webkitTransitionEnd',
                    'MozTransition': 'transitionend',
                    'OTransition': 'oTransitionEnd',
                    'msTransition': 'MSTransitionEnd',
                    'transition': 'transitionend'
                },
                transEndEventName = transEndEventNames[Modernizr.prefixed('transition')],
                support = {transitions: Modernizr.csstransitions};

                if ($('#book')) {
                    $('#book').on('click', function () {
                        if ($('#to')) {
                            $('#to').val('events');
                        }
                    });
                }
            }
        }
    });
})(jQuery, window, document);

(function($){
    // AJAX Login
    var options = {
        success: function(response, statusText, xhr, $form) {
            if (response.success) {
                window.location.href = response.redirect_to;
            } else {
                var $errorsContainer = $('.errors', $form);
                $errorsContainer.text(response.message);
                setTimeout(function(){
                    $errorsContainer.text('');
                }, 2000)
            }

        },  // post-submit callback
        dataType: 'json' // 'xml', 'script', or 'json' (expected server response type)
    };

    var signUpOptions = {
        success: function(response, statusText, xhr, $form){
            if (response.success==true) {
                window.location.href = response.redirect_to;
            } else {
                var errorsFeed = response.errors;
                var errors = Object.keys(errorsFeed);
                var $errorContainer;

                $('.help-block strong').each(function() {
                    $(this).text('');
                });

                for(var i = 0; i < errors.length; i++) {
                    $errorContainer = $('.' + errors[i] + '-error', $form);
                    $errorContainer.text(errorsFeed[errors[i]][0]);
                }
            }
        },  // post-submit callback
        dataType: 'json' // 'xml', 'script', or 'json' (expected server response type)
    };
    // bind form using 'ajaxForm'
    if ($('#login-form')){
        $('#login-form').ajaxForm(options);
    }
    if ($('#registration')){
        $('#registration').ajaxForm(signUpOptions);
    }
})(jQuery);

function array_intersect(needle, haystack){
    if (haystack.length == 0){
        return true;
    }

    var len = needle.length;
    for (var i=0; i<len; i++){
        if (haystack.indexOf(needle[i]) > -1){
            return true;
        }
    }

    return false;
}

function feeToString(fee) {
    fee = (fee > 4) ? 4: fee;
    fee = (fee < 1 ) ? 1: fee;

    var map = {
        1: "$",
        2: "$$",
        3: "$$$",
        4: "$$$$"
    }

    return map[fee];
}


function getBrowserDimensions() {
    return {
        window: {
            width: $(window).width(),
            height: $(window).height()
        },
        document: {
            width: $(document).width(),
            height: $(document).height()
        }
    }
}

function renderYoutubeVideosList(youtube_channel_name, container, showCountItems, callback) {
    var n = 6;
    if (showCountItems) {
        n = parseInt(showCountItems)
    }
    var youtubeObj = new YoutubeObject(youtube_channel_name, null);
    var i=0;
    for(var key in youtubeObj.videos)
    {
        var link1 = "&lt;iframe height=&#039;200&#039; width=&#039;266&#039; src=&#039;https://www.youtube.com/embed/";
        var link2 = "?rel=0&amp;showinfo=0&amp;modestbranding=1&amp;autoplay=1&#039; frameborder=&#039;0&#039; allowfullscreen&gt;&lt;/iframe&gt;";
        var link = link1 + youtubeObj.videos[key] + link2;
        container.append("<li><img src='"+ key +"' data-video='"+ link +"'></li>");

        i++;
        if (i==n) {
            break;
        }
    }

    $("img", container).on("click", function() {
        var link = $(this).attr("data-video");
        $(this).parent().html(link);
    });

    if ('function' === typeof callback) {
        callback(container)
    }
}

/**
 * Save browser timezone to the cookies.
 * If user doesn't have time_zone set in the profile we use cookie.
 * If cookie doesn't exist or doesn't fit to the TimeZoneType we use server time zone.
 */
// (function ($) {
//     $(function() {
//          if (jstz) {
//              var tz = jstz.determine();
//              $.cookie('time_zone', tz.name());
//          }
//     });
// })(window.jQuery);