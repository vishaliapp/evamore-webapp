window.events = {};

(function(){
	"use strict";

	$.ajax({
		url: '/events/past/get',
		method: 'GET',
		dataType: "json",
    success: function(result)
    {
      var eventData = eventHTML(result, false);
			$(".sidebar-nav-count").html(result.number);
      $("#pastEvents").html(result.number);
      $("#eventData").html(eventData);
    },
    error: function(result)
    {
			$('#error_msg').text(result.responseText);
    }
  });
  return false;		
}());

function eventHTML(data, filter){
  events = data.events;
  var eventCards = "";

	if (data.addEvent != null){
		eventCards += " \
			<li class='event-box active' style='width:25%'> \
				<a href='/events/create' class='event-box-add'>Add an event <span><i class='ico-plus'></i></span></a> \
			</li><!-- event-box -->";
	}

  for (i in events){
		
		//if index is not numeric (ie. if its metadata about the resulting array)
		if (!(!jQuery.isArray(i) && (i - parseFloat(i) + 1) >= 0)){
			continue;
		}

		var date = events[i].starttime.split(" ")[0].split("-");
		var day = date[2]; var month = date[1]; var year=date[0];
		var genres = events[i].genres.split(",").join(" & ");
		var url = "/events/" + events[i].id + "/details";
		eventCards += " \
			<li class='filter-item event-box event-box-fill'> \
				<h4 class='event-box-title'>" + events[i].name +"</h4><!-- event-box-title --> \
				<p class='event-box-meta'>" + month +"/" + day + "/" + year + " – " + events[i].location + "</p><!-- event-box-meta --> \
				<ul class='list-event-details'> \
					<li> \
						<p>Open to</p> \
						<h6>" + genres + " Bands</h6> \
						<h6 class='event-details-price'>$" + events[i].min_budget + " – $" + events[i].max_budget + "</h6><!-- event-details-price --> \
					</li> \
				</ul><!-- list-event-details --> \
				<div class='actions'> \
					<a href=" + url +" class='btn btn-fill btn-invert'>View</a> \
				</div><!-- event-box-actions --> \
			</li><!-- filter-item -->";
	}

  return eventCards;
}

