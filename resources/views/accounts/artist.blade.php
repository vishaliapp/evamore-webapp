@extends('layouts.dash')

@section('content')
    <div class="section-head">
        <h1>Update artist profile.</h1>


        @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
        @endif
    </div><!-- /.section-head -->
			<div class="section-body">
				<div class="form form-apply">
          {{-- FORM --}}
          {{ Form::model($user, [
            'route' => ['editartist_post', $user->id],
            'method' => 'post',
            'enctype' => 'multipart/form-data'
          ]) }}
                    {{ csrf_field() }}
            @include('accounts.partials._artist-form')
          {{ Form::close() }}
				</div><!-- /.form form-apply -->
			</div><!-- /.section-body -->
		<!--</div> /.container -->
	<!--</section> /.section -->
  <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Modal title</h4>
                </div>
                <div class="modal-body">
                    <button id="js-main-image" type="button" class="btn btn-primary btn-invert upload-result" >Set My Picture</button>

                    <div class="demo-wrap upload-demo">
                        <div id="avatar_crop_image" class="container">
                            <div class="grid">
                                <div class="col-1-2">
                                    <div class="upload-msg">
                                        Upload a file to start cropping
                                    </div>
                                    <div class="upload-demo-wrap">
                                        <div id="upload-demo"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="{{asset('demo/prism.js')}}"></script>
<script src="{{asset('croppie.js')}}"></script>
<script src="{{asset('/addons/croper.main.js')}}"></script>
<script>
  $( document ).ready(function() {
    Demo.init();
  });
</script>

@stop

@section('footerjs')
    <script src="{{asset("/addons/jquery-ui-1.12.1/jquery-ui.min.js", null, false)}}"></script>
    <script src="{{asset("/addons/picture-cut-master/src/jquery.picture.cut.js", null, false)}}"></script>
@stop
