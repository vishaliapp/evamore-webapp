@extends('layouts.dash')

@section('content')
    <div class="main-body">
        <div class="account">
            <div class="account-head">
                <h1>Update profile</h1>
            </div>
            <!-- /.account-head -->
            <div class="account-body">
                @if(Session::has('message'))
                    <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
                @endif
                <div>
                    <form action="{{ route('createEvent') }}">
                        <input type="hidden" name="id" value="{{ $user->id }}">
                        <input type="submit" style="position: absolute; right: 15px;" class="btn btn-primary btn-invert" value="Create event for this user">
                    </form>
                </div>
                <form enctype="multipart/form-data" action="{{ route('edituser') }}" method="POST" class="dropzone">
                    {{ csrf_field() }}
                    <center>
                        <div class="account-image-outer">
                            <div class="account-image" id="avatarUploader" data-default-image="<{{Image::url('uploads/avatars/'.$user->avatar,139,139,array('crop'))}}">

                                <div class="file-upload">
                                    <div class="file-upload-btn">
			                            <span class="file-upload-text">
                                            <img class="picture-element-image" id="user-avatar" src="{{ Image::url('uploads/avatars/'.$user->avatar,array('crop')) }}" height="100%"
                                                 width="100%" alt="">
			                            	@if (empty($user->avatar))
                                                Drag & drop an image here <span>or upload</span>
                                            @endif
										</span>
                                        <input type="hidden" id="image_base_64" name="upload_image">
                                        <input name="image" accept="image/*" type="file" id="upload" class="file-upload-input">
                                    </div><!-- /.file-upload-btn -->
                                </div><!-- /.file-upload -->
                            </div><!-- /.account-image -->

                            <label for="upload_image" class="file-upload-ico">
                                <i class="ico-cam"></i>
                            </label>
                        </div>
                        <div class="form-group {{ $errors->has('avatar') ? ' has-error' : '' }}">
                            @if ($errors->has('avatar'))
                                <span class="help-block">
	                                        <strong>{{ $errors->first('avatar') }}</strong>
	                                    </span>
                            @endif
                        </div>
                    </center><!-- /.account-image-outer -->
                    <p><a id="avatarUploaderImageClean" href="#">Cancel</a></p>
                    <!-- /.account-image-outer -->


                    <div class="form-group {{ $errors->has('avatar') ? ' has-error' : '' }}">
                        @if ($errors->has('avatar'))
                            <span class="help-block">
                                <strong>{{ $errors->first('avatar') }}</strong>
                            </span>
                        @endif
                    </div>

                    <!--</form><!-- Upload form -->
                    <!--<form action="#" method="post" id="user_update">-->
                    <div class="form form-account" class="dropzone">
                        <input type="hidden" name="id"
                               value="{{ ($user->id) }}"/>

                        <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                            <input type="text" class="form-control form-control-alt" id="name" name="name"
                                   placeholder="Name" value="{{ $user->name }}"/>

                            <i class="ico-pencil"></i>
                            @if ($errors->has('name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>
                        <!-- /.form-group -->

                        <div class="form-group {{ $errors->has('organization') ? ' has-error' : '' }}">
                            <input type="text" class="form-control" id="organization" name="organization"
                                   placeholder="Organization" value="{{ $user->organization }}"/>

                            <i class="ico-pencil"></i>
                            @if ($errors->has('organization'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('organization') }}</strong>
                                </span>
                            @endif
                        </div>
                        <!-- /.form-group -->

                        <div class="form-group {{ $errors->has('org_position') ? ' has-error' : '' }}">
                            <input type="text" class="form-control" id="org_position" name="org_position"
                                   placeholder="Position" value="{{ $user->org_position }}"/>

                            <i class="ico-pencil"></i>
                            @if ($errors->has('org_position'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('org_position') }}</strong>
                                </span>
                            @endif
                        </div>
                        <!-- /.form-group -->

                        <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                            <input type="text" class="form-control" id="email" name="email"
                                   placeholder="Email" value="{{ $user->email }}"/>

                            <i class="ico-pencil"></i>
                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                        <!-- /.form-group -->

                        <div class="form-group {{ $errors->has('phone') ? ' has-error' : '' }}">
                            <input type="tel" class="form-control" id="phone" name="phone"
                                   placeholder="Phone" value="{{ $user->phone }}"/>

                            <i class="ico-pencil"></i>
                            @if ($errors->has('phone'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('phone') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group {{ $errors->has('time_zone') ? ' has-error' : '' }}">
                            <select class="form-control profile-timezone" id="time_zone" name="time_zone">
                                <option value="">What’s your Time Zone?</option>
                                @foreach(\App\ModelType\TimeZoneType::getTimeZones() as $value => $zone_value)
                                    <option
                                            @if ((old("time_zone") && old("time_zone")==$value) || ($user->time_zone == $value))
                                            selected="selected"
                                            @endif
                                            value="{{$value}}">{{\App\ModelType\TimeZoneType::getTimeZoneTitle($value)}}</option>
                                @endforeach
                            </select>

                            @if ($errors->has('time_zone'))
                                <span class="help-block">
	                                        <strong>{{ $errors->first('time_zone') }}</strong>
	                                    </span>
                            @endif
                        </div><!-- /.form-group -->
                        <!-- /.form-group -->

                        <!--<div class="form-group">
                            <input type="password" class="form-control" id="password" name="password" placeholder="password">

                            <i class="ico-pencil"></i>
                        </div>--><!-- /.form-group -->

                        <div class="form-actions">
                            <input type="submit" value="Confirm" name="submit" class="btn btn-primary btn-invert"/>

                            @if($currentUser->hasRole('Admin'))
                                <p><a id='deactivate' href="#">deactivate account</a></p>
                            @endif
                        </div>
                        <!-- /.form-actions -->
                    </div>
                    <!-- /.form form-account -->
                </form>
            </div>
            <!-- /.account-body -->
        </div>
        <!-- /.account -->
    </div><!-- /.main-body -->

    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Modal title</h4>
                </div>
                <div class="modal-body">
                    <button id="js-main-image" type="button" class="btn btn-primary btn-invert upload-result" >Set My Picture</button>

                    <div class="demo-wrap upload-demo">
                        <div id="avatar_crop_image" class="container">
                            <div class="grid">
                                <div class="col-1-2">
                                    <div class="upload-msg">
                                        Upload a file to start cropping
                                    </div>
                                    <div class="upload-demo-wrap">
                                        <div id="upload-demo"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>

@stop
@section('js')
    {{--<script src="{{asset("/bower_components/jquery.cookie/jquery.cookie.js", null, false)}}"></script>--}}
    {{--<script src="{{asset("/addons/jstz.main.js", null, false)}}"></script>--}}
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    {{--<script src="{{asset("/addons/bootstrap/js/bootstrap-select.min.js", null, false)}}"></script>--}}
    {{--<script src="{{ asset('https://unpkg.com/sweetalert/dist/sweetalert.min.js') }}"></script>--}}
    {{--<script src="{{asset('//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js')}}"></script>--}}
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="{{asset('demo/prism.js')}}"></script>
    <script src="{{asset('croppie.js')}}"></script>
    <script src="{{asset('/addons/croper.main.js')}}"></script>
    <script>
        $( document ).ready(function() {
            Demo.init();
        });

    </script>
@endsection
@section('footerjs')




    {{--<script src="{{asset('/addons/croper.main.js')}}"></script>--}}
    {{--<script src="{{asset("/addons/jquery-ui-1.12.1/jquery-ui.min.js", null, false)}}"></script>--}}
    {{--<script src="{{asset("/addons/picture-cut-master/src/jquery.picture.cut.js", null, false)}}"></script>--}}

    {{--<script>--}}
        {{--$( document ).ready(function() {--}}
            {{--Demo.init();--}}
        {{--});--}}

    {{--</script>--}}

@stop