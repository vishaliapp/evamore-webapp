@extends('layouts.secondary')

@section('content')
    <section class="section section-apply">
        <div class="container">
            <div class="section-head">
                <h1>Please enter your email address below</h1>

                <h2>We'll send you a link to reset your password</h2>
            </div><!-- /.section-head -->

            <div class="section-body">
                <div class="form form-apply {{ $errors->has('email') ? ' has-error' : '' }}">

                    <form action="{{ route('reset_password_by_email') }}" method="POST">
                        {{ csrf_field() }}
                        <div class="row">
                            <!--<div class="col-sm-6">-->
                            <div class="form-section">
                                <p>Email</p>

                                <div class="form-group">
                                    <input type="email"
                                           class="form-control"
                                           id="email"
                                           name="email"
                                           placeholder="The email used at registration"
                                           value="{{ Request::old('email') }}">

                                    <i class="ico-pencil"></i>
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif

                                    @if (session('status'))
                                        <span class="help-block" style="color: #a94442;">
                                            <strong>{{ session('status') }}</strong>
                                        </span>
                                    @endif

                                </div><!-- /.form-group -->

                            </div><!-- /.form-section -->
                            <!--</div>--><!-- /.col-sm-6 -->

                        </div><!-- /.row -->

                        <div class="form-actions">
                            <p id='error_msg'></p>
                            <input type="submit"
                                   value="SEND"
                                   name="submit"
                                   class="btn btn-primary btn-invert form-btn"
                                   formnovalidate>
                        </div><!-- /.form-actions -->



                    </form>
                </div><!-- /.form form-apply -->
            </div><!-- /.section-body -->
        </div><!-- /.container -->
    </section><!-- /.section -->

@stop

