@extends('layouts.secondary')

@section('content')

<section id="register" class="section password-recovery-wrapper">
    <div class="container">
        <div class="row">
            <h1>Sorry,</h1>
            <h3>Recovery link is expired.</h3>
        </div><!-- /.row -->
    </div><!-- /.container -->
</section><!-- /.section-sign -->
@stop

