@extends('layouts.secondary')

@section('content')

<section id="register" class="section section-sign" style="background: url('/images/temp/girl.jpg')">
    <div class="container" >
        <div class="row">
            <div class="col-md-6">
                <h1>Hey,</h1>
                <h3>Please enter a new password below</h3>

                <div class="form form-sign {{ $errors->has('password') ? ' has-error' : '' }}">
                    <form action="{{ route('save_new_password') }}" method="POST">
                        {{ csrf_field() }}

                        <input type="hidden" name="user_id" value=" <?php echo $user->id ?>">
                        <input type="hidden" name="key" value="{{$key}}">

                        <div class="row">
                            <div class="col-xs-6">
                                <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                                    <label for="password" class="form-label">PASSWORD</label>
                                    <div class="form-controls">
                                        <input type="password"
                                               class="form-control"
                                               id="password"
                                               name="password"
                                               placeholder="not your dog's name">
                                    </div><!-- /.form-controls -->
                                </div><!-- /.form-group -->
                            </div><!-- /.col-sm-6 -->

                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label for="confirmPass" class="form-label">CONFIRM PASSWORD</label>
                                    <div class="form-controls">
                                        <input type="password"
                                               class="form-control"
                                               id="password-confirm"
                                               name="password_confirmation"
                                               placeholder="or your cat's">
                                    </div><!-- /.form-controls -->
                                </div><!-- /.form-group -->
                            </div><!-- /.col-sm-6 -->

                            <div class="col-xs-12">
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong class="email-error text-danger">  {{ $errors->first('password') }}</strong>
                                    </span>
                                @endif

                                <div class="form-actions">
                                    <input type="submit" value="SUBMIT" name="submit" class="btn btn-primary form-btn" formnovalidate>
                                </div><!-- /.form-actions -->
                            </div>
                        </div><!-- /.row -->
                    </form>
                </div><!-- /.form form-sign -->
            </div><!-- /.col-md-6 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</section><!-- /.section-sign -->
@stop

