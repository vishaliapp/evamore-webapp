<style>
    #hometown, #fee, #help, #description, #set_length {
        padding-left: 4px !important;
    }

    #help {
        padding-right: 0 !important;
    }

    @if(!isset($user))
	.dz-default {
        display: none !important;
    }

    @endif

	#dropzoneUpload {
        background-color: #e6e6e6;
        border: 2px dashed #c1c1c1;
        min-height: 210px;
        border-radius: 5px;
        position: relative;
        cursor: pointer;
    }

    #dropzoneUpload span {
        position: absolute;
        left: 50%;
        top: 50%;
        transform: translate(-50%, -50%);
        opacity: 0.5;
    }

    .dz-filename span {
        overflow: hidden;
        justify-content: center;
        width: 110px;
        display: flex;
        margin-top: 43px;
    }
</style>
{{-- AVATAR --}}
<center>
    <div class="account-image-outer">
        @if (isset($user))
            <div class="account-image" id="avatarUploader"
                 data-default-image="{{Image::url('uploads/avatars/'.$user->avatar,139,139,array('crop'))}}">
                <div class="file-upload">
                    <div class="file-upload-btn">
						<span class="file-upload-text">
							<img class="picture-element-image" id="user-avatar"
                                 src="{{ Image::url('uploads/avatars/'.$user->avatar,array('crop')) }}" height="100%"
                                 width="100%" alt="">
                            @if (empty($user->avatar))
                                Drag & drop an image here <span>or upload</span>
                            @endif
						</span>
                        <input type="hidden" id="image_base_64" name="upload_image">
                        <input name="image" accept="image/*" type="file" id="upload" class="file-upload-input">
                    </div><!-- /.file-upload-btn -->
                </div><!-- /.file-upload -->
            </div><!-- /.account-image -->
        @else
            <div class="account-image" id="avatarUploader"
                 data-default-image=" @if (old('image')) {{old('image')}} @else /images/drag_drop_white.png @endif">
                <div class="file-upload">
                    <div class="file-upload-btn">
						<span class="file-upload-text">
							<img class="picture-element-image" id="user-avatar"
                                 src="{{ Image::url('images/drag_drop_white.png',array('crop')) }}" height="100%"
                                 width="100%" alt="">
						</span>
                        <input type="hidden" id="image_base_64" name="upload_image">
                        <input name="image" accept="image/*" type="file" id="upload" class="file-upload-input">
                    </div><!-- /.file-upload-btn -->
                </div><!-- /.file-upload -->
            </div><!-- /.account-image -->
        @endif
        <label for="upload_image" class="file-upload-ico">
            <i class="ico-cam"></i>
        </label>
    </div>
    <div class="form-group {{ $errors->has('avatar') ? ' has-error' : '' }}">
        @if ($errors->has('avatar'))
            <span class="help-block">
			<strong>{{ $errors->first('avatar') }}</strong>
		</span>
        @endif
    </div>
</center><!-- /.account-image-outer -->

<div class="row">
    {{-- THE BASICS --}}
    <div class="col-sm-6">
        <div class="form-section">
            <p>The Basics</p>

            {{-- ID --}}
            @if (isset($user))
                {{ Form::hidden('id', $user->id) }}
            @endif

            {{-- Name --}}
            <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                {{ Form::text('name', null, [
                    'class' => 'form-control',
                    'id' => 'name',
                    'placeholder' => 'What’s your name or band name?'
                ]) }}
                <i class="ico-pencil"></i>

                @if ($errors->has('name'))
                    <span class="help-block">
						<strong>{{ $errors->first('name') }}</strong>
					</span>
                @endif
            </div><!-- /.form-group -->

            <div class="form-group {{ $errors->has('username') ? ' has-error' : '' }}">
                {{ Form::text('username', null, [
                    'class' => 'form-control',
                    'id' => 'username',
                    'placeholder' => 'Choose a username.'
                ]) }}

                <i class="ico-pencil"></i>
                @if ($errors->has('username'))
                    <span class="help-block">
						<strong>{{ $errors->first('username') }}</strong>
					</span>
                @endif
            </div><!-- /.form-group -->

            {{-- Email --}}
            <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                {{ Form::email('email', null, [
                    'class' => 'form-control',
                    'id' => 'email',
                    'placeholder' => 'What’s your email?'
                ]) }}
                <i class="ico-pencil"></i>

                @if ($errors->has('email'))
                    <span class="help-block">
						<strong>{{ $errors->first('email') }}</strong>
					</span>
                @endif
            </div><!-- /.form-group -->

            {{--Secondary Email--}}
            @if($currentUser && $currentUser->hasRole('ARTIST_MANAGER'))
                <div class="form-group {{ $errors->has('secondaryEmail') ? ' has-error' : '' }}">
                    {{ Form::email('secondaryEmail', null, [
                        'class' => 'form-control',
                        'id' => 'secondaryEmail',
                        'placeholder' => 'Optional artist email'
                    ]) }}
                    <i class="ico-pencil"></i>

                    @if ($errors->has('secondaryEmail'))
                        <span class="help-block">
							<strong>{{ $errors->first('secondaryEmail') }}</strong>
						</span>
                    @endif
                </div>
            @endif
            {{-- Password --}}
            @if (!isset($user))
                <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                    {{ Form::password('password', [
                        'class' => 'form-control',
                        'id' => 'password',
                        'placeholder' => 'Choose your password?'
                    ]) }}
                    <i class="ico-pencil"></i>

                    @if ($errors->has('password'))
                        <span class="help-block">
							<strong>{{ $errors->first('password') }}</strong>
						</span>
                    @endif
                </div><!-- /.form-group -->
            @endif

            {{-- Phone --}}
            <div class="form-group {{ $errors->has('phone') ? ' has-error' : '' }}">
                {{ Form::number('phone', null, [
                    'class' => 'form-control',
                    'id' => 'phone',
                    'placeholder' => 'What’s your phone number?'
                ]) }}
                <i class="ico-pencil"></i>

                @if ($errors->has('phone'))
                    <span class="help-block">
						<strong>{{ $errors->first('phone') }}</strong>
					</span>
                @endif
            </div><!-- /.form-group -->

            {{-- Time Zone --}}
            <div class="form-group {{ $errors->has('time_zone') ? ' has-error' : '' }}">
                {{ Form::select('time_zone', App\ModelType\TimeZoneType::getTimeZones(), null, [
                    'class' => 'form-control',
                    'id' => 'time_zone',
                    'placeholder' => 'What’s your time zone'
                ]) }}

                @if ($errors->has('time_zone'))
                    <span class="help-block">
						<strong>{{ $errors->first('time_zone') }}</strong>
					</span>
                @endif
            </div><!-- /.form-group -->

        </div><!-- /.form-section -->
    </div><!-- /.col-sm-6 -->

    {{-- SOCIAL --}}
    <div class="col-sm-6">
        <div class="form-section">
            @if(isset($user) && $user && $user->artist->videos && (4 - $user->artist->videos()->count()) > 0 )
                <div id="dropzoneUpload" class="dropzone">
                    <span>Drop files here or click</span>
                    <input type="file" class="hidden">
                </div>
            @endif
            <p>Social</p>

            {{-- Spotify --}}
            <div class="form-group {{ $errors->has('social.spotify_uri') ? ' has-error' : '' }}">
                {{ Form::text('social[spotify_uri]', null, [
                    'class' => 'form-control',
                    'id' => 'spotify_uri',
                    'placeholder' => 'What’s your spotify URI?'
                ]) }}

                <i class="ico-pencil"></i>
                @if ($errors->has('social.spotify_uri'))
                    <span class="help-block">
						<strong>{{ $errors->first('social.spotify_uri') }}</strong>
					</span>
                @endif
            </div><!-- /.form-group -->

            {{-- YouTube --}}
            {{--<div class="form-group {{ $errors->has('social.channel_name') ? ' has-error' : '' }}">--}}
            {{--{{ Form::text('social[channel_name]', null, [--}}
            {{--'class' => 'form-control',--}}
            {{--'id' => 'channel_name',--}}
            {{--'placeholder' => 'What’s your Youtube channel?'--}}
            {{--]) }}--}}

            {{--<i class="ico-pencil"></i>--}}
            {{--@if ($errors->has('social.channel_name'))--}}
            {{--<span class="help-block">--}}
            {{--<strong>{{ $errors->first('social.channel_name') }}</strong>--}}
            {{--</span>--}}
            {{--@endif--}}
            {{--</div><!-- /.form-group -->--}}

            {{-- Facebook --}}
            <div class="form-group {{ $errors->has('social.facebook_id') ? ' has-error' : '' }}">
                {{ Form::text('social[facebook_id]', null, [
                    'class' => 'form-control',
                    'id' => 'facebook_id',
                    'placeholder' => 'What’s your Facebook profile link?'
                ]) }}

                <i class="ico-pencil"></i>
                @if ($errors->has('social.facebook_id'))
                    <span class="help-block">
						<strong>{{ $errors->first('social.facebook_id') }}</strong>
					</span>
                @endif
            </div><!-- /.form-group -->

            {{-- Twitter --}}
            <div class="form-group {{ $errors->has('social.twitter_handle') ? ' has-error' : '' }}">
                {{ Form::text('social[twitter_handle]', null, [
                    'class' => 'form-control',
                    'id' => 'twitter_handle',
                    'placeholder' => 'What’s your Twitter handle?'
                ]) }}

                <i class="ico-pencil"></i>
                @if ($errors->has('social.twitter_handle'))
                    <span class="help-block">
						<strong>{{ $errors->first('social.twitter_handle') }}</strong>
					</span>
                @endif
            </div><!-- /.form-group -->

            {{-- Instagram --}}
            <div class="form-group {{ $errors->has('social.instagram_user') ? ' has-error' : '' }}">
                {{ Form::text('social[instagram_user]', null, [
                    'class' => 'form-control',
                    'id' => 'instagram_user',
                    'placeholder' => 'What’s your Instagram user?'
                ]) }}

                <i class="ico-pencil"></i>
                @if ($errors->has('social.instagram_user'))
                    <span class="help-block">
						<strong>{{ $errors->first('social.instagram_user') }}</strong>
					</span>
                @endif

                @if(isset($user))
                    <p>
                        <a href="{{Instagram::getLoginUrl(array(),'&state=' . $user->artist->id)}}">Link with
                            Instagram</a>
                    </p>
                @endif
            </div><!-- /.form-group -->

            {{-- Web Site --}}
            <div class="form-group {{ $errors->has('social.website') ? ' has-error' : '' }}">
                {{ Form::text('social[website]', null, [
                    'class' => 'form-control',
                    'id' => 'website',
                    'placeholder' => 'Do you have a website?'
                ]) }}
                <i class="ico-pencil"></i>
                @if ($errors->has('social.website'))
                    <span class="help-block">
						<strong>{{ $errors->first('social.website') }}</strong>
					</span>
                @endif
            </div><!-- /.form-group -->
        </div><!-- /.form-section -->
    </div><!-- /.col-sm-6 -->
</div><!-- /.row -->

<div class="row">
    <div class="col-sm-6">
        <div class="form-section">
            <p>Tell us more about your you</p>

            {{-- Hometown --}}
            <div class="form-group form-group-alt {{ $errors->has('artist.hometown') ? ' has-error' : '' }}">
                {{ Form::text('artist[hometown]', null, [
                    'class' => 'form-control form-control-small',
                    'id' => 'hometown',
                    'placeholder' => 'What is your current location?'
                ]) }}

                {{ Form::hidden('artist[place_lat]', null, ['id' => 'place-lat']) }}
                {{ Form::hidden('artist[place_lng]', null, ['id' => 'place-lng']) }}
                <i class="ico-pencil"></i>

                @if ($errors->has('artist.hometown'))
                    <span class="help-block">
						<strong>{{ $errors->first('artist.hometown') }}</strong>
					</span>
                @endif
            </div><!-- /.form-group -->

            {{-- Fee --}}
            <div class="form-group {{ $errors->has('artist.fee') ? ' has-error' : '' }}">
                {{--{{ Form::select('artist[fee]', $feeTypes, null, [--}}
                {{--'class' => 'form-control',--}}
                {{--'id' => 'fee',--}}
                {{--'placeholder' => 'What is the minimum amount you will play for?'--}}
                {{--]) }}--}}

                {{ Form::text('artist[fee]', null, [
                    'class' => 'form-control form-control-small min-amount',
                    'id' => 'fee',
                    'placeholder' => 'What is the minimum amount you will play for?',
                    'data-toggle' => 'tooltip',
                    'data-placement' =>'top',
                    'data-viewport' => '{}',
                    'title' => 'You will only receive event requests with a budget that is at least or higher than the minimum number you provide'
                ]) }}
                <script>
                    $('#fee').tooltip()
                </script>


                {{--<i class="fa fa-dollar"></i>--}}
                {{--{{ Form::text('artist[fee]', null, [--}}
                {{--'class' => 'form-control form-control-small min-amount',--}}
                {{--'id' => 'fee',--}}
                {{--'placeholder' => '0.00',--}}
                {{--]) }}--}}
                <script>
                    $(document).ready(function () {
                        $('.min-amount').on('keypress', function (e) {
                            e = e || window.event;
                            var charCode = (typeof e.which == "undefined") ? e.keyCode : e.which;
                            var charStr = String.fromCharCode(charCode);
                            if (/\d+$/.test(charStr)) {
                                return true;
                            } else {
                                return false;
                            }
                        });

                        $('.min-amount').on('change', function () {
                            if ($(this).val() != '') {
                                var amount = Number($(this).val());
                                amount = '$' + amount.toFixed(2);
                                $(this).val(amount);
                            }
                        });

                        $('.min-amount').on('focusin', function () {
                            $(this).attr('placeholder', '$0.00');
                        });

                        $('.min-amount').on('focusout', function () {
                            $(this).attr('placeholder', 'What is the minimum amount you will play for?')
                        });

                    });
                </script>

                @if ($errors->has('artist.fee'))
                    <span class="help-block">
					<strong>{{ $errors->first('artist.fee') }}</strong>
				</span>
                @endif
            </div><!-- /.form-group -->

            {{--Set Length--}}
            <div class="form-group {{ $errors->has('artist.set_length') ? ' has-error' : '' }}">
                {{ Form::number('artist[set_length]', null, [
                    'class' => 'form-control form-control-small',
                    'id' => 'set_length',
                    'placeholder' => 'What is your maximum set length?',
                    'data-toggle' => 'tooltip',
                    'data-placement' =>'top',
                    'data-viewport' => '{}',
                    'title' => 'Maximum set length in hours'

                ]) }}
                <script>
                    $('#set_length').tooltip()
                </script>

                @if ($errors->has('artist.set_length'))
                    <span class="help-block">
						<strong>{{ $errors->first('artist.set_length') }}</strong>
					</span>
                @endif
            </div>

            {{-- State --}}
            {{--<div class="form-group {{ $errors->has('artist.statetown') ? ' has-error' : '' }}">--}}
            {{--{{ Form::select('artist[statetown]', $states, null, [--}}
            {{--'class' => 'form-control form-control-small',--}}
            {{--'id' => 'statetown',--}}
            {{--'placeholder' => 'Select your state'--}}
            {{--]) }}--}}

            {{--@if ($errors->has('artist.statetown'))--}}
            {{--<span class="help-block">--}}
            {{--<strong>{{ $errors->first('artist.statetown') }}</strong>--}}
            {{--</span>--}}
            {{--@endif--}}
            {{--</div>--}}

            <div class="form-group form-group-alt {{ $errors->has('artist.mileage') ? ' has-error' : '' }}">
                {{ Form::select('artist[mileage]',$mileage, null, [
                    'class' => 'form-control form-control-small',
                    'id' => 'mileage',
                    'placeholder' => 'How far are you willing to travel for gigs?'
                ]) }}

                @if ($errors->has('artist.mileage'))
                    <span class="help-block">
						<strong>{{ $errors->first('artist.mileage') }}</strong>
					</span>
                @endif
            </div>

            {{-- Equipments --}}
            <div class="form-group form-group-alt {{ $errors->has('artist.equipments') ? ' has-error' : '' }}">
                {{ Form::select('artist[equipments]',$equipments, null, [
                    'class' => 'form-control form-control-small',
                    'id' => 'equipments',
                    'placeholder' => 'Do you have your own sound equipment?',
                ]) }}
                {{ Form::label('help', "What's this?", [
                    'class' => 'form-label form-label-half col-md-3 text-left',
                    'id' => 'help',
                    'data-toggle' => 'tooltip',
                    'data-placement' => 'right',
                    'title' => 'a PA or speaker system that can provide sound for you as an acoustic or stripped down setup.',

                ]) }}
                <script>
                    $('#help').tooltip()
                </script>

                @if ($errors->has('artist.equipments'))
                    <span class="help-block">
						<strong>{{ $errors->first('artist.equipments') }}</strong>
					</span>
                @endif
            </div>

            {{-- Description --}}
            @if (isset($user) && !empty($user->artist->description))
                @php
                    $description = preg_replace( '#(\\\r|\\\r\\\n|\\\n)#', '<br/>', $user->artist->description);
                @endphp
            @endif

            <div class="form-group form-group-alt {{ $errors->has('artist.description') ? ' has-error' : '' }}">
                {{ Form::textarea('artist[description]', null, [
                    'cols' => '30',
                    'rows' => '10',
                    'class' => 'form-control form-control-small',
                    'id' => 'description',
                    'placeholder' => 'Provide a short bio about you or your band.'
                ]) }}
                <i class="ico-pencil"></i>
                @if ($errors->has('artist.description'))
                    <span class="help-block">
						<strong>{{ $errors->first('artist.description') }}</strong>
					</span>
                @endif
            </div><!-- /.form-group -->
            <div class="form-group {{ $errors->has('artist.fee') ? ' has-error' : '' }}">
                {{ Form::text('artist[hear_about_us]', null, [
                'class' => 'form-control form-control-small',
                'id' => 'about_us',
                'placeholder' => 'How did you hear about us?'
            ]) }}
            </div>
        </div><!-- /.form-section -->
    </div><!-- /.col-sm-6 -->

    {{-- ABOUT MUSIC --}}
    <div class="col-sm-6">
        <div class="form-section">
            <p>TELL US MORE ABOUT YOUR MUSIC</p>
            {{-- Genres --}}
            <div class="form-group form-group-alt {{ $errors->has('genre') ? ' has-error' : '' }}">
                {{ Form::label('genre[]', 'Select the genres the best describe your music.', ['class' => 'form-label']) }}
                @php
                    $allGenres = [];
                @endphp
                @if (isset($user))
                    @php
                        $artistGenres =  !empty($user->artist->genres) ?  $user->artist->genres : '';
                    @endphp
                    @foreach($artistGenres as $artistGenre)
                        @php
                            $allGenres[] = $artistGenre->id;
                        @endphp
                    @endforeach
                @endif

                <div class="form-controls">


                    <ul class="list-tags">
                        @if (!empty (old('genre')))
                            <?php $allGenres = old('genre');?>
                        @endif
                        @foreach ($genres as $genre)
                            @if ($genre->name!='Trending')
                                <li>
                                    <input type="checkbox" class="genre" name="genre[]" value="{{$genre->id}}"
                                           id="{{$genre->id}}"
                                           @if (in_array($genre->id, $allGenres)) checked="checked" @endif />
                                    <label for="{{$genre->id}}">{{$genre->name}}</label>
                                </li>
                            @endif
                        @endforeach
                        <script type="text/javascript">
                            $('input.genre').on('change', function (evt) {
                                if ($("input[name='genre[]']:checked").length > 3) {
                                    this.checked = false;
                                }
                            });
                        </script>
                    </ul><!-- /.list-tags -->
                </div><!-- /.form-controls -->
                @if ($errors->has('genre'))
                    <span class="help-block">
						<strong>{{ $errors->first('genre') }}</strong>
					</span>
                @endif
            </div>

            {{-- Event Types --}}
            <div class="form-group form-group-alt {{ $errors->has('eventTypes') ? ' has-error' : '' }}">
                {{ Form::label('eventTypes[]', "Select the types of events you'd like to play.", ['class' => 'form-label']) }}
                @php
                    $allTypes = [];
                @endphp
                @if (isset($user))
                    @php
                        $artistEventTypes =  !empty($user->artist->eventTypes) ?  $user->artist->eventTypes : '';
                    @endphp
                    @foreach($artistEventTypes as $artistEventType)
                        @php
                            $allTypes[] = $artistEventType->id;
                        @endphp
                    @endforeach
                @endif

                <div class="form-controls">
                    <ul class="list-tags">
                        @if (!empty (old('eventTypes')))
                            <?php $allTypes = old('eventTypes');?>
                        @endif
                        @foreach ($eventTypes as $type)
                            <li>
                                <input type="checkbox" class="eventType" name="eventTypes[]" value="{{$type->id}}"
                                       id="type_{{$type->id}}"
                                       @if (in_array($type->id, $allTypes)) checked="checked" @endif />
                                <label for="type_{{$type->id}}">{{$type->name}}</label>
                            </li>
                        @endforeach
                        <script type="text/javascript">
                            $('input.eventType').on('change', function (evt) {
                                if ($("input[name='eventType[]']:checked").length > 3) {
                                    this.checked = false;
                                }
                            });
                        </script>
                    </ul><!-- /.list-tags -->
                </div><!-- /.form-controls -->
                @if ($errors->has('eventTypes'))
                    <span class="help-block">
						<strong>{{ $errors->first('eventTypes') }}</strong>
					</span>
                @endif
            </div>

            {{--Song Types--}}

            <div class="form-group form-group-alt {{ $errors->has('songTypes') ? ' has-error' : '' }}">
                {{ Form::label('songTypes[]', "Select your song types.", ['class' => 'form-label']) }}
                @php
                    $allTypes = [];
                @endphp
                @if (isset($user))
                    @php
                        $artistSongTypes =  !empty($user->artist->songTypes) ?  $user->artist->songTypes : '';
                    @endphp
                    @foreach($artistSongTypes as $artistSongType)
                        @php
                            $allTypes[] = $artistSongType->id;
                        @endphp
                    @endforeach
                @endif

                <div class="form-controls">
                    <ul class="list-tags">
                        @if (!empty (old('songTypes')))
                            <?php $allTypes = old('songTypes');?>
                        @endif
                        @foreach ($songTypes as $type)
                            <li>
                                <input type="checkbox" class="songType" name="songTypes[]" value="{{$type->id}}"
                                       id="song_type_{{$type->id}}"
                                       @if (in_array($type->id, $allTypes)) checked="checked" @endif />
                                <label for="song_type_{{$type->id}}">{{$type->name}}</label>
                            </li>
                        @endforeach
                        <script type="text/javascript">
                            $('input.songType').on('change', function (evt) {
                                if ($("input[name='songType[]']:checked").length > 3) {
                                    this.checked = false;
                                }
                            });
                        </script>
                    </ul><!-- /.list-tags -->
                </div><!-- /.form-controls -->
                @if ($errors->has('songTypes'))
                    <span class="help-block">
						<strong>{{ $errors->first('songTypes') }}</strong>
					</span>
                @endif
            </div>

            {{-- Sounds Like --}}
            <?php
            if (isset($user) && !empty($user->artist->sounds_like)) {
                $sounds_like = explode(',', $user->artist->sounds_like);
            }
            if (isset($sounds_like[0])) {
                $sounds_like1 = trim($sounds_like[0]);
            }
            if (isset($sounds_like[1])) {
                $sounds_like2 = trim($sounds_like[1]);
            }
            if (isset($sounds_like[2])) {
                $sounds_like3 = trim($sounds_like[2]);
            }
            ?>

            <div class="form-group form-group-alt">
                <p>TELL US WHO YOU SOUND LIKE</p>
                <div class="form-controls">
                    <label for="soundlike1" class="form-label">1.</label>
                    <input type="text" class="form-control form-control-small" id="soundlike1" name="soundlike[]"
                           value='<?php !empty($sounds_like1) ? print $sounds_like1 : print ""; ?>'
                           placeholder="Name up to 3 artists your music sounds like.">
                </div><!-- /.form-controls -->

                <div class="form-controls">
                    <label for="soundlike2" class="form-label">2.</label>
                    <input type="text" class="form-control form-control-small" id="soundlike2" name="soundlike[]"
                           value='<?php !empty($sounds_like2) ? print $sounds_like2 : print ""; ?>'>
                </div><!-- /.form-controls -->

                <div class="form-controls">
                    <label for="soundlike3" class="form-label">3.</label>
                    <input type="text" class="form-control form-control-small" id="soundlike3" name="soundlike[]"
                           value='<?php !empty($sounds_like3) ? print $sounds_like3 : print ""; ?>'>
                </div><!-- /.form-controls -->
                <i class="ico-pencil"></i>
            </div><!-- /.form-group -->
        </div><!-- /.form-section -->
    </div><!-- /.col-sm-6 -->
</div><!-- /.row -->
<input id="videosCount" type="hidden"
       value="{{ (isset($user) && $user && $user->artist->videos) ? $user->artist->videos()->count() : 0 }}">
{{-- BUTTON --}}
<div class="form-actions">
    <p id='error_msg'></p>
    {{ Form::submit('APPLY', ['class' => 'btn btn-primary btn-invert form-btn']) }}
</div><!-- /.form-actions -->
<script type="text/javascript"
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDuCCI1t1qsTUO7xA57m7tY0ujVGgcOPjM&libraries=places"></script>
<script src="{{ asset('new-assets/dropzone/dropzone.js') }}"></script>
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': '{!! csrf_token() !!}'
        }
    });
    var count = 4 - $("#videosCount").val()
    if (count > 0) {
        @if(isset($user) && $user && $user->artist)
        $("#dropzoneUpload").dropzone({
            addRemoveLinks: true,
            url: "/artist/videos/{{ $user->artist->id }}",
            maxFiles: count,
            acceptedFiles: ".mp4",
            maxFilesize: 5000,
            headers: {
                'X-CSRF-TOKEN': '{!! csrf_token() !!}'
            },
            success: function (file, response) {
                if (response.success == true) {
                    toastr.success(response.message)
                } else {
                    toastr.error(response.message)
                }
            },
            error: function (file, message) {
                toastr.error(message)
            }
        });
        @endif
    }


    var input = document.getElementById('hometown');
    var options = {
        types: ['(cities)'],
        componentRestrictions: {country: 'us'}
    };
    var autocomplete = new google.maps.places.Autocomplete(input, options);

    //Get lat and log
    google.maps.event.addListener(autocomplete, 'place_changed', function () {
        var place = autocomplete.getPlace();
        var lat = place.geometry.location.lat();
        var lng = place.geometry.location.lng();

        $('#place-lat').val(lat);
        $('#place-lng').val(lng);
    })
</script>