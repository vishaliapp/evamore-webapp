@extends('layouts.secondary')

@section('content')
    <section id="register" class="section password-changed-wrapper">
        <div class="container">
            <div class="row">
                <h1>Hey,</h1>
                <h3>New password good to go. Click <a href="#" data-toggle="modal" data-target="#popup-login">here</a> to login.</h3>
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section><!-- /.section-sign -->
@stop