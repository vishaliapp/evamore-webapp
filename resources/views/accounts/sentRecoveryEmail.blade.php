@extends('layouts.secondary')
@section('content')
    <section class="section section-apply">
        <div class="container">
            <div class="section-head">
                @if (session('email'))
                    <h1>Password reset request sent to {{ session('email') }}</h1>
                @else
                    <h1>Password reset request sent to the notified email.</h1>
                @endif
            </div><!-- /.section-head -->
        </div><!-- /.container -->
    </section><!-- /.section -->
@stop

