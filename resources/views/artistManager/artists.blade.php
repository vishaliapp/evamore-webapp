@extends('layouts.dash')

@section('content')
    <div class="main-body">
        <div class="content artist-management">

            <div class="content-head">
                <h2>Artist Management</h2>
            </div><!-- /.content-head -->

            <div class="content-body">
                <div class="artist-manager-artists">
                    <div class="row">
                        <ul>
                            <li class="artist-box artist-box-add">
                                <div class="circle">
                                    <i class='glyphicon glyphicon-plus'></i>
                                </div>

                                <a href="{{route("artists_registration")}}" class="btn btn-fill">Add new artist</a>



                            </li>
                            @foreach($artistUsers as $artistUser)
                                <li class="artist-box active-{{$artistUser->active}}">
                                    <div>
                                        <div class="artist-image">
                                            @if ($artistUser->active)
                                                <a href="{{ route('artist_show', ['username' => $artistUser->username]) }}">
                                                    <img src="<?=Image::url('uploads/avatars/'.$artistUser->avatar,150,150,array('crop'))?>" alt="" width="150" height="150">
                                                </a>
                                            @else
                                                <img src="<?=Image::url('uploads/avatars/'.$artistUser->avatar,150,150,array('crop'))?>" alt="" width="150" height="150">
                                            @endif
                                        </div>
                                    </div>

                                    <div class="artist-name" style="text-decoration: none">
                                        {{$artistUser->name}}
                                    </div>
                                    <div class="actions">
                                        @if ($artistUser->active)
                                            <a href="{{ route('artist_show', ['username' => $artistUser->username]) }}"><span class="glyphicon glyphicon-eye-open"></span> VIEW</a>
                                            <span class="delimiter">|</span>
                                            <a href="{{ route('editartist', ['username'=>$artistUser->id]) }}"><span class="glyphicon glyphicon-edit"></span> EDIT</a>
                                        @else
                                            <span>INACTIVE</span>
                                        @endif
                                    </div>


                                    <div class="artist-events">
                                        <ul>
                                            <li><span>
                                                    {{array_key_exists($artistUser->id, $confirmedArtistsBids) ? $confirmedArtistsBids[$artistUser->id] : 0 }}
                                                </span> upcoming events</li>
                                            <li><span>
                                                    {{array_key_exists($artistUser->id, $newArtistsBids) ? $newArtistsBids[$artistUser->id] : 0 }}
                                                </span> requests</li>
                                        </ul>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div><!-- /.content-body -->
        </div><!-- /.content -->
    </div><!-- /.main-body -->
@stop