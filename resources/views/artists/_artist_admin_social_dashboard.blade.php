<div class="fluid-container">
  <div class="row">
  <div class="col-md-8">
<div class="fluid-container social-stats">
  <div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-5">
      <h4>Social Stats</h4>
    </div>
  </div>
  <div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-4">
        <h5>Followers</h5>
    </div>
    <div class="col-md-4">
        <h5>Interactions</h5>
    </div>
  </div>
  <div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-4">
        <?php echo intval(0); ?><i class="fa fa-caret-down" aria-hidden="true"></i>
    </div>
    <div class="col-md-4">
        <?php echo intval(0); ?><i class="fa fa-caret-down" aria-hidden="true"></i>
    </div>
  </div>
  <div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-4">
      <div class="fluid-container">
        <div class="row">
          <div class="col-md-3">
            <sub>1d</sub>
          </div>
          <div class="col-md-3">
            <sub>1w</sub>
          </div>
          <div class="col-md-3">
            <sub>1m</sub>
          </div>
          <div class="col-md-3">
            <sub>6m</sub>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-4">
      <div class="fluid-container">
        <div class="row">
          <div class="col-md-3">
            <sub>1d</sub>
          </div>
          <div class="col-md-3">
            <sub>1w</sub>
          </div>
          <div class="col-md-3">
            <sub>1m</sub>
          </div>
          <div class="col-md-3">
            <sub>6m</sub>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-4">
      <div class="fluid-container">
        <div class="row">
          <div class="col-md-3">
            <sub>0 <i class="fa fa-caret-down" aria-hidden="true"></i></sub>
          </div>
          <div class="col-md-3">
            <sub>0 <i class="fa fa-caret-down" aria-hidden="true"></i></sub>
          </div>
          <div class="col-md-3">
            <sub>0 <i class="fa fa-caret-down" aria-hidden="true"></i></sub>
          </div>
          <div class="col-md-3">
            <sub>0 <i class="fa fa-caret-down" aria-hidden="true"></i></sub>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-4">
      <div class="fluid-container">
        <div class="row">
          <div class="col-md-3">
            <sub>0 <i class="fa fa-caret-down" aria-hidden="true"></i></sub>
          </div>
          <div class="col-md-3">
            <sub>0 <i class="fa fa-caret-down" aria-hidden="true"></i></sub>
          </div>
          <div class="col-md-3">
            <sub>0 <i class="fa fa-caret-down" aria-hidden="true"></i></sub>
          </div>
          <div class="col-md-3">
            <sub>0 <i class="fa fa-caret-down" aria-hidden="true"></i></sub>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-5">
      <div class="fluid-container">
        <div class="row">
          <div class="col">&nbsp;
          </div>
        </div>
        <div class="row">

          <div class="col-md-16">
      <p><sup>Networks: </sup>
          @if($artistSocial->instagram_user)
              <i class="fa fa-instagram text-primary"></i>
          @else
              <i class="fa fa-instagram text-muted"></i>
          @endif
          <i class="fa fa-facebook text-muted"></i>
          <i class="fa fa-twitter text-muted"></i>
          <i class="fa fa-spotify text-muted"></i>
          <i class="fa fa-youtube text-muted"></i>
      </p>
          </div>
        </div>
      </div>
  </div>
</div>
</div>
</div>
</div>
