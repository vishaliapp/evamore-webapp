<div @if( isset($modal) && $modal ) id="myModal" data-toggle="modal" data-target="#artistInfo" @endif data-artist_id="{{$artist->user_id}}" class="col-bootstrap-default dark-slider-item col-xs-6 col-sm-6 col-lg-3 col-md-3 col-xl-3 text-center open-box">
    <div class="dark-slider-item-container">
        <div class="row">
            {{--<a href="{{route('artist_show', [$artist->user_name])}}"> sadasd </a>--}}
            <div class="col-xs-12 col-sm-12 col-lg-12 col-md-12 col-xl-12">
                <div class="dark-artist-image">
                    <a @if(  isset($modal) && $modal ) onclick="event.preventDefault()" @endif href="{{route('artist_show', ['username' => $artist->user_name])}}">
                        <img src="/uploads/avatars/{{$artist->avatar}}" alt="">
                        <input type="" name="" class="hidden artistName" value="{{route('artist_show', [$artist->user_name])}}">
                    </a>
                </div>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="dark-cell-caption col-xs-12 col-sm-12 col-lg-12 col-md-12 col-xl-12 checkbox">
                @if (isset($checkbox) && $checkbox && isset($status) && $status)
                    <label for="">
                        <input type="checkbox" class="artists-cell" value="{{$artist->user_id}}" />
                    </label>
                @endif
                <a @if(  isset($modal) && $modal ) onclick="event.preventDefault()" @endif href="{{route('artist_show', ['name' => $artist->user_name])}}"  class="artist-name" data-name="{{ $artist->user_name }}">
                    {{$artist->name}}

                </a>
            </div>
        </div>
        @if ( isset($showGenres) && $showGenres )
            <div class="row">
                <div class="artist-genres-list col-xs-12 col-sm-12 col-lg-12 col-md-12 col-xl-12">
                    @foreach($artist->genres as $key => $g)
                        <span class="artist-cell-genre">{{strtolower($g->name)}}</span>
                    @endforeach
                </div>
            </div>
        @endif
    </div>
</div>