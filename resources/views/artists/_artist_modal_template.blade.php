<script type="text/template" id="artist-in-modal">
    <div class="modal-artist-details" id="modal-artist-<%= user.id %>">
        <div class="avatar">
            <img src="<%= user.avatar %>"/>
        </div>

        <p class="name"><%= user.name %></p>
        <p class="hometown">Based In <%= user.hometown %></p>
        <p class="sounds_like">Sounds like <%= user.sounds_like %></p>
        <div class="genres">
            <% for(var i=0; i<user.genres.length; i++) { %>
                <span><%= user.genres[i].name %></span>
            <% } %>
        </div>

        <div class="media">
            <div class="channel_name"><%= user.description %></div>
            <div class="youtube_videos">
                <% if(user.channel_name) { %>
                    <ul></ul>
                    <span class="clear"></span>
                <% } %>
                <p class="request">
                    <a href="/events/create?booked_user_id=<%= user.id %>" class="btn btn-fill btn-invert">REQUEST TO BOOK</a>
                </p>
            </div>
            <% if(user.channel_name) { %>
                <p class="channel_link">
                <i class="fa fa-youtube-play" aria-hidden="true"></i>
                    <a href="http://www.youtube.com/channel/<%= user.channel_name %>" target="_blank">
                    youtube.com/<%= user.channel_name %></a>
                </p>
            <% } %>
            <% if(user.facebook_id) { %>
                <p class="facebook_link">
                <i class="fa fa-facebook" aria-hidden="true"></i>
                    <a href="https://www.facebook.com/<%= user.facebook_id %>" target="_blank">
                    facebook.com/<%= user.facebook_id %></a>
                </p>
            <% } %>
            <% if(user.twitter_handle) { %>
                <p class="twitter_link">
                <i class="fa fa-twitter" aria-hidden="true"></i>
                    <a href="https://twitter.com/<%= user.twitter_handle %>" target="_blank">
                    https://twitter.com/<%= user.twitter_handle %></a>
                </p>
            <% } %>
            <% if(user.website) { %>
                <p class="website_link">
                <i class="fa fa-globe" aria-hidden="true"></i>
                    <a href="<%= user.website %>" target="_blank"><%= user.website %></a>
                </p>
            <% } %>
        </div>
    </div>
</script>