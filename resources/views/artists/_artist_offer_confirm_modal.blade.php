{{--Loader--}}
<div class="loading" style="display: none">Loading&#8230;</div>
<!-- Modal -->
<div class="modal fade modal-artist-offer" id="popup-finish-payment" >
    <div class="modal-dialog" role="document">
        <button type="button" class="close outer" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <div class="modal-content">
            <div class="modal-body"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-fill form-btn btn-agree" data-dismiss="modal" >Close</button>
            </div>
        </div>
    </div>
</div>
<script>
        $(document).on('click','#forwardModalClose',function (e) {
            $('#modalLoginForm').modal('hide');
        })
        $(document).on('click','#forwardPayment',function (e) {
            e.preventDefault()
            console.log($('#forwardPayment').attr('data-bid-id'));
            var bidId = $('#forwardPayment').attr('data-bid-id');
            var mail = $('#email').val();
            var payerName = $('#payerName').val();
            $.ajax({
                url:'/forward/pay/'+bidId+'/'+mail+'',
                method:'post',
                data:{
                    mail:mail,
                    bidId:bidId,
                    payerName:payerName
                },
                success:function () {
                    $('#modalLoginForm').modal('hide');
                }
            })
        })

</script>
<script type="text/template" id="artist-offer-1">
    <div class="modal-artist-offer">
    <div class="modal-header">
        <h3>Good News!</h3>
        <div>
            <ul class="pay-steps">
                <li class="active step-n">REVIEW<br/><span>1</span></li>
                <li class=""><div></div></li>
                <li class="step-n">CONTRACT<br/><span>2</span></li>
                <li><div></div></li>
                <li class="step-n">PAYMENT<br/><span>3</span></li>
            </ul>
            {{--Good News! <%= artistUser.name %> has offered to play in <%= bid.event.name %>--}}
        </div>
    </div>
    <div class="modal-body details">
        <div class="row">
            <div class="col-12 col-sm-12 text-center">
                <a href="/artists/<%= artistUser.username %>" target="_blank"><%= artistUser.name %></a> has offered to perform in <a href="/events/<%= bid.event.id %>/display" target="_blank"><%= bid.event.name %></a>
            </div>
        </div>

        <div class="row">
            <div class="col-4 col-sm-4 details-artist">
                <div class="sub-title"><%= artistUser.name %></div>
                <div class="artist-image">
                    <img src="<%= artistUser.avatar %>"/>
                </div>
                <% if (artistUser.sounds_like) {%>
                    <div class="sounds-like">Sounds like <%= artistUser.sounds_like %></div>
                <% } %>
            </div>
            <div class="col-7 col-sm-7 details-event">
                <div class="sub-title">The Details</div>
                <table class="table">
                    <tr>
                        <td class=text-center><i class="fa fa-calendar" aria-hidden="true"></i></td>
                        <td>Date</td>
                        <td><%= bid.event.format_startdate %></td>
                    </tr>
<!--                    <tr>-->
<!--                        <td class="text-center"><i class="fa fa-globe" aria-hidden="true"></i></td>-->
<!--                        <td>City, State</td>-->
<!--                        <td><%= bid.event.city %>, <%= bid.event.state %></td>-->
<!--                    </tr>-->
                    <tr>
                        <td class="text-center"><i class="fa fa-map-marker" aria-hidden="true"></i></td>
                        <td>Venue</td>
                        <td><%= bid.event.venue %></td>
                    </tr>
                    <tr>
                        <td class="text-center"><i class="fa fa-clock-o" aria-hidden="true"></i></td>
                        <td>Duration</td>
                        <td><%= bid.event.duration %> min</td>
                    </tr>
                    <tr>
                        <td class="text-center"><i class="fa fa-credit-card" aria-hidden="true"></i></td>
                        <td>Total Fee</td>
                        <td>$<%= bid.price %></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <div class="row">
      <div class="col-12 col-sm-12 text-center">
          Minimum $<%= (Math.round(bid.price * 100/2) / 100).toFixed(2) %> due now.
      </div>
    </div>
    <div class="modal-footer">
        <input type='button' class='btn btn-danger decline' style="pointer-events: all" data-dismiss="modal" value="DECLINE"/>
        <a href="" class="btn btn-primary btn-invert">I AGREE</a>
    </div>
    </div>
</script>

<script type="text/template" id="payment-form-1">
    <div class="modal-bid-payment-form">
                <div class="align-center">
                <button class="btn btn-default btn-rounded mb-4" data-toggle="modal" data-target="#modalLoginForm">Forward Payment</button>

                </div>

                <div class="modal fade" id="modalLoginForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">Sign in</h4>
                <button type="button" class="close" id="forwardModalClose" >
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body mx-3">
                <div class="md-form mb-5">
                <i class="fas fa-user"></i>
                <input class="form-control validate" type="text" id="payerName" name="payerName">
                    <label data-error="wrong" data-success="right" for="payerName">Payer's Name</label>
                </div>

                <div class="md-form mb-4">
                    <i class="fa fa-envelope prefix grey-text"></i>
                    <input class="form-control validate" type="text" id="email" name="email">
                    <label data-error="wrong" data-success="right" for="email">Payer's E'mail</label>
                </div>

            </div>
            <div class="modal-footer d-flex justify-content-center">
                <button data-bid-id="<%= bid.id %>" id="forwardPayment" class="btn btn-default">SEND</button>
            </div>
        </div>
    </div>
</div>

    <form>
        {{ csrf_field() }}
        <input type="hidden" name="bid_id" value="<%= bid.id %>">
        <div class="modal-header">
            <h3>Payment & Confirmation</h3>
            <div>
                <ul class="pay-steps">
                    <li class="active step-n">REVIEW<br/><span>1</span></li>
                    <li class="active"><div></div></li>
                    <li class="active step-n">CONTRACT<br/><span>2</span></li>
                    <li class="active"><div></div></li>
                    <li class="active step-n">PAYMENT<br/><span>3</span></li>
                </ul>
                {{--Good News! <%= artistUser.name %> has offered to play in <%= bid.event.name %>--}}
            </div>
        </div>
        <div class="modal-body">
            <div class="error-message error-global"></div>

            <div class="for-support">Prefer to pay check? Please contact us at <br/><a href="mailto:{{config('mail.support_email')}}">{{config('mail.support_email')}}</a></div>
            <div>
                <table>
                    <tr>
                        <td colspan="2">
                            <div class="row pay-amount-selector">
                                <div class="col-12 col-xs-12">
                                    <span class="error-message error-payAmount"></span>
                                </div>
                                <div class="<% if(bid.order === null || bid.order.status == 0) { %>col-6 col-xs-6<%} else {%>col-12 col-xs-12<% } %>">
                                    <input type="radio" name="payAmount" value="{{ App\ModelType\BidType::HALF_AMOUNT }}" id="payAmount-1" checked/> <label for="payAmount-1">50%</label>
                                </div>

                                <% if(bid.order === null || bid.order.status == 0) { %>
                                    <div class="col-6 col-xs-6">
                                        <input type="radio" name="payAmount" value="{{ App\ModelType\BidType::FULL_AMOUNT }}" id="payAmount-2"/> <label for="payAmount-2">100%</label>
                                    </div>
                                <% } %>
                            </div>
                        </td>
                    </tr>

                    <% if(planerUser.payscapeVault !== null) { %>
                        <tr>
                            <td><input type="radio" name="payMethod" value="{{ App\ModelType\PaymentType::PAY_BY_VAULT }}" id="payMethod-1"
                                       <% if(planerUser.payscapeVault !== null) { %>
                                       checked
                                       <% } else { %>
                                        disabled
                                       <% } %>
                                        /> </td>
                            <td><label for="payMethod-1">Continue using previously entered card</label></td>
                        </tr>

                        <tr>
                            <td></td>
                            <td>Card #: xxxx - xxxx - xxxx -<% if(planerUser.payscapeVault !== null) { %>
                                    <%= planerUser.payscapeVault.card_tail %>
                                <% } else { %>
                                    xxxx
                                <% } %>
                            </td>
                        </tr>
                    <% } %>

                    <tr>
                        <td><input type="radio" name="payMethod" value="{{ App\ModelType\PaymentType::PAY_BY_CARD }}" id="payMethod-2" <% if(planerUser.payscapeVault === null) { %>checked<% } %>/> </td>
                        <td><label for="payMethod-2">Enter new credit card</label></td>
                    </tr>
                    <tr class="payment_method" <% if(planerUser.payscapeVault === null) { %>style="display: table-row"<% } else {%>style="display: none"<%} %>>
                        <td colspan="2" style="padding: 5px 10px">
                            <div class="row">
                                <div class="col-12 col-xs-12 text-center" style="padding: unset !important">
                                <select id="payment-type-change" class="form-control">
                                    <option value="default" selected disabled>Select payment method</option>
                                    <option value="amex">AMEX</option>
                                    <option value="other">VISA</option>
                                    <option value="other">MasterCard</option>
                                </select>
                                </div>
                            </div>
                        </td>
                    </tr>

                    <tr class="amex" style="display: none">
                        <td>
                            AMEX Card Number
                            <span class="error-message error-cardNumber"></span>
                        </td>
                        <td>
                            <div class="row">
                                <div class="col-xs-12">
                                    <input type="text" class="form-control card-info" id="amexCardNumber" name="amexCardNumber"/>
                                </div>
                            </div>
                        </td>
                    </tr>

                    <tr class="other" style="display: none">
                        <td>
                            Card Number
                            <span class="error-message error-cardNumber"></span>
                        </td>
                        <td>
                            <div class="row">
                                <div class="col-xs-12">
                                    <input type="text" class="form-control card-info" id="cardNumber" name="cardNumber"/>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr class="exp-date" style="display: none">
                        <td>
                            Expiration date
                            <span class="error-message error-expirationYear error-expirationMonth"></span>
                        </td>
                        <td>
                            <div class="row">
                                <div class="col-xs-4">
                                    <select class="form-control card-info" id="expirationMonth" name="expirationMonth" size="1">
                                        <% for (var i=1; i<=12; i++) { %>
                                            <option value="<%= i  %>"><%= (i<10) ? "0":"" %><%= i %></option>
                                        <% } %>
                                    </select>
                                </div>

                                <div class="col-xs-8">
                                    <select class="form-control card-info" id="expirationYear" name="expirationYear" size="1">
                                        <% var now = new Date(); var yearFrom = now.getFullYear(); %>
                                        <% for (var i=yearFrom; i<=(yearFrom+10); i++) { %>
                                            <option value="<%= i %>"><%= i %></option>
                                        <% } %>
                                    </select>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr class="other" style="display: none">
                        <td>
                            CVC
                            <span class="error-message error-cvc"></span>
                        </td>
                        <td>
                            <div class="row">
                                <div class="col-xs-4">
                                    <input type="text" class="form-control card-info" id="cvc" name="cvc" placeholder="" maxlength="3"/>
                                </div>
                            </div>
                        </td>

                    </tr>
                    <tr class="amex"  style="display: none">
                         <td>
                            CVV
                            <span class="error-message error-cvv"></span>
                        </td>
                        <td>
                            <div class="row">
                                <div class="col-xs-4">
                                    <input type="text" class="form-control card-info" id="cvv" name="cvv" placeholder="" maxlength="4"/>
                                </div>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td><input type="checkbox" name="saveCard" value="1" class="card-info" id="saveCardCheckbox"/></td>
                        <td>
                            <label for="saveCardCheckbox">Save this to your account</label>
                            <a href="#" data-toggle="tooltip" title="Saving card will enable you to pay in one click without entering card number, expiration date and CVV, and to schedule automated payment of the 50% remainder for each event in the future. In case you have already saved a different card, this will overwrite previous card one and the new one will be charged instead." data-placement="top"><span class="glyphicon glyphicon-info-sign"></span></a>
                        </td>
                    </tr>

                    <% if(bid.order === null || bid.order.status == 0) { %>
                    <tr>

                        <td><input type="checkbox" name="automaticalyPayment" id="automaticalyPayment" value="1"/></td>
                        <td>
                            <label for="automaticalyPayment">Pay remaning 50% automaticaly</label>
                            <a href="#" data-toggle="tooltip" title="Your saved credit card will be charged..." data-placement="top"><span class="glyphicon glyphicon-info-sign"></span></a>
                        </td>
                    </tr>
                    <% } %>
                </table>

            </div>
        </div>
        <div class="modal-footer">
            <button type="submit" class="btn btn-primary btn-invert" id="show-performance-contract">
                SUBMIT PAYMENT
            </button>
        </div>
    </form>
        </div>
</script>

<script type="text/template" id="booking-artist-details">
    <div class="modal-artist-details" id="modal-artist-<%= user.id %>">
        <div class="avatar">
            <img src="<%= user.avatar %>"/>
        </div>

        <p class="name"><%= user.name %></p>
        <p class="sounds_like">Sounds like <%= user.artist.sounds_like %></p>
        <div class="genres">
            <% for(var i=0; i<user.artist.genres.length; i++) { %>
            <span><%= user.artist.genres[i].name %></span>
            <% } %>
        </div>

        <div class="media">
            <div class="channel_name"><%= user.artist.description %></div>
            <p class="hometown">Based In <%= user.artist.hometown %></p>
            <p class="hometown">Based In <%= user.artist.hometown %></p>

            <div class="youtube_videos">
                <% if(user.artist.social.channel_name) { %>
                <ul></ul>
                <span class="clear"></span>
                <% } %>
            </div>
            <% if(user.artist.social.channel_name) { %>
            <p class="channel_link">
                <a href="http://www.youtube.com/channel/<%= user.artist.social.channel_name %>" target="_blank">youtube.com/<%= user.artist.social.channel_name %></a>
            </p>
            <% } %>
        </div>
    </div>
</script>

<script>
    $(function() {

        $(".offer-button").offerWindow({
            modal: $("#baseModal").clone().modal({show: false}),
            offerDetailsTemplate: _.template($('#artist-offer-1').text()),
            afterShowModal: function(offerWindowModal, bid, artistUser, planerUser) {

                $(".modal-footer", offerWindowModal).contractForm({
                    bid: bid,
                    artistUser: artistUser,
                    planerUser: planerUser,
                    afterShowModal: function(contractFormModal, bid, artistUser) {
                        offerWindowModal.modal('hide');
                    },
                    confirmContractCallback: function() {
                        var $m = $("#baseModal").clone().modal({show: false});

                        $m.on('shown.bs.modal', function (e) {
                          $("body").addClass("modal-open");
                        });
                        $m.on('hidden.bs.modal', function (e) {
                          $("body").removeClass("modal-open");
                        });

                        $m.paymentForm({
                            'template': _.template($('#payment-form-1').text()),
                            'bid': bid,
                            'artistUser': artistUser,
                            'planerUser': planerUser,
                            afterShowCallback: function(){
                                contractFormModal.modal('hide');
                            }
                        });
                    }
                });
            }

    });


        $('.booked-artist-details .details-1').on('click', function(){
            var id = $(this).attr("data-id");

            $.ajax({
                url: "/api/account/"+id,
                type: "GET",
                dataType: "json",
                success: function(response)
                {
                    if (response.success) {
                        $("#baseModal").clone().modal({show: false})
                        var tpl = _.template($('#booking-artist-details').text());
                        var html = tpl({user: response.user});

                        var $m = $('#baseModal').clone();
                        $m.addClass("modal-artist-info");
                        $(".modal-dialog", $m).addClass("modal-lg");
                        $(".modal-content", $m).html(html);

                        if (response.user.artist.social.channel_name) {
                            renderYoutubeVideosList(response.user.artist.social.channel_name, $(".youtube_videos ul", $m), 2);
                        }
                        $m.modal('show');
                    }
                }
            });
            return false;
        })
    });

</script>

@section('footerjs')
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.js"></script>
    <script src="{{asset("/js/dashOfferWindow.js")}}"></script>
    <script>
    </script>
@stop

