@if(count($artistsBidsToEvent) > 0)
<div class="event-artists">
    <h5>
            <span>
                <span id="artistCount">
                    {{count($artistsBidsToEvent)}} <span>Artists </span>have requested to play this event
                </span>
            </span>
    </h5>

    <ul class="artists artists-alt">
        @foreach($artistsBidsToEvent as $bid)
            <?php $artistUser = $bid->artistUser; ?>

            <li class="artist">
                <div class="artist-image-outer">
                    <div class="artist-image">
                        <a href="{{ route('artist_show', ['username' => $artistUser->username]) }}"><img src="<?=Image::url('uploads/avatars/'.$artistUser->avatar,95,95,array('crop'))?>" alt="" width="95" height="95"></a>
                    </div><!-- /.artist-image -->

                    <span class="artist-price">{{ App\Artist::getFeeType($artistUser->artist->fee) }}</span>
                </div><!-- /.artist-image-outer -->

                <div class="artist-inner">

                    {{--@if ($eventHasConfirmedArtist)--}}
                    {{--<span class="artist-name">{{$artistUser->name}}</span>--}}
                    {{--@else--}}
                    <a class="artist-name" href="{{ route('artist_show', ['username'=>$artistUser->username, 'bid'=>$bid->id])}}">{{$artistUser->name}}</a>
                    {{--@endif--}}

                    @if ($artistUser->confirmed_request)
                        <p><span class="text-success">booked</span></p>
                    @endif

                    @if($artistUser->artist->sounds_like)
                        <div>Sounds like {{$artistUser->artist->sounds_like}}</div>
                    @endif

                    @if($isEventOwner || $isAdmin)
                        <?php $order = $bid->getOrder(); ?>
                        <div>
                            @if(!$order || $order->status != \App\ModelType\OrderType::FULL_PAID)
                                <a class="offer-button" data-bid-id="{{$bid->id}}">VIEW OFFER</a>
                            @endif

                            @if($order)

                                @if($order->status == \App\ModelType\OrderType::HALF_PAID)
                                    <span style="color:#FF0000">50% paid</span>
                                @elseif($order->status == \App\ModelType\OrderType::FULL_PAID)
                                    <span style="color:#FF0000">100% paid</span>
                                @endif
                            @endif
                        </div>
                    @endif
                </div><!-- /.artist-inner -->
            </li><!-- /.artist -->
        @endforeach
    </ul><!-- /.artists -->
</div>



@endif

