@extends('layouts.artist')

@section('js')
	<script src="{{asset(file_exists('/js/requestedArtists.min.js') ? '/js/requestedArtists.min.js' : '/js/requestedArtists.js')}}"></script>
@stop

@section('content')
		<div class="main-body">
			<div class="content">
				<div class="content-head">
					<h2>Our Artists</h2>
					@if($user[0]->role_name == 'Admin')
    					<a href='/artists/registration' style='color:red'><i>Add an artist</i></a>
					@endif

					<p>SELECT A GENRE</p>
				</div><!-- /.content-head -->

				<div class="content-body">
                    @php
                    $allGenres = [];
                    @endphp

                    <ul id="filters" class="list-tags list-tags-alt">
                        @if (!empty (old('genre')))
                            <?php $allGenres = old('genre')?>
                        @endif;
                        @foreach ($genres as $genre)
                            <li>
                                <input type="checkbox" name="genre[]" value="{{$genre->id}}" id="{{$genre->id}}"
                                @if (in_array($genre->id, $allGenres))
                                    checked="checked"
                                @endif
                                />
                                <label for="{{$genre->id}}">{{$genre->name}}</label>
                            </li>
                        @endforeach
                    </ul><!-- /.list-tags -->

                    @if ($errors->has('genre'))
                        <span class="help-block">
			            <strong>{{ $errors->first('genre') }}</strong>
			                </span>
                    @endif


					<ul id='requestedArtistData' class="artists">
						<!-- Fill This in Dynamically with Artist Data-->
					</ul><!-- /.artists -->
					<center>
						<form id='activateArtist' method='post' action="{{ route('artistactivate') }}">
							{{ csrf_field() }}
							<input type='submit' value='activate' class="btn btn-primary btn-invert">
						</form>
					</center>
				</div><!-- /.content-body -->
			</div><!-- /.content -->
		</div><!-- /.main-body -->
@stop