@extends('layouts.dash', ['user'=>$currentUser])

@section('js')
    <script src="{{asset("/js/youtube.min.js")}}"></script>
@stop
@section('css')
    @if(env('APP_ENV') == 'prod')
        <style>
            #expBut {
                display: none;
            }

            #exampleModal {
                display: none;
            }
        </style>
    @endif
@endsection
@section('content')
    <div class="main-body">
        <div class="content">
            <div class="event-detail">
                <div class="event-detail-head">
                    {{--Download Modal--}}
                    <button style="float: right;" type="button" id="expBut" class="btn btn-primary" data-toggle="modal"
                            data-target="#exampleModal">
                        Export
                    </button>
                    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
                         aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Export Options</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <h1>Please, select which sections of the artist profile to export:</h1>
                                    <div class="form-group" style="display: flex;justify-content: space-between;">
                                        <div class="form-group">
                                            <h2>Artist Info</h2>
                                            @foreach($artistData as $key => $data)
                                                <div class="form-group">
                                                    <input class="pdfDatas" id="{{ $data }}" type="checkbox"
                                                           value="{{$key}}">
                                                    <label for="{{$data}}">{{$data}}</label>
                                                </div>
                                            @endforeach
                                        </div>
                                        <div class="form-group">
                                            <h2>Artist social info</h2>
                                            @foreach($artistPdfSocial as $key => $data)
                                                <div class="form-group">
                                                    <input class="pdfSocialDatas" id="{{ $data }}" type="checkbox"
                                                           value="{{$data}}">
                                                    <label for="{{$data}}">{{$key}}</label>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>

                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="button" id="downloadPdf" class="btn btn-primary">Save changes</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    @if(Session::has('message'))
                        <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
                    @endif

                    <h2>
                        @if (!empty($artistUser->name))
                            {{$artistUser->name}}
                        @endif

                        <small>
                            @if (!empty($artistUser->artist->sounds_like))
                                Sounds like: {{$artistUser->artist->sounds_like}}
                            @endif
                        </small>
                    </h2>

                    @php
                        $genres = $artistUser->artist->genres;
                    @endphp
                    @if (!empty($genres))
                        <ul class='list-tags'>"
                            @foreach($genres as $genre)
                                <li class='selected'>
                                    <a href="{{ route('dashboard', ['genre_id'=>$genre->id]) }}">{{$genre->name}}</a>
                                </li>
                            @endforeach
                        </ul><!-- /.list-tags -->
                    @endif

                </div><!-- /.event-detail-head -->

                <div class="event-detail-body">
                    <div class="row">

                        <div class="col-md-9 col-md-offset-3">
                            <h4>
                                @if($artistAbout->hometown)
                                    Hailing from {{$artistAbout->hometown}}
                                @endif
                            </h4>
                        </div>

                        <div class="col-md-3">
                            <div class="event-detail-avatar">
                                <img src="<?=Image::url('uploads/avatars/' . $artistUser->avatar, 195, 195, array('crop'));?>"
                                     height="195" width="195" alt="">
                            </div><!-- /.event-detail-avatar -->
                        </div><!-- /.col-sm-3 -->

                        <div class="col-md-5 col-sm-6">
                            <div class="event-detail-content">
                                {{--<p class="event-detail-icons">--}}
                                {{--<a href="#">--}}
                                {{--<i class="ico-heart"></i>--}}
                                {{--</a>--}}

                                {{--<a href="#">--}}
                                {{--<i class="ico-bubble"></i>--}}
                                {{--</a>--}}

                                {{--<a href="#">--}}
                                {{--<i class="ico-block"></i>--}}
                                {{--</a>--}}
                                {{--</p><!-- /.event-detail-icons -->--}}

                                <p>
                                    @if($artistAbout->description)
                                        {{$artistAbout->description}}
                                    @endif
                                </p>

                                <div class="actions">
                                    <p>
                                        @if($artistSocial->website)

                                            @if(!starts_with($artistSocial->website, 'http://') && !starts_with($artistSocial->website, 'https://'))
                                                @php
                                                    $pre = "http://";
                                                @endphp
                                            @else
                                                @php
                                                    $pre = "";
                                                @endphp
                                            @endif
                                            <a target="_blank" href="{{$pre}}{{$artistSocial->website}}"><i
                                                        class="fa fa-globe"></i>

                                            </a>
                                        @endif
                                    </p>

                                    <p>
                                        @if($artistSocial->facebook_id)
                                            <a target="_blank"
                                               href='https://www.facebook.com/{{$artistSocial->facebook_id}}'<i
                                                    class="fa fa-facebook"></i>

                                            </a>
                                        @endif
                                    </p>
                                    <p>
                                        @if($artistSocial->twitter_handle)
                                            <a target="_blank"
                                               href="https://twitter.com/{{$artistSocial->twitter_handle}}"><i
                                                        class="fa fa-twitter"></i>

                                            </a>
                                        @endif
                                    </p>
                                    <p>
                                        @if($artistSocial->instagram_user)
                                            <a target="_blank"
                                               href="https://www.instagram.com/{{$artistSocial->instagram_user}}"><i
                                                        class="fa fa-instagram"></i></a>
                                        @endif
                                    </p>
                                </div><!-- /.actions -->
                            </div><!-- /.event-detail-content -->
                        </div><!-- /.col-sm-5 -->

                        <div class="col-md-4 col-sm-6">
                            <div class="event-detail-callout">
                                @if ($artistAbout->fee)
                                    <h6><span>Booking Fee</span></h6>
                                    {{--<span class="event-detail-price" data-toggle="tooltip" data-placement="top"--}}
                                    {{--title="min. ${{$artistAbout->fee}}">${{ $artistAbout->fee }}</span>--}}
                                    <span class="event-detail-price" data-toggle="tooltip" data-placement="top"
                                          title="min. ${{$artistAbout->fee}}">{{ App\Artist::getFeeTypeText($artistAbout->fee)['sign'] }}</span>
                                @endif

                                <div class="actions">
                                    <?php if ($isUserAdmin || $isManager){ ?>
                                    <a href="{{ route('editartist', ['id'=>$artistUser->id]) }}"
                                       class="btn btn-primary btn-invert">Update Artist</a><br/><br/>
                                    <?php } ?>

                                    @if($canConfirmRequest && $bid)
                                        @if($bid->status == \App\ModelType\BidType::STATUS_CONFIRMED)
                                            <span class="btn btn-fill btn-inver">Artist Booked</span>
                                        @else
                                            <button type="button" class="btn btn-fill btn-inver offer-button"
                                                    data-bid-id="{{$bid->id}}" style="padding: 0 16px">Confirm This
                                                Artist
                                            </button>

                                            {{--<form action='{{route('confirm_artist_request')}}' method='post' id='request_to_confirm'>--}}
                                            {{--{{ csrf_field() }}--}}
                                            {{--<input type='hidden' name='bid_id' value='{{$bid->id}}'>--}}
                                            {{--<input type='submit' class='btn btn-fill btn-inver' value='Confirm This Artist' style="padding: 0 16px"/>--}}
                                            {{--</form>--}}
                                        @endif
                                    @else

                                        @if($isUserCanBook)
                                            <form action='{{route('artist_bookrequest')}}' method='post'
                                                  id='book_artist'>
                                                {{ csrf_field() }}
                                                <input type='hidden' name='artist_id' value='{{$artistUser->id}}'>
                                                {{--<input type='submit' class='btn btn-fill btn-inver' value='Request To Book'>--}}

                                                <div class="form-group {{ $errors->has('fee') ? ' has-error' : '' }}">
                                                    <select class="form-control selectpicker " title="Request to book"
                                                            name="event_id" data-style="btn btn-fill btn-inver">
                                                        <option value="new" class="new_event">+ Create new Event
                                                        </option>
                                                        @if(count($planerEvents)>0)
                                                            @foreach($planerEvents as $planerEvent)
                                                                <option value="{{ $planerEvent->id }}">{{ $planerEvent->name }}</option>
                                                            @endforeach
                                                        @endif
                                                    </select>
                                                </div>
                                            </form>

                                        @endif
                                    @endif
                                </div><!-- /.actions -->
                            </div><!-- /.event-detail-callout -->
                        </div><!-- /.col-sm-4 -->
                    </div><!-- /.row -->
                    @if ($isUserAdmin && $artistSocial->instagram_user)
                        @include("artists._artist_admin_social_dashboard")
                    @endif
                </div><!-- /.event-detail-body -->
            </div><!-- /.event-detail -->

            <div class="row event-detail">
                {{ $youtube_channel_name = '' }}
                {{ $youtube_channel_id = '' }}
                @if($artistUser->artist->videos)
                    <div class="col-md-6 socials-block">
                        @foreach($artistUser->artist->videos as $video)
                            <video controls>
                                    <source src="{{ asset('/uploads/videos/'.$video->path) }}" type="video/mp4" >
                            </video>

                        @endforeach
                    </div>
                @endif


                <?php $spotify_uri = $artistSocial->spotify_uri; ?>
                @if ($spotify_uri)
                    <div class="col-md-6 socials-block">
                        <header class="socials-block-head spotify">
                            <span class="socials-block-icon"><i class="fa fa-spotify"></i></span>
                        </header><!-- /.socials-block-head -->

                        <?php /* Added block to display twitter feed of related artist based on their twiiter page detail */ ?>
                        <div class="socials-block-body">
                            @if (!empty($spotify_uri))
                                <p>
                                <center>
                                    <iframe src="https://open.spotify.com/embed?uri={{$spotify_uri}}" width="80%"
                                            height="445" frameborder="0" allowtransparency="true"></iframe>
                                </center>
                                </p>

                            @endif
                        </div><!-- /.socials-block-body -->
                    </div><!-- /.col-sm-6 -->
                @endif


                <?php $twitter_handle = $artistSocial->twitter_handle; ?>
                @if (!$spotify_uri && $twitter_handle)
                    <div class="col-md-6 socials-block">
                        <header class="socials-block-head">
                            <span class="socials-block-icon"><i class="ico-twitter-logo"></i></span>
                            <p>
                                @if ($twitter_handle)
                                    <a target="_blank"
                                       href="https://twitter.com/{{$twitter_handle}}">#{{$twitter_handle}}</a>
                                @endif
                            </p>
                        </header><!-- /.socials-block-head -->

                        <?php /* Added block to display twitter feed of related artist based on their twiiter page detail */ ?>
                        <script src="{{asset("https://platform.twitter.com/widgets.js")}}"></script>
                        <div class="socials-block-body">
                            @if (!empty($twitter_handle))
                                <a id="twitter"
                                   class="twitter-timeline"
                                   data-chrome="noheader noscrollbar transparent"
                                   data-screen-name="{{$twitter_handle}}"
                                   height="470px"
                                   href="https://twitter.com/{{$twitter_handle}}"
                                   data-widget-id="600069081396568064" data-show-replies="false">
                                </a>
                            @endif
                        </div><!-- /.socials-block-body -->
                    </div><!-- /.col-sm-6 -->
                @endif
            </div><!-- /.row -->
            @if($artistInstagram)
                <div class="row instagram-gallery">

                    <h4>Instagram Gallery</h4>

                    @foreach ($artistInstagram['media']->data as $media)
                        <div class="col-md-3">
                            <a href="{{$media->link}}" target="_blank">
                                <img style="width: 195px; height: 195px; margin:10px auto;"
                                     class="img-thumbnail img-responsive"
                                     src="{{$media->images->low_resolution->url}}"/>
                            </a>
                        </div>

                    @endforeach
                </div>
            @endif
        </div><!-- /.content -->
    </div><!-- /.main-body -->

    <div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel"
         id="confirm-book-artist-to-event">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-body"></div>
                <div class="modal-footer modal-footer-confirm" style="text-align: center">
                    <button type="button" class="btn btn-danger btn-cancel" data-dismiss="modal">CANCEL</button>
                    <button type="button" class="btn btn-fill form-btn btn-confirm" data-dismiss="modal"
                            onclick="$('#book_artist').submit()">CONFIRM
                    </button>
                </div>
            </div>
        </div>
    </div>
    <script>
        $('#avatar').val('banner_directory');
        var pdfDatas = [];
        var pdfSocialDatas = [];
        $('#downloadPdf').on('click', function () {
            $(".pdfDatas:checked").each(function () {
                pdfDatas.push($(this).val());
            });
            $(".pdfSocialDatas:checked").each(function () {
                pdfSocialDatas.push($(this).val());
            });

            $.ajax({
                headers: {
                    'X-CSRF-Token': "{{ csrf_token() }}",
                },
                url: "{{ route('download.pdf',$artistAbout->id) }}",
                method: 'post',
                data: {
                    data: pdfDatas,
                    socialData: pdfSocialDatas
                },
                success: function (response) {
                    $('#exampleModal').modal('hide');
                    $(".pdfDatas:checked").each(function () {
                        $(this).prop('checked', false);
                    });
                    if (response != '') {
                        window.open("{!! url('file/') !!}" + '/' + response)
                    }
                }
            });
        });
    </script>

    <script type="text/template" id="youtube-iframe-tpl">
        <iframe width="<%= data.width %>" height="<%= data.height %>" src="https://www.youtube.com/embed/<%= data.video_id%>?rel=0&amp;showinfo=0&amp;modestbranding=1&amp;autoplay=1" frameborder="0" allowfullscreen></iframe>
</script>

<script type="text/javascript">
    $(function(){
        <?php echo "artistInfo = " . json_encode($artistUser); ?>;
        <?php echo "var planerEvents = " . json_encode($planerEvents); ?>;
        <?php echo "var planerEventsForArtist = " . json_encode($planerEventsForArtist); ?>;

        var requestedEvents = [];
        for(var i=0; i<planerEventsForArtist.length; i++) {
            requestedEvents.push(planerEventsForArtist[i].id);
        }

        $('#book_artist select').on('changed.bs.select', function (event, clickedIndex, newValue, oldValue) {
            var $form = $(" #book_artist");
        var selected_value = $("option:eq("+clickedIndex+")", $(event.currentTarget)).val();
        if (selected_value == "new") {
        $form.submit();
        } else

        if (parseInt(selected_value) > 0){

        var selectedEvent;
        for (var i=0; i
        <planerEvents.length; i++) {
        if (selected_value == planerEvents[i].id) {
        selectedEvent = planerEvents[i];
        break;
        }
        }
        if (selectedEvent) {
        var $modal = $("#confirm-book-artist-to-event");

        var d = new Date();
        d.setTime(Date.parse(selectedEvent.starttime));
        var hours, apm;
        if (d.getHours() > 12) {
        hours = d.getHours() - 12;
        apm = "pm";
        } else {
        hours = d.getHours();
        apm = "am";
        }

        var content;
        if (requestedEvents.indexOf(selected_value) < 0){
        content = "Request "+artistInfo.name+" to play at "+selectedEvent.name+" on "+selectedEvent.format_starttime+"?";
        $(".btn-cancel", $modal).text("CANCEL");
        $(".btn-confirm", $modal).show();
        } else {
        content = artistInfo.name+" has already been requested to play at "+selectedEvent.name +".";
        $(".btn-cancel", $modal).text("CLOSE");
        $(".btn-confirm", $modal).hide();
        }

        $(".modal-body", $modal).html(content);
        $modal.modal('show');
        }
        }
        });
        })


    </script>


    @include("artists._artist_offer_confirm_modal")
    @include('layouts._modal_payment_performance_agreement')
@stop
