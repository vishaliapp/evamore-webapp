<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <title>EVAmore</title>
    <link rel="shortcut icon" type="image/x-icon" href="/images/favicon.ico"/>
    <link href='{{asset("//fonts.googleapis.com/css?family=Lato:400,300,300italic,700")}}' rel='stylesheet'
          type='text/css'>
    <link href='{{asset('//fonts.googleapis.com/css?family=Open+Sans:300,400,400italic,600,700')}}' rel='stylesheet'
          type='text/css'>
    <link rel="Stylesheet" type="text/css" href="{{asset('demo/prism.css')}}"/>
    {{--        <link rel="Stylesheet" type="text/css" href="{{asset('bower_components/sweetalert/dist/sweetalert.css')}}" />--}}
    <link rel="Stylesheet" type="text/css" href="{{asset('croppie.css')}}"/>
    <link rel="Stylesheet" type="text/css" href="{{asset('demo/demo.css')}}"/>
    <link rel="icon" href="{{asset('//foliotek.github.io/favico-64.png')}}"/>

    <!-- Vendor Styles -->
    <link rel="stylesheet" href="{{asset("/addons/bootstrap/css/bootstrap.min.css", null, false)}}" type="text/css"
          media="all"/>
    <link rel="stylesheet" href="{{asset("/addons/bootstrap/css/bootstrap-select.min.css", null, false)}}"
          type="text/css" media="all"/>
    <link rel="stylesheet" href="{{asset("/addons/formstone/dropdown.css", null, false)}}" type="text/css" media="all"/>
    <link rel="stylesheet" href="{{asset("/addons/bootstrap-datepicker/bootstrap-datepicker.min.css", null, false)}}"
          type="text/css"
          media="all"/>
    <link rel="stylesheet" href="{{asset("/css/jquery-ui-bootstrap-master/jquery.ui.theme.css", null, false)}}"
          type="text/css" media="all"/>

    <!-- App Styles -->
    <link rel="stylesheet" href="{{asset( '/css/style.css' )}}"/>
    <link rel="stylesheet"
          href="{{asset("/bower_components/components-font-awesome/css/font-awesome.min.css", null, false)}}"
          type="text/css" media="all"/>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <![endif]-->
    <script src="{{asset("/js/underscore-min.js", null, false)}}"></script>

    <!-- Vendor JS -->
    <script src="{{asset("/addons/moment-with-locales.js", null, false)}}"></script>
    <script src="{{asset("/addons/jquery-1.11.3.min.js", null, false)}}"></script>
    <script src="{{asset("/addons/jquery.form.min.js", null, false)}}"></script>
    <script src="{{asset("/addons/bootstrap/js/bootstrap.min.js", null, false)}}"></script>
    <script src="{{asset("/addons/formstone/core.js", null, false)}}"></script>
    <script src="{{asset("/addons/formstone/touch.js", null, false)}}"></script>
    <script src="{{asset("/addons/formstone/dropdown.js", null, false)}}"></script>
    <script src="{{asset("/addons/modernizr.custom.js", null, false)}}"></script>
    <script src="{{asset("/addons/classie.js", null, false)}}"></script>
    <script src="{{asset("/addons/bootstrap-datepicker/bootstrap-datepicker.min.js", null, false)}}"></script>
    <script src="{{asset("/addons/bootstrap/js/bootstrap-select.min.js", null, false)}}"></script>

    <!-- App JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.7.3/js/bootstrap-select.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.js"></script>
    <script src="{{asset(file_exists('/js/functions.min.js') ? '/js/functions.min.js' : '/js/functions.js')}}"></script>
    <script src="{{ asset('js/forwardPayment.js') }}"></script>
    <script src="{{asset(file_exists('/js/constants.min.js') ? '/js/constants.min.js' : '/js/constants.js')}}"></script>



</head>
<body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<script src="{{ asset('js/cpanelPendingBids.js') }}"></script>
<div class="loading" style="display: none">Loading&#8230;</div>
<div class="modal fade modal-artist-offer" id="popup-finish-payment">
    <div class="modal-dialog" role="document">
        <button type="button" class="close outer" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
        </button>
        <div class="modal-content">
            <div class="modal-body"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-fill form-btn btn-agree" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<input class="bidId" type="hidden" value="{{ $bid->id }}">
<div class="container" id="firstStepDiv" style="overflow: hidden;">
    <div class="modal-artist-offer">
        <div class="modal-header">
            <h3>Good News!</h3>
            <div>
                <ul class="pay-steps">
                    <li class="active step-n">REVIEW<br/><span>1</span></li>
                    <li class="">
                        <div></div>
                    </li>
                    <li class="step-n">CONTRACT<br/><span>2</span></li>
                    <li>
                        <div></div>
                    </li>
                    <li class="step-n">PAYMENT<br/><span>3</span></li>
                </ul>
                {{--Good News! <%= artistUser.name %> has offered to play in <%= bid.event.name %>--}}
            </div>
        </div>
        <div class="modal-body details">
            <div class="row">
                <div class="col-12 col-sm-12 text-center">
                    <a href="/artists/{{ $artist->user->username }}" target="_blank">{{ $artist->user->name }}</a> has
                    offered to perform in
                    <a href="/events/{{ $event->id }}/display" target="_blank">{{ $event->name }}</a>
                </div>
            </div>

            <div class="row">
                <div class="col-4 col-sm-4 details-artist">
                    <div class="sub-title">{{ $artist->user->name }}</div>
                    <div class="artist-image">
                        <img src="{{ asset('uploads/avatars/'.$artist->user->avatar ) }}"/>
                    </div>
                    @if($artist->sounds_like)
                        <div class="sounds-like">Sounds like {{ $artist->sounds_like }}</div>
                    @endif
                </div>
                <div class="col-7 col-sm-7 details-event">
                    <div class="sub-title">The Details</div>
                    <table class="table">
                        <tr>
                            <td class=text-center><i class="fa fa-calendar" aria-hidden="true"></i></td>
                            <td>Date</td>
                            <td>{{ $event->starttime }}</td>
                        </tr>
                        <tr>
                            <td class="text-center"><i class="fa fa-globe" aria-hidden="true"></i></td>
                            <td>City, State</td>
                            <td>{{ $event->city }}, {{ $event->state }}</td>
                        </tr>
                        <tr>
                            <td class="text-center"><i class="fa fa-map-marker" aria-hidden="true"></i></td>
                            <td>Venue</td>
                            <td>{{ $event->venue }}</td>
                        </tr>
                        <tr>
                            <td class="text-center"><i class="fa fa-clock-o" aria-hidden="true"></i></td>
                            <td>Duration</td>
                            <td>{{ $event->duration }} min</td>
                        </tr>
                        <tr>
                            <td class="text-center"><i class="fa fa-credit-card" aria-hidden="true"></i></td>
                            <td>Total Fee</td>
                            <td>${{ $bid->price }}</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-sm-12 text-center">
                {{--//Math priceeeee--}}
                {{--Minimum $<%= (Math.round(bid.price * 100/2) / 100).toFixed(2) %> due now.--}}
            </div>
        </div>
        <div class="modal-footer">
            <input type='button' class='btn btn-danger decline' style="pointer-events: all" data-dismiss="modal"
                   value="DECLINE"/>
            <button id="firstStepButton" class="btn btn-primary btn-invert">I AGREE</button>
        </div>
    </div>
</div>

<div class="container" id="secondStepContract" style="overflow: hidden;display: none;">
    @include('layouts.new_contract_modal')
</div>



{{--<script type="text/template" id="payment-form-1">--}}
<div class="container" id="thirdStepPay"
     style=" overflow: hidden;display: none">
    @include('layouts.paymentLastStep')
</div>
<script>
    {{--console.log($("input:checked[name='payMethod']").val())--}}
    {{--$('#thirdStepPayButton').on('click', function (e) {--}}
        {{--e.preventDefault();--}}
        {{--$.ajax({--}}
            {{--url: '{{ route('api_forward_pay_bid') }}',--}}
            {{--method: 'post',--}}
            {{--data: {--}}
                {{--bid_id: '{{ $bid->id }}',--}}
                {{--pay_method: paymentMethod--}}


            {{--}--}}
        {{--})--}}

    {{--})--}}
</script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.js"></script>
    <script>
    </script>
</body>
</html>