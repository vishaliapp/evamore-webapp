@extends('layouts.dark_layout')
@section('css')
    @parent
    @if(env('APP_ENV') == 'prod')
        <style>
            .pdfData{
                display: none;
            }
            #exampleModal{
                display: none;
            }
            #export{
                display: none;
            }
        </style>
    @endif
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <link rel="stylesheet" href="{{asset('/css/dark_style.css')}}"/>
@endsection
@section('sidebar_content')
    <section class="sidebar dark-sidebar artist-dark-sidebar">
        <div class="sidebar-container clear">
            <a class="navbar-brand" href="/">Evamore</a>

            <div class="sidebar-inner visible-xs">
                <div class="user">
			<span class="user-avatar">
				<img src="<?=Image::url('uploads/avatars/' . $currentUser->avatar, 39, 39, array('crop'));?>"
                     height="39"
                     width="39" alt="">
			</span><!-- /.user-avatar -->

                    <ul class="user-dropdown">
                        <li>
                            <a href="{{ ($isArtist) ? route('editartist', ['id'=>$currentUser->id]) : route('edit_account', ['id'=>$currentUser->id]) }}">SETTINGS</a>
                        </li>

                        <li>
                            <a href="{{ route('logout') }}">LOGOUT</a>
                        </li>
                    </ul>
                    <!-- /.user-dropdown -->
                </div>
                <!-- /.user -->

                <button type="button" class="navbar-toggle collapsed" id="trigger-overlay" data-toggle="collapse"
                        data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <!-- /.visible-xs -->

            <div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
                <?php $left_menu_selected = isset($left_menu_selected) ? $left_menu_selected : '' ?>
                <ul class="sidebar-nav">
                    @if(!$isArtistManager && !$isArtist && !$currentUser->hasRole('ADMIN'))
                        <li class="{{ ($left_menu_selected == 'dashboard')? 'active' : '' }}">
                            <a href="/dashboard">
                            <span class="sidebar-nav-ico">
<i class="fa fa-arrows ico-arrows" aria-hidden="true"></i>
                            </span>
                                <span>Dashboard</span>
                            </a>
                        </li>
                    @endif
                    <li class="{{ ($left_menu_selected == 'upcoming')? 'active' : '' }}">
                        <a href="/events/upcoming">
                            <span class="sidebar-nav-ico">
                                <i class="ico-calendar"></i>
                            </span>
                            @if($isAdmin)
                                <span>UPCOMING EVENTS</span>
                            @else
                                <span>MY EVENTS</span>
                            @endiF

                            @if(!$isArtist)
                                <span class="sidebar-nav-count">@currentUserEventsCount()</span>
                            @endif
                        </a>
                    </li>
                    @pastEvents
                    <li class="{{ ($left_menu_selected == 'past')? 'active' : '' }}">
                        <a href="/events/past">
                            <span class="sidebar-nav-ico">
                              <i class="ico-past"></i>
                            </span>
                            <span>PAST EVENTS</span>
                        </a>
                    </li>
                    @endPastEvents
                    @if($isArtistManager)
                        <li class="{{ ($left_menu_selected == 'artists')? 'active' : '' }}">
                            <a href="{{ route('manager_artists') }}">
                                <span class="sidebar-nav-ico"><i class="ico-note"></i></span>
                                <span>MY ARTISTS</span>
                            </a>
                        </li>
                    @elseif(!$isArtist)
                        <li class="{{ ($left_menu_selected == 'artists')? 'active' : '' }}">
                            <a href="{{ route('artistsearch') }}">
                                <span class="sidebar-nav-ico"><i class="ico-note"></i></span>
                                <span>ARTISTS</span>
                            </a>
                        </li>
                        <div class="hidden-xs sidebar-container dark-sidebar-container clear">
                            <div class="row">
                                <div class="dark-genres-sidebar-header col-xs-12 col-sm-12 col-lg-11 col-md-11 col-xl-11 text-left">
                                    <i class="fa fa-music"></i>&nbsp;
                                    <div class="dark-genres-sidebar-header-text">
                                        <strong>Genres</strong><br>
                                        <small class="text-muted">(Select up to 3)</small>
                                    </div>
                                </div>
                            </div>
                            <div class="row hidden-xs">
                                <div class="col-xs-12 col-sm-12 col-lg-11 col-md-11 col-xl-11 text-left filter-wrapper">
                                    @include("widgets._menugenres")
                                </div>
                            </div>
                            <br>
                        </div>
                        <div class="hidden-xs sidebar-container dark-sidebar-container clear">
                            <div class="row">
                                <div class="dark-genres-sidebar-header col-xs-12 col-sm-12 col-lg-11 col-md-11 col-xl-11 text-left">
                                    <i class="fa fa-tag"></i>&nbsp;
                                    <div class="dark-genres-sidebar-header-text">
                                        <strong>Price Range</strong><br>
                                        <small class="text-muted">(Select up to 3)</small>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-lg-11 col-md-11 col-xl-11 text-left filter-wrapper">
                                    @foreach (\App\Artist::getFeeTypesText() as $value => $text)
                                        <div class="dark-genres-link-wrapper dark-genres-link-wrapper-money">
                                            <a data-fee_value="{{$value}}"
                                               class="artist-filter-price {{in_array($value, $request_fee) !== false ? 'active' : ''}}"
                                               href="#" data-toggle="tooltip" data-placement="top"
                                               title="{{$text['text']}}">
                                                <span>{{$text['sign']}}</span>
                                            </a>
                                        </div>
                                    @endforeach
                                    <br>
                                </div>
                            </div>
                        </div>
                        <div class="hidden-xs sidebar-container dark-sidebar-container clear">
                            {{--Location Filter--}}
                            <div class="row">
                                <div class="dark-genres-sidebar-header col-xs-12 col-sm-12 col-lg-11 col-md-11 col-xl-11 text-left">
                                    <i class="fa fa-map-marker"></i>&nbsp;
                                    <div class="dark-genres-sidebar-header-text">
                                        <strong>Location</strong><br>
                                        {{--<small class="artist-filter-location text-muted reset">Reset</small>--}}
                                        <small class="text-muted">(Choose a location)</small>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-lg-11 col-md-11 col-xl-11 text-left filter-wrapper"
                                     style="padding-bottom: 20px">
                                    <select style="width: 90%; background: #444; color: #fff; border: none"
                                            id="select-filter-location" class="js-example-basic-multiple">
                                        <option value="">
                                            Choose a Location
                                        </option>
                                        <option value="" class="artist-filter-location currentLocation">
                                            My Current Location
                                        </option>
                                        @foreach ($locations as $location)
                                            <div class="dark-genres-link-wrapper dark-genres-link-wrapper-money">
                                                <option value="{{ $location }}"
                                                        class="artist-filter-location" {{$location == $req_location ? 'selected' : ''}}>
                                                    {{ $location }}
                                                </option>
                                            </div>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        {{----}}
                    @endif

                    <li class="{{ ($left_menu_selected == 'support')? 'active' : '' }}">
                        <a href="{{ route('support') }}">
                    <span class="sidebar-nav-ico">
                        <i class="ico-support"></i>
                    </span>
                            <span>Support</span>
                        </a>
                    </li>


                    @if($currentUser->hasRole('ADMIN'))

                        <li class="{{ ($left_menu_selected == 'user_management')? 'active' : '' }}">
                            <a href="{{route('cpanel_users_index')}}">
                                <span class="glyphicon glyphicon-user" style="font-size:21px;"></span>
                                <span>User Management</span>
                            </a>
                        </li>
                        <li class="{{ ($left_menu_selected == 'artists_payments')? 'active' : '' }}">
                            <a href="{{route('cpanel_payments_page')}}">
                                <span class="glyphicon glyphicon-usd" style="font-size:21px;"></span>
                                <span>PAYMENT SCHEDULING</span>
                            </a>
                        </li>
                    @endif
                </ul>
                <!-- /.sidebar-nav -->
            </div>
        </div>
    </section>
@endsection
@section('content')
    <div class="main" style="padding-bottom: 10px;">
        <div class="visible-xs"><br><br><br></div>
        <div class="container">
            <div class="main-head clear">
                <div class="main-head-inner text-center" style="margin-left: 40px;">
                    @if(!$isArtist)
                        @if($currentUser->hasRole('ADMIN'))
                            <p><span id="upcomingEvents">@upcomingEventsCount()</span> Upcoming events for all users</p>
                        @else
                            <p>You have <span id="upcomingEvents">@currentUserEventsCount()</span> upcoming event(s)</p>
                        @endif
                    @endif
                </div>
                <!-- /.main-head-inner -->

                <div class="user">
                    <span class="arrow-down glyphicon glyphicon-triangle-bottom"></span>
                    <span class="user-avatar">
						<img src="<?=Image::url('uploads/avatars/' . $currentUser->avatar, 39, 39, array('crop'));?>"
                             height="39" width="39" alt="">
					</span><!-- /.user-avatar -->

                    <span class="user-name">{{ $currentUser->username }}</span>

                    <ul class="user-dropdown">
                        <li>
                            <a href="{{ ($isArtist) ? route('editartist', ['id'=>$currentUser->id]) : route('edit_account', ['id'=>$currentUser->id]) }}">SETTINGS</a>
                        </li>

                        <li>
                            <a href="/logout">LOGOUT</a>
                        </li>
                    </ul>
                    <!-- /.user-dropdown -->
                </div>
                <!-- /.user -->
            </div>
            <!-- /.main-head -->

            @yield('content')

        </div>
        <!-- /.container -->
    </div>
    {{--@if( ! $isAdmin && $featured_artists->count() > 0 )--}}
    {{--<div class="dark-container-wrapper dark-container-wrapper-loggedin">--}}
    {{--<div id="myCarousel" class="carousel slide dark-container dark-container-wrapper-featured" data-ride="carousel">--}}
    {{--<h3 class="dark-carousel-header">FEATURED ARTISTS</h3>--}}
    {{--<div class="carousel-inner" role="listbox">--}}
    {{--@foreach ($featured_artists as $key => $artists_array)--}}
    {{--<div class="item {{$key == 0 ? 'active' : ''}}">--}}
    {{--<div class="row">--}}
    {{--@foreach ($artists_array as $a)--}}
    {{--@include('artists._artist_cell', ['artist' => $a])--}}
    {{--@endforeach--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--@endforeach--}}
    {{--</div>--}}
    {{--<a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">--}}
    {{--<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>--}}
    {{--<span class="sr-only">Previous</span>--}}
    {{--</a>--}}
    {{--<a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">--}}
    {{--<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>--}}
    {{--<span class="sr-only">Next</span>--}}
    {{--</a>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--@endif--}}
    @if(Session::has('message'))
        <section class="section section-artists dark-section">
            <div class="container-fluid dark-container-fluid">
                <div class="dark-filtered-artists-wrapper">
                    <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
                </div>
            </div><!-- /.container -->
        </section><!-- /.section -->
    @endif
    <section class="section section-artists dark-section">
        <div class="container-fluid dark-container-fluid">
            <div class="dark-filtered-artists-wrapper">
                @if ($isAdmin)
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-lg-12 col-md-12 col-xl-12 text-center">
                            <a class="text-danger" href='/artists/registration'><i>Add an artist</i></a><br/>
                            <a class="text-danger" href='{{route("artistsearch", ['status'=>'inactive'])}}' id="active"><i>Review
                                    artist profile requests</i></a><br/>
                            <a class="text-danger" href='{{route("artistsearch", ['status'=>'active'])}}' id="inactive"><i>Review
                                    active artist profiles</i></a>
                            <br>
                        </div>
                    </div>
                    <br><br>
                @endif
                <div class="row">
                    <div class="col-xs-5 col-lg-3 text-right">
                        <h4 class="hidden-xs  filtered-results-header">FILTERED RESULTS</h4>
                    </div>
                    <div class="col-xs-12 col-sm-7">
                        <div id="imaginary_container">
                            <div class="input-group stylish-input-group">
                                <input type="search" class="form-control py-2 border-right-0 border" id="searchBar"
                                       placeholder="Search">
                                <span class="input-group-addon">
                                    <i class="fa fa-search"></i>
                                </span>
                            </div>
                        </div>
                        <div id="mainSearch">
                            <ul id="searchResponse" class="artist_search"></ul>
                        </div>
                    </div>
                </div>


                <div class="row  hidden-lg hidden-md">
                    <div class="hidden-xs">
                        <br><br>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-lg-12 col-md-12 col-xl-12 text-center">
                        <script type="text/javascript">
                            (function ($) {
                                $(function () {
                                    $("#select-filter-genre").select2({maximumSelectionLength: 3});
                                })
                            })(window.jQuery);
                        </script>
                        <label for="select-filter-genre">Genres</label><br>
                        <select style="width: 90%;" id="select-filter-genre" class="js-example-basic-multiple"
                                multiple="multiple">
                            @foreach ($genres as $g)
                                <option {{in_array($g->name, $request_genres) !== false ? 'selected="selected"' : ''}} data-genre_name="{{$g->name}}"
                                        value="{{$g->name}}">{{$g->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="row  hidden-lg hidden-md">
                    <div class="col-xs-12 col-sm-12 col-lg-12 col-md-12 col-xl-12 text-center">
                        <script type="text/javascript">
                            (function ($) {
                                $(function () {
                                    $("#select-filter-fee").select2({maximumSelectionLength: 3});
                                })
                            })(window.jQuery);
                        </script>

                        <label for="select-filter-fee">Fee</label><br>
                        <select style="width: 90%;" id="select-filter-fee" class="js-example-basic-multiple"
                                multiple="multiple">
                            @foreach (\App\Artist::getFeeTypesText() as $value => $data)
                                <option {{in_array($value, $request_fee) !== false ? 'selected="selected"' : ''}} value="{{$value}}">{{$data['sign']}} {{$data['spaces']}}
                                    <div class="fee-type-amount">{{$data['text']}}</div>
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>
                @if ( ! $artists->count() )
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-lg-12 col-md-12 col-xl-12 text-center">
                            <br><br>
                            <h1 class="text-muted">
                                Select genres to view artists.
                            </h1>
                        </div>
                    </div>
                @endif
                @foreach ($artists->chunk(4) as $set)
                    <div class="row">
                        @foreach ($set->chunk(2) as $artistSet)
                            @foreach ($artistSet as $artist)
                                <div @if( isset($modal) && $modal ) id="myModal" data-toggle="modal"
                                     data-target="#artistInfo" @endif data-artist_id="{{$artist->user_id}}"
                                     class="col-bootstrap-default dark-slider-item col-xs-6 col-sm-6 col-lg-3 col-md-3 col-xl-3 text-center open-box">
                                    <div class="dark-slider-item-container">
                                        <div class="row">
                                            <input class="pdfData" type="checkbox" value="{{ $artist->id }}">
                                            {{--<a href="{{route('artist_show', [$artist->user_name])}}"> sadasd </a>--}}
                                            <input type="" name="" class="hidden artistName"
                                                   value="{{route('artist_show', [$artist->user_name])}}">

                                            <div class="col-xs-12 col-sm-12 col-lg-12 col-md-12 col-xl-12">
                                                <div class="dark-artist-image">
                                                    <a @if(  isset($modal) && $modal ) onclick="event.preventDefault()"
                                                       @endif href="{{url('artists',$artist->artist_username ) }}">
                                                        <img src="/uploads/avatars/{{$artist->avatar}}" alt="">
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div class="dark-cell-caption col-xs-12 col-sm-12 col-lg-12 col-md-12 col-xl-12 checkbox">
                                                @if (isset($checkbox) && $checkbox && isset($status) && $status)
                                                    <label for="">
                                                        <input type="checkbox" class="artists-cell"
                                                               value="{{$artist->user_id}}"/>
                                                    </label>
                                                @endif
                                                <a @if(  isset($modal) && $modal ) onclick="event.preventDefault()"
                                                   @endif href="{{url('artists',$artist->artist_username ) }}"
                                                   class="artist-name">
                                                    {{$artist->name}}
                                                </a>
                                            </div>
                                        </div>
                                        @if ( isset($showGenres) && $showGenres )
                                            <div class="row">
                                                <div class="artist-genres-list col-xs-12 col-sm-12 col-lg-12 col-md-12 col-xl-12">
                                                    @foreach($artist->genres as $key => $g)
                                                        <span class="artist-cell-genre">{{strtolower($g->name)}}</span>
                                                    @endforeach
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            @endforeach
                            {{--Artists may have different genres amount thus height may differ--}}
                            <div class="clearfix visible-xs visible-sm"></div>
                        @endforeach
                    </div>
                @endforeach
                @if ($artists->count() < 5)
                    {{-- Hack for the left sidebar --}}
                    <div class="visible-lg">
                        <br><br><br><br><br><br><br><br><br><br><br><br><br><br>
                    </div>
                @endif

                <div class="row">
                    <div class="dark-pagination-block col-xs-12 col-sm-12 col-lg-12 col-md-12 col-xl-12 text-right">
                        {{$artists->appends($appends)->links()}}
                    </div>
                </div>
                @if($isAdmin)
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-lg-12 col-md-12 col-xl-12">
                            <form method="POST" action="{{route('artists_activation_change')}}">
                                {{csrf_field()}}
                                <input type="hidden" id="artists_ids" name="artists" value="[]">
                                <div class="text-center">
                                    @if($status == "inactive")
                                        <input type="hidden" name="action" value="activate">
                                        <input type="submit" class="btn btn-primary btn-invert"
                                               id="btn-activate-artists"
                                               value="ACTIVATE"/>
                                    @elseif  ($status == "active")
                                        <input type="hidden" name="action" value="deactivate">
                                        <input type="submit" class="btn btn-primary btn-invert"
                                               id="btn-activate-artists"
                                               value="DEACTIVATE"/>
                                    @endif
                                </div>
                            </form>
                        </div>
                    </div>
                    <script>
                        (function ($) {
                            $(function () {
                                $artistsCell = $('.artists-cell'),
                                    $artists_ids = $('#artists_ids');

                                $artistsCell.change(function () {
                                    var arr = [];
                                    $artistsCell.each(function () {
                                        var $this = $(this);
                                        if ($this.prop('checked')) {
                                            arr.push(+$this.val());
                                        }
                                    });
                                    console.log('arr', arr);
                                    console.log('string = ', JSON.stringify(arr));
                                    $artists_ids.val(JSON.stringify(arr));
                                });
                            });
                        })(window.jQuery);
                    </script>
                @endif

                {{--Pdf data export modal--}}
                <button id="export" data-toggle="modal" class="btn btn-success">EXPORT
                </button>
                    <p class="text-danger" id="exportError"></p>
                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
                     aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Export Options</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <h1>Please, select which sections of the artist profile to export:</h1>
                                <div class="form-group" style="display: flex;justify-content: space-between;">
                                    <div class="form-group">
                                        <h2>Artist Info</h2>
                                        <div class="form-group">
                                            <input id="checkAll" type="checkbox" >
                                            <label for="checkAll">Choose all</label>
                                        </div>
                                        @foreach($artistData as $key => $data)
                                            <div class="form-group">
                                                <input class="pdfDatas" id="{{ $data }}" type="checkbox"
                                                       value="{{$key}}">
                                                <label for="{{$data}}">{{$data}}</label>
                                            </div>
                                        @endforeach
                                        @foreach($artistUserData as $key => $data )
                                            <div class="form-group">
                                                <input class="pdfArtistUserDatas" id="{{ $data }}" type="checkbox"
                                                       value="{{$key}}">
                                                <label for="{{$data}}">{{$data}}</label>
                                            </div>
                                        @endforeach
                                    </div>
                                    <div class="form-group">
                                        <h2>Artist social info</h2>
                                        @foreach($artistPdfSocial as $key => $data)
                                            <div class="form-group">
                                                <input class="pdfSocialDatas" id="{{ $data }}" type="checkbox"
                                                       value="{{$data}}">
                                                <label for="{{$data}}">{{$key}}</label>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>

                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="button" id="downloadPdf" class="btn btn-primary">Save changes</button>
                            </div>
                        </div>
                    </div>
                </div>

                    <div class="loading" style="display: none">Loading&#8230;</div>

                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-lg-12 col-md-12 col-xl-12">
                        <p class="text-center">
                            <small>Have a different artist in mind? Email <a href="mailto:support@evamore.co">support@evamore.co</a>
                            </small>
                        </p>
                    </div>
                </div>
            </div>
        </div><!-- /.container -->
    </section><!-- /.section -->

    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $("body").click(function (evt) {
            if (evt.target.id !== "searchBar") {
                $('#searchResponse').empty();
            }

        });
        $('#searchBar').on('input', function () {
            if ($(this).val() == '') {
                $('#searchResponse').empty();
            }

            $.ajax({
                url: '/artist/search-bar/' + $('#searchBar').val(),
                method: 'post',
                success: function (data) {

                    $('#searchResponse').empty();
                    var url = {!! json_encode(url('/')) !!};
                    data.forEach(function (item) {
                        $('#searchResponse').append('<li class="searchItems">' +
                            '<a href="' + url + '/artists/' + item.username + '">' +
                            '<img src="uploads/avatars/' + item.avatar + '" height="39" width="39">'
                            + item.name +
                            '</a>' +
                            '</li>');
                    });
                    $('#searchResponse').remove('.searchItems');

                }
            })


        });



        //get wrapper element
        var wrapper = $('.dark-filtered-artists-wrapper');

        //get sidebar and wrapper heights
        var sideHeight = $('.sidebar-container').height();
        var wrapHeight = wrapper.height();
        //check if sidebar height is bigger than wrapper height
        if (sideHeight > wrapHeight) {
            wrapper.css({'min-height': sideHeight - 90});
        }
    </script>
    {{-- equaling sidebar and wrapper heights --}}
@endsection
@section('js')
    @parent
    <script src="{{asset( '/js/filterArtists.js'  )}}"></script>
    <script src="{{asset("/js/youtube.min.js")}}"></script>
    <link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet"/>
    <script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    <script type="text/javascript"
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDuCCI1t1qsTUO7xA57m7tY0ujVGgcOPjM"></script>

    {{--Get Current Location--}}
    <script>

            var artistId = [];
        $('.pdfData').on('click', function () {
            artistId.push($(this).val())
        })
            $('#checkAll').on('click',function() {
                var checkedStatus = this.checked;
                $('.pdfDatas').each(function() {
                    $(this).prop('checked', checkedStatus);
                });
                $('.pdfSocialDatas').each(function() {
                    $(this).prop('checked', checkedStatus);
                });
                $('.pdfArtistUserDatas').each(function() {
                    $(this).prop('checked', checkedStatus);
                });
            });

        $('#export').on('click',function () {
            if (artistId.length == 0){
                $('#exportError').html('Select artists for exporting data')
            }  else {
                $('#exportError').html('')
                $('#exampleModal').modal();
            }
        })

        $('#downloadPdf').on('click', function () {
            var pdfDatas = [];
            var pdfSocialDatas = [];
            var artistUserData = [];
            $(".pdfDatas:checked").each(function () {
                pdfDatas.push($(this).val());
            });
            $(".pdfSocialDatas:checked").each(function () {
                pdfSocialDatas.push($(this).val());
            });
            $(".pdfArtistUserDatas:checked").each(function () {
                artistUserData.push($(this).val());
            });


            $.ajax({
                url: '{{ route('dowload.checked.pdf') }}',
                method: 'post',
                data: {
                    artists: artistId,
                    pdfDatas: pdfDatas,
                    socialDatas: pdfSocialDatas,
                    artistUserData:artistUserData
                },
                beforeSend: function () {
                    $('.loading').css({display: 'block'});
                },
                success: function (response) {
                    $('.loading').css({display: 'none'});
                    $('#exampleModal').modal('hide');
                    $(".pdfDatas:checked").each(function () {
                        $(this).prop('checked', false);
                    });
                    $(".pdfSocialDatas:checked").each(function () {
                        $(this).prop('checked', false);
                    });
                    $(".pdfArtistUserDatas:checked").each(function () {
                        $(this).prop('checked', false);
                    });

                    if (response != ''){
                        downloadURI(response);

                    }

                }

            })
        })

        function downloadURI(id)
        {
            var link = document.createElement("a");
            link.download = 'PdfData';
            link.href = "{!! url('file/') !!}" + '/' + id;
            link.click();
        }


        getLocation();
        var geocoder;

        function getLocation() {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(successFunction, errorFunction);
            }
        }

        //Get the latitude and the longitude;
        function successFunction(position) {
            var lat = position.coords.latitude;
            var lng = position.coords.longitude;
            codeLatLng(lat, lng);
        }

        function errorFunction() {
            console.log("Geocoder failed");
        }

        function codeLatLng(lat, lng) {
            geocoder = new google.maps.Geocoder();
            var latlng = new google.maps.LatLng(lat, lng);
            geocoder.geocode({'latLng': latlng}, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[1]) {
                        //find country name
                        for (var i = 0; i < results[0].address_components.length; i++) {
                            for (var b = 0; b < results[0].address_components[i].types.length; b++) {
                                //there are different types that might hold a city admin_area_lvl_1 usually does in come cases looking for sublocality type will be more appropriate
                                if (results[0].address_components[i].types[b] == "administrative_area_level_1") {
                                    city = results[0].address_components[i];
                                    break;
                                }
                            }
                        }
                        //city data
                        current_location = city.short_name;
                        $('.currentLocation').attr('value', current_location);
                    } else {
                        console.log("No results found");
                    }
                } else {
                    console.log("Geocoder failed due to: " + status);
                }
            });
        }
    </script>
@stop
