<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap-grid.min.css">
    <style>


        table {
            border-collapse: collapse;
            width: 100%;
            background: #eee;
        }

        th {
            height: 50px;
        }

        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }
        body {
            min-width: 320px;
            background: #fff;
            font-family: 'Lato', sans-serif;
            font-size: 18px;
            line-height: 1.5;
            color: #333;
        }

        .img-thumbnail {
            padding: 4px;
            float: left;
            line-height: 1.42857143;
            background-color: #fff;
            border: 1px solid #ddd;
            border-radius: 4px;
            -webkit-transition: all .2s ease-in-out;
            -o-transition: all .2s ease-in-out;
            transition: all .2s ease-in-out;
            display: inline-block;
            max-width: 100%;
            height: auto;
        td, th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even) {
            background-color: #dddddd;
        }

        .event-detail-avatar {
            position: relative;
            overflow: hidden;
            width: 100%;
            height: 0;
            padding-top: 100%;
            border-radius: 50%;
        }
    </style>

</head>
<body>
@if(array_key_exists('name', $pdfData))
    <h2>{{ $pdfData['name'] }}</h2>
@endif
@if(array_key_exists('sounds_like', $pdfData))
    <small>Sounds like:{{ $pdfData['sounds_like'] }}</small>
@endif
@if(array_key_exists('statetown',$pdfData))
    <small>Statetown:{{ $pdfData['statetown'] }}</small>
@endif
@if(array_key_exists('fee',$pdfData))
    <small>Fee:{{ $pdfData['fee'] }}</small>
@endif
<img style="float: left;"
     src="<?=Image::url('uploads/avatars/' . $pdfData['banner_directory'], 195, 195, array('crop'));?>" alt="">
@if(array_key_exists('hometown',$pdfData))
    <h4>Hailing from{{ $pdfData['hometown'] }}</h4>
@endif
@if(array_key_exists('description',$pdfData))
    <h4>{{ $pdfData['description'] }}</h4>
@endif
{{--Twitter handle--}}
<p>
    @if($pdfSocialData['twitter_handle'])
        <a target="_blank"
           href="https://twitter.com/{{$pdfSocialData->twitter_handle}}"><i
                    class="fa fa-twitter"></i>

        </a>
    @endif
</p>

{{--Artist' Youtube channel--}}
@if($pdfSocialData->channel_name)
    <?php
    $youtube_channel_name = '';
    $youtube_channel_id = '';
    if (!empty($pdfData['channel_name'])) :
        $youtube_channel_name = $pdfSocialData->channel_name;
    endif;
    ?>
    <div class="socials-block" style="width:100%">
        <header class="socials-block-head">
            <span class="socials-block-icon"><i class="ico-youtube-logo"></i></span>
            <p>
                @if ($youtube_channel_name)
                    @if(str_contains($youtube_channel_name,'youtube.com/'))
                        <a target="_blank" href='{{$youtube_channel_name}}'>{{$youtube_channel_name}}</a>
                    @else
                        <a target="_blank" href='http://www.youtube.com/{{$youtube_channel_name}}'>Youtube.com/{{$youtube_channel_name}}</a>
                    @endif
                @endif
            </p>
        </header><!-- /.socials-block-head -->


        {{--Instagram gallery--}}
        @if($pdfSocialData->instagram_user)
            <div class="instagram-gallery" style="width:100%">

                <h4>Instagram Gallery</h4>

                @foreach ($artistInstagram['media']->data as $media)
                    <span style="width:100%">
                             <a href="{{$media->link}}" target="_blank">
                            <img style=""
                                 class="img-fluid"
                                 src="{{$media->images->low_resolution->url}}"/>
                             </a>
                        </span>



                @endforeach
            </div>

            <h2>Social Stats</h2>
            <table>
                <thead>
                <tr>
                    <th>Followers</th>
                    <th>Interactions</th>
                </tr>
                <tr>
                    <th>{{ $artistInstagram['user']->data->counts->followed_by }}</th>
                    <th>0</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>1d</td>
                    <td>1w</td>
                    <td>1m</td>
                    <td>6m</td>
                    <td>1d</td>
                    <td>1w</td>
                    <td>1m</td>
                    <td>6m</td>
                </tr>
                <tr>
                    <td>0</td>
                    <td>0</td>
                    <td>0</td>
                    <td>0</td>
                    <td>0</td>
                    <td>0</td>
                    <td>0</td>
                    <td>0</td>
                </tr>
                </tbody>
            </table>








                @endif


                <div class="socials-block-body">
                    <ul class="list-videos"></ul>

                    <script type="text/javascript">
                        $(document).ready(function () {
                            var iframeTemplate = _.template($('#youtube-iframe-tpl').text());


                            var youtubeObj = new YoutubeObject("<?php echo $youtube_channel_name; ?>", "<?php echo $youtube_channel_id; ?>");
                            for (var key in youtubeObj.videos) {
                                $(".list-videos").append("<li><img src='" + key + "' data-video='" + youtubeObj.videos[key] + "'></li>");
                            }

                            $(".list-videos img").on("click", function () {
                                var height = $(this).parent().height();
                                var link = $(this).attr("data-video");
                                var content = iframeTemplate({
                                    data: {
                                        video_id: link,
                                        width: $(this).width(),
                                        height: height
                                    }
                                });
                                $(this).parent().html(content).height(height);
                            });
                        });
                    </script>
                </div><!-- /.socials-block-body -->
            </div><!-- /.col-sm-6 -->
@endif

{{--Twitter--}}
<?php $twitter_handle = $pdfSocialData->twitter_handle; ?>
@if ($twitter_handle)
    <div class="col-md-6 socials-block">
        <header class="socials-block-head">
            <span class="socials-block-icon"><i class="ico-twitter-logo"></i></span>
            <p>
                @if ($twitter_handle)
                    <a target="_blank"
                       href="https://twitter.com/{{$twitter_handle}}">#{{$twitter_handle}}</a>
                @endif
            </p>
        </header><!-- /.socials-block-head -->
        {{--<script src="https://platform.twitter.com/widgets.js"></script>--}}
        {{--<div class="socials-block-body">--}}
            {{--@if (!empty($twitter_handle))--}}
                {{--<a id="twitter"--}}
                   {{--class="twitter-timeline"--}}
                   {{--data-chrome="noheader noscrollbar transparent"--}}
                   {{--data-screen-name="{{$twitter_handle}}"--}}
                   {{--height="470px"--}}
                   {{--href="https://twitter.com/{{$twitter_handle}}"--}}
                   {{--data-widget-id="600069081396568064" data-show-replies="false">--}}
                {{--</a>--}}
            {{--@endif--}}
        {{--</div><!-- /.socials-block-body -->--}}
    </div><!-- /.col-sm-6 -->
@endif

</body>
</html>
