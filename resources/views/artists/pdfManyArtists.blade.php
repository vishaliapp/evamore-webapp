        <!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap-grid.min.css">
    <style>


        table {
            border-collapse: collapse;
            width: 100%;
            background: #eee;
        }

        th {
            height: 50px;
        }

        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        body {
            min-width: 320px;
            background: #fff;
            font-family: 'Lato', sans-serif;
            font-size: 18px;
            line-height: 1.5;
            color: #333;
        }

        .img-thumbnail {
            padding: 4px;
            float: left;
            line-height: 1.42857143;
            background-color: #fff;
            border: 1px solid #ddd;
            border-radius: 4px;
            -webkit-transition: all .2s ease-in-out;
            -o-transition: all .2s ease-in-out;
            transition: all .2s ease-in-out;
            display: inline-block;
            max-width: 100%;
            height: auto;
        }

        td, th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even) {
            background-color: #dddddd;
        }

        .event-detail-avatar {
            position: relative;
            overflow: hidden;
            width: 100%;
            height: 0;
            padding-top: 100%;
            border-radius: 50%;
        }
    </style>

</head>
<body>
@foreach($artistsId as $data)

        @if($data['avatar'])
            <img src="<?=Image::url('uploads/avatars/' . $data['avatar'], 100, 100, array('crop'));?>"
                 height="100" width="100" alt="">
            <br/>
        @endif

        @if($data['name'])
            <small>Name: {{ $data['name'] }}</small>
            <br/>
        @endif

    @if($data['sounds_like'])
        <small>Sounds like:{{ $data['sounds_like'] }}</small>
        <br/>
    @endif

    @if($data['statetown'])
        <small>Statetown:{{ $data['statetown'] }}</small>
        <br/>
    @endif

    @if($data['fee'])
        <small>Fee: {{ $data['fee'] }}</small>
        <br/>
    @endif

    @if($data['hometown'])
        <small>{{ $data['hometown'] }}</small>
        <br/>
    @endif

    @if($data['description'])
        <h4>{{ $data['description'] }}</h4>
        <br/>
    @endif



        {{--Artist' Youtube channel--}}
        @if($data['channel_name'])
            <a target="_blank"
               href="https://www.youtube.com/channel/{{$data['channel_name']}}">#{{$data['channel_name']}}</a>
        @endif
        <?php $twitter_handle = $data['twitter_handle']; ?>
        @if ($twitter_handle)
            <div class="col-md-6 socials-block">
                <header class="socials-block-head">
                    <span class="socials-block-icon"><i class="ico-twitter-logo"></i></span>
                    <p>
                        @if ($twitter_handle)
                            <a target="_blank"
                               href="https://twitter.com/{{$twitter_handle}}">#{{$twitter_handle}}</a>
                        @endif
                    </p>
                </header><!-- /.socials-block-head -->
            </div><!-- /.col-sm-6 -->
        @endif

{{--Twitter--}}
        <pagebreak></pagebreak>
        @endforeach

</body>
</html>

