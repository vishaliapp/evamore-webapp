@extends('layouts.secondary')

@section('content')	
	<section class="section section-apply">
		<div class="container">
			<div class="section-head">
				<h1>Thank You!</h1>
				
				<h2>We are not bots. A real member of the EVAmore team will check out your application and let you know within 48 hours.</h2>
			</div><!-- /.section-head -->

			<div class="section-body">
				<div class="socials socials-alt">
					<p>SHARE</p>

					<ul>
						<li>
							<a href="#">
								<i class="ico-facebook-large"></i>
							</a>
						</li>
						
						<li>
							<a href="#">
								<i class="ico-twitter-large"></i>
							</a>
						</li>
					</ul>
				</div><!-- /.socials -->
			</div><!-- /.section-body -->
		</div><!-- /.container -->
	</section><!-- /.section -->
@stop