<table width="100%">
    <tr>
        <td style="text-align: right; padding: 5px;">Event Planner:</td>
        <td style="padding: 5px;">{{$plannerUser->name}}</td>
    </tr>
    <tr>
        <td style="text-align: right; padding: 5px;">Artist:</td>
        <td style="padding: 5px;">{{$artistUser->name}}</td>
    </tr>
    <tr>
        <td style="text-align: right; padding: 5px;">Event:</td>
        <td style="padding: 5px;">{{$event->name}}</td>
    </tr>
    <tr>
        <td style="text-align: right; padding: 5px;">Venue:</td>
        <td style="padding: 5px;">{{$event->venue}}</td>
    </tr>
    {{--<tr>--}}
        {{--<td style="text-align: right; padding: 5px;">City, State:</td>--}}
        {{--<td style="padding: 5px;">{{$event->city}}, {{$event->state}}</td>--}}
    {{--</tr>--}}
    <tr>
        <td style="text-align: right; padding: 5px;">Event Date:</td>
        <td style="padding: 5px;">{{$startDate}}</td>
    </tr>
    <tr>
        <td style="text-align: right; padding: 5px;">Total Payment:</td>
        @if($type == 'planer')
            <td style="padding: 5px;">$ {{$bid->price}}</td>
        @elseif($type == 'artist')
            <td style="padding: 5px;">$ {{$bid->artist_fee}}</td>
        @endif
    </tr>
    <tr>
        <td style="text-align: right; padding: 5px;">Set Duration:</td>
        <td style="padding: 5px;">{{$event->duration}}</td>
    </tr>
    <tr>
        <td style="text-align: right; padding: 5px;">Load In Time:</td>
        <td style="padding: 5px;">
            {{$request_json['load_in_time_hours']}}:{{$request_json['load_in_time_minutes']}} {{$request_json['load_in_time_ampm']}} ({{$time_zone_title}})
        </td>
    </tr>
    <tr>
        <td style="text-align: right; padding: 5px;">Load Out Time:</td>
        <td style="padding: 5px;">{{$request_json['load_out_time_hours']}}:{{$request_json['load_out_time_minutes']}} {{$request_json['load_out_time_ampm']}} ({{$time_zone_title}})</td>
    </tr>
</table>
