@extends('layouts.dash', ['left_menu_selected'=>$left_menu_selected])

@section('upcoming-count')
    <span class="sidebar-nav-count">0</span>
@stop

@section('content')
    <div class="main-body">
        <div class="container">
            @if(Session::has('message'))
                <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
            @endif

            <div class="filter">
                <div class="filter-head">
                    @include('events._form_events_sort_filter')
                </div><!-- /.filter-head -->

                <div class="filter-body">
                    <p id='error_msg'></p>
                    <ul id="eventData" class="filter-items event-boxes">

                        <li class='event-box active' style='width:24%'>
                            <a href='/events/create' class='event-box-add'>Add an event <span><i
                                            class='ico-plus'></i></span>
                            </a>
                        </li>

                        @foreach ($events as $event)
                            <?php
                            $tz = new \DateTimeZone(App\ModelType\TimeZoneType::getTimeZone($event->time_zone));
                            $eventTime = new \DateTime($event->starttime);
                            $eventTime->setTimeZone($tz);
                            $genres = $event->genres;
                            ?>
                            <li class='filter-item event-box event-box-fill' id="event-{{$event->id}}">
                                <div class="event-box-content">
                                    <h4 class='event-box-title'>{{$event->name }}</h4><!-- event-box-title -->
                                    <p class='event-box-meta'>{{ $eventTime->format("m/d/y") }}
                                        – {{$event->location}}</p><!-- event-box-meta -->
                                    <ul class='list-event-details'>
                                        <li>
                                            <p>User</p>
                                            <h6>{{$event->user()->first()->username}}
                                                @if(in_array($event->status, [App\ModelType\EventType::PENDING, App\ModelType\EventType::CANCELLED]))
                                                    <span class="event-status-{{$event->status}}"> - {{ App\ModelType\EventType::getStatusName($event->status) }}</span>
                                                @endif
                                            </h6>

                                            <p>Open to</p>
                                            <h6>
                                                @foreach ($genres as $genre)
                                                    {{$genre->name}}
                                                @endforeach
                                                Bands
                                            </h6>
                                            <h6 class='event-details-price'>${{$event->min_budget}} –
                                                ${{$event->max_budget}}</h6><!-- event-details-price -->
                                        </li>
                                    </ul><!-- list-event-details -->
                                    <div class='actions'>
                                        <a href="/events/{{$event->id}}/display"
                                           class='btn btn-fill btn-invert'>View</a>
                                    </div><!-- event-box-actions -->
                                </div>
                                @if(array_key_exists($event->id, $eventRequestsCounter))
                                    <div class="bids" data-event-id="{{$event->id}}">
                                        <a href="#event-{{$event->id}}">bids {{$eventRequestsCounter[$event->id]}}</a>
                                    </div>
                                @else
                                    <div class="bids">bids 0</div>
                                @endif
                            </li><!-- filter-item -->
                        @endforeach
                    </ul><!-- /.filter-items -->
                </div><!-- /.filter-body -->

                <div id="event-bids" class="event-bids"></div>

                <p>
                    <?php echo $events->appends($sorts)->render(); ?>
                </p>

            </div><!-- /.filter -->
        </div><!-- /.container -->
    </div><!-- /.main-body -->

    <!-- Modal -->
    <div class="modal fade modal-bid-price-form" id="pendingBidFormContainer">
        <div class="modal-dialog" role="document">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
            <div class="modal-content"></div>
        </div>
    </div>

    <style>
        /** { padding: 0; margin: 0; }*/
        /*html { min-height: 1000%; }*/

        .bids-grid-window {
            overflow: auto;
            /*position: fixed;*/
            /*top: 100px;*/
            /*left: 200px;*/
            /*width: 200px;*/
            max-height: 300px;
            /*background: #ccc;*/
        }
    </style>

    <script src="https://rawgit.com/jquery/jquery-mousewheel/master/jquery.mousewheel.js"></script>

    <script type="text/template" id="event-bids-grid-1">
        <div class="event-bids-grid">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
            <form class="notifyEventPlanerForm">
                {{csrf_field()}}
                <input type="hidden" name="event_id" value="<%= bids[0].event_id %>">
            <div class="event-bids-grid-head">
                <input type="submit" class="btn btn-fill btn-invert" value="Notify Event Planner">
            </div>
        </form>
        <div class="bids-grid-window">
        <table>
            <tr>
                <th>Budget Min-Max</th>
                <th colspan="2">Set Price</th>
                <th colspan="2">Artist Fee</th>
                <th>Artist Name</th>
                <th>Artist Email</th>
                <th>Artist Phone #</th>
                <th>Event Planer</th>
                <th>Event Planer Email</th>
                <th>Event Planer Phone #</th>
            </tr>
            <% _.each(bids, function(bid) { %>
                <tr id="bid-<%= bid.id %>">
                    <td>$<%= bid.min_budget %> - $<%= bid.max_budget %></td>
                    <td>
                        <% if (bid.price > 0) { %>
                            <b>$<%= bid.price %></b>
                        <% } else { %>
                            Set Price ?
                        <% } %>
                    </td>
                    <td>
                        <% if(bid.bid_artist_fee !== null) { %>
                        <a id="bid-<%= bid.id %>" data-bid-id="<%= bid.id %>" href="#" class="price-form-btn">
                            <span class="glyphicon glyphicon-pencil"></span>
                        </a>
                        <% } %>
                    </td>
                    <td>
                        <% if(bid.bid_artist_fee === null) { %>
                            Set fee?
                        <% } else { %>
                            $<%= bid.bid_artist_fee %></td>
                        <% }%>
                    <td>
                        <a data-bid-id="<%= bid.id %>" href="#" class="artist-fee-form-btn">
                            <span class="glyphicon glyphicon-pencil"></span>
                        </a>
                    </td>
                    <td><%= bid.artist_name %></td>
                    <td><%= bid.artist_email %></td>
                    <td><%= bid.artist_phone %></td>
                    <td><%= planer.name %></td>
                    <td><%= planer.email %></td>
                    <td><%= planer.phone %></td>
                </tr>
            <% }); %>
        </table>
            </div>
    </div>
</script>

<script type="text/template" id="event-bid-price-form-1">
    <div class="modal-header">
        <h3>Set Price for Event Planer</h3>
    </div>
    <div class="modal-body">
        <div>
            Set price for <b><%= bid.artist_name %></b> to play at <b><%= bid.event_name%></b>.<br/>
            Once saved an email notification will be sent to the EP
        </div>

        <div class="budget">
            <b>BUDGET:</b> $<%= bid.min_budget %> - $<%= bid.max_budget %> | <b>ARTIST FEE:</b> <%= feeToString(bid.artist_fee) %>
        </div>
        <form>
            {{ csrf_field() }}
            <input type="hidden" name="id" value="<%= bid.id %>"/>
            <div>
                <input type="text" value="<%= (bid.price > 0) ? bid.price : "" %>" placeholder="set price" class="form-control" name="price"/>
                <div class="error-message text-danger"></div>
            </div>
            <div class="action-buttons">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-fill btn-invert">Save</button>
            </div>
        </form>
    </div>
</script>

<script type="text/template" id="event-bid-artistFee-form-1">
    <div class="modal-header">
        <h3>Set Artist Fee</h3>
    </div>
    <div class="modal-body">
        <div class="text-center">
            By setting the Artist Fee you are indicating how much the artist charges to play at this event.
        </div>

        <div class="budget">
            <b>BUDGET:</b> $<%= bid.min_budget %> - $<%= bid.max_budget %> | <b>ARTIST FEE:</b> <%= feeToString(bid.artist_fee) %>
        </div>
        <form>
            {{ csrf_field() }}
            <input type="hidden" name="id" value="<%= bid.id %>"/>
            <div>
                <input type="text" value="<%= (bid.bid_artist_fee !== null) ? bid.bid_artist_fee : "" %>" placeholder="set artist fee" class="form-control" name="artist_fee"/>
                <div class="error-message text-danger"></div>
            </div>
            <div class="action-buttons">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-fill btn-invert">Save</button>
            </div>
        </form>
    </div>
</script>

<script type="text/template" id="broken-bids-alert-1">
    <div class="modal-body text-center">
        We noticed few bids that aren't filled out. These bids will NOT be sent to the Event Planner.<br/>
        Are you sure you want to skip these bids?
        <div class="error-message text-danger"></div>
    </div>
    <div class="modal-footer">
        <form>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>

            {{ csrf_field() }}
            <input type="hidden" name="event_id" value="<%= event.id %>"/>
            <input type="hidden" name="accepted" value="1"/>

            <button type="submit" class="btn btn-fill btn-invert">Skip</button>
        </form>
    </div>
</script>

<script type="text/template" id="success-bids-alert-1">
    <div class="modal-body text-center">
        Planner successfully notified
    </div>
</script>

<script>
    $(function(){
        $("#eventData").pendingBids({
            bids_container: $("#event-bids"),
            bibsGridTemplate: _.template($('#event-bids-grid-1').text()),
            bibPriceFormTemplate: _.template($('#event-bid-price-form-1').text()),
            bibArtistFeeFormTemplate: _.template($('#event-bid-artistFee-form-1').text()),
            form_modal: $("#pendingBidFormContainer").modal({show: false})
        });
    });
</script>

@stop

@section('footerjs')
    <script src="{{asset("/js/cpanelPendingBids.js")}}"></script>
@stop