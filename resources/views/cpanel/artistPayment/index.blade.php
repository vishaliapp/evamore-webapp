@extends('layouts.dash')

@section('content')
    <div class="main-body">
        <div class="container">
            <div class="filter">
                @if(Session::has('message'))
                    <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
                @endif

                <div data-bids-type="upcoming" class="bids-upcoming bids-container">
                    <h2>Upcoming</h2>
                    <div class="bids">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Event Title</th>
                                    <th>Total Price</th>
                                    <th>Alternative payment method</th>
                                    <th colspan="2">Amount Paid</th>
                                    <th>Contact Info</th>
                                    <th>Artist Fee</th>
                                    <th>Note</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                    <div class="page-selection"></div>
                </div>

                <div data-bids-type="ended" class="bids-ended bids-container">
                    <h2>Ended</h2>
                    <div class="bids">
                        <div class="bids">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>Event Title</th>
                                        <th>Total Price</th>
                                        <th colspan=2">Amount Paid</th>
                                        <th>Contact Info</th>
                                        <th>Artist Fee</th>
                                        <th colspan="2">Artist Paid?</th>
                                        <th>Note</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                    <div class="page-selection"></div>
                </div>
            </div><!-- /.filter -->
        </div><!-- /.container -->
    </div><!-- /.main-body -->

<!-- Modal -->
<div class="modal fade modal-bid-artist-payment-form" id="bidArtistPaymentForm" >
    <div class="modal-dialog" role="document">
        <button type="button" class="close outer" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <div class="modal-content"></div>
    </div>
</div>

<script type="text/template" id="bid-note-form-1">
        <form>
            {{csrf_field()}}
            <div class="modal-header">
                <h3>Note</h3>
            </div>
            <div class="modal-body">
                <input type="hidden" name="bid_id" value="<%= bid.id %>"/>

                <div class="form-group">
                    <textarea name="note" style="width: 100%; padding:5px" rows="5"><%= bid.note %></textarea>

                    <p class="text-danger error-message"></p>
                </div>
            </div>

            <div class="modal-footer">
                <input type="button" class="btn btn-danger" style="pointer-events: all" data-dismiss="modal" value="CANCEL">
                <input type="submit" class="btn btn-primary btn-invert" value="SAVE">
            </div>
        </form>
</script>

<script type="text/template" id="simple-message-1">
        <div class="modal-header">
            <h3>Message</h3>
        </div>
        <div class="modal-body text-center">
            <%= message %>
        </div>

        <div class="modal-footer">
            <div class="text-center">
                <input type="button" class="btn btn-danger" style="pointer-events: all" data-dismiss="modal" value="CLOSE">
            </div>
        </div>
</script>

<script type="text/template" id="bid-row-upcoming">
    <tr>
        <td><a href="/events/<%= bid.event.id %>/display" target="_blank"><%= bid.event.name %></a></td>
        <td><b>$<%= bid.price %></b></td>
        <td>
            <% if(bid.payment_method == 2) { //PAYMENT_METHOD_ALTERNATIVE  %>
                <span data-bid-id="<%= bid.id %>" class="glyphicon glyphicon-check paid-method paid-method-alternative" data-payment-method="2"></span>
            <% } else if(bid.payment_method == 1) { //PAYMENT_METHOD_PAYSCAPE  %>
                <span>Payscape</span>
            <% } else { %>
                <span data-bid-id="<%= bid.id %>" class="glyphicon glyphicon-unchecked paid-method paid-method-none" data-payment-method="0"></span>
            <% }%>
        </td>
        <td>
            <% if(bid.payment_method == 2) { //PAYMENT_METHOD_ALTERNATIVE  %>
                <form id="bid-paid-amount-form-<%= bid.id %>">
                    {{csrf_field()}}
                    <input type="hidden" name="bid_id" value="<%= bid.id %>"/>
                    <select name="amount" placeholder="$" id="paid-amount-<%= bid.id %>"  class="paid-amount-selector" data-bid-id="<%= bid.id %>">
                        <option value="0">Not Paid</option>
                        <option value="1" <% if(bid.order_info != null && bid.order_info.status==1) { %>selected<% } %> >50%</option> <!-- const HALF_PAID = 1; -->
                        <option value="2" <% if(bid.order_info != null && bid.order_info.status==2) { %>selected<% } %> >100%</option> <!-- const FULL_PAID = 2; -->
                    </select>
                    <div class="error-message text-danger"></div>
                </form>
            <% } else if(bid.payment_method == 1 && bid.order_info != null) { //PAYMENT_METHOD_PAYSCAPE  %>
                <% if(bid.order_info.status == 1) { %>
                    50%
                <% } else if(bid.order_info.status == 2) { %>
                    100%
                <% } %>
            <% } %>
        </td>
        <td>
            <% if(bid.order_info != null) { %>
                $<%= bid.order_info.paid_amount %>
            <% } else {%>
                $0
            <% } %>
        </td>
        <td>
            <div class="dropdown">
                <button class="dropbtn"><%= bid.planerUser.email %></button>
                <div class="dropdown-content">
                    <ul>
                        <li><b>EVENT PLANER</b></li>
                        <li><%= bid.planerUser.name %></li>
                        <li><%= bid.planerUser.phone %></li>
                        <li><%= bid.planerUser.email %></li>
                        <li class="divider"></li>
                        <li><b>ARTIST</b></li>
                        <li><%= bid.artistUser.name %></li>
                        <li><%= bid.artistUser.phone %></li>
                        <li><%= bid.artistUser.email %></li>
                    </ul>
                </div>
            </div>

        </td>
        <td>$<%= bid.artist_fee %></td>
        <td>
            <% if(bid.note == "") { %>
                <span data-bid-id="<%= bid.id %>" class="glyphicon glyphicon-pencil bid-note"></span>
            <% } else { %>
                <span data-bid-id="<%= bid.id %>" class="glyphicon glyphicon-comment bid-note"></span>
            <% } %>
        </td>
    </tr>
</script>

<script type="text/template" id="bid-row-ended">
    <tr>
        <td><a href="/events/<%= bid.event.id %>/display" target="_blank"><%= bid.event.name %></a></td>
        <td><b>$<%= bid.price %></b></td>
        {{--<td><span data-bid-id="<%= bid.id %>" class="glyphicon glyphicon-pencil paid-amount"></span></td>--}}
        {{--<td>$<%= bid.paid_amount %></td>--}}
        <td>
            <% if(bid.payment_method == 2) { //PAYMENT_METHOD_ALTERNATIVE  %>
            <form id="bid-paid-amount-form-<%= bid.id %>">
                {{csrf_field()}}
                <input type="hidden" name="bid_id" value="<%= bid.id %>"/>
                <select name="amount" placeholder="$" id="paid-amount-<%= bid.id %>"  class="paid-amount-selector" data-bid-id="<%= bid.id %>">
                    <option value="0">Not Paid</option>
                    <option value="1" <% if(bid.order_info != null && bid.order_info.status==1) { %>selected<% } %> >50%</option> <!-- const HALF_PAID = 1; -->
                    <option value="2" <% if(bid.order_info != null && bid.order_info.status==2) { %>selected<% } %> >100%</option> <!-- const FULL_PAID = 2; -->
                </select>
                <div class="error-message text-danger"></div>
            </form>
            <% } else if(bid.payment_method == 1 && bid.order_info != null) { //PAYMENT_METHOD_PAYSCAPE  %>
                <% if(bid.order_info.status == 1) { %>
                    50%
                <% } else if(bid.order_info.status == 2) { %>
                    100%
                <% } %>
            <% } %>
        </td>
        <td>
            <% if(bid.order_info != null) { %>
                $<%= bid.order_info.paid_amount %>
            <% } else {%>
                $0
            <% } %>
        </td>
        <td>
            <div class="dropdown">
                <button class="dropbtn"><%= bid.planerUser.email %></button>
                <div class="dropdown-content">
                    <ul>
                        <li><b>EVENT PLANER</b></li>
                        <li><%= bid.planerUser.name %></li>
                        <li><%= bid.planerUser.phone %></li>
                        <li><%= bid.planerUser.email %></li>
                        <li class="divider"></li>
                        <li><b>ARTIST</b></li>
                        <li><%= bid.artistUser.name %></li>
                        <li><%= bid.artistUser.phone %></li>
                        <li><%= bid.artistUser.email %></li>
                    </ul>
                </div>
            </div>
        </td>
        <td>$<%= bid.artist_fee %></td>
        <td>
            <% if(bid.status == 6) {  %>
                <span class="glyphicon glyphicon-check"></span>
            <% } else { %>
                <span data-bid-id="<%= bid.id %>" class="glyphicon glyphicon-unchecked paid-date"></span>
            <% }%>
        </td>
        <td><%= bid.format_artist_paid_at %></td>
        <td>
            <% if(bid.note == "") { %>
                <span data-bid-id="<%= bid.id %>" class="glyphicon glyphicon-pencil bid-note"></span>
            <% } else { %>
                <span data-bid-id="<%= bid.id %>" class="glyphicon glyphicon-comment bid-note"></span>
            <% } %>
        </td>
    </tr>
</script>

<script type="text/template" id="bid-artist-paid-date-form-1">
    <form>
        {{csrf_field()}}
        <div class="modal-header">
            <h3>Payment Date</h3>
        </div>
        <div class="modal-body">
            <input type="hidden" name="bid_id" value="<%= bid_id %>"/>
            <p>Please select the date that the artist was paid</p>
            <div class="form-group" id="date-datepicker-container">
                <div class='input-group date' id="paid-datetimepicker">
                    <input type="text" name="date" placeholder="mm/dd/yyyy"  class="form-control simplebox"/>
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
                <p class="text-danger error-message"></p>
            </div>
        </div>

        <div class="modal-footer">
            <input type="button" class="btn btn-danger" style="pointer-events: all" data-dismiss="modal" value="CANCEL">
            <input type="submit" class="btn btn-primary btn-invert" value="SAVE">
        </div>
    </form>
</script>

<script type="text/javascript">
    (function($){
        $.ajaxSetup({ headers: { 'csrftoken' : '{{ csrf_token() }}' } });

        $.fn.artistsBidsPayment = function(options){
            var self = this;

            self.settings = {
                bidType: ""
            };
            $.extend(self.settings, options);

            self.currentPage = {};

            self.getCurrentPage = function(bidPeriod){
                var keys = Object.keys(self.currentPage);
                if (keys.indexOf(bidPeriod) >= 0) {
                    return self.currentPage[bidPeriod];
                }
                return 1;
            };

            var bidRowTpl = _.template($("#bid-row-"+self.settings.bidType).text());

            var modal = $("#bidArtistPaymentForm").modal({show:false});

            var render = function(bidPeriod, bids) {
                var $bidsContainer = $(".bids-" + bidPeriod);
                var rowsContainer = $("table tbody", $bidsContainer);
                rowsContainer.html("");
                for(var i=0; i<bids.length; i++) {
                    var htmlRow = bidRowTpl({bid: bids[i]});
                    rowsContainer.append(htmlRow);
                }
                addListeners(rowsContainer);
                return $bidsContainer;
            };

            var loadBids = function(bidPeriod, activatePagination) {
                var url = "/cpanel/api/payments/"+bidPeriod;

                var page = self.getCurrentPage(bidPeriod);
                url += '?page='+page

                $.ajax({
                    url: url,
                    type: "GET",
                    dataType: "json",
                    data: {},
                    success: function(response)
                    {
                        if (response.success) {
                            var $container = render(bidPeriod, response.bids)
                            if (activatePagination) {
                                initPagination($container, response.paginator)
                            }
                        }
                    }
                })
            };

            var loadBid = function(id, callback) {
                var url = "/cpanel/api/payments/bid/"+id;

                $.ajax({
                    url: url,
                    type: "GET",
                    dataType: "json",
                    data: {},
                    success: function(response)
                    {
                        if (response.success) {
                            callback(response.bid);
                        }
                    }
                })
            }

            var addListeners = function(container) {
                $(".paid-amount-selector", container).on("change", function(e){
                    e.preventDefault();
                    var $this = $(this);
                    var bid_id = $this.attr("data-bid-id");
                    var $form = $("#bid-paid-amount-form-"+bid_id, container);
                    var options = {
                        //target:        '#output1',   // target element(s) to be updated with server response
                        beforeSubmit:  function(formData, jqForm, options) {
                            $('.error-message', $form).html("");
                        },
                        url: '/cpanel/api/payments/amount',
                        type: 'post',
                        dataType: 'json',
                        success: function(response, statusText, xhr, $form){
                            if (response.success) {
                                var $bidsContainer = $this.parents(".bids-container").eq(0);
                                var bidsPeriod = $bidsContainer.attr('data-bids-type');
                                loadBids(bidsPeriod);
                            } else {
                                $('.error-message', $form).text(response.message);
                            }
                        }
                    };
                    $form.ajaxSubmit(options);
                    return false;
                });


                $(".paid-date", container).on("click", function(e){
                    e.preventDefault();

                    var $this = $(this);
                    var bid_id = $this.attr("data-bid-id");

                    var tpl = _.template($("#bid-artist-paid-date-form-1").text());
                    $(".modal-content", modal).html(tpl({
                        bid_id: bid_id
                    }));

                    var options = {
                        //target:        '#output1',   // target element(s) to be updated with server response
                        beforeSubmit:  function(formData, jqForm, options){
                            $('.error-message', modal).html("");
                        },
                        url: '/cpanel/api/payments/date',
                        type: 'post',
                        dataType: 'json',
                        success: function(response, statusText, xhr, $form){
                            if (response.success) {
                                var $bidsContainer = $this.parents(".bids-container").eq(0);
                                var bidsPeriod = $bidsContainer.attr('data-bids-type');
                                loadBids(bidsPeriod);
                                modal.modal('hide');
                            } else {
                                $('.error-message', modal).text(response.message);
                            }
                        }
                    };

                    var $o = $('#paid-datetimepicker')

                    $o.datepicker({
                        format: 'mm/dd/yyyy',
                        container: "#date-datepicker-container"
                    });
                    $("form", modal).ajaxForm(options);
                    modal.modal('show');

                    return false
                });

                $(".paid-method", container).on("click", function(e){
                    e.preventDefault();

                    var $this = $(this);
                    var bid_id = $this.attr("data-bid-id");
                    var payment_method = $this.attr("data-payment-method");
                    var new_method = (payment_method==0) ? 2 : 0;
                    $.ajax({
                        url: '/cpanel/api/payments/method',
                        type: 'post',
                        dataType: 'json',
                        data: {
                            "_token": "{{ csrf_token() }}",
                            method: new_method,
                            bid_id: bid_id
                        },
                        success: function(response, statusText, xhr, $form){
                            if (response.success) {
                                var $bidsContainer = $this.parents(".bids-container").eq(0);
                                var bidsPeriod = $bidsContainer.attr('data-bids-type');
                                loadBids(bidsPeriod);
                            } else {

                                var tpl = _.template($("#simple-message-1").text());
                                var modal = $('#baseModal').clone();
                                $(".modal-content", modal).html(tpl({
                                    message: response.message
                                }));

                                modal.modal('show');

//                                alert(response.message);
//                                $('.error-message', modal).text(response.message);
                            }
                        }
                    });

                    return false
                });

                $(".bid-note", container).on("click", function(e){
                    e.preventDefault();

                    var $this = $(this);
                    var bid_id = $this.attr("data-bid-id");
                    loadBid(bid_id, function(bid){
                        var tpl = _.template($("#bid-note-form-1").text());
                        $(".modal-content", modal).html(tpl({
                            bid: bid
                        }));

                        var options = {
                            //target:        '#output1',   // target element(s) to be updated with server response
                            beforeSubmit:  function(formData, jqForm, options){
                                $('.error-message', modal).html("");
                            },
                            url: '/cpanel/api/payments/note',
                            type: 'post',
                            dataType: 'json',
                            success: function(response, statusText, xhr, $form){
                                if (response.success) {
                                    var $bidsContainer = $this.parents(".bids-container").eq(0);
                                    var bidsPeriod = $bidsContainer.attr('data-bids-type');
                                    loadBids(bidsPeriod);
                                    modal.modal('hide');
                                } else {
                                    $('.error-message', modal).text(response.message);
                                }
                            }
                        };

                        $("form", modal).ajaxForm(options);
                        modal.modal('show');

                       return false;
                    });

                    return false
                });
            };

            var initPagination = function(container, paginationData) {

                $('.page-selection', container).bootpag({
                    total: paginationData.count_pages
                }).on("page", function(event, num){
                    var bidPeriod = container.attr("data-bids-type");
                    self.currentPage[bidPeriod] = num;
                    loadBids(bidPeriod);
                });
            };

            var init = function() {
                $.each(self, function(e, el){
                    var $el = $(el);
                    loadBids(self.settings.bidType, true)
                })
            };

            init();
        };

        $(function() {
            $(".bids-upcoming").artistsBidsPayment({
                bidType: "upcoming"
            })
            $(".bids-ended").artistsBidsPayment({
                bidType: "ended"
            })
        });
    })(jQuery)
</script>

@stop

@section('footerjs')
    <script src="{{asset("/addons/bootstrap/js/jquery.bootpag.min.js", null, false)}}"></script>

@stop