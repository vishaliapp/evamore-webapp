@extends('layouts.dash')

@section('js')

@stop

@section('content')
    <div class="main-body">
        <div class="account">
            <div class="account-head">
                <h1>Create account with '{{$role}}' permissions</h1>
            </div>
            <!-- /.account-head -->
            <div class="account-body">
                @if(Session::has('message'))
                    <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
                @endif

                <form enctype="multipart/form-data" action="{{ route('cpanel_account_create_save', ['role'=>$role]) }}" method="POST" id="user_updater">
                    {{ csrf_field() }}

                    <center>
                        <div class="account-image-outer">
                            <div class="account-image" id="avatarUploader" data-default-image=" @if (old('image')) {{old('image')}} @else /images/drag_drop_white.png @endif">

                                <div class="file-upload">
                                    <div class="file-upload-btn">
                                        <span class="file-upload-text">
                                            <img class="picture-element-image" id="user-avatar" src="{{ Image::url('images/drag_drop_white.png',array('crop')) }}" height="100%"
                                                 width="100%" alt="">
                                        </span>
                                        <input type="hidden" id="image_base_64" name="upload_image">
                                        <input name="image" accept="image/*" type="file" id="upload" class="file-upload-input">
                                    </div><!-- /.file-upload-btn -->
                                </div><!-- /.file-upload -->
                            </div><!-- /.account-image -->

                            <label for="upload_image" class="file-upload-ico">
                                <i class="ico-cam"></i>
                            </label>
                        </div>
                        <p><a id="avatarUploaderImageClean" href="#">Cancel</a></p>

                        <div class="form-group {{ $errors->has('avatar') ? ' has-error' : '' }}">
                            @if ($errors->has('avatar'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('avatar') }}</strong>
                                </span>
                            @endif
                        </div>
                    </center>

                    <div class="form-group {{ $errors->has('avatar') ? ' has-error' : '' }}">
                        @if ($errors->has('avatar'))
                            <span class="help-block">
                                <strong>{{ $errors->first('avatar') }}</strong>
                            </span>
                        @endif
                    </div><!-- /.account-image-outer -->

                    <!--</form><!-- Upload form -->
                    <!--<form action="#" method="post" id="user_update">-->
                    <div class="form form-account" class="dropzone">
                        <div class="form-group {{ $errors->has('first_name') ? ' has-error' : '' }}">
                            <input type="text" class="form-control" id="first_name" name="first_name"
                                   placeholder="First name" value="{{ old('first_name') }}"/>

                            <i class="ico-pencil"></i>
                            @if ($errors->has('first_name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('first_name') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group {{ $errors->has('last_name') ? ' has-error' : '' }}">
                            <input type="text" class="form-control" id="last_name" name="last_name"
                                   placeholder="Last name" value="{{ old('last_name') }}"/>

                            <i class="ico-pencil"></i>
                            @if ($errors->has('last_name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('last_name') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group {{ $errors->has('username') ? ' has-error' : '' }}">
                            <input type="text" class="form-control" id="username" name="username"
                                   placeholder="Username" value="{{ old('username') }}"/>

                            <i class="ico-pencil"></i>
                            @if ($errors->has('username'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('username') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                            <input type="password" class="form-control" id="password" name="password"
                                   placeholder="Password" value=""/>

                            <i class="ico-pencil"></i>
                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                        <!-- /.form-group -->

                        <div class="form-group {{ $errors->has('organization') ? ' has-error' : '' }}">
                            <input type="text" class="form-control" id="organization" name="organization"
                                   placeholder="Organization" value="{{ old('organization') }}"/>

                            <i class="ico-pencil"></i>
                            @if ($errors->has('organization'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('organization') }}</strong>
                                </span>
                            @endif
                        </div>
                        <!-- /.form-group -->

                        <div class="form-group {{ $errors->has('org_position') ? ' has-error' : '' }}">
                            <input type="text" class="form-control" id="org_position" name="org_position"
                                   placeholder="Position" value="{{ old('org_position') }}"/>

                            <i class="ico-pencil"></i>
                            @if ($errors->has('org_position'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('org_position') }}</strong>
                                </span>
                            @endif
                        </div>
                        <!-- /.form-group -->

                        <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                            <input type="email" class="form-control" id="email" name="email"
                                   placeholder="Email" value="{{ old('email') }}"/>

                            <i class="ico-pencil"></i>
                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                        <!-- /.form-group -->

                        <div class="form-group {{ $errors->has('phone') ? ' has-error' : '' }}">
                            <input type="number" class="form-control" id="phone" name="phone"
                                   placeholder="Phone" value="{{ old('phone') }}"/>

                            <i class="ico-pencil"></i>
                            @if ($errors->has('phone'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('phone') }}</strong>
                                </span>
                            @endif
                        </div>
                        <!-- /.form-group -->

                        <div class="form-actions">
                            <input type="submit" value="Create {{$role}}" name="submit" class="btn btn-primary btn-invert"/>
                        </div>
                        <!-- /.form-actions -->
                    </div>
                    <!-- /.form form-account -->
                </form>
            </div>
            <!-- /.account-body -->
        </div>
        <!-- /.account -->
    </div><!-- /.main-body -->

    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Modal title</h4>
                </div>
                <div class="modal-body">
                    <button id="js-main-image" type="button" class="btn btn-primary btn-invert upload-result" >Set My Picture</button>

                    <div class="demo-wrap upload-demo">
                        <div id="avatar_crop_image" class="container">
                            <div class="grid">
                                <div class="col-1-2">
                                    <div class="upload-msg">
                                        Upload a file to start cropping
                                    </div>
                                    <div class="upload-demo-wrap">
                                        <div id="upload-demo"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="{{asset('demo/prism.js')}}"></script>
<script src="{{asset('croppie.js')}}"></script>
<script src="{{asset('/addons/croper.main.js')}}"></script>
<script>
    $( document ).ready(function() {
        Demo.init();
    });
</script>
@stop

@section('footerjs')
    <script src="{{asset("/addons/jquery-ui-1.12.1/jquery-ui.min.js", null, false)}}"></script>
    <script src="{{asset("/addons/picture-cut-master/src/jquery.picture.cut.js", null, false)}}"></script>
@stop