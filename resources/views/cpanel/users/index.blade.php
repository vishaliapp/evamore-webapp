@extends('layouts.dash')
<meta name="csrf-token" content="{{ csrf_token() }}"/>
<link rel="stylesheet" href="{{asset('/css/dark_style.css')}}"/>

@section('js')
    <!--<script src="{{asset(file_exists('/js/events.min.js') ? print '/js/events.js' : print '/js/events.js')}}"></script>-->
@stop

@section('upcoming')
    class='active'
@stop


@section('upcoming-count')
    <span class="sidebar-nav-count">0</span>
@stop

@section('content')
    <div class="main-body">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3">
                    <div id="imaginary_container">
                        <div class="input-group stylish-input-group">
                            <input id="sBar" type="text" class="form-control"
                                   style="border-bottom: 3px solid #5cb85c !important;" placeholder="Search">
                        </div>
                        <div id="mainSearch">
                            <ul id="searchResponse" class="artist_search"></ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="filter">

                @if(Session::has('message'))
                    <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
                @endif

                <div class="filter-head">
                    <form action="{{route('cpanel_users_index')}}" method="GET" id="usersSortFilterForm">
                        <p>
                            <label>Select a role:</label>

                            <select name="filter_role">
                                <option value="0">None</option>
                                @foreach($roles as $role)
                                    <option value="{{$role->id}}">{{$role->name}}</option>
                                @endforeach
                            </select>
                        </p>
                        <p><input type="submit" value="Filter"/></p>
                    </form>
                </div><!-- /.filter-head -->

                <p class="cpanel-create-account-menu">
                    <a href="{{route("artists_registration")}}" class="btn btn-sm btn-success">Create Artist</a>
                    <a href="{{route("cpanel_account_create", ['role'=>'FAN'])}}" class="btn btn-sm btn-success">Create
                        Fan</a>
                    <a href="{{route("cpanel_account_create", ['role'=>'ADMIN'])}}" class="btn btn-sm btn-success">Create
                        Admin</a>
                    <a href="{{route("cpanel_account_create", ['role'=>'ARTIST_MANAGER'])}}"
                       class="btn btn-sm btn-success">Create Artist Manager</a>
                </p>
                <div class="filter-body">
                    <p id='error_msg'></p>
                    <table class="table users-list">
                        <tr>
                            <th>ID</th>
                            <th>Active</th>
                            <th>Username</th>
                            <th>Email</th>
                            <th>Roles</th>
                            <th></th>
                        </tr>

                        @foreach($usersObjects as $user)
                            <tr>
                                <td>{{$user->id}}</td>
                                <td>{{$user->active}}</td>
                                <td>{{$user->username}}</td>
                                <td>{{$user->email}}</td>
                                <td>
                                    @foreach($user->roles()->get() as $r)
                                        <span>{{$r->title}}</span>
                                    @endforeach
                                </td>
                                <td>
                                    @if($user->hasRole('Artist'))
                                        <a href="{{route('editartist', ['id'=>$user->id])}}">Edit</a>
                                    @else
                                        <a href="{{route('edit_account', ['id'=>$user->id])}}">Edit</a>
                                    @endif
                                    <a href="#" class="delete-account" data-id="{{$user->id}}">Delete</a>
                                </td>
                            </tr>
                        @endforeach

                    </table>

                    <p>
                        <?php echo $users->appends($filter)->render(); ?>
                    </p>
                </div><!-- /.filter-body -->

            </div><!-- /.filter -->
        </div><!-- /.container -->
    </div><!-- /.main-body -->
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $(document).click(function (evt) {
            if (evt.target.id !== "searchBar") {
                $('#searchResponse').empty();
                $(this).css('border-bottom', ' 3px solid #5cb85c ')
            }

        });
        $('#sBar').on('input', function () {
            $(this).css('border-bottom', ' 3px solid #398439 ')
            var search = $(this).val();
            if ($(this).val() == '') {
                $('#searchResponse').empty();
                $(this).css('border-bottom', ' 3px solid #5cb85c ')
            }
            $.ajax({
                url: '/users/search-bar/' + search,
                method: 'post',
                success: function (data) {

                    $('#searchResponse').empty()
                    $('#sBar').css('border-bottom', ' 3px solid #5cb85c ');
                    var url = {!! json_encode(url('/')) !!};
                    data.forEach(function (item) {
                        if (item.artist == null) {
                            $('#searchResponse').append('<li class="searchItems">' +
                                '<a target="_blank" href="' + url + '/account/' + item.id +'/edit'+ '">' +
                                '<img src="/uploads/avatars/' + item.avatar + '" height="39" width="39">'
                                + item.name +
                                '</a>' +
                                '</li>');
                        } else {
                            $('#searchResponse').append('<li class="searchItems">' +
                                '<a target="_blank" href="' + url + '/artists/' + item.username + '">' +
                                '<img src="/uploads/avatars/' + item.avatar + '" height="39" width="39">'
                                + item.name +
                                '</a>' +
                                '</li>');
                        }


                    });
                    $('#searchResponse').remove('.searchItems');
                    $('#sBar').css('border-bottom', ' 3px solid #5cb85c ')
                }
            })
        })
    </script>

    <script type="text/template" id="userDeleteConfirmModal">
        <form>
            {{csrf_field()}}
            <div class="modal-header">
                <h3>Delete Account</h3>
            </div>
            <div class="modal-body">
                <div class="form-group text-center">
                    <p>Are you sure?</p>
                    <div class="error-message text-danger"></div>
                </div>
            </div>

            <div class="modal-footer" style="text-align: center">
                <input type="button" class="btn btn-danger" style="pointer-events: all" data-dismiss="modal"
                       value="CANCEL">
                <input type="submit" class="btn btn-primary btn-invert" value="DELETE">
            </div>
        </form>
    </script>

    <script type="text/template" id="messageModal">
        <div class="modal-body">
            <div class="form-group text-center">
    <%= message %>
</div>
</div>

<div class="modal-footer" style="text-align: center">
<input type="button" class="btn btn-danger" style="pointer-events: all" data-dismiss="modal" value="CANCEL">
</div>
</script>

<script type="text/javascript">
(function($){
$.ajaxSetup({ headers: { 'csrftoken' : '{{ csrf_token() }}' } });

$.fn.accountActions = function(options){
    var self = this;
    var modal = $("#baseModal").modal({show:false});

    var loadAccount = function(id, successCallback, errorCallback) {
        $.ajax({
            url: '/api/account/'+id,
            type: "GET",
            dataType: "json",
            data: {},
            success: function(response)
            {
                if (response.success) {
                    successCallback(response.user)
                } else {
                    errorCallback(response.message)
                }
            }
        })
    };

    var deleteAccount = function(id, successCallback, errorCallback) {

    };

    var addListeners = function() {
        $(".delete-account", self).on('click', function(e){
            e.preventDefault();

            var id = $(this).attr('data-id');

            loadAccount(id, function(user){
                if ((user.artist !== null && user.active == 0) || user.artist === null) {
                    showDeleteConfirmDialog(user.id, function(id){
                        location.reload();
                    });
                } else {
                    showErrorWindow("You should deactivate artist before delete.");
                }
            })
        });
        return false;
    };

    var showDeleteConfirmDialog = function(id, confirmCallback) {
        var modalContentTpl =  _.template($("#userDeleteConfirmModal").text());

        $(".modal-content", modal).html(modalContentTpl({
            account_id: id
        }));

        var options = {
            //target:        '#output1',   // target element(s) to be updated with server response
            beforeSubmit:  function(formData, jqForm, options){
                $('.error-message', modal).html("");
            },
            url: '/cpanel/api/account/delete/'+id,
            type: 'post',
            dataType: 'json',
            success: function(response, statusText, xhr, $form){
                if (response.success) {
                    modal.modal('hide');
                    confirmCallback(id)
                } else {
                    $('.error-message', modal).text(response.message);
                }
            }
        };

        $("form", modal).ajaxForm(options);
        modal.modal('show');

    };

    var showErrorWindow = function(message){
        var tpl =  _.template($("#messageModal").text());

        $(".modal-content", modal).html(tpl({
            message: message
        }));

        modal.modal('show');
    };

    var init = function(){
        addListeners();
    };
    init();
};

$(function() {
    $(".users-list").accountActions()
});
})(jQuery)

</script>
@stop