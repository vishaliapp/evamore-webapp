<tr>
    <td colspan="3" align="center"  style="padding: 0 64px 58px 64px">
        <p style="margin: 0 0 0 0; font-size: 19px; line-height: 25px; color: #505050;padding-bottom: 50px;"><i>Notification from EVAmore</i></p>
    </td>
</tr>
<tr>
    <td colspan="3" bgcolor="#efeff0" align="center" style="padding: 58px 64px 58px 64px">
        <p style="margin: 0; font-size: 12px; line-height: 16px; color: #bcbec0;">&copy;2016 EVAmore
            <a href="#" style="color: #30b6ba; text-decoration: none;">Unsubscribe</a>
            or
            <a href="#" style="color: #30b6ba; text-decoration: none;">view in browser</a>
        </p>
        <p style="margin: 0; font-size: 12px; line-height: 16px; color: #bcbec0;">You received this because you're a registered EVAmore user</p>
    </td>
</tr>