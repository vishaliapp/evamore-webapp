<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Hey Support</title>
    <style>
        .block {
            background: gray;
        }
    </style>
</head>
<body style="margin: 0;">
<table border="0" cellpadding="0" cellspacing="0" align="center" style="width: 100%; font-family:Verdana,Arial;">
    <tbody>
    <tr bgcolor="#30b6ba">
        <td colspan="3" style="padding: 58px 64px 58px 64px">
            <a href="#">
                <img alt="" src="{{$domain}}/images/white_logo.png" style="vertical-align: top;" />
            </a>
        </td>
    </tr>
    <tr bgcolor="#30b6ba">
        <td colspan="3" align="center" style="padding: 0 64px 58px 64px;">
            <p style="margin: 0; font-size: 24px; color: #ffffff;">Hey Support,</p>
            <p style="margin: 0; font-size: 36px; color: #ffffff;">
                You have new message from<br/>
                {{$first_name}} {{$last_name}} &lt;{{$email}}&gt;
            </p>
        </td>
    </tr>
    <tr bgcolor="#ffffff">
        <td align="center">&nbsp;</td>
        <td align="center">&nbsp;</td>
    </tr>
    <tr>
        <td colspan="3" align="left" style="padding: 4px 64px 58px 64px">
            <p style="font-size:20px;padding: 20px 0 20px 0;">Text message:</p>
            <p style="font-size:14px;">{{$textMessage}}</p>
        </td>
    </tr>

    @include('emails._email_footer')

    </tbody>
</table>
</body>
</html>
