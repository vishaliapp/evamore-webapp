<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Hey Team</title>
    <style>
        .block {
            background: gray;
        }
    </style>
</head>
<body style="margin: 0;">
<table border="0" cellpadding="0" cellspacing="0" align="center" style="width: 100%; font-family:Verdana,Arial;">
    <tbody>
    <tr bgcolor="#30b6ba">
        <td colspan="3" style="padding: 58px 64px 58px 64px">
            <a href="#">
                <img alt="" src="{{$domain}}/images/white_logo.png" style="vertical-align: top;"/>
            </a>
        </td>
    </tr>
    <tr bgcolor="#30b6ba">
        <td colspan="3" align="center" style="padding: 0 64px 58px 64px;">
            <p style="margin: 0; font-size: 36px; color: #ffffff;">Dear {{ $artistUser->username }} </strong> Event
                where you applied has changes you can check by clicking the
                @if(isset($event))
                    <a style="padding: 0 25px;
                        color: #fff !important;"
                       href="{{ route('event_show', ['id'=> $event['event_id'] ]) }}">
                        here
                    </a>
                @endif
            </p>

        </td>

    </tr>
    <tr bgcolor="#30b6ba">
        <td align="center" style="padding: 0 64px 0 64px; height: 125px">&nbsp;</td>
        <td rowspan="2" align="center" style="width: 250px">
            <img src="{{$domain}}{{ $emailArtistAvatarUrl }}"
                 height="250"
                 width="250"
                 style="vertical-align: top;"
            />
        </td>
        <td align="center" style="padding: 0 64px 0 64px; height: 125px">&nbsp;</td>
    </tr>
    <tr bgcolor="#ffffff">
        <td align="center">&nbsp;</td>
        <td align="center">&nbsp;</td>
    </tr>
    <tr>
        <td colspan="3" align="center" style="padding: 4px 64px 58px 64px">
            <p>Got any question? Contact us at <a
                        href="mailto:{{config('mail.support_email')}}">{{config('mail.support_email')}}</a></p>
        </td>
    </tr>

    @include('emails._email_footer')

    </tbody>
</table>
</body>
</html>