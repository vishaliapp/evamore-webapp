<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Hey Team</title>
    <style>
        .block {
            background: gray;
        }
    </style>
</head>
<body style="margin: 0;">
<table border="0" cellpadding="0" cellspacing="0" align="center" style="width: 100%; font-family:Verdana,Arial;">
    <tbody>
    <tr bgcolor="#30b6ba">
        <td colspan="3" style="padding: 58px 64px 58px 64px">
            <a href="#">
                <img alt="" src="{{$domain}}/images/white_logo.png" style="vertical-align: top;" />
            </a>
        </td>
    </tr>
    <tr bgcolor="#30b6ba">
        <td colspan="3" align="center" style="padding: 0 64px 58px 64px;">
            <p style="margin: 0; font-size: 24px; color: #ffffff;">{{$message}}</p>
            <p style="margin: 0; font-size: 36px; color: #ffffff;">{{ $event->name }}</p>
        </td>
    </tr>

    <tr>
        <td colspan="3" align="center" style="padding: 4px 64px 58px 64px">
            <a href="{{ route('event_show', ['id'=>$event->id]) }}"
               style="font-size: 18px;
                      line-height: 54px;
                      background-color: #30b6ba;
                      color: #ffffff;
                      text-decoration: none;
                      display: inline-block;
                      padding: 0 19px 0 19px;">
                CHECK OUT THE EVENT
            </a>
        </td>
    </tr>
    <tr>
        <td colspan="3" align="center" style="padding: 4px 64px 58px 64px">
            <p style="font-size:14px">Was this our Mistake? Send us an email now at <a href="mailto:{{config('mail.support_email')}}">{{config('mail.support_email')}}</a></p>
        </td>
    </tr>

    @include('emails._email_footer')

    </tbody>
</table>
</body>
</html>