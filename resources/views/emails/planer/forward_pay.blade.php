<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Hey Team</title>
    <style>
        .block {
            background: gray;
        }
    </style>
</head>
<body style="margin: 0;">
<table border="0" cellpadding="0" cellspacing="0" align="center" style="width: 100%; font-family:Verdana,Arial;">
    <tbody>

    <tr>
        <td colspan="3" align="center" style="padding: 4px 64px 58px 64px">
            <p> Dear {{ $payerName }},
                {{ $event->user->name }} has requested payment to {{ $artist->name }} for the {{ $event->name }}
                event.</p>
        </td>
    </tr>


    <tr bgcolor="#30b6ba">
        <td colspan="3" align="center" style="padding: 0 64px 58px 64px;">
            <p>Click this link to be taken to the payment page: <a href="{{ route('forwardPay',[$token]) }}">Link</a>
            </p>
        </td>
    </tr>
    <tr bgcolor="#30b6ba">
        <td colspan="3" align="center" style="padding: 0 64px 58px 64px;">
            Event: {{$event->name}}
        </td>
    </tr>
    <tr bgcolor="#30b6ba">
        <td colspan="3" align="center" style="padding: 0 64px 58px 64px;">
            Artist: {{ $artist->name }}
        </td>
    </tr>
    <tr bgcolor="#30b6ba">
        <td colspan="3" align="center" style="padding: 0 64px 58px 64px;">
            Total: {{ $bid->price }}
        </td>
    </tr>
    <tr bgcolor="#30b6ba">
        <td colspan="3" align="center" style="padding: 0 64px 58px 64px;">
            Booker: {{ $planner->name }}
        </td>
    </tr>

    <tr bgcolor="#ffffff">
        <td align="center">&nbsp;</td>
        <td align="center">&nbsp;</td>
    </tr>
    <tr>
        <td colspan="3" align="center" style="padding: 4px 64px 58px 64px">
            <p>Got some questions? Contact us at <a
                        href="mailto:{{config('mail.support_email')}}">{{config('mail.support_email')}}</a></p>
        </td>
    </tr>


    </tbody>
</table>
</body>
</html>