<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Hey Team</title>
    <style>
        .block {
            background: gray;
        }
    </style>
</head>
<body style="margin: 0;">
<table border="0" cellpadding="0" cellspacing="0" align="center" style="width: 100%; font-family:Verdana,Arial;">
    <tbody>
    <tr bgcolor="#30b6ba">
        <td colspan="3" style="padding: 58px 64px 58px 64px">
            <a href="#">
                <img alt="" src="{{$domain}}/images/white_logo.png" style="vertical-align: top;" />
            </a>
        </td>
    </tr>
    <tr bgcolor="#30b6ba">
        <td colspan="3" align="center" style="padding: 0 64px 58px 64px;">
            <p style="margin: 0; font-size: 24px; color: #ffffff;">Hey {{$user->name}},</p>
            <p style="margin: 0; font-size: 36px; color: #ffffff;">
                Looks like time for a password reset.
            </p>
        </td>
    </tr>
    <tr bgcolor="#30b6ba">
        <td align="center" style="padding: 0 64px 0 64px; height: 125px">&nbsp;</td>
        <td rowspan="2" align="center" style="width: 250px">
            <img src="{{$domain}}{{ $emailUserAvatarUrl }}"
                 height="250"
                 width="250"
                 style="vertical-align: top;"
                    />
        </td>
        <td align="center" style="padding: 0 64px 0 64px; height: 125px">&nbsp;</td>
    </tr>
    <tr bgcolor="#ffffff">
        <td align="center">&nbsp;</td>
        <td align="center">&nbsp;</td>
    </tr>
    <tr>
        <td colspan="3" align="center" style="padding: 4px 64px 58px 64px">
            <p style="font-size:20px;padding: 60px 0 40px 0;color: #30b6ba;">Your next move:</p>

            <a href="{{ $link }}"
               style="font-size: 18px;
                      line-height: 54px;
                      background-color: #30b6ba;
                      color: #ffffff;
                      text-decoration: none;
                      display: inline-block;
                      padding: 0 19px 0 19px;">
                RESET YOUR PASSWORD
            </a>
        </td>
    </tr>

    <tr>
        <td colspan="3" align="center"  style="padding: 0 64px 58px 64px">
            <p style="margin: 0 0 0 0; font-size: 18px; line-height: 25px; color: #505050;">Thanks for relying on EVAmore</p>
            <p style="margin: 0 0 0 0; font-size: 18px; line-height: 25px; color: #505050;padding-bottom: 50px;"><i>Channing, Makenzie & the team</i></p>
        </td>
    </tr>
    <tr>
        <td colspan="3" bgcolor="#efeff0" align="center" style="padding: 58px 64px 58px 64px">
            <p style="margin: 0; font-size: 12px; line-height: 16px; color: #bcbec0;">&copy;2016 EVAmore
                <a href="#" style="color: #30b6ba; text-decoration: none;">Unsubscribe</a>
                or
                <a href="#" style="color: #30b6ba; text-decoration: none;">view in browser</a>
            </p>
            <p style="margin: 0; font-size: 12px; line-height: 16px; color: #bcbec0;">You received this because you're a registered EVAmore user</p>
        </td>
    </tr>

    </tbody>
</table>
</body>
</html>




<html>
<head>
<link href="{{asset("https://fonts.googleapis.com/css?family=Lato", null, false)}}" rel="stylesheet">
</head>
<body style="font-family:Lato,Verdana,Arial;margin:0px;letter-spacing:3px;">

<p>Hey, {{$user->name}} !</p>
<p>Please click the link below to reset your password</p>

<p>
    <a href="{{$link}}">Reset your password</a>
</p>

<div style="width:100%;text-align:center;background-color:#EFEFF0;padding:67px 0;
        color:#BCBCBC;font-size:21px;letter-spacing:1px;">
    <p style="margin: 2px 0">&copy;2016 EVAmore. <a href="#" style="color:#30b6ba;text-decoration:none">Unsubscribe</a> or <a href="#" style="color:#30b6ba;text-decoration:none">view in browser</a></p>
    <p style="margin: 2px 0">You received this because you're a registered EVAmore user</p>
</div>

</body>
</html>
