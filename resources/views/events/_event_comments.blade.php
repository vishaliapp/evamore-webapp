@if($canReadComments || $canWriteComments)
    <meta name="csrf-token" content="{{ csrf_token() }}"/>

    <div class="content">
        <div class="loading" style="display: none">Loading&#8230;</div>
        <div class="row">
            <div class="event-comments">
                <div class="form-head">
                    <h2>Comments</h2>
                </div>
                <div class="comments-container"></div>
                @if($canWriteComments)
                    <div class="comment-form">
                        <form class="form-horisontal" enctype="multipart/form-data" id="comment-form" method="post"
                              action="{{route('api_create_comment')}}">
                            {{--                            {{ csrf_token()  }}--}}
                            <input type="hidden" name="token" value="{{ csrf_token() }}">
                            <input id="event_id" type="hidden" name="event_id" value="{{$event->id}}"/>
                            <div class="form-group">
                                {{--<label class="sr-only" for="exampleInputAmount">Amount (in dollars)</label>--}}
                                <div class="input-group">
                                    <input type="text" class="form-control" id="content"
                                           name="content" placeholder="Add a comment" autocomplete="off">
                                    <div class="input-group-addon">
                                        <span class="files-amount-wrapper">
                                            <span class="glyphicon fileLength glyphicon-file file-form-btn"></span>
                                        </span>
                                    </div>
                                </div>
                                <div class="input-group">
                                    <input type="hidden" value="" id="file" name="files" autocomplete="off"/>
                                </div>
                            </div>
                            <div class="comment-files" id="new-comm-file"></div>
                            <div class="form-group alignright">
                                <button id="postComment" class="btn btn-primary btn-invert">Post Comment</button>
                            </div>
                        </form>
                    </div>
                @endif
            </div>
        </div>
    </div>
    @include('events._modal_upload_files')

@endif
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
<script>
    $.fn.fileForm = function (options) {
        var self = this;

        self.settings = {
            entity: null,
            entity_id: null,
            comment_id: null,
            successUploadCallback: function (file) {
            },
            errorUploadCallback: function (error) {
            },
            beforeUpload: function () {
            }
        };
        $.extend(self.settings, options);

        // Grab the files and set them to our variable
        function prepareUpload(event) {
            files = event.target.files;
        }

        // Catch the form submit and upload the files
        function uploadFiles(event) {
            event.stopPropagation(); // Stop stuff happening
            event.preventDefault(); // Totally stop stuff happening
            self.settings.beforeUpload();
            // START A LOADING SPINNER HERE
            // Create a formdata object and add the files
            var data = new FormData();
            data.append('entity', self.settings.entity);
            data.append('entity_id', self.settings.entity_id);
            $.each(files, function (key, value) {
                data.append('file', value);
            });

            //Add CSRF token
            var _token = $("input[name=_token]", self).val();

            data.append('_token', _token);

            $.ajax({
                url: '/api/file/upload?proceed=1',
                type: 'POST',
                data: data,
                cache: false,
                dataType: 'json',
                processData: false, // Don't process the files
                contentType: false, // Set content type to false as jQuery will tell the server its a query string request
                success: function (data, textStatus, jqXHR) {
                    var files = [];
                    var leng = $('#new-comm-file span').length + 1;
                    $('.fileLength').empty();
                    $('.fileLength').append(leng + '/' + '{{ \App\Comment::ALLOWED_FILES_AMOUNT }}');
                    if (data) {
                        if (self.settings.comment_id === null) {
                            $('#new-comm-file').append(
                                '<span class="comment-file-wrapper">' +
                                '<a href="' + data.file.url + '" id="comment-file-' + data.file.id + '">' + data.file.name + '</a>' +
                                '<i href="#" data-id="' + data.file.id + '" style="cursor:pointer" class="comment-file-edit-remove-link">[x]</i>' +
                                '</span>'
                            );

                        } else {
                            $('#comm-file-' + self.settings.comment_id).append(
                                '<span class="comment-file-wrapper">' +
                                '<a href="' + data.file.url + '" id="comment-file-' + data.file.id + '">' + data.file.name + '</a>' +
                                '<i href="#" data-id="' + data.file.id + '" style="cursor:pointer" class="comment-file-edit-remove-link">[x]</i>' +
                                '</span>'
                            )
                        }
                        var files_count = $('#comment-form').find('.comment-file-wrapper').length;
                        // files.push(data);

                        $(this).html(files_count + '/' + '{{\App\Comment::ALLOWED_FILES_AMOUNT}}');
                    }
                    if (data.success) {
                        self.settings.successUploadCallback(data.file);
                    }
                    else {
                        self.settings.errorUploadCallback(data.errors);
                    }

                },
            });
        }

        var init = function () {
            // Add events
            $('input[type=file]', self).on('change', prepareUpload);

            $('form', self).on('submit', uploadFiles);
        };

        init();
    }

    var event_id = $('#event_id').val();

    function deleteFile(file_id) {
        $.ajax({
            type: 'POST',
            url: '/api/files/delete/' + file_id,
            success: function () {
                $('.fileLength').empty();
                var leng = $('#new-comm-file span').length + 1;
                $('.fileLength').append(leng - 1 + '/' + '{{ \App\Comment::ALLOWED_FILES_AMOUNT }}');
                if ($('.fileLength').text() == '0/5') {
                    $('.fileLength').empty();
                }

            }

        })
    }

    $(document).on("click", ".comment-file-edit-remove-link", function (e) {
        $(".file-form-error-message").remove();
        var file_id = $(this).attr("data-id");
        deleteFile(file_id);
        $(this).parent().remove();

    });
    $(document).on('click', '.comment-edit-btn', function (e) {
        $(this).parent().parent().parent().parent().next().show("slow")
    });
    $(document).on('click', '.edit-comment-cancel-button', function () {
        $(this).parent().parent().parent().hide('slow');
    });


    $(document).on('click', '.update-comment', function () {
        var files = [];
        var comment = $(this).parent().parent().find($('input[name=content]')).val();
        var comment_files = $(this).parent().prev('.comment-files').find('.comment-file-edit-remove-link');
        var comment_id = $(this).attr('data-id');
        comment_files.each(function (index, element) {
            files.push($(element).attr('data-id'));
        });
        // $('.comment-file-wrapper').length

        $.ajax({
            url: '/api/comment/update',
            type: 'post',
            data: {comment_id: comment_id, content: comment, files: files, origin_files: ''},
            beforeSend: function () {
                $('.loading').css({display: 'block'});
            },
            success: function () {
                testAjax();
            }
        })
    });

    function testAjax() {
        $.ajax({
            url: '/api/comments/' + event_id,
            type: 'get',
            success: function (response) {
                var filesIds = [];
                $('.comments-container').empty();
                $('.comments-container').append(response.html);
                $('.loading').css({display: 'none'});
            }
        })
    };
    $(document).on('ready', function () {
        testAjax()
    });
    //comment
    var options = {
        beforeSubmit: function (formData, jqForm, options) {
            for (var i = 0; i < formData.length; i++) {
                var field = formData[i];
                if (field.name == 'content' && field.value == '') {
                    return false;
                }
            }
            return true;
        },  // pre-submit callback
        success: function (response, statusText, xhr, $form) {
            if (response.success) {

                var $comment = renderComment(response.comment, $("#comment-" + response.comment.id));

            }
        },
        dataType: 'json'        // 'xml', 'script', or 'json' (expected server response type)
//                                clearForm: true,        // clear all form fields after successful submit
//                                resetForm: true       // reset the form after successful submit
    };
    var commentFormTpl = _.template($("#edit-comment-form-tpl").text());

    var id = $(this).data('id');
    var $editFormContainer = $('.comment-body', $('#comment-' + id));
    var $form = $('form', $editFormContainer);
    $form.ajaxForm(options);

    $(document).on('click', '.file-form-btn', function () {
        $(".file-form-error-message").remove();
        let _this = $(this);
        var $files = $("input[name=files]");
        var ar = ($files.val() == "") ? [] : $files.val().split(",");
        if (ar.length >= {{\App\Comment::ALLOWED_FILES_AMOUNT}}) {
            $(this).parents('.form-group').next('.comment-files').prepend('<div class="file-form-error-message errors message-danger" style="margin: 10px; color:#ff6666; font-size: 14px">You can\'t upload more than {{\App\Comment::ALLOWED_FILES_AMOUNT}} documents at a time in a single comment.</div>');
            return;
        }
        var $modal = $("#baseModal").clone().modal({show: false});

        var tpl = _.template($("#upload-file-form-tpl").text());
        $(".modal-content", $modal).html(tpl({}));

        $modal.modal('show');
        $modal.fileForm({
            entity: 'none',
            entity_id: 0,
            comment_id: $(this).parents('.form-group').prev('.comm-id').val(),
            successUploadCallback: function (file) {
                $(".file-form-error-message").remove();
                var $files = $("input[name=files]")

                var ar = ($files.val() === "") ? [] : $files.val().split(",");
                ar.push(file.id)

                $files.val(ar.join(","));
                $modal.modal('hide');

                var remove_link = $('<a href=""  class="comment-file-edit-remove-link"></a>').attr("data-id", "#" + file.id).text("[x]");
                var $file = $('<a href=""></a>').attr("href", file.url).text(file.name).attr("id", "comment-file-" + file.id);
                var wrapper = $('<span class="comment-file-wrapper"></span>').append($file).append(remove_link);
                $form.find('.comment-files').append(wrapper);
            },
            errorUploadCallback: function (errors) {
                $(".file-form-error-message").remove();
                var keys = Object.keys(errors);
                _.each(keys, function (k) {
                    $('.errors', $modal).append(errors[k]);
                });
            },
            beforeUpload: function () {
                $('.errors', $modal).html('');
            }
        });
    });

    $(document).on('click', '.comment-delete-btn', function () {
        var $this = $(this);
        var id = $this.attr('data-id');
        $.post('/api/comment/delete', {
            id: id
        }, function (response) {
            if (response.success) {
                $('#comment-' + id).remove();
            }
        });
        return false;
    });


    $('#postComment').on('click', function (e) {
        e.preventDefault();
        var content = $('#content').val();
        var file = $('#file').val();
        $.ajax({
            url: '/api/comment/create',
            type: 'POST',
            data: {event_id: event_id, content: content, files: file},
            beforeSend: function () {
                $('.loading').css({display: 'block'});
            },
            success: function () {
                $('#content').val('');
                $('.comment-files').empty();
                testAjax();
            }
        });
    });

    $(document).on('click', '.showHide', function (e) {
        e.preventDefault();
        var $this = $(this);
        if ($this.hasClass("hidden-att")) {
            $this.next().show();
            $this.removeClass("hidden-att")
        } else {
            $this.next().hide();
            $this.addClass("hidden-att")
        }
    });

    $(document).on('click', '.show-profile', function (event) {
        if ($(this).attr('data-artist') === '[object Object]') {
            window.open('{{url('/artists')}}' + '/' + $(this).attr('data-user'), '_blank');
        }
    });

</script>