<div class="content">
    <div class="row">
        <div class="event-comments">
            <div class="form-head">
                <h2>Comments</h2>
                Let artists know a little more about your event
            </div>
            <div class="comments-container"></div>

            <div class="comment-form">
                <div class="form-group">
                    {{--<label class="sr-only" for="exampleInputAmount">Amount (in dollars)</label>--}}
                    <div class="input-group">
                        <input type="text" class="form-control" id="comment-content"
                               name="content" placeholder="Add a comment" autocomplete="off"
                               value="{{ old('content') }}" required>
                        <div class="input-group-addon">
                                <span class="glyphicon glyphicon-file file-form-btn">
                                    <span class="files-amount-wrapper">
                                        (<span class="files-amount"></span>/<span
                                                class="amount-allowed">{{\App\Comment::ALLOWED_FILES_AMOUNT}}</span>)
                                    </span>
                                </span>
                        </div>
                    </div>
                    <div class="input-group">
                        <input type="hidden" value="{{ old('files') }}" name="files"/>
                    </div>
                </div>
                <div class="form-group alignright">

                </div>
                @if ($errors->has('content'))
                    <span style="color: red;" class="help-block">
                        <strong>{{ $errors->first('content') }}</strong>
                    </span>
                @endif
                <div class="comment-files">
                    <div>
                    </div>
                </div>
            </div>
        </div>

        <script type="text/template" id="comment-in-list">
            <div class="comment-item" data-id="<%= comment.id %>" id="comment-<%= comment.id %>">
        <div class="comment-content">
            <div class="side"><span class="time"><%= comment.created_at %></span></div>
        </div>
        <div class="comment-content">
            <div class="side"><%= comment.user.name %></div>
            <div class="data"><%= comment.content %></div>
        </div>
        <% if(comment.files.length > 0) { %>
        <div class="comment-content files">
            <div class="side hidden-att">
                <span class="hide-att">Hide Att.</span>
                <span class="show-att">Show Att.</span>
                <span class="glyphicon glyphicon-triangle-right"></span>
            </div>
            <div class="data" style="display:none">
                <% _.each(comment.files, function(f){ %>
                    <span><a href="<%= f.url %>"><%= f.name %></a></span>
                <% }); %>
            </div>
        </div>
        <% } %>
    </div>
</script>

@include('events._modal_upload_files')

<script type="text/javascript">
    (function($){
        $.fn.eventComments = function(options){
            var self = this;

            self.settings = {
                event_id: null
            };
            $.extend(self.settings, options);

            var addListeners = function (){
                $('.file-form-btn').on('click', function(){
                    var $commentForm = $('.comment-form');
                    $(".file-form-error-message").remove();
                    var $files = $(".comment-file-wrapper", $commentForm);
                    if ($files.length >= {{\App\Comment::ALLOWED_FILES_AMOUNT}}) {
                        $commentForm.prepend('<div class="file-form-error-message errors message-danger" style="margin: 10px; color:#ff6666; font-size: 14px">You can\'t upload more than {{\App\Comment::ALLOWED_FILES_AMOUNT}} documents at a time in a single comment.</div>');
                        return;
                    }
                    var $modal = $("#baseModal").clone().modal({show: false});

                    var tpl = _.template($("#upload-file-form-tpl").text());
                    $(".modal-content", $modal).html(tpl({}));

                    $modal.modal('show');

                    $modal.fileForm({
                        entity: 'none',
                        entity_id: 0,
                        successUploadCallback: function(file) {
                            $(".file-form-error-message").remove();
                            var $files = $("input[name=files]", "#create_event")

                            var ar = ($files.val() == "") ? [] : $files.val().split(",");
                            ar.push(file.id)

                            $files.val(ar.join(","));
                            $modal.modal('hide');

                            var remove_link = $('<a href="" class="comment-file-remove-link"></a>').attr("href", "#" + file.id).text("[x]");
                            var $file = $('<a href=""></a>').attr("href", file.url).text(file.name).attr("id", "comment-file-" + file.id);
                            var wrapper = $('<span class="comment-file-wrapper"></span>').append($file).append(remove_link);

                            $('.comment-files', self).append(wrapper);
                            FilesAmount.setFileAmount( $('.comment-files > .comment-file-wrapper ').length );
                        },
                        errorUploadCallback: function(errors) {
                            $(".file-form-error-message").remove();
                            var keys = Object.keys(errors);
                            _.each(keys, function(k){
                                $('.errors', $modal).append(errors[k]);
                            });
                        },
                        beforeUpload:function(){
                            $('.errors', $modal).html('');
                        }
                    });
                });

                // Remove already choosed file.
                $(".comment-files").on("click", ".comment-file-remove-link", function(e) {
                    $(".file-form-error-message").remove();
                  e.preventDefault();

                  var id = $(this).attr("href");
                  id = id.replace("#", "");
                  var $files = $("input[name=files]", "#create_event");
                  var ar = ($files.val() == "") ? [] : $files.val().split(",");
                  var index = ar.indexOf(id);
                  if(index!=-1){
                    ar.splice(index, 1);
                    $files.val(ar.join(","));
                    $('.comment-files', self).find("a#comment-file-" + id).parent(".comment-file-wrapper").remove();
                      FilesAmount.setFileAmount( $('.comment-files > .comment-file-wrapper ').length );
                  }
                });
            };

            var init = function() {
              var $files = $("input[name=files]", "#create_event");
              var $files_val = $files.val();


              if ($files_val != "") {
                $.ajax({
                  url: '/api/file/get-list/' + $files_val,
                  type: "GET",
                  dataType: "json",
                  success: function (response) {
                    // Render files.
                    if (typeof response.files != "underfined") {
                        FilesAmount.setFileAmount( response.files.length );
                      for(var n in response.files) {
                        let item = response.files[n];

                        var remove_link = $('<a href="" class="comment-file-remove-link"></a>').attr("href", "#" + item.id).text("[x]");
                        var $file = $('<a href=""></a>').attr("href", item.url).text(item.name).attr("id", "comment-file-" + item.id);
                        var wrapper = $('<span class="comment-file-wrapper"></span>').append($file).append(remove_link);
                        $('.comment-files', self).append(wrapper);
                      }
                    }

                    addListeners();
                  },
                  error: function (response) {
                    console.log("Error", response);
                    addListeners();
                  }
                });
              } else {
                addListeners();
              }
            }

            init();
        };

        $(".event-comments").eventComments({
            'event_id': '-1'
        })

    })(jQuery)

    $.fn.fileForm = function(options) {
        var self = this;

        self.settings = {
            entity: null,
            entity_id: null,
            successUploadCallback: function(file){},
            errorUploadCallback: function(error){},
            beforeUpload: function(){}
        };
        $.extend(self.settings, options);

        // Grab the files and set them to our variable
        function prepareUpload(event)
        {
            files = event.target.files;
        }

        // Catch the form submit and upload the files
        function uploadFiles(event)
        {
            event.stopPropagation(); // Stop stuff happening
            event.preventDefault(); // Totally stop stuff happening
            self.settings.beforeUpload()

            let target = event.target || event.srcElement;
            let submit_button = $(target).find("button.btn-invert");

            if (submit_button.length) {
                submit_button.prop("disabled", true);
                let event_html_origin = submit_button.html();
                submit_button.html(event_html_origin + ' <span class="eva-loading"></span>');
                submit_button.attr('origin_text', event_html_origin);
            }

            // START A LOADING SPINNER HERE

            // Create a formdata object and add the files
            var data = new FormData();
            data.append('entity', self.settings.entity);
            data.append('entity_id', self.settings.entity_id);
            $.each(files, function(key, value) {
                data.append('file', value);
            });

            //Add CSRF token
            var _token = $("input[name=_token]", self).val()

            data.append('_token', _token);

            $.ajax({
                url: '/api/file/upload?proceed=1',
                type: 'POST',
                data: data,
                cache: false,
                dataType: 'json',
                processData: false, // Don't process the files
                contentType: false, // Set content type to false as jQuery will tell the server its a query string request
                success: function(data, textStatus, jqXHR)
                {
                    if (submit_button.length) {
                        submit_button.prop("disabled", false);
                        submit_button.html(submit_button.attr('origin_text'));
                    }
                    if(data.success) {
                        // Success so call function to process the form
                        //submitForm(event, data);
                        self.settings.successUploadCallback(data.file);
                        //console.log(data);
                    }
                    else
                    {
                        // Handle errors here
//                        console.log('ERRORS: ' + data.error);
                        self.settings.errorUploadCallback(data.errors);
                    }
                },
                error: function(jqXHR, textStatus, errorThrown)
                {
                    if (submit_button.length) {
                        submit_button.prop("disabled", false);
                        submit_button.html(submit_button.attr('origin_text'));
                        submit_button.html(event_html_origin);
                    }
                    self.settings.errorUploadCallback(textStatus);
                    // Handle errors here
//                    console.log('ERRORS: ' + textStatus);
                    // STOP LOADING SPINNER
                }
            });
        }

        var init = function() {
            // Add events
            $('input[type=file]', self).on('change', prepareUpload);
            $('form', self).on('submit', uploadFiles);
        };

        init();
    }
</script>