<form action="" method="GET" id="eventsSortFilterForm" class="events-sort-filter-form">
    <div class="input-group col-md-9">
        <div class="col-md-8 event-sort-form">
            <p>SORT BY:
                <span class="filter-options">
                    <a class="sort" href="#" data-type="sort_location">Location</a>
                    <input class="sort-field" type="hidden" name="sort_location" value="{{ $sorts['sort_location'] }}"/>

                    <a class="sort" href="#" data-type="sort_max_budget">Budget</a>
                    <input class="sort-field" type="hidden" name="sort_max_budget" value="{{ $sorts['sort_max_budget'] }}"/>

                    <a class="sort" href="#" data-type="sort_starttime">Event Date</a>
                    <input class="sort-field" type="hidden" name="sort_starttime" value="{{ $sorts['sort_starttime'] }}"/>

                    <a class="sort" href="#" data-type="sort_created_at">Last Created</a>
                    <input class="sort-field" type="hidden" name="sort_created_at" value="{{ $sorts['sort_created_at'] }}"/>

                    <a class="sort" href="#" data-type="sort_updated_at">Last Edited</a>
                    <input class="sort-field" type="hidden" name="sort_updated_at" value="{{ $sorts['sort_updated_at'] }}"/>
                </span><!-- /.filter-options -->
            </p>
        </div>

        <div class="col-md-4 event-search-form">
            <div class="form-group">
                <label class="sr-only" for="exampleInputAmount">Amount (in dollars)</label>
                <div class="input-group">
                    <input type="text" class="form-control" id="exampleInputAmount" placeholder="search your events" name="search" value="{{ $filters['search'] }}"/>
                    <div class="input-group-addon"><span class="glyphicon glyphicon-search" id="search-btn"></span></div>
                </div>
            </div>
        </div>
    </div>
</form>

<script>
    (function(){
        "use strict";
        var $form = $("#eventsSortFilterForm");
        var $sortFields = $("input.sort-field", $form);
        $('a.sort, #search-btn', $form).click(function () {
            var $sortBtn = $(this);
            var name = $sortBtn.attr('data-type');
            var $sortField = $("input[name="+name+"]");
            var direction = ($sortField.val() == 'desc') ? 'asc' : 'desc';
            $sortFields.val("");
            $sortField.val(direction);
            $form.submit();
        });
        return false;
    }());
</script>