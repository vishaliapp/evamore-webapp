<script type="text/template" id="upload-file-form-tpl">
    <form class="">
        {{csrf_field()}}
        <div class="modal-header">
            <h3>Upload Files</h3>
        </div>
        <div class="modal-body">

            <div class="modal-body-wrapper">
                <div class="form-group">
                    <div class="input-group">
                        <input class="input-file-align" required="required" type="file" name="file"/><span style="display:none">spinner</span>
                    </div>
                    <div class="errors message-danger" style="margin-top: 10px; color:#ff6666; font-size: 14px"></div>
                </div>
            </div>
            <div style="display: none;" class="text-center files-upload-form-message bg-warning text-warning modal-body-error">
                You can attach up to {{\App\Comment::ALLOWED_FILES_AMOUNT}} files to a single comment.
            </div>

        </div>
        <div class="modal-footer upload-modal-footer">
            <button type="submit" class="btn btn-primary btn-invert">
                UPLOAD
            </button>
        </div>
    </form>
</script>
<script>
    window.FilesAmount = function($) {
        /**
         * @param {Number|undefined} amount - files amount
         */
        function setFileAmount(amount) {
            var $filesAmountWrapper = $('.files-amount-wrapper'),
                    $filesAmountElem = $('.files-amount').eq(0),
                    filesAmount = _.isNumber(amount) ? amount : +$filesAmountElem.text() || 0;

            $filesAmountElem.text(filesAmount);
            if (filesAmount) {
                $filesAmountWrapper.show();
            } else {
                $filesAmountWrapper.hide();
            }
        }

        return {
            setFileAmount: setFileAmount
        };
    }(window.jQuery);
</script>