@foreach($comments as $index => $comment)
    <div class="comment-item" data-id="{{ $comment->id }}" id="comment-{{ $comment->id }}">
        <div>
            <div class="comment-content row">
                <div class="side col-md-3 col-sm-3">
                    <span class="time">{{ $comment->created_at }}</span>
                </div>
                @if(Auth::user()->id == $event->user_id || Auth::user()->hasRole("ADMIN") )
                    <div class="data col-md-9 col-sm-9">
                        <i style="cursor:pointer"; data-id="{{ $comment->id }}" class="comment-edit-btn"><span
                                    class="glyphicon glyphicon-pencil" style="margin-right: 10px"></span></i>
                        <i style="cursor:pointer"; data-id="{{ $comment->id }}" class="comment-delete-btn"><span
                                    class="glyphicon glyphicon-trash"></span></i>
                    </div>
                @endif
            </div>
        </div>

        <div class="comment-content row">
            <div class="side col-md-3 col-sm-3 comentLink">
                <a href="@if($comment->user->artist) {{route('artist_show', ['username' =>$comment->user->username]) }} @else javascript:void(0) @endif " target="_blank" style="text-decoration:none;" class="show-profile"
                   data-artist="{{ $comment->artist }}"
                   data-user="{{ $comment->user->username }}">{{ $comment->user->name }}</a>
            </div>
            <div class="data col-md-9 col-sm-9">
                <div class="comment-body">{{ $comment->content }}</div>
            </div>
        </div>
        @if(count($comment->files))
            <div class="comment-content files row">
                <div class="side showHide hidden-att col-md-3 col-sm-3">
                    <span class="hide-att">Hide Att.</span>
                    <span class="show-att">Show Att.</span>
                    <span class="glyphicon glyphicon-triangle-right"></span>
                </div>
                <div class="data col-md-9 col-sm-9" style="display:none">
                    @foreach($comment->files as $file)
                        <span id="comment-file-{{$file->id}}"><a href="{{ url('file/'.$file->id) }}" download>{{ $file->name }}</a>
                        <i class="{{$file->downloaded_by_artist ? 'text-success fa fa-eye' : '' }}"></i> </span>
                    @endforeach
                </div>
            </div>
        @endif
    </div>
    <div class="comment-form comments-edits" style="width: 75%;margin: 0px auto;overflow: hidden;display: none">
        <form style="" class="form-horisontal edit-form">
            <input class="comm-id" type="hidden" name="comment_id" value="{{ $comment->id }}"/>
            <div class="form-group comments-upd">
                <div class="input-group">
                    <input type="text" name="content" class="form-control upd-comment" id="input-{{ $index }}" name="content" autocomplete="off"
                           value="{{ $comment->content }}">
                    <div class="input-group-addon">
                        <span class="glyphicon glyphicon-file file-form-btn"></span>
                    </div>
                </div>
                <div class="input-group"></div>
            </div>
            <div class="comment-files" id="comm-file-{{$comment->id}}">
                @foreach($comment->files as $file)
                    <span  class="comment-file-wrapper">
                      <a href="{{ $file->path }}" id="comment-file-{{ $file->id }}">{{ $file->name }}</a>
                      <i id="file-{{ $index }}" data-id="{{ $file->id }}" style="cursor:pointer"
                         class="comment-file-edit-remove-link comment-file">[x]</i>
                    </span>
                @endforeach
            </div>
            <div class="form-group alignright">
                <button type="button" class="btn btn-danger edit-comment-cancel-button">Cancel</button>
                <button type="button" data-index="{{ $index }}" data-id="{{ $comment->id }}"class="btn btn-primary btn-invert update-comment">Save</button>
            </div>
        </form>
    </div>
@endforeach