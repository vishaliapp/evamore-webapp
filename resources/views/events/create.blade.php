@extends('layouts.dash')

@section('content')
    <div class="main-body">
        <form action="{{ route('saveEvent') }}" method="POST" id="create_event" novalidate>
            @if($fan)
                <input type="hidden" name="fan_id" value="{{ $fan->id }}">
            @endif
            <div class="content">
                <div class="form form-alt form-event">
                    {{ csrf_field() }}
                    @if ($bookedArtist)
                        <input type="hidden" name="booked_user_id" value="{{ $bookedArtist->id  }}">
                    @endif

                    <div class="form-head {{ $errors->has('name') ? ' has-error' : '' }}">
                        <h2><input name='name' placeholder='New Event' value="{{ old('name') }}"><i
                                    class="ico-pencil"></i></h2>
                        @if ($errors->has('name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                    </div><!-- /.form-head -->

                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group {{ $errors->has('date') ? ' has-error' : '' }}">
                                    <label class="form-label">DATE</label>

                                    <div class="datepicker" id="datepicker-inline"
                                         @if (old('date'))
                                         @php $date = old('date');@endphp
                                         data-year="{{ date('Y', strtotime($date))}}"
                                         data-month="{{ date('n', strtotime($date)) }}"
                                         data-day="{{ date('j', strtotime($date)) }}"
                                            @endif
                                    >
                                        <input type='hidden' id='date' name='date'/>
                                    </div>

                                    @if ($errors->has('date'))
                                        <span class="help-block">
		                                <strong>{{ $errors->first('date') }}</strong>
		                            </span>
                                    @endif
                                </div><!-- /.form-group -->

                            </div><!-- /.col-sm-4 -->

                            <div class="col-md-4">
                                <div class="form-group {{ $errors->has('hours')||$errors->has('minutes') ? ' has-error' : '' }}">
                                    <div class="event-start-time">
                                        <label for="starttime" class="form-label">TIME:</label>
                                        <select name="time_zone" class="selectpicker">
                                            @foreach(App\ModelType\TimeZoneType::$zonesTitles as $id=>$name)
                                                <option value="{{$id}}">{{$name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-controls clear">
                                        <input type="number" class="form-control form-control-one-third" id="hours"
                                               value="{{ old('hours') }}" name="hours" placeholder="12" required/>

                                        <span class="form-separator">:</span>

                                        <input type="number" class="form-control form-control-one-third" id="monutes"
                                               value="{{ old('minutes') }}" name="minutes" placeholder="00" required/>

                                        <select class="form-control form-control-one-third" id="time_period"
                                                name="period" placeholder="PM" required>
                                            <option value="AM"
                                                    @if (old("period")=='AM')
                                                    selected
                                                    @endif>
                                                AM
                                            </option>
                                            <option value="PM"
                                                    @if (old("period")=='PM')
                                                    selected
                                                    @endif>
                                                PM
                                            </option>
                                        </select>
                                    </div><!-- /.form-controls -->
                                    @if ($errors->has('hours'))
                                        <span class="help-block">
			                                <strong>{{ $errors->first('hours') }}</strong>
			                            </span>
                                    @endif

                                    @if ($errors->has('minutes'))
                                        <span class="help-block">
			                                <strong>{{ $errors->first('minutes') }}</strong>
			                            </span>
                                    @endif
                                    @if ($errors->has('time_zone'))
                                        <span class="help-block">
			                                <strong>{{ $errors->first('time_zone') }}</strong>
			                            </span>
                                    @endif
                                </div><!-- /.form-group -->

                                {{--<div class="form-group {{ $errors->first('city') ||  $errors->first('state') ? ' has-error' : '' }}">--}}
                                {{--<div class="row">--}}
                                {{--<div class="col-md-7">--}}
                                {{--<label for="city" class="form-label">City</label>--}}
                                {{--<div class="form-controls">--}}
                                {{--<input type="text" class="form-control" id="city"--}}
                                {{--name="city"--}}
                                {{--value="{{ old('city') }}" placeholder="City"--}}
                                {{--required>--}}
                                {{--</div><!-- /.form-controls -->--}}
                                {{--</div>--}}
                                {{--<div class="col-md-5">--}}
                                {{--<label for="state" class="form-label">State</label>--}}
                                {{--<div class="form-controls">--}}
                                {{--<span class="form-separator"></span>--}}
                                {{--<input type="text" class="form-control" id="state"--}}
                                {{--name="state"--}}
                                {{--value="{{ old('state') }}" placeholder="State"--}}
                                {{--required>--}}
                                {{--</div><!-- /.form-controls -->--}}

                                {{--</div>--}}
                                {{--</div>--}}
                                {{--@if ($errors->has('city'))--}}
                                {{--<span class="help-block">--}}
                                {{--<strong>{{ $errors->first('city') }}</strong>--}}
                                {{--</span>--}}
                                {{--@endif--}}
                                {{--@if ($errors->has('state'))--}}
                                {{--<span class="help-block">--}}
                                {{--<strong>{{ $errors->first('state') }}</strong>--}}
                                {{--</span>--}}
                                {{--@endif--}}
                                {{--</div><!-- /.form-group -->--}}
                                <div class="form-group {{ $errors->first('venue') ? ' has-error' : '' }}">
                                    <label for="venue" class="form-label">Venue</label>

                                    <div class="form-controls">
                                        <input type="text" class="form-control" id="venue" name="venue"
                                               value="{{ old('venue') }}" placeholder="Anywhere but your bathroom"
                                               required>
                                    </div><!-- /.form-controls -->
                                    @if ($errors->has('venue'))
                                        <span class="help-block">
			                                <strong>{{ $errors->first('venue') }}</strong>
			                            </span>
                                    @endif
                                </div><!-- /.form-group -->

                                <div class="form-group {{ $errors->first('address') ? ' has-error' : '' }}">
                                    <label for="address" class="form-label">Address</label>

                                    <div class="form-controls">
                                        <input type="text" class="form-control" id="add" name="add"
                                               value="{{ old('address') }}" placeholder="Anywhere but your bathroom"
                                               required>
                                        <input type="hidden" value="{{ old('place_lat') }}" name="place_lat" id="place-lat">
                                        <input type="hidden" value="{{ old('place_lng') }}" name="place_lng" id="place-lng">
                                        <input type="hidden" value="{{ old('state') }}" name="state" id="administrative_area_level_1">
                                        <input type="hidden" value="{{ old('city') }}" name="city" id="locality">
                                        <input type="hidden" value="{{ old('address') }}" name="address" id="route">

                                        <input type="hidden" id="street_number">
                                        <input type="hidden" id="postal_code">
                                        <input type="hidden" id="country">
                                    </div><!-- /.form-controls -->
                                    @if ($errors->has('address'))
                                        <span class="help-block">
			                                <strong>{{ $errors->first('address') }}</strong>
			                            </span>
                                    @endif
                                </div>


                                <div id="map"></div>

                                <div class="form-group {{ $errors->first('duration') ||  $errors->first('min_budget') || $errors->first('max_budget')? ' has-error' : '' }}">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label for="duration" class="form-label" required>Duration</label>
                                            <div class="form-controls ">
                                                <input type="text" class="form-control" id="duration" name="duration"
                                                       placeholder="Time in Minutes" required
                                                       value="{{ old('duration') }}">
                                            </div><!-- /.form-controls -->
                                        </div>
                                        <div class="col-md-8">
                                            <label for="min_budget" class="form-label">BUDGET</label>

                                            <div class="form-controls">
                                                <input type="number"
                                                       class="form-control form-control-half form-control-half-smaller"
                                                       id="min_budget"
                                                       value="{{ old('min_budget') }}" name="min_budget"
                                                       placeholder="Min"
                                                       required>

                                                <span class="form-separator">to</span>

                                                <input type="number"
                                                       class="form-control form-control-half form-control-half-smaller"
                                                       id="max_budget"
                                                       value="{{ old('max_budget') }}" name="max_budget"
                                                       placeholder="Max"
                                                       required>
                                            </div>
                                        </div>
                                        @if ($errors->has('duration'))
                                            <span class="help-block">
                                                    <strong>{{ $errors->first('duration') }}</strong>
                                                </span>
                                        @endif
                                        @if ($errors->has('min_budget'))
                                            <span class="help-block">
                                                    <strong>{{ $errors->first('min_budget') }}</strong>
                                                </span>
                                        @endif
                                        @if ($errors->has('max_budget'))
                                            <span class="help-block">
                                                    <strong>{{ $errors->first('max_budget') }}</strong>
                                                </span>
                                        @endif
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label for="" class="form-label">Has sound production been arranged for this
                                        event?</label>
                                    <div class="form-check">
                                        <input class="form-check-input" value="1" name="radio" type="radio"
                                               id="radio120">
                                        <label class="form-check-label" for="radio120">Yes</label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" value="0" name="radio" type="radio"
                                               id="radio121" checked>
                                        <label class="form-check-label" for="radio121">No</label>
                                    </div>
                                    @if ($errors->has('radio'))
                                        <span class="help-block">
			                                <strong>{{ $errors->first('radio') }}</strong>
			                            </span>
                                    @endif

                                    {{--Popup--}}
                                    <label id="help"
                                           data-toggle="tooltip"
                                           data-placement='right'
                                           title='a PA or speaker system big enough for the act and venue.  Check with the venue manager if you are not sure.'>
                                        What's this?
                                    </label>

                                    <script>
                                        $('#help').tooltip()
                                    </script>
                                </div>

                            </div>

                            <div class="col-md-4">
                                <div class="form-group {{ $errors->has('event_type') ? ' has-error' : '' }}">
                                    <label class="form-label">What kind of event is this?</label>

                                    <div class="form-controls">
                                        <ul class="list-tags">
                                            @if (old('event_type'))
                                                <?php $selectedEventType = old('event_type')?>
                                            @endif;
                                            @foreach ($eventTypes as $type)
                                                <li>
                                                    <input type="radio" name="event_type" value="{{$type->id}}"
                                                           id="type_{{$type->id}}"
                                                           @if ($type->id == $selectedEventType))
                                                           checked="checked"
                                                            @endif
                                                    />
                                                    <label for="type_{{$type->id}}">{{$type->name}}</label>
                                                </li>
                                            @endforeach
                                        </ul><!-- /.list-tags -->
                                    </div><!-- /.form-controls -->
                                    @if ($errors->has('event_type'))
                                        <span class="help-block">
			                                <strong>{{ $errors->first('event_type') }}</strong>
			                            </span>
                                    @endif
                                </div><!-- /.form-group -->
                            </div>

                            {{--Sound Types--}}

                            <div class="col-md-4">
                                <div class="form-group {{ $errors->has('songTypes') ? ' has-error' : '' }}">

                                    @if ($bookedArtist)
                                        <label class="form-label" style="display: inline-block" data-toggle="tooltip"
                                               title="If interested, select additional genres so that other qualified artists will be notified of your event if the requested artist is unavailable."
                                               data-placement="right">Song Type's <span
                                                    class="glyphicon glyphicon-info-sign"></span></label>
                                    @else
                                        <label class="form-label">Song Type's</label>
                                    @endif

                                    <div class="form-controls">
                                        <ul class="list-tags">
                                            @if (old('song_type'))
                                                <?php $selectedSongTypes = old('song_type')?>
                                            @endif;
                                            @foreach ($songTypes as $songType)
                                                <li>
                                                    <input type="checkbox" name="song_type[]" value="{{$songType->id}}"
                                                           id="song_type_{{$songType->id}}"
                                                           @if (isset($selectedSongTypes) && in_array($songType->id, $selectedSongTypes))
                                                           checked="checked"
                                                            @endif
                                                    />
                                                    <label for="song_type_{{$songType->id}}">{{$songType->name}}</label>
                                                </li>
                                            @endforeach
                                        </ul><!-- /.list-tags -->
                                    </div><!-- /.form-controls -->
                                    @if ($errors->has('song_type'))
                                        <span class="help-block">
			                                <strong>{{ $errors->first('song_type') }}</strong>
			                            </span>
                                    @endif
                                </div><!-- /.form-group -->
                            </div><!-- /.col-sm-4 -->

                            <div class="col-md-4">
                                <div class="form-group {{ $errors->has('genre') ? ' has-error' : '' }}">

                                    @if ($bookedArtist)
                                        <label class="form-label" style="display: inline-block" data-toggle="tooltip"
                                               title="If interested, select additional genres so that other qualified artists will be notified of your event if the requested artist is unavailable."
                                               data-placement="right">GENRE(S) <span
                                                    class="glyphicon glyphicon-info-sign"></span></label>
                                    @else
                                        <label class="form-label">GENRE(S)</label>
                                    @endif

                                    <div class="form-controls">
                                        <ul class="list-tags">
                                            @if (old('genre'))
                                                <?php $selectedGenres = old('genre')?>
                                            @endif;
                                            @foreach ($genres as $genre)
                                                <li>
                                                    <input type="checkbox" name="genre[]" value="{{$genre->id}}"
                                                           id="{{$genre->id}}"
                                                           @if (in_array($genre->id, $selectedGenres))
                                                           checked="checked"
                                                            @endif
                                                    />
                                                    <label for="{{$genre->id}}">{{$genre->name}}</label>
                                                </li>
                                            @endforeach
                                        </ul><!-- /.list-tags -->
                                    </div><!-- /.form-controls -->
                                    @if ($errors->has('genre'))
                                        <span class="help-block">
			                                <strong>{{ $errors->first('genre') }}</strong>
			                            </span>
                                    @endif
                                </div><!-- /.form-group -->


                                @if ($bookedArtist)
                                    <div class="booked-artist-details">
                                        <label class="form-label">REQUESTED ARTIST</label>
                                        <input type="hidden" name="booked_user_id" value="{{ $bookedArtist->id }}">
                                        <div class="details-1">
                                            <div class="avatar">
                                                <img src="{{ $bookedArtist->avatar }}"/>
                                            </div>
                                            <div class="name">{{ $bookedArtist->name }}</div>
                                        </div>
                                        <div class="sounds_like">{{ $bookedArtist->sounds_like }}</div>
                                        <div class="genres">
                                            @foreach($bookedArtist->genres as $genre)
                                                <span>{{ $genre->name }}</span>
                                            @endforeach
                                        </div>

                                    </div>
                                @endif

                            </div><!-- /.col-sm-4 -->
                        </div><!-- /.row -->
                    </div><!-- /.form-body -->

                    @include("events._event_create_comments")

                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-6">
                                <a href="{{ route('events_by_type', ['type'=>'upcoming']) }}"
                                   class="btn btn-danger btn-invert">Cancel</a>
                            </div>
                            <div class="col-md-6" style="text-align: right">
                                <input type="submit" value="ADD THIS EVENT" name="submit"
                                       class="btn btn-primary btn-invert">
                            </div>
                        </div>
                    </div><!-- /.form-actions -->
                </div><!-- /.form form-event -->
            </div><!-- /.content -->
        </form>
    </div><!-- /.main-body -->

    <script type="text/javascript"
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDuCCI1t1qsTUO7xA57m7tY0ujVGgcOPjM&libraries=places"></script>
    <script>
        function myFunction() {
            var popup = document.getElementById("myPopup");
            popup.classList.toggle("show");
        }

        $(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });

        //Google API
        var address = document.getElementById('add');
        var addressOprions = {
            types: ['address'],
            componentRestrictions: {country: 'us'}
        };
        var componentForm = {
            street_number: 'short_name',
            route: 'long_name',
            locality: 'long_name',
            administrative_area_level_1: 'short_name',
            country: 'long_name',
            postal_code: 'short_name'
        };

        var autocomplete = new google.maps.places.Autocomplete(address, addressOprions);

        google.maps.event.addListener(autocomplete, 'place_changed', function () {
            var place = autocomplete.getPlace();
            for (var component in componentForm) {
                document.getElementById(component).value = '';
                document.getElementById(component).disabled = false;
            }

            for (var i = 0; i < place.address_components.length; i++) {
                var addressType = place.address_components[i].types[0];
                if (componentForm[addressType]) {
                    var val = place.address_components[i][componentForm[addressType]];
                    document.getElementById(addressType).value = val;
                }

            }
            $('#city').val();
            $('#address').val(add);

            var lat = place.geometry.location.lat();
            var lng = place.geometry.location.lng();

            $('#place-lat').val(lat);
            $('#place-lng').val(lng);
            $('#state').val(state);

        })
    </script>
@stop