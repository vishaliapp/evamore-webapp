@extends('layouts.dash')

@section('content')
    @if($isArtistManager)
        <style>
            .footer {
                position: relative !important;
            }
        </style>
    @endif
    <div class="main-body">
        <div class="content">
            <div class="form form-alt form-event"> <!-- // form-disabled // -->
                <div class="form-head {{ $errors->has('name') ? ' has-error' : '' }}">
                    <h2>{{$event->name}}</h2>
                </div><!-- /.form-head -->

                @if($isAdmin || $isEventOwner)
                    @if($left_menu_selected == 'past' && in_array($event->status, [App\ModelType\EventType::CANCELLED]))
                    <div style="float: right;">
                        <button class="btn btn-danger btn-invert"><a href="{{ route('deleteEvent',['id' => $event->id]) }}">DELETE</a></button>
                    </div>
                        @endif
                @endif
                @if(isset($artChanges) && $artChanges && !$artChanges->artistAplly->first()->pivot->apply)
                    <button class="btn btn-primary btn-invert confirm-changes">Confirm</button>
                @endif
                @if(Session::has('confirmPlanErrors'))
                    <p class="alert alert-info">
                        @foreach(Session::get('confirmPlanErrors') as $error)
                            <span>{{$error}}</span>
                        @endforeach
                    </p>
                @endif

                @include('artists._artists_requested_event')

                <div class="form-body">
                    <div class="row">
                        <div class="col-md-4">
                            <?php
                            $tz = new \DateTimeZone(App\ModelType\TimeZoneType::getTimeZone($event->time_zone));
                            $eventTime = new \DateTime($event->starttime);
                            $eventTime->setTimeZone($tz);
                            ?>

                            <div class="form-group {{ $errors->has('date') ? ' has-error' : '' }}">
                                <label class="form-label">DATE</label>

                                <div class="datepicker" id="datepicker-inline"
                                     data-year="{{ $eventTime->format("Y") }}"
                                     data-month="{{ $eventTime->format("n") }}"
                                     data-day="{{ $eventTime->format("j") }}">
                                    <input type='hidden' id='date' name='date'/>
                                </div>
                            </div><!-- /.form-group -->
                        </div><!-- /.col-sm-4 -->

                        <div class="col-md-4">
                            <div class="form-group {{ $errors->has('time') ? ' has-error' : '' }}">
                                <div class="event-start-time">
                                    <label for="starttime" class="form-label">TIME:</label>
                                    <span class="time-zone-name">{{App\ModelType\TimeZoneType::getTimeZoneTitle($event->time_zone)}}</span>
                                    <div class="clear"></div>
                                </div>


                                <div class="form-controls clear">
                                    <input type="number" class="form-control form-control-one-third"
                                           id="starttime" name="starttime" placeholder="12" disabled
                                           value="{{ $eventTime->format("g") }}"/>

                                    <span class="form-separator">:</span>

                                    <input type="number" class="form-control form-control-one-third"
                                           id="starttime_m" name="starttime_m" placeholder="00" disabled
                                           value="{{ $eventTime->format("i") }}"/>
                                    <select class="form-control form-control-one-third" disabled>
                                        <option value="AM"
                                                @if ($eventTime->format("A") == 'AM') selected="selected" @endif>AM
                                        </option>
                                        <option value="PM"
                                                @if ($eventTime->format("A") == 'PM') selected="selected" @endif>PM
                                        </option>
                                    </select>
                                </div><!-- /.form-controls -->
                            </div><!-- /.form-group -->

                            <div class="form-group {{ $errors->first('venue') ? ' has-error' : '' }}">
                                <label for="venue" class="form-label">Venue</label>

                                <div class="form-controls">
                                    <input type="text" class="form-control" id="venue" name="venue"
                                           value="{{ $event->venue }}" disabled=""
                                           placeholder="Anywhere but your bathroom"
                                           required>
                                </div><!-- /.form-controls -->
                                @if ($errors->has('venue'))
                                    <span class="help-block">
                                      <strong>{{ $errors->first('venue') }}</strong>
                                  </span>
                                @endif
                            </div><!-- /.form-group -->
                            @if($event->city)
                                <div class="form-group {{ $errors->first('city') ? ' has-error' : '' }}">
                                    <label for="venue" class="form-label">City</label>

                                    <div class="form-controls">
                                        <input type="text" class="form-control" id="city" name="city"
                                               value="{{ $event->city }}" disabled=""
                                               placeholder="Anywhere but your bathroom"
                                               required>
                                    </div><!-- /.form-controls -->
                                    @if ($errors->has('city'))
                                        <span class="help-block">
                                          <strong>{{ $errors->first('city') }}</strong>
                                      </span>
                                    @endif
                                </div>
                            @endif
                            @if($event->state)
                                <div class="form-group {{ $errors->first('state') ? ' has-error' : '' }}">
                                    <label for="venue" class="form-label">State</label>

                                    <div class="form-controls">
                                        <input type="text" class="form-control" id="state" name="state"
                                               value="{{ $event->state }}" disabled=""
                                               placeholder="Anywhere but your bathroom"
                                               required>
                                    </div><!-- /.form-controls -->
                                    @if ($errors->has('state'))
                                        <span class="help-block">
                                      <strong>{{ $errors->first('state') }}</strong>
                                  </span>
                                    @endif
                                </div>
                            @endif

                            {{--<div class="form-group {{ $errors->first('city') ||  $errors->first('state') ? ' has-error' : '' }}">--}}
                            {{--<div class="row">--}}
                            {{--<div class="col-md-7">--}}
                            {{--<label for="city" class="form-label">City</label>--}}
                            {{--<div class="form-controls">--}}
                            {{--<input type="text" class="form-control" id="city"--}}
                            {{--name="city"--}}
                            {{--disabled--}}
                            {{--value="{{ $event->city }}" placeholder="City"--}}
                            {{--required>--}}
                            {{--</div><!-- /.form-controls -->--}}
                            {{--</div>--}}
                            {{--<div class="col-md-5">--}}
                            {{--<label for="state" class="form-label">State</label>--}}
                            {{--<div class="form-controls">--}}
                            {{--<span class="form-separator"></span>--}}
                            {{--<input type="text" class="form-control" id="state"--}}
                            {{--name="state"--}}
                            {{--disabled--}}
                            {{--value="{{ $event->state }}" placeholder="State"--}}
                            {{--required>--}}
                            {{--</div><!-- /.form-controls -->--}}

                            {{--</div>--}}
                            {{--</div>--}}
                            {{--@if ($errors->has('city'))--}}
                            {{--<span class="help-block">--}}
                            {{--<strong>{{ $errors->first('city') }}</strong>--}}
                            {{--</span>--}}
                            {{--@endif--}}
                            {{--@if ($errors->has('state'))--}}
                            {{--<span class="help-block">--}}
                            {{--<strong>{{ $errors->first('state') }}</strong>--}}
                            {{--</span>--}}
                            {{--@endif--}}
                            {{--</div><!-- /.form-group -->--}}

                            <div class="form-group {{ $errors->first('duration') ||  $errors->first('min_budget') || $errors->first('max_budget')? ' has-error' : '' }}">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label for="duration" class="form-label" required>Duration</label>
                                        <div class="form-controls ">
                                            <input type="text" class="form-control" id="duration" name="duration"
                                                   disabled
                                                   placeholder="Time in Minutes" required
                                                   value="{{ $event->duration }}">
                                        </div><!-- /.form-controls -->
                                    </div>
                                    <div class="col-md-8">
                                        <label for="min_budget" class="form-label">BUDGET</label>

                                        <div class="form-controls">
                                            <input type="number"
                                                   class="form-control form-control-half form-control-half-smaller"
                                                   id="min_budget"
                                                   disabled
                                                   value="{{ $event->min_budget }}" name="min_budget"
                                                   placeholder="Min"
                                                   required>

                                            <span class="form-separator">to</span>

                                            <input type="number"
                                                   class="form-control form-control-half form-control-half-smaller"
                                                   id="max_budget"
                                                   disabled
                                                   value="{{ $event->max_budget }}" name="max_budget"
                                                   placeholder="Max"
                                                   required>
                                        </div>
                                    </div>
                                    @if ($errors->has('duration'))
                                        <span class="help-block">
                                                    <strong>{{ $errors->first('duration') }}</strong>
                                                </span>
                                    @endif
                                    @if ($errors->has('min_budget'))
                                        <span class="help-block">
                                                    <strong>{{ $errors->first('min_budget') }}</strong>
                                                </span>
                                    @endif
                                    @if ($errors->has('max_budget'))
                                        <span class="help-block">
                                                    <strong>{{ $errors->first('max_budget') }}</strong>
                                                </span>
                                    @endif
                                </div>
                            </div>
                        </div><!-- /.col-sm-4 -->
                        @if($selectedEventType)
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="form-label">WHAT KIND OF EVENT IS THIS?</label>

                                    <div class="form-controls">
                                        <ul class="list-tags">
                                            <li>
                                                @if($event->event)
                                                    <input type="radio" name="event_type" value="{{$event->event->event_type}}"
                                                           id="type_{{$event->event->event_type}}" checked="checked"
                                                           disabled/>
                                                    <label for="type_{{$event->event->event_type}}">{{$selectedEventType->name}}</label>
                                                    @else
                                                    <input type="radio" name="event_type" value="{{$event->event_type}}"
                                                           id="type_{{$event->event_type}}" checked="checked"
                                                           disabled/>
                                                    <label for="type_{{$event->event_type}}">{{$selectedEventType->name}}</label>
                                                @endif
                                            </li>
                                        </ul><!-- /.list-tags -->
                                    </div><!-- /.form-controls -->
                                </div><!-- /.form-group -->
                            </div>
                        @endif
                        @if($isArtist)
                        <script>
                            $(document).ready(function () {
                                $(document).on('click', '.confirm-changes', function () {

                                    $.ajax({
                                        url: '{{ route('apply_event') }}',
                                        headers: {
                                            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                                        },
                                        type: 'post',
                                        data: {
                                            eventId: "{{ $event->id }}",
                                            artistId: "{{ Auth::user()->artist->id }}"
                                        },
                                        success: function (response) {
                                            toastr.success('You successfully applied changes')
                                        },
                                        cache: function (error) {
                                            toastr.error(error)
                                        }
                                    })
                                })
                            })
                        </script>
                        @endif
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="form-label">GENRE<span>(S)</span></label>

                                <div class="form-controls">
                                    @php
                                        $eventGenres = [];
                                    @endphp
                                    @php
                                        if (!$event->genres){
                                            $eventGenres = $event->event->genres;
                                        }else{
                                            $eventGenres = $event->genres->groupBy('id');
                                        }
                                    @endphp
                                    <ul class="list-tags">
                                        @foreach ($eventGenres as $genre)
                                            <li>
                                                <input type="checkbox" name="genre[]" value="{{$genre->first()->id}}"
                                                       id="{{$genre->first()->id}}" checked="checked" disabled/>
                                                <label for="{{$genre->first()->id}}">{{$genre->first()->name}}</label>
                                            </li>
                                        @endforeach
                                    </ul><!-- /.list-tags -->
                                </div><!-- /.form-controls -->
                            </div><!-- /.form-group -->
                            @if($planerBid && $planerBid->status==App\ModelType\BidType::STATUS_NEW)
                                <div class="form-group">
                                    <form action='{{route('artists_confirm_planer_request')}}' method='post'
                                          id='planer_request_to_confirm'>
                                        {{ csrf_field() }}
                                        <input type='hidden' name='bid_id' value='{{ $planerBid->id }}'>
                                        <input type='hidden' name='event_id' value='{{ $event_id }}'>
                                        <div class="confirm-request-actions">
                                            <p><span class="glyphicon glyphicon-exclamation-sign"></span></p>
                                            <p>You have been <br/>requested for an event</p>
                                            <label class="text-white form-label-small">Enter Bid Amount</label>
                                            <input class="form-control" id="artist_amount" type="text" value=""
                                                   placeholder="Amount" name="price">
                                            <div class="text-right small">
                                                Evamore Fee: <span class="eva_cut"></span><br/>
                                                Artist Cut: <span class="artist_cut"></span>
                                            </div>
                                            <input type='submit' class='btn btn-primary btn-invert' name="action"
                                                   value='accept' style="pointer-events: all"/>
                                            <input type='button' class='btn btn-danger btn-invert' data-toggle="modal"
                                                   data-target="#decline-event-form" style="pointer-events: all"
                                                   value="decline"/>
                                        </div>
                                        <script language="JavaScript">
                                            (function ($) {
                                                $('#planer_request_to_confirm').on('submit', function () {
                                                    if ($('#artist_amount').val() == '') {
                                                        alert('Please enter bid amount');
                                                        return false;
                                                    }
                                                    return confirm("Are you sure you want to submit this bid for the event?");
                                                });
                                                var artist_value = 0;
                                                var eva_cut = 0;
                                                var artist_cut = 0;
                                                $(".eva_cut").html(" - $" + eva_cut);
                                                $(".artist_cut").html("$" + artist_cut);
                                                //prevent non integer input
                                                $('.input-selector').on('keypress', function (e) {
                                                    if (!(e.metaKey || // cmd/ctrl
                                                        e.which <= 0 || // arrow keys
                                                        e.which == 8 || // delete key
                                                        /[0-9]/.test(String.fromCharCode(e.which)))) {
                                                        e.preventDefault();
                                                    }
                                                })

                                                $("#artist_amount").on('input', function () {
                                                    var artist_value = Math.floor($("#artist_amount").val());
                                                    var eva_cut = Math.floor(artist_value / 10);
                                                    var artist_cut = artist_value - eva_cut;
                                                    $(".eva_cut").html(" - $" + eva_cut);
                                                    $(".artist_cut").html("$" + artist_cut);
                                                });
                                            })(jQuery);
                                        </script>
                                    </form>
                                </div>
                            @endif
                            @if(!$artChanges && $isArtist && !$artistsHasBidsToEvent)
                                @if(!$planerBid)
                                    <div class="form-group event-request-to-play">
                                        <form method="get" id="request_to_play"
                                              action="{{route('event_interested', ['userId'=>$user->id, 'eventId'=>$event->id])}}">
                                            {{ csrf_field() }}
                                            <h4>Request to Play</h4>

                                            <Label class="form-label" for="price">Enter Bid Amount</Label>
                                            <input class="form-control" id="artist_amount" type="text" value=""
                                                   placeholder="Amount" name="price">
                                            <div class="text-right small">
                                                Evamore Fee: <span class="eva_cut"></span><br/>
                                                Artist Cut: <span class="artist_cut"></span>
                                            </div>
                                            <div class="text-right">
                                                <button type="submit" class="form-control btn btn-fill btn-inver">
                                                    REQUEST TO PLAY
                                                </button>
                                                <script language="javascript">
                                                    (function ($) {
                                                        $("#request_to_play").on("submit", function () {
                                                            var amount = Number($('#artist_amount').val());
                                                            console.log(amount)
                                                            if (amount == 0 || isNaN(amount)) {
                                                                alert('Please enter your amount');
                                                                return false;
                                                            }
                                                            return confirm("Are you sure you want to submit this bid for the event?");
                                                        });
                                                        var artist_value = 0;
                                                        var eva_cut = 0;
                                                        var artist_cut = 0;
                                                        $(".eva_cut").html(" - $" + eva_cut);
                                                        $(".artist_cut").html("$" + artist_cut);
                                                        //prevent non integer input
                                                        $('.input-selector').on('keypress', function (e) {
                                                            if (!(e.metaKey || // cmd/ctrl
                                                                e.which <= 0 || // arrow keys
                                                                e.which == 8 || // delete key
                                                                /[0-9]/.test(String.fromCharCode(e.which)))) {
                                                                e.preventDefault();
                                                            }
                                                        })

                                                        $("#artist_amount").on('input', function () {
                                                            var artist_value = Math.floor($("#artist_amount").val());
                                                            var eva_cut = Math.floor(artist_value / 10);
                                                            var artist_cut = artist_value - eva_cut;
                                                            $(".eva_cut").html(" - $" + eva_cut);
                                                            $(".artist_cut").html("$" + artist_cut);
                                                        });
                                                    })(jQuery);
                                                </script>
                                        </form>
                                    </div>
                                @endif
                            @elseif($isArtist && $artistsHasBidsToEvent)
                                <div class="form-group event-request-to-play">
                                    <p class="form-label" for="price">You already made a bid for this event</p>
                                    <hr>
                                    <p class="small">Event bid amount: ${{ $artistsHasBidsToEvent->artist_fee }}</p>
                                </div>
                        @endif

                        @if($isArtistManager)
                            @if(!$planerBid)
                                <!-- <div class="form-group event-request-to-play"> -->
                                    <form method="get" id="request_to_play"
                                          action="{{route('event_interested', ['userId'=>$user->id, 'eventId'=>$event->id])}}">

                                        <h4>Request to Play for</h4>

                                        <select class="form-control" name="artistId" id="artistId">
                                            <?php
                                            $artistCicle = 0;
                                            $bidCicle = 0;
                                            $artistArraySize = count($artistUsers);
                                            $bidArraySize = count($artistsBidsToEvent);
                                            ?>
                                            @foreach($artistUsers as $artistUser)
                                                @if($artistUser->active)
                                                    @if($artistUser->bid)
                                                        <option disabled value="{{$artistUser->id}}"
                                                                name="{{$artistUser->id}}">{{$artistUser->name}} -
                                                            ${{$artistUser->bid}}</option>
                                                    @else
                                                        <option value="{{$artistUser->id}}"
                                                                name="{{$artistUser->id}}">{{$artistUser->name}}</option>
                                                    @endif
                                                @endif
                                            @endforeach
                                        </select>

                                        <Label class="form-label" for="price">Enter Bid Amount</Label>
                                        <input class="form-control" id="artist_amount" type="text" value=""
                                               placeholder="Amount" name="price">
                                        <div class="text-right small">
                                            Evamore Fee: <span class="eva_cut"></span><br/>
                                            Artist Cut: <span class="artist_cut"></span>
                                        </div>
                                        <div class="text-right">
                                            <button type="submit" class="form-control btn btn-fill btn-inver">REQUEST TO
                                                PLAY
                                            </button>
                                            <script language="javascript">
                                                (function ($) {
                                                    $("#request_to_play").on("submit", function () {
                                                        var amount = Number($('#artist_amount').val());
                                                        console.log(amount)
                                                        if (amount == 0 || isNaN(amount)) {
                                                            alert('Please enter your amount');
                                                            return false;
                                                        }
                                                        return confirm("Are you sure you want to submit this bid for the event?");
                                                    });
                                                    var artist_value = 0;
                                                    var eva_cut = 0;
                                                    var artist_cut = 0;
                                                    $(".eva_cut").html(" - $" + eva_cut);
                                                    $(".artist_cut").html("$" + artist_cut);
                                                    //prevent non integer input
                                                    $('.input-selector').on('keypress', function (e) {
                                                        if (!(e.metaKey || // cmd/ctrl
                                                            e.which <= 0 || // arrow keys
                                                            e.which == 8 || // delete key
                                                            /[0-9]/.test(String.fromCharCode(e.which)))) {
                                                            e.preventDefault();
                                                        }
                                                    })

                                                    $("#artist_amount").on('input', function () {
                                                        var artist_value = Math.floor($("#artist_amount").val());
                                                        var eva_cut = Math.floor(artist_value / 10);
                                                        var artist_cut = artist_value - eva_cut;
                                                        $(".eva_cut").html(" - $" + eva_cut);
                                                        $(".artist_cut").html("$" + artist_cut);
                                                    });
                                                })(jQuery);
                                            </script>
                                        </div>
                                    </form>

                        </div>
                        @endif
                        @endif

                    </div><!-- /.col-sm-4 -->
                </div><!-- /.row -->
            </div><!-- /.form-body -->
        </div><!-- /.form form-event -->
    </div><!-- /.content -->

    {{--Contract--}}
    @if($bidContract != '' && $bidContract->count())
        <div class="content">
            <div class="row">
                <div class="form-head">
                    <h2>Contracts</h2>
                </div>
                @foreach($bidContract as $contract)
                    <span id="{{ $contract->id }}>">
                        <i class="fa fa-file-pdf-o"> {{ $contract->name }}</i>
                        <br>
                        <i class="fa fa-download"></i>
                        <a href="{{ route('file_download', ['id' => $contract->id]) }}">Download Contract</a>
                        <i class="{{ $contract->bidContract->downloaded_by_artist ? 'text-success fa fa-eye' : '' }}"></i>
                    </span>
                    <hr>
                @endforeach
            </div>
        </div>
        @endif

        @include("events._event_comments")

        </div><!-- /.main-body -->

        @if(!is_null($planerBid))
            <!-- Modal -->
            <div class="modal fade modal-decline-event-form" id="decline-event-form">
                <div class="modal-dialog" role="document">
                    <button type="button" class="close outer" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <div class="modal-content">
                        <form action='{{route('artists_confirm_planer_request')}}' method='post'
                              id='planer_request_to_confirm'>
                            <div class="modal-header">
                                <h3>Let us know why you can't make it</h3>
                            </div>
                            <div class="modal-body">
                                {{ csrf_field() }}
                                <input type='hidden' name='bid_id' value='{{ $planerBid->id }}'>
                                <input type='hidden' name="action" value='decline'/>
                                <ul>
                                    <li>
                                        <input type="radio" name="declineReason" value="1" id="declineReason-1"/>
                                        <label for="declineReason-1">The budget is too low</label></li>
                                    <li>
                                        <input type="radio" name="declineReason" value="2" id="declineReason-2"/>
                                        <label for="declineReason-2">There is scheduling conflict</label>
                                    </li>
                                    <li>
                                        <input type="radio" name="declineReason" value="3" id="declineReason-3"/>
                                        <label for="declineReason-3">Other</label>
                                        <p>
                                            <input type="text" name="declineReasonText" placeholder="enter-text"
                                                   disabled/>
                                        </p>
                                    </li>
                                </ul>
                            </div>
                            <div class="modal-footer">
                                <input type='submit' class='btn btn-primary btn-invert' value="Submit"/>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <script type="text/javascript">
                (function ($) {
                    $.fn.declineEventReason = function () {
                        var self = this;

                        var $declineReasonText = $("input[name=declineReasonText]", self);
                        var $submitButton = $("input[type=submit]", self);

                        var init = function () {
                            $submitButton.prop('disabled', true);
                            $("input[type=radio]", self).on("change", function () {
                                var reasonId = $(this).val();
                                if (reasonId == 3) {
                                    $declineReasonText.prop('disabled', false);
                                } else {
                                    $declineReasonText.prop('disabled', true);
                                }

                                $submitButton.prop('disabled', false);
                            })
                        };

                        init();
                    };

                    $("#decline-event-form").declineEventReason()

                })(jQuery)
            </script>
        @endif

@stop
