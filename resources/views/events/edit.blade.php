@extends('layouts.dash')
<style>
    .checking_ubtade_info{
        color: #fff;
        display: inline-block;
        padding: 5px 10px;
        border-radius: 3px;
        margin-top:5px;
        margin-left:5px;
    }
    .update_apply{
        background: #ee6467;
    }
    .update_decline{
        background: #51b6ba;
    }
</style>
@section('content')
    <div class="main-body">
        <div class="content">
            <div class="row">
                {{--<div>--}}
                    {{--@if(!$event->changes->isEmpty())--}}
                        {{--@foreach($event->changes as $changes)--}}
                            {{--@foreach($changes->artistAplly as $artist)--}}
                                {{--<div class="checking_ubtade_info {{ $artist->pivot->apply ? "update_decline" : "update_apply" }}">--}}
                                    {{--@if($artist->pivot->apply)--}}
                                        {{--{{ $artist->user->username }} - applied--}}
                                    {{--@else--}}
                                        {{--{{ $artist->user->username }} - ?--}}
                                    {{--@endif--}}
                                {{--</div>--}}
                            {{--@endforeach--}}
                        {{--@endforeach--}}
                    {{--@endif--}}
                {{--</div>--}}

                <div class="form form-alt form-event form-event-edit">
                    @if(in_array($event->status, [App\ModelType\EventType::PENDING, App\ModelType\EventType::CANCELLED]))
                        <div class="row">
                            <div class="col-6 col-md-6">
                                <span class="event-status-{{$event->status}}"> {{ App\ModelType\EventType::getStatusName($event->status) }}</span>
                            </div>
                            @if($isAdmin)
                                <form class="alignright invisible"
                                      action="{{ route('publishEvent', ["event_id"=>$event->id]) }}" method="POST"
                                      id="publish_event" novalidate>
                                    {{ csrf_field() }}
                                    <input type="submit"
                                           class="btn btn-fill btn-invert btn-sm publish-event-btn publish_event_btn"
                                           value="Publish"/>
                                </form>
                            @endif
                        </div>
                    @endif
                    @if($event->status == App\ModelType\EventType::PENDING)
                        <form action="{{ route('saveEvent') }}" method="POST" id="create_event" novalidate>
                            @else

                                <form action="{{ route('updateEvent') }}" method="POST" id="create_event" novalidate>
                                    @endif
                                    <input type="hidden" name="event_id" value="{{$event->id}}"/>
                                    {{ csrf_field() }}
                                    @if($event->bids && $event->bids->status==App\ModelType\BidType::STATUS_CONFIRMED)
                                        <h3 class="alert-paid">PAID</h3>
                                    @endif
                                    <div class="form-head {{ $errors->has('name') ? ' has-error' : '' }}">
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12 col-lg-6 col-md-6 col-xl-6">
                                                <h2><input name='name' placeholder='Edit Event' value="{{$event->name}}"
                                                           autocomplete="off"><i class="ico-pencil"></i></h2>
                                                @if ($errors->has('name'))
                                                    <span class="help-block">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                                                @endif
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-lg-6 col-md-6 col-xl-6">
                                                <div class="form-actions clear-padding">
                                                    <div class="row">
                                                        <div class="col-xs-12 col-sm-12 col-lg-12 col-md-12 col-xl-12 text-right">
                                                            @if(in_array($event->status, [App\ModelType\EventType::PENDING, App\ModelType\EventType::CANCELLED]))
                                                                @if($isAdmin)
                                                                    <button class="btn btn-fill btn-invert btn-sm publish-event-btn"
                                                                            onclick="event.preventDefault();jQuery('#publish_event').submit();">
                                                                        Publish
                                                                    </button>
                                                                @endif
                                                            @endif

                                                            @if($isAdmin && $event->status != App\ModelType\EventType::PUBLISHED)
                                                                <input type="submit" value="UPDATE EVENT" name="submit"
                                                                       class="btn btn-primary btn-invert update-event-btn">
                                                            @endif
                                                            @if($event->status == App\ModelType\EventType::PUBLISHED)
                                                                <input type="submit" value="UPDATE EVENT" name="submit"
                                                                       class="btn btn-primary btn-invert update-event-btn">
                                                            @endif
                                                                @if($isAdmin || $isEventOwner)
                                                                    @if(in_array($event->status, [App\ModelType\EventType::CANCELLED]))
                                                                        <div style="float: right;">
                                                                            <button class="btn btn-danger btn-invert"><a href="{{ route('deleteEvent',['id' => $event->id]) }}">DELETE</a></button>
                                                                        </div>
                                                                    @endif
                                                                @endif
                                                            @if(in_array($event->status, [App\ModelType\EventType::PUBLISHED, App\ModelType\EventType::PENDING]))
                                                                <a href="{{ route('cancelEvent', ['id'=>$event->id]) }}"
                                                                   class="text-muted cancel-icon"
                                                                   onclick="return confirm('Are you sure you want to cancel this event?');">
                                                                    <i class="glyphicon glyphicon-trash"></i>
                                                                </a>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div><!-- /.form-actions -->
                                            </div>
                                        </div>
                                    </div><!-- /.form-head -->

                                    @if(Session::has('message'))
                                        <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
                                    @endif

                                    @include('artists._artists_requested_event')

                                    <div class="form-body">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <?php
                                                $tz = \Carbon\ Carbon::parse()->tz($event->time_zone);
                                                $eventTime = \Carbon\Carbon::parse($event->starttime);
                                                //  $eventTime->setTimeZone($tz);
                                                ?>

                                                <div class="form-group {{ $errors->has('date') ? ' has-error' : '' }}">
                                                    <label class="form-label">DATE</label>

                                                    <div class="datepicker" id="datepicker-inline"
                                                         data-year="{{ $eventTime->format("Y") }}"
                                                         data-month="{{ $eventTime->format("n") }}"
                                                         data-day="{{ $eventTime->format("j") }}">
                                                        <input type='hidden' id='date' name='date'/>
                                                    </div>
                                                </div><!-- /.form-group -->
                                                @if ($errors->has('date'))
                                                    <span class="help-block">
		                                <strong>{{ $errors->first('date') }}</strong>
		                            </span>
                                                @endif
                                            </div><!-- /.col-sm-4 -->

                                            <div class="col-md-4">
                                                <div class="form-group {{ ($errors->has('hours') || $errors->has('minutes')) ? ' has-error' : '' }}">
                                                    <div class="event-start-time">
                                                        <label for="starttime" class="form-label">TIME:</label>
                                                        <select name="time_zone" class="selectpicker">
                                                            @foreach(App\ModelType\TimeZoneType::$zonesTitles as $id=>$name)
                                                                <option value="{{$id}}"
                                                                        @if ($event->time_zone == $id) selected="selected" @endif>{{$name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>

                                                    <div class="form-controls clear">
                                                        <input type="number" class="form-control form-control-one-third"
                                                               id="hours" name="hours" placeholder="12" required
                                                               value="{{ $eventTime->format("g") }}"/>

                                                        <span class="form-separator">:</span>

                                                        <input type="number" class="form-control form-control-one-third"
                                                               id="minutes" name="minutes" placeholder="00" required
                                                               value="{{ $eventTime->format("i") }}"/>

                                                        <select class="form-control form-control-one-third"
                                                                id="time_period"
                                                                name="period" placeholder="PM" required>
                                                            <option value="AM"
                                                                    @if ($eventTime->format("A") == 'AM') selected=="selected" @endif>
                                                                AM
                                                            </option>
                                                            <option value="PM"
                                                                    @if ($eventTime->format("A") == 'PM') selected=="selected" @endif>
                                                                PM
                                                            </option>
                                                        </select>
                                                    </div><!-- /.form-controls -->
                                                    @if ($errors->has('hours'))
                                                        <span class="help-block">
			                                <strong>{{ $errors->first('hours') }}</strong>
			                            </span>
                                                    @endif

                                                    @if ($errors->has('minutes'))
                                                        <span class="help-block">
			                                <strong>{{ $errors->first('minutes') }}</strong>
			                            </span>
                                                    @endif
                                                </div><!-- /.form-group -->
                                                @if($event->venue)
                                                    <div class="form-group {{ $errors->first('venue') ? ' has-error' : '' }}">
                                                        <label for="venue" class="form-label">Venue</label>

                                                        <div class="form-controls">
                                                            <input type="text" class="form-control" id="venue"
                                                                   name="venue"
                                                                   value="{{ $event->venue }}"
                                                                   placeholder="Anywhere but your bathroom"
                                                                   required>
                                                        </div><!-- /.form-controls -->
                                                        @if ($errors->has('venue'))
                                                            <span class="help-block">
			                                <strong>{{ $errors->first('venue') }}</strong>
			                            </span>
                                                        @endif
                                                    </div>
                                                @endif
                                                <div class="form-group {{ $errors->first('address') ? ' has-error' : '' }}">
                                                    <label for="address" class="form-label">Address</label>

                                                    <div class="form-controls">
                                                        <input type="text" class="form-control" id="add" name="add"
                                                               value="{{ $event->address }}"
                                                               placeholder="Anywhere but your bathroom"
                                                        >
                                                        <input type="hidden" name="place_lat"
                                                               value="{{ $event->place_lat }}" id="place-lat">
                                                        <input type="hidden" name="place_lng"
                                                               value="{{ $event->place_lng }}" id="place-lng">
                                                        <input type="hidden" value="{{ $event->state }}" name="state"
                                                               id="administrative_area_level_1">
                                                        <input type="hidden" value="{{ $event->city }}" name="city"
                                                               id="locality">
                                                        <input type="hidden" value="{{ $event->address }}"
                                                               name="address"
                                                               id="route">
                                                        <input type="hidden" name="add_res"
                                                               value="{{ $event->address  }}">
                                                        <input type="hidden" id="street_number">
                                                        <input type="hidden" id="postal_code">
                                                        <input type="hidden" id="country">
                                                    </div><!-- /.form-controls -->
                                                    @if ($errors->has('address'))
                                                        <span class="help-block">
			                                <strong>{{ $errors->first('address') }}</strong>
			                            </span>
                                                    @endif
                                                </div>
                                                @if($event->city || $event->state)
                                                    <div class="form-group {{ $errors->first('city','state') ? ' has-error' : '' }}">
                                                        <label for="city-state" class="form-label">City/State</label>

                                                        <div class="form-controls">
                                                            <input type="text" class="form-control" id="city-state"
                                                                   name="city-state"
                                                                   value="{{ $event->city }} / {{ $event->state  }}"
                                                                   placeholder="Anywhere but your bathroom"
                                                                   required>
                                                        </div><!-- /.form-controls -->
                                                        @if ($errors->has('city'))
                                                            <span class="help-block">
			                                <strong>{{ $errors->first('city') }}</strong>
			                            </span>
                                                        @endif
                                                        @if ($errors->has('state'))
                                                            <span class="help-block">
			                                <strong>{{ $errors->first('state') }}</strong>
			                            </span>
                                                        @endif
                                                    </div><!-- /.form-group -->
                                                @endif

                                                {{--<div class="form-group {{ $errors->first('city') ||  $errors->first('state') ? ' has-error' : '' }}">--}}
                                                {{--<div class="row">--}}
                                                {{--<div class="col-md-7">--}}
                                                {{--<label for="city" class="form-label">City</label>--}}
                                                {{--<div class="form-controls">--}}
                                                {{--<input type="text" class="form-control" id="city"--}}
                                                {{--name="city"--}}
                                                {{--value="{{ $event->city }}" placeholder="City"--}}
                                                {{--required>--}}
                                                {{--</div><!-- /.form-controls -->--}}
                                                {{--</div>--}}
                                                {{--<div class="col-md-5">--}}
                                                {{--<label for="state" class="form-label">State</label>--}}
                                                {{--<div class="form-controls">--}}
                                                {{--<span class="form-separator"></span>--}}
                                                {{--<input type="text" class="form-control" id="state"--}}
                                                {{--name="state"--}}
                                                {{--value="{{ $event->state }}" placeholder="State"--}}
                                                {{--required>--}}
                                                {{--</div><!-- /.form-controls -->--}}

                                                {{--</div>--}}
                                                {{--</div>--}}
                                                {{--@if ($errors->has('city'))--}}
                                                {{--<span class="help-block">--}}
                                                {{--<strong>{{ $errors->first('city') }}</strong>--}}
                                                {{--</span>--}}
                                                {{--@endif--}}
                                                {{--@if ($errors->has('state'))--}}
                                                {{--<span class="help-block">--}}
                                                {{--<strong>{{ $errors->first('state') }}</strong>--}}
                                                {{--</span>--}}
                                                {{--@endif--}}
                                                {{--</div><!-- /.form-group -->--}}

                                                <div class="form-group {{ $errors->first('duration') ||  $errors->first('min_budget') || $errors->first('max_budget')? ' has-error' : '' }}">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <label for="duration" class="form-label"
                                                                   required>Duration</label>
                                                            <div class="form-controls ">
                                                                <input type="text" class="form-control" id="duration"
                                                                       name="duration"
                                                                       placeholder="Time in Minutes" required
                                                                       value="{{ $event->duration }}">
                                                            </div><!-- /.form-controls -->
                                                        </div>
                                                        <div class="col-md-8">
                                                            <label for="min_budget" class="form-label">BUDGET</label>

                                                            <div class="form-controls">
                                                                <input type="number"
                                                                       class="form-control form-control-half form-control-half-smaller"
                                                                       id="min_budget"
                                                                       value="{{ $event->min_budget }}"
                                                                       name="min_budget"
                                                                       placeholder="Min"
                                                                       required>

                                                                <span class="form-separator">to</span>

                                                                <input type="number"
                                                                       class="form-control form-control-half form-control-half-smaller"
                                                                       id="max_budget"
                                                                       value="{{ $event->max_budget }}"
                                                                       name="max_budget"
                                                                       placeholder="Max"
                                                                       required>
                                                            </div>
                                                        </div>
                                                        @if ($errors->has('duration'))
                                                            <span class="help-block">
                                                    <strong>{{ $errors->first('duration') }}</strong>
                                                </span>
                                                        @endif
                                                        @if ($errors->has('min_budget'))
                                                            <span class="help-block">
                                                    <strong>{{ $errors->first('min_budget') }}</strong>
                                                </span>
                                                        @endif
                                                        @if ($errors->has('max_budget'))
                                                            <span class="help-block">
                                                    <strong>{{ $errors->first('max_budget') }}</strong>
                                                </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="" class="form-label">Has sound production been arranged
                                                        for this
                                                        event?</label>

                                                    <div class="form-check">
                                                        <input class="form-check-input" value="1" name="radio"
                                                               type="radio"
                                                               id="radio120">
                                                        <label class="form-check-label" for="radio120">Yes</label>
                                                    </div>
                                                    <div class="form-check">
                                                        <input class="form-check-input" value="0" name="radio"
                                                               type="radio"
                                                               id="radio121" checked>
                                                        <label class="form-check-label" for="radio121">No</label>
                                                    </div>
                                                    @if ($errors->has('radio'))
                                                        <span class="help-block">
			                                <strong>{{ $errors->first('radio') }}</strong>
			                            </span>
                                                    @endif

                                                    <div class="popup" onclick="myFunction()">What's this?
                                                        <span class="popuptext" id="myPopup">a PA or speaker system big enough for the act and venue.  Check with the venue manager if you are not sure.</span>
                                                    </div>
                                                </div><!-- /.form-group -->
                                            </div><!-- /.col-sm-4 -->

                                            <div class="col-md-4">
                                                <div class="form-group {{ $errors->has('event_type') ? ' has-error' : '' }}">
                                                    <label class="form-label">WHAT KIND OF EVENT IS THIS?</label>

                                                    <div class="form-controls">
                                                        @php
                                                            $selectedEventType = $event->event_type;
                                                        @endphp
                                                        @if (old('event_type'))
                                                            <?php $selectedEventType = old('event_type')?>
                                                        @endif
                                                        <ul class="list-tags">
                                                            @foreach ($eventTypes as $type)
                                                                <li>
                                                                    <input type="radio" name="event_type"
                                                                           value="{{$type->id}}"
                                                                           id="type_{{$type->id}}"
                                                                           @if ($type->id == $selectedEventType)
                                                                           checked="checked"
                                                                            @endif
                                                                    />
                                                                    <label for="type_{{$type->id}}">{{$type->name}}</label>
                                                                </li>
                                                            @endforeach
                                                        </ul><!-- /.list-tags -->
                                                    </div><!-- /.form-controls -->
                                                    @if ($errors->has('genre'))
                                                        <span class="help-block">
			                                <strong>{{ $errors->first('genre') }}</strong>
			                            </span>
                                                    @endif
                                                </div><!-- /.form-group -->
                                            </div>
                                            {{--Song Type--}}
                                            <div class="col-md-4">
                                                <div class="form-group {{ $errors->has('songType') ? ' has-error' : '' }}">
                                                    <label class="form-label">Song Type's</label>

                                                    <div class="form-controls">
                                                        @php
                                                            $eventSongTypes = [];
                                                        @endphp
                                                        @foreach($event->songTypes as $g)
                                                            @php
                                                                $eventSongTypes[] = $g->id;
                                                            @endphp
                                                        @endforeach

                                                        <ul class="list-tags">
                                                            @foreach ($songTypes as $songType)
                                                                <li>
                                                                    <input class="songTypes" type="checkbox"
                                                                           name="songType[]"
                                                                           value="{{$songType->id}}"
                                                                           id="song_type_{{$songType->id}}"
                                                                           @if (in_array($songType->id, $eventSongTypes))
                                                                           checked="checked"
                                                                            @endif
                                                                    />
                                                                    <label for="song_type_{{$songType->id}}">{{$songType->name}}</label>
                                                                </li>
                                                            @endforeach
                                                        </ul><!-- /.list-tags -->
                                                    </div><!-- /.form-controls -->
                                                    @if ($errors->has('songType'))
                                                        <span class="help-block">
			                                <strong>{{ $errors->first('songType') }}</strong>
			                            </span>
                                                    @endif
                                                </div><!-- /.form-group -->
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group {{ $errors->has('genre') ? ' has-error' : '' }}">
                                                    <label class="form-label">GENRE<span>(S)</span></label>

                                                    <div class="form-controls">
                                                        @php
                                                            $eventGenres = [];
                                                        @endphp
                                                        @foreach($event->genres as $g)
                                                            @php
                                                                $eventGenres[] = $g->id;
                                                            @endphp
                                                        @endforeach

                                                        <ul class="list-tags">
                                                            @foreach ($genres as $genre)
                                                                <li>
                                                                    <input class="genres" type="checkbox" name="genre[]"
                                                                           value="{{$genre->id}}"
                                                                           id="{{$genre->id}}"
                                                                           @if (in_array($genre->id, $eventGenres))
                                                                           checked="checked"
                                                                            @endif
                                                                    />
                                                                    <label for="{{$genre->id}}">{{$genre->name}}</label>
                                                                </li>
                                                            @endforeach
                                                        </ul><!-- /.list-tags -->
                                                    </div><!-- /.form-controls -->
                                                    @if ($errors->has('genre'))
                                                        <span class="help-block">
			                                <strong>{{ $errors->first('genre') }}</strong>
			                            </span>
                                                    @endif
                                                </div><!-- /.form-group -->

                                                @if (count($planerBidsToEvent)>0)
                                                    <label class="form-label">REQUESTED ARTISTS
                                                        ({{count($planerBidsToEvent)}}
                                                        )</label>

                                                    <div id="requestedArtistsCarousel"
                                                         class="requestedArtistsCarousel carousel slide"
                                                         data-interval="false">
                                                        <!-- Wrapper for slides -->
                                                        <div class="carousel-inner" role="listbox">
                                                            @php
                                                                $i = 0;
                                                            @endphp
                                                            @foreach($planerBidsToEvent as $bid)
                                                                <?php $artistUser = $bid->artistUser ?>
                                                                <div class="booked-artist-details item {{ ($i==0) ? 'active' : '' }}">
                                                                    <div class="details-1"
                                                                         data-id="{{$artistUser->id}}">
                                                                        <div class="avatar">
                                                                            <img src="<?=Image::url('uploads/avatars/' . $artistUser->avatar, 95, 95, array('crop'))?>"
                                                                                 alt="" width="95" height="95">
                                                                            <span class="artist-price">{{ App\Artist::getFeeType($artistUser->artist->fee) }}</span>
                                                                        </div>
                                                                        <a href="{{ route('artist_show', ['username' => $artistUser->username]) }}"
                                                                           class="name"
                                                                           alt="{{ $artistUser->name }}">{{ $artistUser->name }}</a>

                                                                    </div>

                                                                    @if($isEventOwner || $isAdmin)

                                                                        <?php $order = $bid->getOrder(); ?>
                                                                        <p style="height: 29px">
                                                                            @if ($bid->price > 0)
                                                                                <?php $order = $bid->getOrder(); ?>
                                                                                @if(!$order || $order->status != \App\ModelType\OrderType::FULL_PAID)
                                                                                    <a class="offer-button"
                                                                                       data-bid-id="{{$bid->id}}">VIEW
                                                                                        OFFER</a>
                                                                                @endif

                                                                                @if($order)
                                                                                    @if($order->status == \App\ModelType\OrderType::HALF_PAID)
                                                                                        <span style="color:#FF0000">50% paid</span>
                                                                                    @elseif($order->status == \App\ModelType\OrderType::FULL_PAID)
                                                                                        <span style="color:#FF0000">100% paid</span>
                                                                                    @endif
                                                                                @endif
                                                                            @endif
                                                                        </p>

                                                                    @endif
                                                                </div>
                                                                @php
                                                                    $i++;
                                                                @endphp
                                                            @endforeach
                                                        </div>
                                                    @if (count($planerBidsToEvent) > 1)
                                                        <!-- Left and right controls -->
                                                            <a class="left carousel-control"
                                                               href="#requestedArtistsCarousel"
                                                               role="button" data-slide="prev">
                                                        <span class="glyphicon glyphicon-triangle-left"
                                                              aria-hidden="true"></span>
                                                                <span class="sr-only">Previous</span>
                                                            </a>
                                                            <a class="right carousel-control"
                                                               href="#requestedArtistsCarousel"
                                                               role="button" data-slide="next">
                                                        <span class="glyphicon glyphicon-triangle-right"
                                                              aria-hidden="true"></span>
                                                                <span class="sr-only">Next</span>
                                                            </a>
                                                        @endif
                                                    </div>
                                                @endif

                                            </div><!-- /.col-sm-4 -->
                                        </div><!-- /.row -->
                                    </div><!-- /.form-body -->
                                </form>
                </div><!-- /.form form-event -->
            </div><!-- /.row -->
        </div><!-- /.content -->
        @if($event->bids)
            @include('events.event_artists_changes_modal')
            <script>
                $(window).ready(function () {
                    $('.update-event-btn').on('click', function (e) {
                        // e.preventDefault()
                        // $('#exampleModal').modal('show')
                        // swal({
                        //     title: 'Payment processed successfully.',
                        //     icon: "success",
                        // })
                        // $('#apply').on('click', function () {
                        //     var values = {};
                        //     var artists = []
                        //     var songTypes = []
                        //     var genres = []
                        //     $.each($('#create_event').serializeArray(), function (i, field) {
                        //         values[field.name] = field.value;
                        //     });
                        //     delete values['genre[]']
                        //     delete values['songType[]']
                        //     $('.songTypes:checked').each(function (i, k) {
                        //         songTypes.push($(this).val())
                        //     });
                        //     $('.genres:checked').each(function (i, k) {
                        //         genres.push($(this).val())
                        //     });
                        //     $('.artBids:checked').each(function (i, k) {
                        //         artists.push($(this).val())
                        //     });
                        //     values['song_type[]'] = songTypes
                        //     values['genre[]'] = genres
                        //     $.each(artists, function (index, value) {
                        //         values.artists = value
                        //         $.ajax({
                        //             url: '/events/' + $("input[name='event_id']").val() + '/change',
                        //             type: 'post',
                        //             data: values,
                        //             success: function (response) {
                        //                 $('#exampleModal').modal('hide')
                        //                 toastr.success('Success')
                        //             }
                        //         })
                        //     });
                        //
                        //
                        // })
                    })


                })

            </script>
        @endif

        <script type="text/javascript"
                src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDuCCI1t1qsTUO7xA57m7tY0ujVGgcOPjM&libraries=places,geometry"></script>
        <script>


            //Google API
            var address = document.getElementById('add');
            var addressOprions = {
                types: ['address'],
                componentRestrictions: {country: 'us'}
            };
            var componentForm = {
                street_number: 'short_name',
                route: 'long_name',
                locality: 'long_name',
                administrative_area_level_1: 'short_name',
                country: 'long_name',
                postal_code: 'short_name'
            };

            var autocomplete = new google.maps.places.Autocomplete(address, addressOprions);

            google.maps.event.addListener(autocomplete, 'place_changed', function () {
                var place = autocomplete.getPlace();
                console.log(place);
                for (var component in componentForm) {
                    document.getElementById(component).value = '';
                    document.getElementById(component).disabled = false;
                }

                for (var i = 0; i < place.address_components.length; i++) {
                    var addressType = place.address_components[i].types[0];
                    if (componentForm[addressType]) {
                        var val = place.address_components[i][componentForm[addressType]];
                        document.getElementById(addressType).value = val;
                    }

                }


                var lat = place.geometry.location.lat();
                var lng = place.geometry.location.lng();
                $('#place-lat').val(lat);
                $('#place-lng').val(lng);
            })
            var latitude1 = 40.2043557;
            var longitude1 = 44.522919;
            var latitude2 = 40.1411879;
            var longitude2 = 44.533553;

            var distance = google.maps.geometry.spherical.computeDistanceBetween(new google.maps.LatLng(latitude1, longitude1), new google.maps.LatLng(latitude2, longitude2));
            console.log(distance)
        </script>

        {{--Contract--}}
        @if($bidContract != '' && $bidContract->count())
            <div class="content">
                <div class="row">
                    <div class="form-head">
                        <h2>Contracts</h2>
                    </div>
                    @foreach($bidContract as $contract)
                        <span id="{{ $contract->id }}>">
                            <i class="fa fa-file-pdf-o"> {{ $contract->name }}</i>
                            <br>
                            <i class="fa fa-download"></i>
                            <a href="{{ route('file_download', ['id' => $contract->id]) }}">Download Contract</a>
                            <i class="{{ $contract->bidContract->downloaded_by_artist ? 'text-success fa fa-eye' : '' }}"></i>
                        </span>
                        <hr>
                    @endforeach
                </div>
            </div>
        @endif

        @include("events._event_comments")

    </div><!-- /.main-body -->

    @if($isEventOwner || $isAdmin)
        @include("artists._artist_offer_confirm_modal")
        @include('layouts._modal_payment_performance_agreement')
    @endif

@section('js')
    @parent
    <script src="{{asset("/js/youtube.min.js")}}"></script>
    <script>
        function myFunction() {
            var popup = document.getElementById("myPopup");
            popup.classList.toggle("show");
        }

    </script>
@stop

@stop
