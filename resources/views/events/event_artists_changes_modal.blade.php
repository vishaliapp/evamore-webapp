<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Select artist's for apply changes</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                @foreach($artistsBidsToEvent as $index => $bid)
                    <input class="artBids" id="art{{$index}}" type="checkbox" value="{{ $bid->artists->first()->id }}">
                    <label for="art{{$index}}">{{ $bid->artists->first()->user->username }}</label>
                @endforeach
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" id="apply" class="btn btn-primary">Apply</button>
            </div>
        </div>
    </div>
</div>