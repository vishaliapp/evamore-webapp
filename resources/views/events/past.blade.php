@extends('layouts.dash')

@section('js')
	<script src="{{asset(file_exists('/js/pastEvents.min.js') ? '/js/pastEvents.min.js' : '/js/pastEvents.js')}}"></script>
@stop

<!-- Give past events link active class-->
@section('past')
	class='active'
@stop


@section('past-count')
	<span class="sidebar-nav-count">0</span>
@stop

@section('content')
	<div class="main-body">
		<div class="container">
			<div class="filter">
				<div class="filter-head">
					<p>SORT BY:</p>

					<p class="filter-options">
						<a href="#">Location</a>
						
						<a href="#">Budget</a>
						
						<a href="#">Date</a>
					</p><!-- /.filter-options -->
				</div><!-- /.filter-head -->

				<div class="filter-body">
					<p id='error_msg'></p>
					<ul id="eventData" class="filter-items event-boxes">
						<!--Dynamically Fill With Event Data -->
					</ul><!-- /.filter-items -->
				</div><!-- /.filter-body -->
			</div><!-- /.filter -->
		</div><!-- /.container -->
	</div><!-- /.main-body -->
@stop