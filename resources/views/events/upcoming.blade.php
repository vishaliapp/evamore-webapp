@extends('layouts.dash', ['left_menu_selected'=>$left_menu_selected])

@section('js')
    <script src="{{asset(file_exists('/js/events.min.js') ? '/js/events.js' : '/js/events.js')}}"></script>
@stop

@section('upcoming')
    class='active'
@stop


@section('upcoming-count')
    <span class="sidebar-nav-count">0</span>
@stop

@section('content')
    <div class="main-body">
        <div class="container">
            @if(Session::has('message'))
                <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
            @endif


            <div class="filter">
                <div class="filter-head">
                    @include('events._form_events_sort_filter')
                    @if ($isArtist)
                        @if(count($events))
                            <h6 class="text-muted text-center">
                                We have matched you with these events created by EVAmore event planners
                                <br>
                                Request to play if you're available and interested
                            </h6>
                        @else
                            <h6 class="text-muted text-center">
                                There is no events yet
                            </h6>
                        @endif
                    @endif
                </div><!-- /.filter-head -->

                <div class="filter-body">
                    <p id='error_msg'></p>
                    <ul id="eventData" class="filter-items event-boxes">
                    @if(!$isArtist && !$isArtistManager)
                        <!--Dynamically Fill With Event Data -->
                            <li class='event-box active' style='width:24%'>
                                <a href='/events/create' class='event-box-add'>Add an event <span><i
                                                class='ico-plus'></i></span>
                                </a>
                            </li><!-- event-box -->
                        @endif
                        @if($isFan)
                            @foreach ($myEvents as $myEvent)
                                <?php
                                $tz =  \Carbon\ Carbon::parse()->tz($myEvent->time_zone);
                                $eventTime = \Carbon\Carbon::parse($myEvent->starttime);
//                                $eventTime->setTimezone($tz->tz);
                                $genres = $myEvent->genres;
                                ?>
                                <li class='filter-item event-box event-box-fill'>
                                    <h4 class='event-box-title'>{{$myEvent->name }}</h4><!-- event-box-title -->
                                    <p class='event-box-meta'>{{ $eventTime->format("m/d/y") }}
                                        – {{$myEvent->location}}</p><!-- event-box-meta -->
                                    <ul class='list-event-details'>
                                        <li>
                                            @if ($isAdmin)
                                                <p>User</p>
                                                <h6>{{$myEvent->user()->first()->username}}</h6>
                                            @endif
                                            @if(in_array($myEvent->status, [App\ModelType\EventType::PUBLISHED,App\ModelType\EventType::PENDING, App\ModelType\EventType::CANCELLED]))
                                                <span class="event-status-{{$myEvent->status}}"> - {{ App\ModelType\EventType::getStatusName($myEvent->status) }}</span>
                                            @endif
                                            <p>Open to</p>
                                            <h6>
                                                @foreach ($genres as $genre)
                                                    {{$genre->name}}
                                                @endforeach
                                                Bands
                                            </h6>
                                            <h6 class='event-details-price'>${{$myEvent->min_budget}} –
                                                ${{$myEvent->max_budget}}</h6><!-- event-details-price -->
                                        </li>
                                    </ul><!-- list-event-details -->
                                    <div class='actions'>
                                        <a href="/events/{{$myEvent->id}}/display"
                                           class='btn btn-fill btn-invert'>View</a>
                                    </div><!-- event-box-actions -->
                                </li><!-- filter-item -->
                            @endforeach
                        @else
                            @foreach ($events as $event)
                                <?php
                                $tz =  \Carbon\ Carbon::parse()->tz($event->time_zone);
                                $eventTime = \Carbon\Carbon::parse($event->starttime);
                                $eventTime->setTimezone($tz->tz);
                                $genres = $event->genres;
                                ?>
                                <li class='filter-item event-box event-box-fill'
                                    @if($isArtist)
                                        @foreach($eventSeen as $item)
                                            @if(!$item->bids)
                                                @foreach($item->artistSee as $value)
                                                    @if($value->pivot->event_id == $event->id && $value->pivot->artist_id == $artUser->id)
                                                        style="opacity: 0.5;"
                                                    @endif
                                                @endforeach
                                            @elseif($item->bids)
                                                @if($item->bids->event_id == $event->id && $item->bids->artist_user_id == $artUser->id)
                                                    style="opacity: 1;"
                                                @endif
                                            @endif
                                        @endforeach
                                    @endif
                                    @If($isArtistManager)
                                        @foreach($eventSeen as $item)
                                                @if(!$item->bids)
                                                    @foreach($item->artistSee as $value)
                                                        @foreach($managerArtists as $manArt)
                                                            @if($value->pivot->event_id == $event->id && $value->pivot->artist_id == $manArt->id)
                                                                style="opacity: 0.5;"
                                                            @endif
                                                        @endforeach
                                                    @endforeach
                                                @endif
                                                @if($item->bids)
                                                    @foreach($managerArtists as $manArt)
                                                        @if($item->bids->event_id == $event->id && $item->bids->artist_user_id == $manArt->id)
                                                            style="opacity: 1;"
                                                        @endif
                                                    @endforeach
                                                @endif
                                        @endforeach
                                    @endif

                                    id="event-{{$event->id}}">
                                    <div class="event-box-content">
                                        <h4 class='event-box-title'>{{$event->name }}</h4><!-- event-box-title -->
                                        <p class='event-box-meta'>{{ $eventTime->format("m/d/y") }}
                                            – {{$event->location}}</p><!-- event-box-meta -->
                                        <ul class='list-event-details'>
                                            <li>
                                                <p>User</p>
                                                <h6>{{$event->user()->first()->username}}
                                                    @if(in_array($event->status, [App\ModelType\EventType::PUBLISHED,App\ModelType\EventType::PENDING, App\ModelType\EventType::CANCELLED]))
                                                        <span class="event-status-{{$event->status}}"> - {{ App\ModelType\EventType::getStatusName($event->status) }}</span>
                                                    @endif

                                                </h6>

                                                <p>Open to</p>
                                                <h6>
                                                    @foreach ($genres as $genre)
                                                        {{$genre->name}}
                                                    @endforeach
                                                    Bands
                                                </h6>
                                                <h6 class='event-details-price'>${{$event->min_budget}} –
                                                    ${{$event->max_budget}}</h6><!-- event-details-price -->
                                            </li>
                                        </ul><!-- list-event-details -->
                                        <div class='actions'>
                                            <a href="/events/{{$event->id}}/display" class='btn btn-fill btn-invert'>View</a>
                                        </div><!-- event-box-actions -->
                                        @if($event->bids && $event->bids->status==App\ModelType\BidType::STATUS_CONFIRMED)
                                            <h3 style="float: right;margin-top: -40px;font-size: 16px;" class="alert-paid">PAID</h3>
                                        @endif
                                    </div>
                                </li><!-- filter-item -->
                            @endforeach
                        @endif
                    </ul><!-- /.filter-items -->
                </div><!-- /.filter-body -->

                <p>
                    <?php echo $events->appends($sorts)->render(); ?>
                </p>

            </div><!-- /.filter -->
        </div><!-- /.container -->
    </div><!-- /.main-body -->
@stop
