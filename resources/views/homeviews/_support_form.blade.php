<div class="tabs">
    <div class="row">
        <div class="col-sm-4">
            <div class="tabs-head">
                <h6>IS YOUR QUESTION COVERED HERE?</h6>

                <nav class="tabs-nav">
                    <ul>
                        <li>
                            <a href="#tab1" class="active">May I contact EVAmore directly?</a>
                        </li>

                        <li>
                            <a href="#tab2">Do I really need to pay and communicate directly through EVAmore?</a>
                        </li>

                        <li>
                            <a href="#tab3">How does this whole payment thing work?</a>
                        </li>

                        <li>
                            <a href="#tab5">What if I don't find exactly the music I'm looking for?</a>
                        </li>

                        <li>
                            <a data-is-conditions="1" href="#tab7">What about the terms and conditions?</a>
                        </li>

                        <li>
                            <a href="#tab8">What if the artist doesn't show up to my event, or something goes wrong?</a>
                        </li>

                        <li>
                            <a href="#tab9">What is the correct way to pronounce &ldquo;EVAmore?&rdquo;</a>
                        </li>
                    </ul>
                </nav><!-- /.tabs-nav -->
            </div><!-- /.tabs-head -->
        </div><!-- /.col-sm-4 -->

        <div class="col-sm-8">
            <div class="tabs-body">
                <div class="tab active" id="tab1">

                    @if(Session::has('message'))
                        {{--<p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>--}}
                        <h2>{{ Session::get('message') }}</h2>
                        <p><a style="color:#51b7ba;" href="{{ route('support') }}">Send another message?</a></p>
                    @else
                        <h2>May I contact EVAmore directly?</h2>

                        <p>Definitely. Just enter your question in the message field below.</p>

                        <form action="" method="post">
                            {{csrf_field()}}
                            <div class="form-group {{ $errors->has('message') ? ' has-error' : '' }}">
                                <label for="message" class="form-label">MESSAGE</label>
                                <textarea class="form-control" id="message" name="message" placeholder="What can we help you with?">{{old('message')}}</textarea>

                                @if ($errors->has('message'))
                                    <span class="help-block">
                                                <strong>{{ $errors->first('message') }}</strong>
                                            </span>
                                @endif
                            </div>


                            <div class="form-group {{ $errors->has('first_name') ? ' has-error' : '' }}">
                                <label for="first_name" class="form-label">NAME</label>
                                <input type="text" class="form-control" id="first_name" name="first_name" placeholder="What's your first name?" value="{{old('first_name')}}">
                                @if ($errors->has('first_name'))
                                    <span class="help-block">
                                                <strong>{{ $errors->first('first_name') }}</strong>
                                            </span>
                                @endif
                            </div>

                            <div class="form-group {{ $errors->has('last_name') ? ' has-error' : '' }}">
                                <label for="last_name" class="form-label">NAME</label>
                                <input type="text" class="form-control" id="last_name" name="last_name" placeholder="What's your last name?" value="{{old('last_name')}}">
                                @if ($errors->has('last_name'))
                                    <span class="help-block">
                                                <strong>{{ $errors->first('last_name') }}</strong>
                                            </span>
                                @endif
                            </div>

                            <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email" class="form-label">EMAIL</label>
                                <input type="text" class="form-control" id="email" name="email" placeholder="What's your email?" value="{{old('email')}}">
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                @endif
                            </div>

                            <input type="submit" name="submit" value="Submit" class="btn btn-fill form-btn">

                        </form>
                    @endif

                </div><!-- /.tab -->

                <div class="tab" id="tab2">
                    <h2>Do I really need to pay and communicate directly through EVAmore?</h2>

                    <p>Communicating and paying through EVAmore helps ensure that you're protected under our terms of service, cancellation and refund policies. Plus it helps us provide other important safeguards designed to make life happier and more fun. Handling everything on our site also makes it super easy for you to find and reference important booking details and other useful information. We can't provide you these benefits if your event isn't booked and paid for directly through EVAmore.</p>
                </div><!-- /.tab -->

                <div class="tab" id="tab3">
                    <h2>How does this whole payment thing work?</h2>

                    <p>We've made this as simple as humanly possible. As soon as an artist confirms your event, we'll need you to make a 50% down payment using a major credit card. The remaining 50% is due the week of your event. All payments are made through EVAmore.  We do this so you don't have to think about money while your big happening is actually happening. No money changes hands while the fun's happening &mdash; we take  care of that for you once the show's over and we know everyone's happy.</p>
                </div><!-- /.tab -->

                <div class="tab" id="tab5">
                    <h2>What if I don't find exactly the music I'm looking for?</h2>

                    <p>Hey, it does actually happen (though not often). But even then, we can still help. We got our start in business as event planners and consultants, and we are pretty OCD when it comes to problem solving. For personal assistance with your search, just email us. From there, we can shift the conversation to phone, Google Hangouts or whatever new social platform happens to be blowing up next week.</p>
                </div><!-- /.tab -->

                <div class="tab" id="tab6">
                    <h2>So what's in the contract?</h2>

                    <p>Short version: Don't do anything you wouldn't want your mother to find out about. Enjoy the official fully lawyerized version here.</p>
                </div><!-- /.tab -->

                <div class="tab" id="tab7">
                    <h2>What about the terms and conditions?</h2>

                    <p>Also quite simple: Respect your EVAmore artists. Do not exploit them. Do not throw live things or dead things at the stage. Enjoy the official fully lawyerized version <a id="conditions-link" href="#" data-toggle="modal" data-target="#popup-terms">here</a>.</p>
                </div><!-- /.tab -->

                <div class="tab" id="tab8">
                    <h2>What if the artist doesn't show up to my event, or something goes wrong?</h2>

                    <p>We work very, very hard to make sure this never happens. Still, everyone should have a Plan B handy. Here's ours: if an EVAmore artist you've confirmed has to cancel a performance at your event, we will issue you a full refund. We will also work with you personally to find a great substitute artist. Even if the unthinkable happens, our goal will be to handle the situation in a way that makes you feel even better about EVAmore than you already did.</p>
                </div><!-- /.tab -->

                <div class="tab" id="tab9">
                    <h2>What is the correct way to pronounce &ldquo;EVAmore?&rdquo;</h2>

                    <p>We say EVE-a-more, using the very same long E sound featured in fine words in like sweet, dreamy, discreet, treehouse and sushi. Of course you're free to pronounce our name any way you like. Our team will still be every bit as happy to find you the best musical acts you could ever hope to book.</p>
                </div><!-- /.tab -->
            </div><!-- /.tabs-body -->
        </div><!-- /.col-sm-8 -->
    </div><!-- /.row -->
</div><!-- /.tabs -->