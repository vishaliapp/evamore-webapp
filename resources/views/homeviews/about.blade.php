@extends('layouts.master')

@section('content')
	<div class="intro intro-about">
		<img src="/images/temp/city.jpg" height="1815" width="2600" alt="" class="fullsize">
	
		<div class="container">
			<div class="intro-content">
				<h1>2 years, 7000 coffees and maybe a zillion live shows ago.</h1>
			</div><!-- /.intro-content -->
		</div><!-- /.container -->
	</div><!-- /.intro intro-landing -->

	<section class="section">
		<div class="container">
			<div class="row">
				<div class="article article-about">
					<div class="row">
						<div class="col-sm-6">
							<div class="article-image">
								<img src="/images/temp/females.png" height="462" width="462" alt="" >
							
								<span class="article-image-caption">It all started with an <em>idea.</em></span>
							</div><!-- /.article-image -->
							
							<p>When Channing Moreland and Makenzie Stokel met at Belmont University, both were already crazy about live music. It wasn't long before the two Music Business majors were organizing everything from large house parties to small festivals. The music and people part was really fun. The messy details and paperwork and uncertainty part, not so much.</p>

 						</div><!-- /.col-md-6 -->							

						<div class="col-sm-6">
							<p>At moments like these, you can either fight the system or fix it. So they got serious and formed a company. Then one day, their little startup was chosen for Project Music, the planet's first music tech incubator. Imagine being two college students surrounded by heavyweight label heads, booking agents and marketing types.</p>
	
							<p>The girls didn’t get much sleep. But they did get loads of brilliant advice, plus some amazing industry connections who all agreed it was time for a change. The result? EVAmore. A disruptive force for good – and for good music.</p>
	
						</div><!-- /.col-md-6 -->
					</div><!-- /.row -->
				</div><!-- /.article -->
			</div><!-- /.row -->
			<div class="row">
				<div class="col-sm-8 col-sm-offset-2 video-margin">
					<style>.embed-container { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; } .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }</style><div class='embed-container'><iframe src='//player.vimeo.com/video/128440384' frameborder='0' webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe></div>
				</div><!-- /.col-md-6 -->
			</div><!-- /.row -->
		</div><!-- /.container -->
	</section><!-- /.section -->

	<div class="section section-dark section-partners">
		<div class="container">
			<!--<div class="section-head">
				<h1>Our Partners</h1>
			</div>--><!-- /.section-head -->

			<div class="section-body">
				<!--<ul class="list-partners">
					<li>
						<a href="#">
							<img src="/images/temp/logo-rca.jpg" height="80" width="80" alt="">
						</a>
					</li>
					
					<li>
						<a href="#">
							<img src="/images/temp/logo-pmm.jpg" height="45" width="127" alt="">
						</a>
					</li>
					
					<li>
						<a href="#">
							<img src="/images/temp/logo-888.jpg" height="91" width="91" alt="">
						</a>
					</li>
					
					<li>
						<a href="#">
							<img src="/images/temp/logo-vanderbilt.jpg" height="99" width="78" alt="">
						</a>
					</li>
					
					<li>
						<a href="#">
							<img src="/images/temp/logo-belmont.jpg" height="107" width="109" alt="">
						</a>
					</li>
					
					<li>
						<a href="#">
							<img src="/images/temp/logo-615.jpg" height="98" width="83" alt="">
						</a>
					</li>
					
					<li>
						<a href="#">
							<img src="/images/temp/logo-sony.jpg" height="106" width="97" alt="">
						</a>
					</li>
					
					<li>
						<a href="#">
							<img src="/images/temp/logo-bmi.jpg" height="62" width="93" alt="">
						</a>
					</li>
				</ul>--><!-- /.list-partners -->
			</div><!-- /.section-body -->
		</div><!-- /.container -->
	</div><!-- /.section-partners -->
@stop
