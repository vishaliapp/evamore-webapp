@extends('layouts.secondary')

@section('content')
	<section class="section section-apply">
		<div class="container">
			<div class="section-head">
                @if ($currentUser && $currentUser->hasRole("ARTIST_MANAGER"))
                    <h1>Create an EVAmore artist.</h1>
                @else
				    <h1>Apply to become an EVAmore artist.</h1>
                @endif
				<h2>Sure we're selective, but it's for your own good.</h2>
			</div><!-- /.section-head -->

			<div class="section-body">
				<div class="form form-apply">
					{{-- FORM --}}
					{{ Form::open(['route' => 'artistsignup', 'id' => 'artist_register', 'class' => 'dropzone', 'enctype' => 'multipart/form-data'
					]) }}
                    {{ csrf_field() }}

						@include('accounts.partials._artist-form')
					
					{{ Form::close() }}
				</div><!-- /.form form-apply -->
			</div><!-- /.section-body -->
		</div><!-- /.container -->
	</section><!-- /.section -->
	<!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Modal title</h4>
                </div>
                <div class="modal-body">
                    <button id="js-main-image" type="button" class="btn btn-primary btn-invert upload-result" >Set My Picture</button>

                    <div class="demo-wrap upload-demo">
                        <div id="avatar_crop_image" class="container">
                            <div class="grid">
                                <div class="col-1-2">
                                    <div class="upload-msg">
                                        Upload a file to start cropping
                                    </div>
                                    <div class="upload-demo-wrap">
                                        <div id="upload-demo"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>

@include('layouts._artist_signup_agreement')
<script>
    $(function(){
        var $tooltip1 = $('<div><table><tr><td>$</td><td>-</td><td>$$$$</td></tr> <tr><td>Low</td><td></td><td>High</td></tr></table><div class="arrow-right"></div></div>').addClass('feeTooltip')

        $("#fee")
            .on('shown.bs.select', function(){
                $('.fee-selector .dropdown-menu.open').css('overflow', 'visible !important').append($tooltip1);
            })
            .on('hide.bs.select', function(){
                $('.fee-selector .dropdown-menu.open .feeTooltip').remove()
            })
    })
</script>

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="{{asset('demo/prism.js')}}"></script>
<script src="{{asset('croppie.js')}}"></script>
<script src="{{asset('/addons/croper.main.js')}}"></script>
<script>
	$( document ).ready(function() {
		Demo.init();
	});
</script>
@stop

@section('footerjs')
    <script src="{{asset("/addons/jquery-ui-1.12.1/jquery-ui.min.js", null, false)}}"></script>
    <script src="{{asset("/addons/picture-cut-master/src/jquery.picture.cut.js", null, false)}}"></script>
@stop
