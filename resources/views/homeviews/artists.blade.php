@extends('layouts.dark_layout')
@section('public_menu')
    @include('widgets._public_menu')
@endsection
@section('css')
    @parent
    <link rel="stylesheet" href="{{asset('/css/dark_style.css')}}" />
@endsection
@section('sidebar_content')
    <section class="hidden-sm hidden-xs sidebar dark-sidebar artist-dark-sidebar" style="padding-top: 133px;">
        <div class="side-container clear">
            <div class="sidebar-container dark-sidebar-container clear">
                <div class="row">
                    <div class="dark-genres-sidebar-header col-xs-12 col-sm-12 col-lg-11 col-md-11 col-xl-11 text-left">
                        <i class="fa fa-music"></i>&nbsp;
                        <div class="dark-genres-sidebar-header-text">
                            <strong>Genres</strong><br>
                            <small class="text-muted">(Select up to 3)</small>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-lg-11 col-md-11 col-xl-11 text-left filter-wrapper">
                        @include("widgets._menugenres")
                        <br>
                    </div>
                </div>
            </div>
            <div class="sidebar-container dark-sidebar-container clear">
                <div class="row">
                    <div class="dark-genres-sidebar-header col-xs-12 col-sm-12 col-lg-11 col-md-11 col-xl-11 text-left">
                        <i class="fa fa-tag"></i>&nbsp;
                        <div class="dark-genres-sidebar-header-text">
                            <strong>Price Range</strong><br>
                            <small class="text-muted">(Select up to 3)</small>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-lg-11 col-md-11 col-xl-11 text-left filter-wrapper">
                        @foreach (\App\Artist::getFeeTypesText() as $value => $text)
                            <div class="dark-genres-link-wrapper dark-genres-link-wrapper-money">
                                <a data-fee_value="{{$value}}" class="artist-filter-price {{in_array($value, $request_fee) !== false ? 'active' : ''}}" href="#" data-toggle="tooltip" data-placement="top" title="{{$text['text']}}">
                                    <span>{{$text['sign']}}</span>
                                </a>
                            </div>
                        @endforeach

                        <br>
                    </div>
                </div>
            </div>
                 <div class="sidebar-container dark-sidebar-container clear">
                     {{--Location Filter--}}
                     <div class="row">
                         <div class="dark-genres-sidebar-header col-xs-12 col-sm-12 col-lg-11 col-md-11 col-xl-11 text-left">
                             <i class="fa fa-map-marker"></i>&nbsp;
                             <div class="dark-genres-sidebar-header-text">
                                 <strong>Location</strong><br>
                                 {{--<small class="artist-filter-location text-muted reset">Reset</small>--}}
                                 <small class="text-muted">(Choose a location)</small>
                             </div>
                         </div>
                     </div>
                     <div class="row">
                         <div class="col-xs-12 col-sm-12 col-lg-11 col-md-11 col-xl-11 text-left filter-wrapper" style="padding-bottom: 20px">
                             <select style="width: 90%; background: #444; color: #fff; border: none" id="select-filter-location"  class="js-example-basic-multiple">
                                 <option value="">
                                     Choose a Location
                                 </option>
                                 <option value="" class="artist-filter-location currentLocation">
                                     My Current Location
                                 </option>
                                 @foreach ($locations as $location)
                                     <div class="dark-genres-link-wrapper dark-genres-link-wrapper-money">
                                         <option value="{{ $location }}" class="artist-filter-location" {{$location == $req_location ? 'selected' : ''}}>
                                             {{ $location }}
                                         </option>
                                     </div>
                                 @endforeach
                             </select>
                         </div>
                     </div>
                     {{----}}
                </div>
            <div class="sidebar-container dark-sidebar-container dark-sidebar-container-microphone clear">
                <div class="row">
                    <div class="dark-genres-sidebar-header dark-genres-sidebar-header-microphone col-xs-12 col-sm-12 col-lg-11 col-md-12 col-xl-12 text-left">
                        <i class="fa icomoon-icon-microphone"></i>&nbsp;
                        <div class="dark-genres-sidebar-header-text">
                            <a href="{{route('artists_registration')}}">
                                <strong>Want to be an</strong><br>
                                <strong>EVAmore Artist?</strong>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.sidebar -->
    </section>
@endsection

@section('content')
    {{--@if($featured_artists->count()>0)--}}
        {{--<div class="dark-container-wrapper dark-container-wrapper-public">--}}
            {{--<div id="myCarousel" class="carousel slide dark-container dark-container-wrapper-featured" data-ride="carousel">--}}
                {{--<h3 class="dark-carousel-header">FEATURED ARTISTS</h3>--}}
                {{--<div class="carousel-inner" role="listbox">--}}
                    {{--@foreach ($featured_artists as $key => $artists_array)--}}
                        {{--<div class="item {{$key == 0 ? 'active' : ''}}">--}}
                            {{--<div class="row">--}}
                                {{--@foreach ($artists_array as $a)--}}
                                    {{--@include('artists._artist_cell', ['artist' => $a, 'modal' => true])--}}
                                {{--@endforeach--}}
                            {{--</div>--}}
                        {{--</div>--}}
                      {{----}}
                    {{--@endforeach--}}
                {{--</div>--}}
                {{--<a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">--}}
                    {{--<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>--}}
                    {{--<span class="sr-only">Previous</span>--}}
                {{--</a>--}}
                {{--<a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">--}}
                    {{--<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>--}}
                    {{--<span class="sr-only">Next</span>--}}
                {{--</a>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--@endif--}}
	<section class="section section-artists dark-section section-artists-public">
		<div class="container-fluid dark-container-fluid dark-container-fluid-public">
            <div class="dark-filtered-artists-wrapper">
                <h3 class="filtered-results-header">FILTERED RESULTS</h3>
                <div class="row  hidden-lg hidden-md">
                    <br>
                    <br>
                    <div class="col-xs-12 col-sm-12 col-lg-12 col-md-12 col-xl-12 text-center">
                        <script type="text/javascript">
                            (function ($) {
                                $(function () {
                                    $("#select-filter-genre").select2({maximumSelectionLength: 3});
                                })
                            })(window.jQuery);
                        </script>
                        <label for="select-filter-genre">Genres</label><br>
                        <select style="width: 90%;" id="select-filter-genre"  class="js-example-basic-multiple" multiple="multiple">
                            @foreach ($genres as $g)
                                <option {{in_array($g->name, $request_genres) !== false ? 'selected="selected"' : ''}} data-genre_name="{{$g->name}}" value="{{$g->name}}">{{$g->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="row  hidden-lg hidden-md">
                    <div class="col-xs-12 col-sm-12 col-lg-12 col-md-12 col-xl-12 text-center">
                        <script type="text/javascript">
                            (function ($) {
                                $(function () {
                                    $("#select-filter-fee").select2({maximumSelectionLength: 3});
                                })
                            })(window.jQuery);
                        </script>

                        <label for="select-filter-fee">Fee</label><br>
                        <select style="width: 90%;" id="select-filter-fee"  class="js-example-basic-multiple" multiple="multiple">
                            @foreach (\App\Artist::getFeeTypesText() as $value => $text)
                                <div class="dark-genres-link-wrapper dark-genres-link-wrapper-money">
                                    <a data-fee_value="{{$value}}" class="artist-filter-price {{in_array($value, $request_fee) !== false ? 'active' : ''}}" href="#" data-toggle="tooltip" data-placement="top" title="{{$text['text']}}">
                                        <span>{{$text['sign']}}</span>
                                    </a>
                                </div>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="row hidden-lg hidden-md">
                    <div class="col-xs-12 col-sm-12 col-lg-12 col-md-12 col-xl-12 text-center">
                        <script type="text/javascript">
                            (function ($) {
                                $(function () {
                                    $("#select-filter-fee").select2({maximumSelectionLength: 3});
                                })
                            })(window.jQuery);
                        </script>

                        <label for="select-filter-fee">Location</label><br>
                        <select style="width: 90%;" id="select-filter-location"  class="js-example-basic-multiple">
                            <option value="">
                                Select Location
                            </option>
                            <option value="" class="artist-filter-location currentLocation">
                                My Current Location
                            </option>
                            @foreach ($locations as $location)
                                <div class="dark-genres-link-wrapper dark-genres-link-wrapper-money">
                                    <option value="{{ $location }}" class="artist-filter-location" {{$location == $req_location ? 'selected' : ''}}>
                                            {{ $location }}
                                    </option>
                                </div>
                            @endforeach
                        </select>
                    </div>
                </div>
                @if ( ! $artists->count() )
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-lg-12 col-md-12 col-xl-12 text-center">
                            <br><br>
                            <h1 class="text-muted">
                                Select genres to view artists.
                            </h1>
                        </div>
                    </div>
                @endif
                @foreach($artists->chunk(4) as $set)
                    <div class="row">
                        @foreach ($set->chunk(2) as $artistSet)
                            @foreach($artistSet as $artist)
                                @include('artists._artist_cell', ['artist' => $artist, 'showGenres' => true, 'modal' => true])
                            @endforeach
                                <div class="clearfix visible-xs visible-sm"></div>
                        @endforeach
                    </div>
                @endforeach

                @if ($artists->count() < 5)
                    {{-- Hack for the left sidebar --}}
                    <div class="visible-lg">
                        <br><br><br><br><br><br><br><br><br><br><br><br><br><br>
                    </div>
                @endif

                {{--Automodal for unauthorized users--}}
                @if(session()->has('artist'))
                    <script>
                        $(document).ready(function () {
                            $('.artist-name').each(function () {
                                if ($(this).attr('data-name') == '{{ session()->get('artist') }}') {
                                    $(this).click();
                                }
                            })
                        })
                    </script>
                @endif
                {{--END--}}

                <div class="row">
                    <div class="dark-pagination-block col-xs-12 col-sm-12 col-lg-12 col-md-12 col-xl-12 text-right">
                        {{$artists->appends($appends)->links()}}
                    </div>
                </div>
                <br>
            </div>
		</div><!-- /.container -->
	</section><!-- /.section -->
    <!-- Modal -->
    <div class="modal fade modal-artist-info" id="artistInfo" >
        <div class="modal-dialog modal-lg" role="document">
            <button type="button" class="close outer" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <div class="modal-content">
                <div class="modal-body"></div>
            </div>
            <div class="modal-button modal-button-prev"><span class="glyphicon glyphicon-chevron-left"></span></div>
            <div class="modal-button modal-button-next"><span class="glyphicon glyphicon-chevron-right"></span></div>
        </div>
    </div>
    @include('artists._artist_modal_template')
    {{-- equaling sidebar and wrapper heights --}}
    <script>
        //get wrapper element
        var wrapper = $('.dark-filtered-artists-wrapper');

        //get sidebar and wrapper heights
        var sideHeight = $('.side-container').height();
        var wrapHeight = wrapper.height();

        //check if sidebar height is bigger than wrapper height
        if (sideHeight > wrapHeight) {
            wrapper.css({'min-height': sideHeight});
        }
    </script>
@endsection
@section('js')
    @parent
    <script src="{{asset( file_exists('/js/artists.min.js') ? '/js/artists.min.js' : '/js/artists.js' )}}"></script>
    <script src="{{asset( '/js/filterArtists.js'  )}}"></script>
    <script src="{{asset("/js/youtube.min.js")}}"></script>
    <link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
    <script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDuCCI1t1qsTUO7xA57m7tY0ujVGgcOPjM"></script>

    {{--Get Current Location--}}
    <script>
        getLocation();
        var geocoder;

        function getLocation() {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(successFunction, errorFunction);
            }
        }
        //Get the latitude and the longitude;
        function successFunction(position) {
            var lat = position.coords.latitude;
            var lng = position.coords.longitude;
            codeLatLng(lat, lng);
        }

        function errorFunction(){
            console.log("Geocoder failed");
        }

        function codeLatLng(lat, lng) {
            geocoder = new google.maps.Geocoder();
            var latlng = new google.maps.LatLng(lat, lng);
            geocoder.geocode({'latLng': latlng}, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[1]) {
                        //find country name
                        for (var i=0; i<results[0].address_components.length; i++) {
                            for (var b=0;b<results[0].address_components[i].types.length;b++) {
                                //there are different types that might hold a city admin_area_lvl_1 usually does in come cases looking for sublocality type will be more appropriate
                                if (results[0].address_components[i].types[b] == "administrative_area_level_1") {
                                    city= results[0].address_components[i];
                                    break;
                                }
                            }
                        }
                        //city data
                        current_location = city.short_name;
                        $('.currentLocation').attr('value', current_location);
                    } else {
                        console.log("No results found");
                    }
                } else {
                    console.log("Geocoder failed due to: " + status);
                }
            });
        }
    </script>
@stop
