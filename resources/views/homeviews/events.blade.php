@extends('layouts.master')
@section('css')
	@parent
	<link rel="stylesheet" href="{{asset('/bower_components/slick-carousel/slick/slick.css', null, false)}}" />
	<link rel="stylesheet" href="{{asset('/bower_components/slick-carousel/slick/slick-theme.css', null, false)}}" />
	<link rel="stylesheet" href="{{asset('/css/events_style.css')}}" />
@endsection
@section('content')
	<div class="intro intro-events">
		<img src="/images/temp/intro-events.jpg" height="840" width="2600" alt="" class="fullsize">
	</div>
	<section class="section section-events">
		<div class="container">
			<div class="event">
				<div class="row">
					<div class="col-sm-6">
						<div class="event-image">
							<img src="/images/temp/event1.png" height="441" width="405" alt="">
						</div><!-- /.event-image -->
					</div><!-- /.col-sm-6 -->

					<div class="col-sm-6">
						<div class="event-content">
							<h2>Proven performers, all right at your fingertips.</h2>

							<p>How do we put this gently? Not every band gets to become an EVAmore band. The acts you see and hear on our site have already made it clear they deserve to be here. Talented, yes. But also reasonable and reliable and professional in every sense.</p>
							<p>Our magic secret algorithm only connects you with those who fit your budget, calendar and genre preferences. Once you select your favorite, lock in any show with a single click. You'll have confirmation within 24 hours.</p>

 						</div><!-- /.event-content -->
					</div><!-- /.col-sm-6 -->
				</div><!-- /.row -->
			</div><!-- /.event -->

			<div class="event event-reverse">
				<div class="row">
					<div class="col-sm-6 col-sm-push-6">
						<div class="event-image">
							<img src="/images/temp/event2.png" alt="" width="405" height="410">
						</div><!-- /.event-image -->
					</div><!-- /.col-sm-6 -->

					<div class="col-sm-6 col-sm-pull-6">
						<div class="event-content">
							<h2>When we tell you it's booked, it's booked.</h2>

							<p>Our team has a solid working relationship with every artist in our network. Musicians appreciate the way we're reinventing this business, plus the fact that we actually do what we say we'll do. So they take your EVAmore show seriously.</p>
							<p>For you, this means extra peace of mind. Once you receive your booking confirmation, feel free to breathe a happy little sigh of relief and move on to the next three hundred items on your to-do list. We've got this one covered.</p>

 						</div><!-- /.event-content -->
					</div><!-- /.col-sm-6 -->
				</div><!-- /.row -->
			</div><!-- /.event -->
		</div><!-- /.container -->
	</section><!-- /.section -->
	<div class="row">
	    <div class="visible-xs visible-sm col-xs-12 col-sm-12 mobile-video-vanderbilt">
			<iframe src="https://www.youtube.com/embed/4siK9MypNkE" frameborder="0" allowfullscreen></iframe>
	    </div>
	</div>
	<div class="hidden-sm hidden-xs container-fluid" style="background: #000;">
		<div class="row">
			<div data-index="0" data-toggle="modal" data-target="#events-modal" class="slide-preview-text modal-slide-trigger text-center col-xs-12 col-sm-12 col-lg-2 col-md-2 col-xl-2" style="padding: 0; ">
				<img src="/images/home-events/3.jpg" alt="">
				<i class="fa fa-file-text-o"></i>
			</div>
			<div data-index="1" data-toggle="modal" data-target="#events-modal" class="slide-preview-video modal-slide-trigger text-center col-xs-12 col-sm-12 col-lg-2 col-md-2 col-xl-2" style="padding: 0; ">
				<img src="/images/home-events/1.jpg" alt="">
				<i class="fa fa-file-text-o"></i>
			</div>
			<div data-index="2" data-toggle="modal" data-target="#events-modal" class="slide-preview-video modal-slide-trigger text-center col-xs-12 col-sm-12 col-lg-2 col-md-2 col-xl-2" style="padding: 0; ">
				<img src="/images/home-events/2.jpg" alt="">
				<i class="fa icomoon-icon-play-button"></i>
			</div>
  			<div data-index="4" data-toggle="modal" data-target="#events-modal" class="slide-preview-video modal-slide-trigger text-center col-xs-12 col-sm-12 col-lg-2 col-md-2 col-xl-2" style="padding: 0; ">
                                <img src="/images/home-events/6.jpg" alt="">
                                <i class="fa fa-file-text-o"></i>
                        </div>
			<div data-index="3" data-toggle="modal" data-target="#events-modal" class="slide-preview-video modal-slide-trigger text-center col-xs-12 col-sm-12 col-lg-2 col-md-2 col-xl-2" style="padding: 0; ">
				<img src="/images/home-events/5.jpg" alt="">
				<i class="fa icomoon-icon-play-button"></i>
			</div>
			<div data-index="5" data-toggle="modal" data-target="#events-modal" class="slide-preview-text modal-slide-trigger text-center col-xs-12 col-sm-12 col-lg-2 col-md-2 col-xl-2" style="padding: 0; ">
				<img src="/images/home-events/4.jpg" alt="">
				<i class="fa fa-file-text-o"></i>
			</div>
		</div>
	</div>
	<section class="section section-sign section-sign-alt">
		<img src="/images/temp/sign.jpg" height="690" width="1300" alt="" class="fullsize">

		<div class="container">
			<div class="row">
				<div class="col-md-offset-2 col-lg-offset-2 col-xs-12 col-sm-12 col-lg-8 col-md-8 col-xl-8 col-bootstrap-default">
					<h1>Sign up for our newsletter.</h1>
				</div>
			</div>
			<div class="row">
				<div class="col-md-offset-3 col-lg-offset-3 col-xs-12 col-sm-12 col-lg-6 col-md-6 col-xl-6 col-bootstrap-default">
					<div class="form form-sign">
						<form action="{{ route('ajax_subscribe') }}" method="get" id="form-subscribe">
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-lg-12 col-md-12 col-xl-12 col-bootstrap-default">
									<div class="form-group">
										<div class="subscribe-respond-message">&nbsp;</div>
										<label for="email" class="form-label">Email</label>
										<div class="form-controls">
											<input type="email"
												   class="form-control"
												   id="email"
												   name="email"
												   placeholder="one you check often">
										</div><!-- /.form-controls -->
									</div><!-- /.form-group -->
								</div>
							</div><!-- /.row -->
							<div class="form-actions">
								<button type="button" class="btn btn-primary form-btn" id="btn-subscribe">
									I’M IN!
								</button>
							</div><!-- /.form-actions -->
						</form>
					</div><!-- /.form form-sign -->
				</div>
			</div><!-- /.row -->
		</div><!-- /.container -->
	</section><!-- /.section-sign -->
    @include('layouts._planner_signup_agreement')
	<!-- Modal -->
	<div class="modal" id="events-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog events-modal-dialog" role="document">
			<div class="modal-content events-modal-content">
				<i class="fa fa-times events-modal-close" aria-hidden="true"></i>
				<div class="modal-body">
					<div class="slide-main">
						@for ($i = 0; $i <2; $i++)
							<div class="slide-article">
								<div class="slide-article-text-wrapper">
									<p>
										<img src="/images/articles/art1img1.jpg" alt="" class="events-article">
									</p>
									<h1>THESE WOMEN CREATED THE FUTURE OF BOOKING LIVE ENTERTAINMENT</h1>
									<small>By Nicole Baksinskas</small>
									<p>
										Music City undoubtedly provides a considerable pool of musical talent. But how does one seamlessly discover and hire those live acts for their next event?
									</p><p>
										That’s where companies like EVAmore (Events, Venues, Artists and more) step in. The online platform automates the process of booking live entertainment for private events from beginning to end. Event planners can browse diverse genres and a hand-picked selection of professional and reliable artists guaranteed to have a strong digital presence and recorded material. Set the date, time and budget, and request to book—then wait for the artist to confirm.
									</p>

									<p>
										<img src="/images/articles/art1img2.jpg" alt="" class="events-article">
									</p>
									<p>
										When EVAmore CEO Channing Moreland and COO Makenzie Stokel met as music business and songwriting majors during freshman orientation at Belmont University, they bonded over a shared love of music events. Near the end of their freshman year, partly motivated by not yet meeting the age requirement set by most venues, they started creating events on their own for the college community.
									</p>
									<p>
										“We started putting all the pieces together—event production, sponsorships, bands and food—because we enjoyed it and wanted to,” Moreland said. “That’s where there were some issues when we tried to book all these bands: load-in, communication, contracting, payments—it can get messy. We saw that was where we could automate live events. It was a little selfish at first because we were just creating these events for ourselves.”
									</p><p>
										In August 2016, three months after their graduation, Moreland and Stokel launched EVAmore with the help of investor funding and Project Music, a 14-week music-tech accelerator program.
									</p><p>
										Project Music, offered through the Nashville Entrepreneur Center, brings music, tech and business leaders together to nurture startups desiring to grow music industry revenue. Moreland and Stokel, the youngest participants and only women in the program, solidified their concept within days of entering the program. They were confident in its potential not only because of their own experience, but also because of extensive research and interviews that proved their technology product would be valuable.
									</p>
									<p>
										<img src="/images/articles/art1img3.png" alt="" class="events-article">
									</p>
									<p>
										“We knew EVAmore would work because we really listened to customers,” Stokel said. “When we saw our biggest pain point was managing bands for events, we realized this was an issue. Then we went and found social chairs and event planners and listened to a ton of them.
									</p><p>
										“During our entire senior year, we traveled on our days off and knocked on doors and asked, ‘What’s the issue here?’ Out came a binder with all the event details and it was a mess. We saw the need and knew this could be a platform. Also, we saw competitors in this space, which was a good sign—we want to see that—but no one is doing exactly what we’re doing.”
									</p><p>
										They were right: EVAmore has been working. As an additive resource for more than 100 bands, the platform works alongside booking agents and management companies since the goal is to be the in-between for event planners. It soothes an event planner’s peace of mind and helps artists get paid, which is significant in a culture of underpaid musicians.
									</p>
									<p>
										<img src="/images/articles/art1img4.png" alt="" class="events-article">
									</p>
									<p>
										The EVAmore online platform was built with the help of Pilgrim Consulting, a Franklin-based website development company with which Moreland and Stokel work closely. “As founders of a tech company, we had to learn the ins and outs of coding and how different languages work. It has been challenging, but we figured out what the process needs to look like for us, and [we] learned how to communicate with developers,” Moreland said.
									</p><p>
										Currently, EVAmore is available in the southeast region, but Moreland and Stokel, with intentions of being an industry powerhouse, see expansions to the southwest, west and midwest regions in the near future. They have a strong college and event planner networks and are currently focused on building stronger relationships with corporate event planners and destination management companies.
									</p><p>
										“We’ve made revenue, have had consistent growth, raised funding and are looking to hire a few people this year. We built the tech and we proved the model, and now we’re ready to move onto marketing and sales. This is new, but we’re starting to see a lot of opportunities for varying revenue streams,” Moreland said.
									</p>
									<p>
										<img src="/images/articles/art1img5.png" alt="" class="events-article">
									</p>
									<p>
										EVAmore is currently holding a fundraiser through iFundWomen, a crowdfunding platform for women-led businesses. They are aiming to raise 15k to launch a Nashville-based private festival showcase and catered event that will introduce EVAmore artists to the industry and to event planners who can book them. They hope the festival will help artists gain traction, recognition and fans while giving back to musicians by helping them be seen and heard.
									</p><p>
										Moreland and Stokel will also present their product at the 36|86 Entrepreneurship and Technology Conference in Nashville for entrepreneurs, thought leaders and investors from across the country. Plus, they will speak at Music Biz 2017, a conference in Nashville that advances the digital and physical monetization of music for artists and rights-holders.
									</p><p>
										The partnership between Moreland and Stokel pushes them forward, as does founding a business they are passionate about. “Someone once said to us, ‘At the end of the day, no one is going to care as much about your company as much as you do.’ That really opened my eyes,” Moreland recalled. “It’s us doing it.”
									</p><p>
										Looking to book a band for your next event or just want more information? Visit their website or social channels for more info.
									</p>
								</div>
							</div>

							<div class="slide-article">
								<div class="slide-article-text-wrapper">
<h1>The Difference Between Good Enough and Great Gatsby.</h1>
<p>
A celebration, soirée, private party or wing-ding (we did not know this word existed, but we are so glad it does. Wing-Ding: a wild, lively, and lavish party) is a perfect snapshot in time. Every detail is in place for the invitees to experience a magical moment together that all disappears when the evening is complete. The event goer is left walking away giddy with the memories of an enchanting night, wondering if any of it actually just happened.
</p>
<img src="/images/articles/art3img1.png" alt="" class="events-article">
<p>
It’s been said time and time again, “I want to throw a party like this ^(damn you, Jay Gatsby) but I just don’t know how.”
</p><p>
We get it. When you walk into a wing-ding all you can think about is how the heck did your friend Taylor pull this off. Well we have found a pretty consistent and simple equation to get you on Taylor’s level.
</p><p>

All you need to get started: Determine the setting and the entertainment for your party. Based on the venue and music you choose, an atmosphere and theme will be defined that you can use throughout the entire event. The rest of the details will naturally follow, because it will become clear based on the theme what kind of refreshments, furniture, oversight, and invitations you might need.
</p><p>

<img src="/images/articles/art3img2.jpg" alt="" class="events-article">

So for the setting: This is personal opinion, but we have found that an intimate venue with soft lighting is a great foundation for most events. Check out local galleries, refined warehouses, or wineries and breweries for starters. If you find a spot with hardwood floors, exposed brick, and a few chandeliers- By George! Book it now. But of course, some events are unprecedented, so start with a list of your must haves and you will instantly have a better idea of what you’re looking for in an event space.
</p><p>

Now the entertainment: Something to remember is that there’s a difference between a private party and a sold out show. At a private party, it’s not about the artist’s fan following, the specific genre of music, which songs for the encore, or the price of tickets. All that matters is how the crowd reacts to the music and what the energy is like in the room; the music’s sole purpose is to amplify the overall experience. Pun intended.
</p><p>

In order for the music to actually enhance a private party, the entertainment performing needs to understand the difference between your event vs. their sold out show. That’s key.
</p><p>

<img src="/images/articles/art3img3.jpg" alt="" class="events-article">

So here are the five qualities we look for in entertainment that qualifies them to play private events, and you should too:
</p><p>
<ol><li>
Pure talent: Can each player hold their own and bring a unique sound to the table? We want it to feel rare, but not preposterous.
</li><li>
Sportsmanship: We don’t need any divas or divos. The band has to want this opportunity, so we look for hard workers with flexibility.
</li><li>
Live event IQ: The intuition and quick versatility to make your event great- and if they don’t have a lengthy touring/performing history, no dice.
</li><li>
An agreement that this night isn’t about them: This requires honesty, self awareness, social skills, vulnerability, and trust. This is a relationship at the end of the day, even if it’s just for one night.
</li><li>
Charisma: They might be the best players in the world, but if they don’t have a sense of direction and leadership, no one in the crowd will feel the energy. Also known as a component of the “It” factor.
</li><li>
Event planners say to us constantly, “Charisma is just as important as being able to play the songs. My clients are going to want to interact with the entertainment and I need someone with a great personality.”
</li></ol>
</p><p>

At the end of the day this is all subjective, but it is something we are required to understand about the entertainment we pitch. It’s not just about the pure talent. It matters who they are as people and what they are able to effortlessly add to your event to take it from the “Oh good, this is decent and won’t get me laughed at,” level to “Wow. This band just upgraded my night X 10.”
</p><p>

We hope our checklist for what to look for when choosing high quality entertainment helped. If you need ideas, check out our pre-vetted roster on EVAmore.
</p>

						
								</div>
							</div>
							<div class="text-center">
								<iframe src="https://www.youtube.com/embed/TBsR7IOh1tQ" frameborder="0" allowfullscreen></iframe>
							</div>



							<div class="text-center">
							<iframe src="https://www.youtube.com/embed/m4WgHSTfI8w" frameborder="0" allowfullscreen></iframe>
							</div>
                                                        <div class="slide-article">
                                                                <div class="slide-article-text-wrapper">
<h1>
							Everything You Never Wanted To Know About Sound Systems
</h1><p>
Planning an event — any event — should ideally be fun, easy and free of surprises. Unicorns and daisies all around, right? This applies to lining up the awesome live entertainment so vital to making a great event great.
So let’s say you’ve nailed down the coolest venue ever, got your drinks and appetizers selected, festive decor is good to go, invitations sent. All that’s left on your checklist is the big one you’ve been putting off as long as possible: booking a band. Yet somehow, the stars align. You find that perfect pop duo or funky Motown cover group. The act you want is available. They’ll perform for $100 less than your music budget. They’ve even agreed to accommodate your boss’s weirdly insistent request to play “On the Wings of Love.”
</p><p>
<img src="/images/articles/mad_men.png" class="events_article">
Via @MadMen_AMC
</p><p>
Seems like everything’s good to go, right? Well, maybe. But if you haven’t given any thought to sound gear, you could be setting yourself up for a nasty letdown.
</p><p>
We’re the first to agree that music can be absolutely magical. But not magical enough to get a room rocking without the help of some pretty serious technology. You need a very specific collection of electronic gear to do that job, along with someone who actually knows how to set everything up and operate it.
</p><p>
The PA system, also known as Front of House/monitor system, is not the same thing as musicians’ personal instruments and amps. Mixing console. Power amps. Microphones and stands. XLR cables, DI boxes, fraznobulators, whizzbangs, the list goes on and on.
</p><p>

We know, we know. This is way too much to think about. The good news? You don’t need to become a live audio expert. All you have to do is make sure you have one there on the big night.
</p><p>

<p><strong>EVAmore’s Sound System Checklist: </strong><br />
<ul>
<li>
Does the venue I’ve chosen provide sound equipment and a sound engineer? If the venue has musical acts performing on a consistent basis, it’s likely that they already have a decent stage and sound equipment. In the negotiation of your venue rental fee, it’s super important that you ask for the sound system and engineer to be included. If you bring this up as part of the negotiation, a venue manager will often just include these items in your package.
</li><li>

Does the band own or provide their own sound equipment? If the venue isn’t providing the sound, ask the band upfront if they can. Smaller acts like acoustic duos or trios don’t generally require a huge PA system, so often they bring their own. Larger groups may have a trusted sound partner that they always use (we promise, this will always be a more cost effective option).
</li><li>

Remember: Sound production is a separate cost from the band. You need to consider this when thinking about your entertainment budget, so make it one of your first questions rather than one of the last. For an acoustic duo, you might be looking at an extra $50, but for larger productions the sound gear and engineer can easily add another $1,000 or more. Standard stuff, totally necessary, but if you’re new to this world it might not occur to you.
</li><li>
Have you heard about Sparkplug? They’ve done a great job of automating the sound equipment rental process online, and should be able to help you solve all of your sonic needs not being handled by the venue or band. Check out this great resource at sparkplug.it.
</li><li>
EVA Event Tip: When you post an event on EVAmore, we always prompt you with questions designed to avoid oversights, misunderstandings and unhappiness in general. Sound equipment is just one example.
</li></ul>
</p><p>
Remember, at EVAmore, our mission in life is to make your life easier. Less headaches. More daisies and unicorns. Better music. Hopefully this checklist helps.
</p><p>
Questions? Hit us up at eva@evamore.co. We’d love to hear from you. #trustEVA</p>
								</div>
							</div>
							<div class="slide-article">
								<div class="slide-article-text-wrapper">
									<h1>EVAmore Bets on Digital Marketplace Model to Disrupt Music Booking for Private Events</h1>
									<p>
										<img src="/images/articles/art2img1.jpg" alt="" class="events-article">
									</p>
									<p>
										Imagine you're throwing a party, and you want to book a musician for the occasion. How do you find and hire someone to perform? If you don’t have any personal connections, then your search will likely involve scouring online databases, contacting booking agencies or posting classified ads on websites like Craigslist.
									</p>
									<p>
										While attending Belmont University in Nashville, TN, Channing Moreland and Makenzie Stokel discovered that booking a band is a time-consuming, largely manual process that’s especially difficult for hosts who are unfamiliar with how it’s done. So they set out to create an online platform through which private event hosts can quickly and easily hire talented local bands with minimal hassle.
									</p>
									<p>
										That solution is EVAmore, which is described as “the streamlined solution to book first-class entertainment for events.” Event hosts can create free listings, including time, location, budget and requested genres of music. Once created, the event gets pushed out to artists on the platform who meet the specified criteria, creating an open line of communication for musicians who are interested.
									</p>
									<p>
										All messaging and payment is handled through the platform, and the EVAmore team works with musicians to take care of production and logistical issues. The goal is to remove the stress and cumbersome work of the booking process so hosts can focus on actually enjoying the event they’ve put together.
									</p>
									<p>
										EVAmore’s online, accessible solution for booking bands has garnered initial attention in Nashville and across college campuses in the Southeastern U.S. Check out Moreland and Stokel’s pitch below to learn more about its potential to disrupt music booking for private events nationwide:
									</p>
									<p class="text-center">
										<iframe width="560" height="315" src="https://www.youtube.com/embed/Mf57-WHLwO0" frameborder="0" allowfullscreen></iframe>
									</p>
									<h2>The Only One-Stop Shop for Vetted Musicians</h2>
									<p>
										Event planners who are searching for live music have a few online options besides EVAmore. Websites GigMasters and GigSalad are similar marketplaces for booking event services and entertainment, including but not limited to musicians. BookABand.com lists registered musicians by state and facilitates direct conversation, but it doesn't handle contracting or payment.
									</p>
									<p>
										But EVAmore is unique in that it’s the first platform to provide algorithm-based matching—combined with integrated payment and backed by hands-on customer support—exclusively for musical entertainment. Additionally, EVAmore sets itself apart by evaluating every musician who applies to join the platform and only accepting those who are both talented and professional.
									</p>
									<p>
										“We vet our artists so that whoever’s booking knows what they’re getting into, and to ensure we can provide a reliable artist,” Stokel said.
									</p>
									<p>
										The company operates on a marketplace model, collecting 10% commision from the musician’s payment as well as an additional 10% fee from the host. If a musician is represented by an agent, the EVAmore team strives to negotiate a 50/50 split on that 10% commission.
									</p>
									<h2>A Disruptive Force with Traction and Funding, But Is It Enough?</h2>
									<p>
										<img src="/images/articles/art2img2.jpg" alt="" class="events-article">
									</p>
									<p>
										Moreland and Stokel have made impressive headway with EVAmore in a quick amount of time. Shortly after founding the company in 2015, they completed the Nashville Entrepreneur Center’s Project Music accelerator as part of the first cohort. They secured a reported $30,000 in seed funding and several music industry veterans for their advisory board as a result of their work in the program.
									</p>
									<p>
										EVAmore’s initial target market includes fraternities and sororities on college campuses. The platform is currently in use at 18 universities, and a campus representative system helps EVAmore expand its user base among the local Greek organizations. Moreland and Stokel are working on increasing its presence on college campuses as well as branching into new markets.
									</p>
									<p>
										“We’ve made partnerships with the National Panhellenic Conference and National Interfraternity Conference to get our Greek life presence into all US campuses. When we generate a market, we’re also going to be looking at the wedding planners, corporate planners and destination management companies in those locations,” Moreland said.
									</p>
									<p>
										Because EVAmore only facilitates private events, the team has been able to present the platform as a service to professional booking agencies rather than a competitor. However, maintaining the same standards for personalized service and vetting could be labor-intensive as EVAmore expands to new markets.
									</p>
									<p>
										Moreland and Stokel started the business as sophomores in college and don’t deny the mistakes they’ve made along the way. But with a Series A round completed, an experienced panel of advisors and a growing presence in eight states, the platform has a chance to add real value to an industry that still relies primarily on phone calls and emails to get the job done.
									</p>
									<p>
										Matt Hunckler is the founder of Verge, a community of tech entrepreneurs and investors. Learn how to grow your business by signing up for his newsletter here.
									</p>
								</div>
							</div>
						@endfor
					</div>
				</div> {{--./ modal-body--}}

			</div>
		</div>
	</div>
@stop
@section('js')
	@parent
	<script src="{{asset('/bower_components/slick-carousel/slick/slick.min.js', null, false)}}"></script>
	<script>
        (function ($) {
            $(function () {
                var $eventsModal = $('#events-modal'),
                    $mainSlider = $('.slide-main'),
                    $eventsModalContent = $('.events-modal-content'),
                    mainSlickOptions = {
                        slidesToScroll: 1,
                        slidesToShow: 1,
                        arrows: true,
                        prevArrow: '<i class="slick-prev slick-arrow  icomoon-icon-circle-left"></i>',
                        nextArrow: '<i class="slick-next slick-arrow  icomoon-icon-circle-right"></i>',
                        fade: true
                    };

                function slickInit(opt) {
                    opt = opt || {};
                    $mainSlider.slick( $.extend(mainSlickOptions, opt) );
                }

                function slickDestroy() {
                    $mainSlider.slick('unslick');
                }

                $eventsModal.on({
                    'shown.bs.modal': function (event) {
                        var $button = $(event.relatedTarget),
                            index = $button.data('index');
                        slickInit({initialSlide: index});
                    },
                    'hidden.bs.modal': function () {
                        slickDestroy();
                    }
                });


                $eventsModalContent.click(function (e) {
                    var $target = $(e.target),
                        isArrows = $target.closest('.slick-arrow').length,
                        isIframe = $target.closest('iframe').length,
                        isPhoto = $target.closest('.photo').length,
                        isPreview = $target.closest('.preview').length,
                        isArticle =$target.closest('.slide-article').length;

                    if (isIframe || isArticle || isPhoto || isPreview || isArrows) {
                        return true;
                    }
                    $eventsModal.modal('hide');
                });

            });

            /**
			 * Subscribe
             */
            $(function () {
                $formSubscribe = $('#form-subscribe'),
					$btnSubscribe = $('#btn-subscribe'),
					$email = $('#email'),
					$messageBlock = $('.subscribe-respond-message');

                $formSubscribe.submit(function(e) {
                    e.preventDefault();
                    if ( ! $btnSubscribe.prop('disabled') ) {
                        $btnSubscribe.trigger('click');
					}
				});

                $btnSubscribe.click(function (e) {
                    var $url = $formSubscribe.attr('action');

                    e.preventDefault();
                    $messageBlock.html('&nbsp;').removeClass('text-danger').removeClass('text-success');
                    $btnSubscribe.prop('disabled', true).addClass('disabled');

					$.get($url, {email: $email.val()})
						.done(function (data) {
                            if (data && data.message) {
                                $messageBlock.text(data.message);
							}
							if (data && data.success && data.success === true) {
                                $messageBlock.addClass('text-success');
							} else {
                                $messageBlock.addClass('text-danger');
							}
                        })
						.fail(function () {
                            $messageBlock.text('Request error. Please, try again later').addClass('text-danger');
                        })
						.always(function () {
                            $btnSubscribe.prop('disabled', false).removeClass('disabled');
                        });
                });
            });
        })(window.jQuery);
	</script>
@endsection
