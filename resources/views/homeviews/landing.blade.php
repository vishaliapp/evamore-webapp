@extends('layouts.master')

@section('content')
    @if(isset($loginModal) && $loginModal)
        <script>
            $(window).on('load', function () {
                $('#popup-login').modal('show');
            });
        </script>
    @endif
    <div class="intro-landing-video-block">
        @if ( ! $isMobile )
            <video @if ( $isIOSEqualOrLess_9 ) poster="{{asset("/images/video/eva.gif")}}"
                   @endif class="intro-landing-video" autoplay loop>
                @if ( ! $isIOSEqualOrLess_9 )
                    <source src="{{asset("/images/video/25seconds_eva.m4v", null, false)}}">
                    <source src="{{asset("/images/video/25seconds_eva.webm", null, false)}}">
                @endif
            </video>
        @endif
    </div>
    @if ( $loginRedirect )
        <script type="text/javascript">
            $(window).on('load', function () {
                $('#popup-login').modal('show');
            });
        </script>
    @endif
    <div class="intro intro-landing fullsize-container">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-lg-6 col-md-6 col-xl-6 col-bootstrap-default">
                    <div class="intro-content">
                        <div class="intro-content-inner">
                            <h1>Book the perfect live music for your next event</h1>

                            <h3>Find amazing artists - handle scheduling, contracts and payment, all right here</h3>

                            <div class="intro-actions hidden-sm">
                                <a id="book" href="#register" class="btn btn-fill">BOOK AN EVENT</a>

                                <a href="{{route('home_artists')}}" class="btn btn-fill">EXPLORE ARTISTS</a>
                            </div><!-- /.intro-actions -->
                        </div><!-- /.intro-content-inner -->
                    </div><!-- /.intro-content -->
                </div>
                <div class="col-xs-12 col-sm-6 col-lg-6 col-md-6 col-xl-6 col-bootstrap-default">
                    <div class="landing-ipad">
                        <div class="ipad-size">
                            <img src="/images/temp/ipad.png">
                        </div>
                    </div><!-- /.sign-ipad -->
                </div>
            </div>
            <div class="row visible-sm">
                <br>
                <div class="col-xs-12 col-sm-12 col-lg-6 col-md-6 col-xl-6 col-bootstrap-default">
                    <a id="book" href="#register" class="btn btn-fill">BOOK AN EVENT</a>

                    <a href="{{route('home_artists')}}" class="btn btn-fill">EXPLORE ARTISTS</a>
                </div>
            </div>
        </div><!-- /.container -->
    </div><!-- /.intro intro-landing -->

    <section id="learn" class="section section-pulses">
        <div class="container-fluid" style="position: relative; top: -40px;">
            <div class="row">
                <div class="col-lg-offset-2 col-xs-12 col-sm-12 col-lg-10 col-md-12 col-xl-10">
                    <h1>How EVAmore works</h1>
                </div>
            </div>
            <br>
            <div class="row pulses-row">
                <div class="col-lg-offset-2 col-xs-12 col-sm-12 col-lg-3 col-md-12 col-xl-3">
                    <div class="article how-evamore-works">

                        <p>
                            Our exclusive online booking platform makes it easy to find the perfect live music for any
                            event. Just post your event details, and let the artists come to you. Remember when it took
                            days (or weeks) to find the talent, negotiate pricing and sign contracts? Ancient history.
                        </p>
                        <p>
                            EVAmore harnesses today’s technology to make the whole process faster, easier and more
                            reliable. So now, you can actually relax and have fun at your own event for a change.
                        </p>

                        <p class="sub-title">We handle all the details:</p>
                        <ul>
                            <li>Reliable Booking</li>
                            <li>Fully Vetted Artists</li>
                            <li>Simple Contracts</li>
                            <li>Secure Online Payment</li>
                        </ul>
                        <p>
                            We created EVAmore because as event planners ourselves, we were fed up with the typical
                            hassles of booking live music. We knew there had to be a better way. EVAmore is that way.
                            Setting up your account is fast, free and painless. Sign up now, and you’ll be done before
                            you know it.
                        </p>

                    </div><!-- /.article -->
                </div>
                <div class="col-xs-12 col-sm-12 col-lg-7 col-md-12 col-xl-7 text-center">
                    <div class="visible-sm visible-md"><br><br></div>
                    <div>
                        <ul class="pulses">
                            <li class="pulse css-pulse-planner">
                                <div class="pulse-inner">
                                    <div class="pulse-image">
                                        <img src="/images/temp/photo1.jpg" height="96" width="96" alt="">
                                    </div><!-- /.pulse-image -->

                                    <div class="pulse-details">
                                        <h6>YOU</h6>

                                        <p>Alpha Gamma Delta</p>

                                        <p>Event Planner</p>
                                    </div><!-- /.pulse-details -->
                                </div><!-- /.pulse-inner -->
                            </li><!-- /.pulse -->

                            <li class="pulse css-pulse-artist-left">
                                <div class="pulse-inner">
                                    <div class="pulse-image">
                                        <img src="/images/artist/pulses/rumr.jpg" alt="" width="96" height="96">
                                    </div><!-- /.pulse-image -->

                                    <div class="pulse-details">
                                        <h6>R.LUM.R</h6>

                                        <p>Pop, R&B</p>
                                    </div><!-- /.pulse-details -->
                                </div><!-- /.pulse-inner -->
                            </li><!-- /.pulse -->

                            <li class="pulse css-pulse-artist-center">
                                <div class="pulse-inner">
                                    <div class="pulse-image">
                                        <img src="/images/artist/pulses/kyliemorgan.jpeg" alt="" width="96" height="96">
                                    </div><!-- /.pulse-image -->

                                    <div class="pulse-details">
                                        <h6>KYLIE MORGAN</h6>

                                        <p>Country, Pop</p>
                                    </div><!-- /.pulse-details -->
                                </div><!-- /.pulse-inner -->
                            </li><!-- /.pulse -->

                            <li class="pulse css-pulse-artist-right">
                                <div class="pulse-inner">
                                    <div class="pulse-image">
                                        <img src="/images/artist/pulses/mrbthetribalhoose.jpg" alt="" width="96"
                                             height="96">
                                    </div><!-- /.pulse-image -->

                                    <div class="pulse-details">
                                        <h6>MR.B & THE TRIBAL HOOSE</h6>

                                        <p>Funk, Hip-Hop, Pop, Rock</p>
                                    </div><!-- /.pulse-details -->
                                </div><!-- /.pulse-inner -->
                            </li><!-- /.pulse -->
                        </ul><!-- /.pulses -->
                    </div>
                </div>
            </div>
        </div><!-- /.container -->
    </section><!-- /.section -->

    <section id="register" class="section section-sign">
        <img src="/images/temp/girl.jpg" height="691" width="1300" alt="" class="fullsize">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-lg-6 col-md-6 col-xl-6 col-bootstrap-default">
                    <h1>Instant access to amazing artists for your next event</h1>

                    <h3 style="font-size: 22px;">Sign up now. And let the bands come to you.</h3>

                    <div class="form form-sign">
                        <form method="post" id="registration" action="{{ route('ajax_signup') }}">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-xs-6">
                                    <div class="form-group">
                                        <label for="fname" class="form-label">First Name</label>

                                        <div class="form-controls">
                                            <input type="text"
                                                   class="form-control"
                                                   id="fname"
                                                   name="fname"
                                                   placeholder="easy first question"
                                            >
                                        </div><!-- /.form-controls -->
                                    </div><!-- /.form-group -->
                                </div><!-- /.col-sm-6 -->

                                <div class="col-xs-6">
                                    <div class="form-group">
                                        <label for="lname" class="form-label">Last Name</label>

                                        <div class="form-controls">
                                            <input type="text"
                                                   class="form-control"
                                                   id="lname"
                                                   name="lname"
                                                   placeholder="another no-brainer"
                                            >
                                        </div><!-- /.form-controls -->
                                    </div><!-- /.form-group -->
                                </div><!-- /.col-sm-6 -->

                                <div class="col-xs-6">
                                        <span class="help-block" style="margin-top:-20px; margin-bottom: -15px;">
		                                    <strong class="fname-error text-danger"></strong>
		                                </span>
                                </div><!-- /.col-sm-6 -->

                                <div class="col-xs-6">
                                        <span class="help-block" style="margin-top:-20px; margin-bottom: -15px;">
		                                    <strong class="lname-error text-danger"></strong>
		                                </span>
                                </div><!-- /.col-sm-6 -->


                            </div><!-- /.row -->

                            <div class="row">
                                <div class="col-xs-6">
                                    <div class="form-group">
                                        <label for="email" class="form-label">Email</label>

                                        <div class="form-controls">
                                            <input type="email"
                                                   class="form-control"
                                                   id="email"
                                                   name="email"
                                                   placeholder="one you check often"
                                            >
                                        </div><!-- /.form-controls -->
                                    </div><!-- /.form-group -->
                                </div><!-- /.col-sm-6 -->

                                <div class="col-xs-6">
                                    <div class="form-group">
                                        <label for="phone" class="form-label">MOBILE PHONE</label>

                                        <div class="form-controls">
                                            <input type="tel"
                                                   class="form-control"
                                                   id="phone"
                                                   name="phone"
                                                   placeholder="just in case"
                                            >
                                        </div> <!-- /.form-controls -->
                                    </div><!-- /.form-group -->
                                </div><!-- /.col-sm-6 -->

                                <div class="col-xs-6">
                                        <span class="help-block"
                                              style="margin-top:-20px; margin-bottom: -15px; text-align: right">
		                                    <strong class="email-error text-danger"></strong>
		                                </span>
                                </div><!-- /.col-sm-6 -->

                                <div class="col-xs-6">
                                        <span class="help-block" style="margin-top:-20px; margin-bottom: -15px;">
		                                    <strong class="phone-error text-danger"></strong>
		                                </span>
                                </div><!-- /.col-sm-6 -->
                            </div><!-- /.row -->

                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="username" class="form-label">USERNAME</label>

                                        <div class="form-controls">
                                            <input type="text"
                                                   class="form-control"
                                                   id="username"
                                                   name="username"
                                                   placeholder="anything but ‘username’"
                                            >
                                        </div><!-- /.form-controls -->
                                    </div><!-- /.form-group -->
                                </div><!-- /.col-sm-12 -->


                                <div class="col-xs-12" style="margin-top: -8px">
                                        <span class="help-block" style="margin-top:-15px; margin-bottom: -15px;">
		                                    <strong class="username-error text-danger"></strong>
		                                </span>
                                </div><!-- /.col-sm-12 -->

                            </div><!-- /.row -->

                            <div class="row">
                                <div class="col-xs-6">
                                    <div class="form-group ">
                                        <label for="password" class="form-label">PASSWORD</label>

                                        <div class="form-controls">
                                            <input type="password"
                                                   class="form-control"
                                                   id="password"
                                                   name="password"
                                                   placeholder="not your dog's name">
                                        </div><!-- /.form-controls -->

                                    </div><!-- /.form-group -->
                                </div><!-- /.col-sm-6 -->

                                <div class="col-xs-6">
                                    <div class="form-group">
                                        <label for="confirmPass" class="form-label">CONFIRM PASSWORD</label>

                                        <div class="form-controls">
                                            <input type="password"
                                                   class="form-control"
                                                   id="password-confirm"
                                                   name="password_confirmation"
                                                   placeholder="or your cat's">
                                        </div><!-- /.form-controls -->

                                    </div><!-- /.form-group -->
                                </div><!-- /.col-sm-6 -->

                                <div class="col-xs-12">
                                        <span class="help-block" style="margin-top:-15px; margin-bottom: -15px; ">
		                                    <strong class="password-error text-danger"></strong>
		                                </span>
                                </div><!-- /.col-sm-12 -->
                            </div><!-- /.row -->


                            <div class="form-group form-actions">
                                <button type="button" class="btn btn-primary form-btn" data-toggle="modal"
                                        data-target="#planner-signup-agreement">
                                    I’M IN!
                                </button>
                            </div><!-- /.form-actions -->
                        </form>
                    </div><!-- /.form form-sign -->
                </div>

                <div class="col-md-5">
                    <div class="iphone-size">
                        <img src="/images/temp/iphone.png">
                    </div>
                </div>
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section><!-- /.section-sign -->

    @include('layouts._planner_signup_agreement')

    <section class="section section-testimonials section-dark">
        <div class="container">
            <div class="section-head">
                <h1>Event planners and artists love using EVAmore</h1>
                <h3>(We’re pretty sure you will too)</h3>
            </div><!-- /.section-head -->

            <div class="section-body">
                <ul class="testimonials row">
                    <li class="testimonial col-sm-4 col-xs-6 col-bootstrap-default">
                        <div class="testimonial-inner">
                            <div class="testimonial-image" style="background-image: url(/images/clayton_carter.jpeg)">
                            </div><!-- /.testimonial-image -->

                            <div class="testimonial-content">
                                <div class="testimonial-author">
                                    <h4>Clayton Carter</h4>

                                    <h5>
                                        <small>Auburn University•Social Chair</small>
                                    </h5>
                                </div><!-- /.testimonial-author -->

                                <blockquote class="testimonial-entry">“EVAmore makes booking bands and events effortless
                                    with everything right at your fingertips. The customer service is top notch and you
                                    can feel at ease knowing they have your back. If you aren’t using EVAmore you are
                                    definitely missing out.”
                                </blockquote><!-- /.testimonial-entry -->
                            </div><!-- /.testimonial-content -->
                        </div><!-- /.testimonial-inner -->
                    </li><!-- /.testimonial -->

                    <li class="testimonial col-sm-4 col-xs-6 col-bootstrap-default">
                        <div class="testimonial-inner">
                            <div class="testimonial-image" style="background-image: url(/images/kim_leggett.jpeg)">
                            </div><!-- /.testimonial-image -->

                            <div class="testimonial-content">
                                <div class="testimonial-author">
                                    <h4>Kim Leggett</h4>

                                    <h5>
                                        <small>Business Owner•Event Planner</small>
                                    </h5>
                                </div><!-- /.testimonial-author -->

                                <blockquote class="testimonial-entry">“EVAmore provided an amazing band for my pop up
                                    event that is a new favorite of mine! It was so easy to book and I didn’t have to
                                    worry about any of the details. I am already working with EVA on my next event."
                                </blockquote><!-- /.testimonial-entry -->
                            </div><!-- /.testimonial-content -->
                        </div><!-- /.testimonial-inner -->
                    </li><!-- /.testimonial -->

                    <li class="testimonial col-sm-4 col-xs-6 col-bootstrap-default">
                        <div class="testimonial-inner">
                            <div class="testimonial-image" style="background-image: url(/images/aaron_sayre.jpeg)">
                            </div><!-- /.testimonial-image -->

                            <div class="testimonial-content">
                                <div class="testimonial-author">
                                    <h4>Aaron Sayre</h4>

                                    <h5>
                                        <small>Corporate Event Planner•DMC</small>
                                    </h5>
                                </div><!-- /.testimonial-author -->

                                <blockquote class="testimonial-entry">“My job requires me to always know the greatest up
                                    and coming bands. EVAmore’s technology makes it easy for me to quickly find what I
                                    am looking for and pitch only the best options to my clients.”
                                </blockquote><!-- /.testimonial-entry -->
                            </div><!-- /.testimonial-content -->
                        </div><!-- /.testimonial-inner -->
                    </li><!-- /.testimonial -->
                </ul><!-- /.testimonials -->
            </div><!-- /.section-body -->
        </div><!-- /.container -->
    </section><!-- /.section section-testimonials section-dark -->

    <section class="section section-callout section-sky">
        <img src="/images/temp/van-bg.jpg" height="1744" width="2600" alt="" class="fullsize">

        <div class="section-bg">
            <span class="section-bg-car"></span>
        </div><!-- /.section-bg -->

        <div class="container">
            <div class="section-content">
                <h1>Artists: want to play amazing shows and get paid on time?</h1>

                <h3>Our platform keeps payment safe in escrow, then deposits the cash into your account right after the
                    event.</h3>

                <div class="actions">
                    <a href="{{route("artists_registration")}}" class="btn btn-secondary">APPLY NOW TO BECOME AN EVAMORE
                        ARTIST.</a>

                    <p><a href="#" data-toggle="modal" data-target="#popup-login">Already verified?</a></p>
                </div><!-- /.actions -->
            </div><!-- /.section-content -->
        </div><!-- /.container -->
    </section><!-- /.section section-callout -->

@stop

