@extends('layouts.master')

@section('content')


    <div class="intro intro-faq">
        <img src="/images/temp/intro-faq.jpg" height="1199" width="2600" alt="" class="fullsize">

        <div class="container">
            <div class="intro-content">
                <h1>We're so<br />here for you.</h1>

                <h3>Ask a question, or see if it's<br />covered in the <em>FAQs</em> below.</h3>
            </div><!-- /.intro-content -->
        </div><!-- /.container -->
    </div><!-- /.intro intro-faq -->

	<section class="section section-faq" id="support-info-container">
		<div class="container">
            @include('homeviews._support_form')
		</div><!-- /.container -->
	</section><!-- /.section section-faq -->

    @if(Session::has('message'))
        <script>
            $(function(){
                $(document).scrollTop( $("#support-info-container").offset().top );
            })(jQuery);
        </script>
    @endif
@stop