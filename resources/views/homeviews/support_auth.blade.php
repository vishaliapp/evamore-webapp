@extends('layouts.dash')

@section('content')
	<section class="section-faq section-faq-auth" style="background-color: #FFFFFF">
		<div class="container" >
            <div style="padding: 0 30px 30px 30px">
                <h1>Support</h1>
                @include('homeviews._support_form')
            </div>
		</div><!-- /.container -->
	</section><!-- /.section section-faq -->
@stop