<div class="modal fade modal-terms" id="popup-dmca" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header modal-head">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h3>Digital Millennium Copyright Act</h3>
            </div><!-- /.modal-head -->

            <div class="modal-body">
                <h5>Digital Millennium Copyright Act (DMCA)</h5>

                <p>EVAmore, Inc. ("Company") is committed to complying with U.S. copyright and related laws, and requires that you comply with these laws. Owners of copyrighted works who believe that their rights under U.S. copyright law have been infringed may take advantage of certain provisions of the Digital Millennium Copyright Act of 1998 ("DMCA") to report alleged infringements. A copyright owner of any Content believed to be infringed should contact Company immediately to report any concerns of infringement by providing notice to Company’s Designated Agent as required by the DMCA, Title 17 U.S.C. § 512. Such notice must be provided by email to Company at the following address, dmca@evamore.co, but you may also simultaneously send such notice to Company via mail at the following address: EVAmore, Inc. attn.: Designated Copyright Agent, 40 Exchange Pl., Suite 1900, New York, NY 10005. Such notice must include, at a minimum:</p>

                <ul>
                    <li>(a)	a physical or electronic signature of a person authorized to act on behalf of the owner of an exclusive right that is allegedly infringed;</li>

                    <li>(b)	the identification of the copyrighted work claimed to have been infringed;</li>

                    <li>(c)	the identification of the material that is claimed to be infringing or to be the subject of infringing activity and that is to be removed or access to which is to be disabled and information reasonably sufficient to permit us to locate the material;</li>

                    <li>(d)	information reasonably sufficient to permit Company to contact you, such as an address, telephone number, and, if available, an e-mail address;</li>

                    <li>(e)	a statement that you have a good-faith belief that use of the material in the manner complained of is not authorized by the copyright owner, its agent or the law; and</li>

                    <li>(f)	a statement that the information in the notification is accurate and, under penalty of perjury, that you are authorized to act on behalf of the owner of an exclusive right that is allegedly infringed.</li>
                </ul>


                <p>For clarity, only DMCA notices should go to the Designated Copyright Agent; any other general communications should be directed to Company at: support@evamore.co. You acknowledge that if you fail to comply with all of the requirements of this section, your DMCA notice may not be valid.</p>

                <p>If you believe that your Content was removed (or to which access was disabled) is not infringing, or that you have the authorization from the copyright owner or the copyright owner’s agent, or pursuant to the law, to post and use such Content, you may send a DMCA counter-notice to Company’s Designated Copyright Agent containing the following information:</p>

                <ul>
                    <li>(i)	your physical or electronic signature;</li>

                    <li>(ii) the identification of the content that has been removed or to which access has been disabled and the location at which the content appeared before it was removed or disabled;</li>

                    <li>(iii) a statement that you have a good-faith belief that the content was removed or disabled as a result of a mistake or a misidentification of the content, and any relevant proof related to the same; and</li>

                    <li>(iv) your name, address, telephone number and e-mail address, a statement that you consent to the jurisdiction of the federal court in Davidson County, Tennessee, and a statement that you will accept service of process from the person who provided notification of the alleged infringement.</li>
                </ul>

                <p>If a counter-notice is received by Company’s Copyright Agent, Company is permitted to and may send a copy of the counter-notice to the original complaining party informing that person or entity that the removed content may be replaced or no longer disabled in 10 business days. Unless the copyright owner files an action seeking a court order against the content provider or user in the next 14 business days after receipt of the counter notice, the removed content may be replaced, or access to it restored, in Company’s sole discretion.</p>

            </div><!-- /.modal-body -->

            <div class="modal-foot">
                <a href="#" data-dismiss="modal" class="btn btn-fill btn-invert">CLOSE</a>
            </div><!-- /.modal-foot -->
        </div>
    </div>
</div><!-- /.modal -->