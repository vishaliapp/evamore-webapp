<div class="modal fade modal-login" id="popup-login" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header modal-head">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3>LOGIN</h3>
            </div><!-- /.modal-head -->

            <div class="modal-body">
                <div class="form form-login">
                    <form method="post" id="login-form" action="{{ route('ajax_signin') }}">
                        <div class="errors text-danger"></div>
                        {{ csrf_field() }}

                        <input type="hidden" id="to" name="to">
                        <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                            <label for="username_login" class="form-label">USERNAME</label>
                            <div class="form-controls">
                                <input type="text" class="form-control" id="username_login" name="username" placeholder="anything but ‘username’" >
                            </div><!-- /.form-controls -->
                        </div><!-- /.form-group -->

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password_login" class="form-label">PASSWORD</label>

                            <div class="form-controls">
                                <input type="password" class="form-control" id="password_login" name="password" placeholder="not your dog’s name" >
                            </div><!-- /.form-controls -->
                        </div><!-- /.form-group -->

                        <div class="form-actions">
                            <input type="submit" name="submit" value="LOGIN" class="btn btn-fill form-btn">
                            <p><a href="/account/forgot">Forgot Password?</a></p>
                            <p>
                                <a href="#" onClick="gotoRegisterForm();">Sign up to be a planner</a> |
                                <a href="{{route("artists_registration")}}">Apply to become an artist</a>
                            </p>
                        </div><!-- /.form-actions -->
                    </form>

                    <form action="/#register" id="to-register-form"><form>
                </div><!-- /.form form-login -->
            </div><!-- /.modal-body -->
        </div>
    </div>
</div><!-- /.modal -->

<script>
    function gotoRegisterForm() {
        $('#to-register-form').submit();
    }
</script>
