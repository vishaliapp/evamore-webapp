<div class="modal fade modal-terms" id="popup-terms" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header modal-head">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h3>Terms & Conditions</h3>
            </div><!-- /.modal-head -->

            <div class="modal-body">
                <h3>Artist Agreement</h3>
                <h5>EVA's responsibility to the Artist</h5>
                <ul>
                    <li>EVA shall place the Artist on Eva's website.</li>

                    <li>EVA shall manage the agreement made between Artist and any Producer.</li>

                    <li>EVA shall enforce guidelines relating to safety and legality at performances.</li>

                    <li>EVA shall pay Artist for Performances from money received from a Producer, less EVA's fee.</li>
                </ul>
                <h5>Artist's responsibility to EVA and to Producer</h5>
                <ul>
                    <li>Artist shall provide Content for EVA to place on its website.</li>

                    <li>Artist shall comply with the Artist Code of Conduct and the terms of any Performance Agreement.</li>
                </ul>
                <p>EVA's role is to be an online marketplace where buyers of entertainment (the “Producer(s)”) and musical artists (the “Artist(s) or “You”)”, can find each other. EVA will manage a platform whereby Producers and Artists can agree to terms for a live performance (“Performance”) and through which Producers can pay Artists for the Performance.</p>

                <p>In order to be listed as an EVA Artist, on the EVA website (the “Site”), You must upload at least one (1) photograph of Yourself, and provide the biographical information required by the Site.  EVA may also require or permit You to upload high-quality audio or video files of You performing Your music. You hereby grant to EVA a non-exclusive, royalty free right license to use your name, likeness and biographical information, as well as your audio and video files (collectively, Your “Content”), on the Site and in Eva's promotional materials, and to use your name and likeness in association with EVA's trademarks in order to denote You as an “EVA Artist,” until You notify EVA in writing that you no longer wish to be listed on the Site, and for a period of six (6) months thereafter. Prior to the end of such six-month period, EVA shall remove all of Your Content from the Site, but EVA shall have, and You hereby grant EVA, a perpetual non-exclusive license to your name and likeness for archival purposes and to display on any portion of its Site that lists prior Artists or prior Performances.</p>

                <p>You hereby warrant and represent to EVA that all Content you provide to EVA shall be owned by you and shall not infringe upon the intellectual property or other proprietary rights of any third party.  You agree to defend and indemnify EVA against all losses, damages, and costs, including attorneys' fees, incurred by EVA in the event any third party claims that any of the CONTENT infringes upon such party's intellectual property or other proprietary rights.   EVA reserves the right not to post any of Your Content it deems, in its sole discretion, to be offensive, possibly infringing of third party rights, or not of sufficient commercial quality.</p>

                <p>You have the right to refuse any offer of a Performance. In the event that You choose to accept a Performance and enter into a Performance Agreement, You hereby represent and warrant that you will comply with the terms of that Performance Agreement to the best of Your ability.  You agree to comply with the EVA Artist Code of Conduct listed at [insert page on website]. You agree to defend and indemnify EVA against all losses, damages and costs, including attorneys' fees, incurred by EVA as a result of any third party claim arising out of your breach of a Performance Agreement or the EVA Artist Code of Conduct.  If, however, you refuse to provide a Performance which you are obligated to provide under a Performance Agreement because of a demonstrated breach by Producer of the Producer Event Production Policy (available at [insert page on website], you may provide evidence of such breach to EVA and EVA will, in its sole discretion and in good faith, determine whether such indemnification obligations shall apply.</p>

                <p>EVA does not promise You that any Producer will enter into an agreement with You for a Performance. To the extent permitted by law, EVA disclaims all liability to You for any damages or losses You may incur as a result of any Performance or under any agreement with any Producer. EVA's only liability to You is to remit to you any payment EVA actually receives from a Producer for a Performance by You, less EVA's Fee. EVA will endeavor to ensure that all Producers comply with the Event Production Policy; however, EVA assumes no responsibility for the quality, safety or legality of the environment or conditions of any Performance.</p>

                <p>You and EVA are independent contractors, and no agency, partnership, joint venture, employment relationship is intended or created by this Artist Agreement. In addition, EVA is not engaging in or carrying on the occupation of a “Talent Agency” as that term is defined in the California Labor Code section 1700.4. EVA does not procure, offer, promise, or attempt to procure employment or engagements for any Artist.</p>

                <h3>Producer Agreement</h3>
                <h5>EVA's responsibility to Producer</h5>
                <ul>
                    <li>EVA shall provide a marketplace for Producer to choose and engage Artists for Performances at Producer's venue.</li>

                    <li>EVA shall enforce guidelines relating to safety and legality at Performances.</li>

                    <li>EVA shall pay Artist for Performances from money received from a Producer, less EVA's fee.</li>
                </ul>
                <h5>Producer's responsibility to EVA and to Artist</h5>
                <ul>
                    <li>Producer shall comply with the Event Production Policy and the terms of any agreement with an Artist.</li>
                </ul>
                <p>EVA's role is to be an online marketplace where buyers of entertainment (the “Producer(s)” (or “You”)) and musical artists (the “Artist(s))”, can find each other. EVA will manage a platform whereby Producers and Artists can agree to terms for a live performance (“Performance”) and through which Producers pay Artists for the Performance.

                <p>In order to be listed as an EVA Producer, on the EVA website (the “Site”), You must provide contact information, including Your name, e-mail address, and telephone number, (your “Data”), as well as certain information about Your organization and venue for the Performance as required by the Site. EVA will use, store, and share Your Data in accordance with EVA's Privacy Policy.</p>

                <p>You hereby warrant and represent to EVA that You are authorized to enter into agreements on behalf of Your organization, including but not limited to this Producer Agreement and any agreements with Artists. You agree to defend and indemnify EVA against all losses, damages, and costs, including attorneys' fees, incurred by EVA in the event any third party claims arising out of a breach by You of this warranty.</p>

                <p>In the event that You enter into an agreement with an Artist (“Performance Agreement”), You hereby represent and warrant that you will comply with the terms of that Performance Agreement and the Event Production Policy. You further warrant and represent that You will comply with all laws and regulations associated with the production of any Performance, including but not limited to obtaining all required permits and licenses. You agree to defend and indemnify EVA against all losses, damages and costs, including attorneys' fees, incurred by EVA as a result of any third party claim arising out of (a) any actual or alleged breach by You of a Performance Agreement or the Event Production Policy, or (b) any actual or alleged injury to person or property occurring during or as a result of any Performance.  If, however, you refuse to provide a Performance which you are obligated to provide under a Performance Agreement because of a demonstrated breach by Artist of the Artist Code of Conduct (available at [insert page on website], you may provide evidence of such breach to EVA and EVA will, in its sole discretion and in good faith, determine whether such indemnification obligations shall apply.</p>

                <p>EVA does not guarantee the success of any Performance or that any Artist will comply with the Artist Code of Conduct or the terms of any Performance Agreement. To the extent permitted by law, EVA disclaims all liability to You for any damages or losses You may incur as a result of any Performance or under any agreement with any Artist.  In any event, EVA's only liability to You is the amount actually paid by You to Eva. </p>

                <p>You and EVA are independent contractors, and no agency, partnership, joint venture, employment relationship is intended or created by this Producer Agreement. You are solely responsible for all costs and obligations associated with the production of any Performance, including but not limited to, permits and licensing requirements, promotion, marketing, equipment, set-up and clean-up.</p>

            </div><!-- /.modal-body -->

            <div class="modal-foot">
                <a href="#" data-dismiss="modal" class="btn btn-fill btn-invert">CLOSE</a>
            </div><!-- /.modal-foot -->
        </div>
    </div>
</div><!-- /.modal -->