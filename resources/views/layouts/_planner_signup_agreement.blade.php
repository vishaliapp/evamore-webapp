<!-- Modal -->
<div class="modal fade modal-terms" id="planner-signup-agreement" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header modal-head">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3>Terms Of Service</h3>
            </div>
            <div class="modal-body">

                <h5>Introduction</h5>

                <p>This website, www.evamore.co, its sub-domains, and all software, feeds, and services provided therein, or through affiliated third parties (individually and collectively, the “Site”) are published and maintained by EVAmore, Inc., a Tennessee Limited Liability Company (“Company”). Please read the following terms of service (“Terms”) fully and carefully before using the Site. The Terms constitute a legally binding agreement and exclusively governs the use of the Site by users, such as Producers (as defined herein) (“Producer”, “You”, “Your”).</p>

                <p>By accessing, browsing, contributing, viewing, registering, or otherwise using the Site and/or Service (as defined herein), You unconditionally accept and agree to be bound by the Terms. If You do not agree to these Terms, You are not granted permission to enter into or use the Service for any purpose, and You must immediately cease using and leave the Site and Service.</p>

                <p>The Terms are effective as of, and were last updated on, September 16, 2016. Company reserves the right to modify, alter, amend, or update the Terms at any time without notice in Company’s sole and absolute discretion, and such new Terms will immediately take effect upon Company posting such new Terms on Company’s website. You are encouraged to frequently visit this website from time to time to review current Terms. Notwithstanding the foregoing, Company will attempt to notify You in advance of any changes to the Terms. Notice of any new or revised Terms will appear online at www.evamore.co/tos. Your continued use of the Site and/or Service following any changes to the Terms shall be deemed Your acceptance of all changes and Your agreement to be bound by the most current Terms. Any questions, requests for assistance, thoughts, or complaints regarding the Terms can be directed to Company at: support@evamore.co.</p>

                <p>The Terms are written in English (USA). Company may make these Terms available to You in languages other than English through third party translation services, such as Google Translate. Such translations of any materials into a language other than English are intended solely as a courtesy to You and for the convenience to non-English reading users. Company does not warrant or represent to the accuracy of any such translations, and You acknowledge that such translations may contain differences due to the difficulty in translating languages. You acknowledge and agree that the English (USA) version of the Terms found on www.evamore.co/tos shall be the controlling version of the Terms in the event of a discrepancy, and shall exclusively govern the entire relationship between Company and You.</p>

                <p>The Site and/or Service may contain links to third party content, including, without limitation, other websites, for the convenience of visitors or for advertising purposes, which Company has not reviewed. Company linking to third party content does not imply an advertisement or endorsement of any service, product, or otherwise, provided by such third party. Company is not responsible for any third party content linked to or from the Service and expressly disclaims, without limitation, any responsibility for any third party content, the accuracy of any information found on any third party web site, or the quality of products of services provided by or advertised on such third party website. Your use of any third party content is at Your own risk, and subject to the terms and conditions of such third party’s website. Company encourages You to review the terms and conditions and privacy policy of any third party website that You visit.</p>

                <h5>The Service</h5>

                <p>Company provides an online marketplace where buyers of entertainment (the “Producer(s)”) and musical artists (the “Artist(s)”) can find each other and engage in a transaction (the “Service”).  Specifically, Company’s Service involves platform whereby Producers and Artists can agree to terms for a live performance (“Performance”) and through which Producers can pay Artists for the Performance.</p>

                <p>The Service is accessible worldwide, unless otherwise specified by Company. Company makes no representation that the Service is suitable for use in any location. Content (as defined below) may not be available in Your particular country, state or locality, and the reference to any Content within the Service does not imply or warrant that the Content will be available at any time in Your particular location. Those who access the Service do so at their own volition and are responsible for compliance with local laws.</p>

                <h5>Intellectual Property</h5>

                <p><u>Company Content</u></p>

                <p>As between You and Company, Company owns, licenses, or is authorized to use or exploit all audio, video, images, software, text, scripts, artwork, trademarks, service marks, data, proprietary rights, and other materials, including, without limitation, the selection, coordination, arrangement, and organization of all material, found on the Service (the "Content"). For the avoidance of doubt, the foregoing does not imply that Company owns the copyright in or to any user-generated content and Company acknowledges that the author of such user-generated content is the exclusive owner of the same and Company’s rights thereto are a non-exclusive license as contemplated by these Terms.</p>

                <p>The Site, Service, and Content are protected by, without limitation, the United States Copyright Act, the Lanham Act, various international conventions and treaties, and other applicable international copyright, trademark, and intellectual property laws. Third parties and not Company may hold copyright, trademark, and other proprietary rights in and to certain Content. You shall not copy, capture, reproduce, remove, perform, transfer, sell, license, modify, manipulate, create derivative works from or based upon, republish, upload, edit, post, transmit, publicly display, frame, link, distribute, or exploit, in whole or in part, the Site, Service, Content, or user-generated content (such as third party Artist Materials, defined below), unless otherwise explicitly permitted by these Terms or law. Any unauthorized activities that infringe upon the intellectual property rights of Company, or its affiliates (such as third party content providers), is expressly prohibited, and all rights in and to the Content are expressly reserved to the respective owner of such Content. Nothing contained in the Terms should be construed as granting, by implication or otherwise, any license or right to use any Content without the express written permission of Company, or its affiliates. Any unauthorized use of the Content, except as explicitly authorized by these Terms, is strictly prohibited and may subject You to civil liability under copyright laws, trademark laws, the laws of publicity or privacy, and other civil and criminal statutes, rules, or regulations.</p>

                <h5>Accounts</h5>

                <p>In order for You to access some or all of the Service, You must create an EVAmore account (“Account”). You may do this on Company’s website at the following address: www.evamore.co. When creating an Account, You will be directed to where You may review these Terms before You are permitted to register.</p>

                <p>When registering an Account, You must provide Company with accurate and complete information, which may include, but is not limited to, Your legal name, email address, and telephone number (“Data”). Company will use, store, and share Your Data in accordance with Company’s Privacy Policy.</p>

                <p>You are also required to authenticate Your Account through entering a username and password prior to Your use of the Service. You are solely responsible for activity that occurs on Your Account, keeping Your username and password secure, and You must notify Company immediately upon any breach of security or unauthorized use of Your Account. If You use the Service on a public computer, or unprotected mobile device, You acknowledge it is Your responsibility to log out of the Service.</p>

                <p>Through creating an Account, or otherwise using the Service, You authorize Company, from time to time, to electronically communicate with You in order to provide information concerning Your Account or the Service.</p>

                <p>You may cancel Your Account at any time by following instructions found on Your Account page. Termination of Your Account will also terminate Your access to certain Content. Regardless of Account cancellation, You remain liable to Company for all amounts due to Company pursuant to Your use of the Service.</p>

                <p>Company reserves the right to terminate a user’s Account and/or access to the Service if a user is determined, in Company’s sole discretion, to violate the Terms herein, or for any reason whatsoever or for no reason. Regardless of Account termination, You remain liable to Company for all amounts due to Company pursuant to Your use of the Service.</p>

                <p><u>Producer Account – Additional Requirements</u></p>

                <p>In order to be listed as an “EVA Producer” on the Site, You may be required to provide additional information about Your organization and the venue for any Performances, as required by the Service. Photographs of the Producer’s venue may be required as well.</p>

                <h5>Use of Service</h5>

                <p>Subject to Your full compliance with all Terms and full payment of all fees and charges pursuant to Your use of the Service, Company grants You permission to access and use the Service. Company reserves the right, in Company’s sole discretion, to change how it operates the Service at any time for any reason whatsoever. Generally, You may access Content on, and submit Content to, the Service. You are solely responsible for Your use of the Service and for Data, Artist Materials and/or other user-generated content You may submit to the Site.</p>

                <p>Company strives to provide accurate descriptions of all Producers, Artists, and Artist Materials found in or on the Service, and encourages its users to provide for accurate descriptions of all Data and Artist Materials uploaded or submitted to the Service; however, Company does not warrant the descriptions of all user-generated content are complete, error-free, or accurate. Notwithstanding the foregoing, every attempt is made to provide You with complete, error-free, and accurate information. If You believe there are any discrepancies, please contact company at support@evamore.co.</p>

                <p>Company does not endorse, nor offer any opinions, on any Content submitted to the Service by a user or other affiliate, nor does Company endorse or offer any opinions on the content contained. You also understand that by using the Service, You may encounter user-generated content (including Artist Materials) You or the general public may deem offensive, indecent, or objectionable. Company does not review all Content on the Service and is not liable to You for any offensive, indecent, or objectionable Content.</p>

                <p>Company may add, change, discontinue, remove, or suspend user-generated content (including Artist Materials) posted to the Service at any time, without notice to You and without any liability.</p>

                <p>By using the Service, You expressly agree that Company is authorized to, and may, charge You any applicable services fees in connection with Performances, taxes associated therewith, and other charges that may occur through Your use of the Service. Such fees and charges in connection with Performances will be automatically charged to Your Account at the time of the Performance booking, and are non-refundable once the Performance has taken place.  Sometimes, You may pay Company via third party payment services, such as Apple Pay, Paypal or Google Wallet, and You agree to abide by any third party terms of service in regards to such third party payment services. You agree to pay all fees and charges to Company in a timely manner.</p>

                <p>You shall have the option of selecting a payment method within Your Account, and agree Company may store any payment information You supply to Company, including, without limitation, credit card numbers.</p>

                <p>Refunds will be extended in Company’s sole discretion. Company may, from time to time, offer promotional credits, free trials, or rebates. All promotional credits, free trials, or rebates extended by Company, or its affiliates, to You are for Your own personal use and are non-transferrable.</p>

                <p>Company may, from time to time, use various technologies to verify Your compliance with the Terms and You consent to Company using any monitoring or other analogous technology associated with monitoring Your access to the Service.</p>

                <h5>Performances</h5>

                <p>In the event that a Producer and Artist enter into an agreement via the Service regarding a Performance (“Performance Agreement”), You hereby represent and warrant that You will comply with the terms of that Performance Agreement.</p>

                <p>Producers agree to comply with all laws and regulations associated with the production of any Performance, including but not limited to obtaining all required permits and licenses, as applicable. Producers are solely responsible for all costs and obligations associated with the production of any Performance, including but not limited to, permits and licensing requirements, promotion, marketing, equipment, set-up and clean-up.</p>

                <h5>EVA’s Commission</h5>

                <p>In the event an Artist and Producer enter a Performance Agreement, Producer shall pay to Company the fee set forth in the Performance Agreement (“Performance Fee”) plus an administrative fee of ten percent (10%) of the Performance Fee (the “EVA Fee”) (collectively, the “Fee”).  Producer shall submit the Fee (as defined below) to Company via the Site, at the time of execution of the Performance Agreement to which the Fee applies.</p>

                <p>The Performance Fee portion of the Fee shall be held in escrow until the Performance takes place. After the conclusion of the Performance to which the Performance Fee applies, Company shall disburse the Performance Fee.</p>

                <p>The EVA Fee shall be deposited into the Company’s operating account and is non-refundable.</p>

                <h5>Representations and Restrictions:</h5>

                <p>You accept and agree to abide by all Terms herein and are under no disability, or other restriction, which prevents Your ability to enter into, perform in accordance thereof, and comply with all Terms herein, as well as the terms of any and all Performance Agreements into which You enter in Your individual capacity or on behalf of a company, organization, band, or group, as applicable.</p>

                <p>You hereby warrant and represent that You have the authority to enter into agreements on behalf of Your company organization, band, or group, as applicable, including but not limited to these Terms and any Performance Agreements into which You enter via the Service.</p>

                <p>You hereby warrant and represent are at least 18 years of age, and if You are under 18, may not, under any circumstance, create an Account or use the Service without parental supervision.</p>

                <p>Your use of the Service is at entirely Your own risk and the Service is provided for “as is” and “with all faults.” Company shall have no liability to You whatsoever in the event any virus or other harmful component infects, harms, or causes damages to Your computer, cell phone, tablet, or other electronic equipment with which You access or view the Service.</p>

                <p>You hereby warrant and represent to Company that You own any user-generated content You upload or otherwise submit to the Site and Service and that nothing you upload or otherwise submit to the Site and Service shall infringe upon the intellectual property or other proprietary rights of any third party. You warrant and represent that You understand Company’s policy is to respond to notices of alleged copyright infringement and terminate accounts of repeat infringers according to the process set out in the Digital Millennium Copyright Act (DMCA), as set forth at www.evamore.co/dmca.</p>

                <p>You agree that Your use of the Service will not violate any law or regulation, including, without limitation, copyright laws, trademark laws, patent laws, trade secret laws, and publicity and privacy laws, or interfere with any third party's use and enjoyment of Service.</p>

                <p>You warrant and represent that You have all necessary licenses, rights, consents, and permissions to publish any user-generated content You submit to the Service, and that You will not upload or display any user-generated content for any purpose not specifically referenced in the Terms.</p>

                <p>It is strictly prohibited to post or transmit any unlawful, threatening, or infringing material or impersonate any persons while using the Service. Company shall have no liability to You for any inaccurate information provided by a user of the Service to Company. Company shall have no liability to You for any inaccurate information provided by a user of the Service to Company.</p>

                <p>You represent, warrant, and agree that You will not, via the Service or otherwise, cause damage to the Service or impair the availability or accessibility of the Service, in any way which is fraudulent, unlawful, illegal, or harmful, including, without limitation, modifying, adapting, bypassing, or hacking the Service to change, de-crypt, interrupt, destroy, or limit the functionality of Company’s computer software, hardware or telecommunications equipment; upload, post, host, or transmit Submissions, unsolicited emails, “spam” messages, worms, or viruses or any code of a destructive nature;  contact any other visitor or user of the Service for any illicit purpose, or who has requested not to be contacted; access the Service through unpermitted automated means, including, without limitation, via "robots," "spiders," "offline readers" or any other analogous software or code; or attempt to gain unauthorized access to Company’s servers or computer system or engage in any activity that interferes with the performance of, or impairs the functionality of the Service or any services provided by Company, or its affiliates.</p>

                <p>You are responsible for all costs associated with accessing or using the Service, including, without limitation, all Internet connectivity or data transmissions fees from Your own Internet or cellular service provider. Likewise, You are responsible for any system software and/or hardware compatibility requirements for use of the Service. Company does not warrant performance of the Service, or its continuing capability, as to any software or hardware.</p>

                <p>You acknowledge that You may not use the Service in any other way not expressly stated herein or approved in an advance writing by Company. Company explicitly reserves all rights related to the Service not specifically contemplated herein.</p>

                <h5>Diclaimer</h5>

                <p>Company does not warrant that the use of Service will not infringe the rights of any third party, and assumes no responsibility or liability arising from such use, or for any error, defamation, omission, obscenity, or danger, contained within the Service and/or via any Content. Company, and its affiliates, are not responsible for and do not guarantee the accuracy or completeness of anything contained on or within the Service, including links, or advertisements. Company, and its affiliates, reserves the right to immediately change, suspend, remove, or disable the Service for any reason or for no reason, and Company assumes no responsibility, and shall not be liable in any way for any such change to the Content or other change to the Service. Company cannot and does not review all user-generated content (such as Artist Materials) or communications uploaded, made on, or through the Service, but, although not obligated to do so, may review, verify, make changes to or remove any user generated content, including Artist Materials, or other material made available in connection with the Service with or without notice in its sole discretion. The Service, and all other features or functionalities associated with the Service, are made available “as is” and “with all faults” and You understand that temporary interruptions of disturbances of the Service may occur as normal events. Company makes no guarantee that Your use of the Service, and all other features or functionalities associated with the Service, or delivery of any Content will be uninterrupted, interference free, or error free, or be free from any viruses, worms, or other security intrusions. Company, to the fullest extent permissible by applicable law, disclaims all warranties, expressed or implied, written or oral, arising from a course of dealing, performance, usage of trade, or otherwise, including, without limitation, warranties of merchantability, fitness for a particular purpose, and non-infringement.</p>

                <p>Company is not engaging in or carrying on the occupation of a “Talent Agency” as that term is defined in the California Labor Code section 1700.4, and is not engaging in the occupation of a booking agent or booking agency, or in any capacity that would require Company to be licensed as an agent in any state or territory. EVA does not procure, offer, promise, or attempt to procure employment or engagements for any Artist. Artist does not and shall not look to Company to seek or procure or attempt to procure employment for Artist. Artist acknowledges that Company has not offered to act contrary to the provisions of this paragraph, and Company is not expected or authorized to do so. You and Company are independent contractors, and no agency, partnership, joint venture, employment relationship is intended or created by this Agreement.</p>

                <h5>Limitation of Liability</h5>

                <p>To the fullest extent allowable by applicable law, Company, and its affiliates, shall not be liable for any direct, indirect, special, incidental, consequential, exemplary, extra-contractual, or punitive damages of any kind whatsoever, including, without limitation, lost revenues or lost profits, which are in any way related to use of the Service, Content, or other materials available through the Service, regardless of legal theory (including, without limitation, contract, tort, personal injury, property damage, negligence, warranty, or strict liability), whether or not Company, or its affiliates, have been advised of the possibility or probability of such damages, and even if the remedies otherwise available fail of their essential purposes.</p>

                <p>This limitation of liability shall extend to any claims by either party to any Performance Agreement against one another or any third parties, as well as any third party claim arising out of (a) any actual or alleged breach by You of a Performance Agreement or (b) any actual or alleged injury to person or property occurring during or related to any Performance.</p>

                <p>Some states do not allow the limitation or exclusion of liability for incidental or consequential damages, so the above limitation or exclusion may not apply to You.</p>

                <p>Company does not guarantee the success of any Performance, nor does Company guarantee that an Artist will comply with the terms of any specific Performance Agreement.</p>

                <p>To the extent permitted by law, Company disclaims all liability to Producer for any damages or losses Producer may incur as a result of a failed Performance Agreement with any Artist.  In any event of a breach of the Performance Agreement by Artist, Company’s only liability to Producer is the Performance Fee.</p>

                <p>If You are dissatisfied with the Service, or with any of these Terms, or feel Company has breached these Terms, Your sole and exclusive remedy beyond recovering any Performance Fee to which You are entitled is to discontinue using the Service and delete Your Account.</p>

                <h5>Indemnification</h5>

                <p>You agree to indemnify Company, and its affiliates, members, managers, directors, officers, employees, agents, attorneys, contractors and licensors (“Indemnified Parties”) against all claims, actions, suits, and other proceedings (“Claims”) arising out of or incurred in connection with Your use of the Service, Your violation of the Terms, Your breach of a Performance Agreement, or Your violation of law (such as the violation of a third party’s copyright rights) and shall indemnify and hold the Indemnified Parties harmless from and against all judgments, losses, liabilities, damages, costs, and expenses (including reasonable attorneys’ fees and legal costs) arising out of or incurred in connection with such Claims. This indemnification obligation shall survive these Terms and Your use of the Service. Company reserves the right to take over the exclusive defense of Claims for which Company is entitled to indemnification, and in such an event, You shall provide reasonable cooperation to Company.</p>

                <p>Without limiting the foregoing, You agree to defend and indemnify Company against all losses, damages and costs, including attorneys’ fees, incurred by Company as a result of any third party claim arising out of Your breach of a Performance Agreement.</p>

                <h5>Force Majeure</h5>

                <p>You understand and agree that Company is not liable for any failure of performance due to any cause beyond its control, including, without limitation, acts of God, fire, explosion, vandalism, terrorism, weather disturbances, national emergencies, riots, wars, labor difficulties, supplier failures, shortages, breaches, action or request by any government, suspension of existing service in compliance with state or federal law, rule, or regulations.</p>

                <h5>Entire Agreement</h5>

                <p>The Terms represent the entire understanding between Company and You, superseding all prior agreements (including previous versions of the Terms), whether oral or written, with respect to Your use of the Service, Content, and all other subject matter contained herein. The Terms shall be binding upon and inure to the benefit of Company and Your respective assigns, successors, heirs, and legal representatives, including, without limitation, any third party that acquires all or part of Company. The Terms cannot be modified or amended, except as expressly provided for herein. The Terms are personal to You and are not assignable, transferable, or sub-licensable by You except with Company’s prior written consent. Company may freely assign the Terms. If any part of the Terms is deemed by a court of law to be void, voidable, illegal, or unenforceable, the remainder of the Terms will remain in full effect as if such void, voidable, illegal, or unenforceable part had not existed.  The waiver by either party hereto of a breach of any of the provisions of the Terms by the other party hereto shall not be construed as a waiver by the non-breaching party of any subsequent breach by the breaching party.  Headings are inserted for convenience only and are not intended to be part of or to affect the meaning or interpretation of the Terms. All provisions of these Terms which by their nature should survive termination or expiration, shall survive termination or expiration.</p>

                <h5>Choice of Law</h5>

                <p>By accessing, viewing, or using the Service, You consent and agree that: (i) the Terms will be exclusively governed by the laws of the State of Tennessee applicable to contracts entered into and performed within the State of Tennessee and notwithstanding any conflict of law principles; (ii) You accept service of process by personal delivery or mail; and (iii) You irrevocably waive the right to trial by jury and any jurisdictional and venue defenses otherwise available. Any dispute related to the Terms shall be exclusively resolved in the state and federal courts located in Davidson County, Tennessee. Any alleged claim or cause of action You may have with respect to Your use of the Service must be commenced within one (1) year after the alleged claim or cause of action arises. All rights and remedies are cumulative and shall in no way affect any remedy available to either party under equity or law. You agree that any violation or breach of the Terms by You will result in irreparable harm to Company, that monetary damages will be inadequate, and You hereby agreed Company shall be entitled to seek injunctive relief.</p>

                <h5>Notice</h5>

                <p>All legal notices pursuant to the Terms shall be in writing and shall be given by email to Company at: support@evamore.co and via mail to EVAmore, Inc., P.O. Box 120891, Nashville, TN 37212. By using the Service, You agree that any notice due under the Terms that Company sends You electronically will satisfy any legal communication or notification requirement.</p>

            </div>
            <div class="modal-footer" style="text-align: center">
                <button type="button" class="btn btn-danger" data-dismiss="modal">DISAGREE</button>
                <button type="button" class="btn btn-fill form-btn" data-dismiss="modal" onclick="$('#registration').submit()">AGREE</button>
            </div>
        </div>
    </div>
</div>