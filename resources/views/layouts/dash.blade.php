<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"/>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="_token" content="{{ csrf_token() }}">
    <title>EVAmore</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
    <link rel="shortcut icon" type="image/x-icon" href="/images/favicon.ico"/>
    <link href='{{asset("//fonts.googleapis.com/css?family=Lato:400,300,300italic,700")}}' rel='stylesheet' type='text/css'>
    <link href='{{asset('//fonts.googleapis.com/css?family=Open+Sans:300,400,400italic,600,700')}}' rel='stylesheet' type='text/css'>
    <link href='https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css' rel='stylesheet' type='text/css'>
    <link rel="Stylesheet" type="text/css" href="{{asset('demo/prism.css')}}" />
    {{--        <link rel="Stylesheet" type="text/css" href="{{asset('bower_components/sweetalert/dist/sweetalert.css')}}" />--}}
    <link rel="Stylesheet" type="text/css" href="{{asset('croppie.css')}}" />
    <link rel="Stylesheet" type="text/css" href="{{asset('demo/demo.css')}}" />
    <link rel="icon" href="{{asset('//foliotek.github.io/favico-64.png')}}" />
    
    <!-- Vendor Styles -->
    <link rel="stylesheet" href="{{asset("/addons/bootstrap/css/bootstrap.min.css", null, false)}}" type="text/css" media="all"/>
    <link rel="stylesheet" href="{{asset("/addons/bootstrap/css/bootstrap-select.min.css", null, false)}}" type="text/css" media="all"/>
    <link rel="stylesheet" href="{{asset("/addons/formstone/dropdown.css", null, false)}}" type="text/css" media="all"/>
    <link rel="stylesheet" href="{{asset("/addons/bootstrap-datepicker/bootstrap-datepicker.min.css", null, false)}}" type="text/css"
          media="all"/>
    <link rel="stylesheet" href="{{asset("/css/jquery-ui-bootstrap-master/jquery.ui.theme.css", null, false)}}" type="text/css" media="all"/>

    <!-- App Styles -->
    <link rel="stylesheet" href="{{asset( '/css/style.css' )}}"/>
    <link rel="stylesheet" href="{{asset("/bower_components/components-font-awesome/css/font-awesome.min.css", null, false)}}" type="text/css" media="all"/>
    <link rel="stylesheet" href="{{ asset('new-assets/dropzone/dropzone.css') }}">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <![endif]-->
    <script src="{{asset("/js/underscore-min.js", null, false)}}"></script>

    <!-- Vendor JS -->
    <script src="{{asset("/addons/moment-with-locales.js", null, false)}}"></script>
    <script src="{{asset("/addons/jquery-1.11.3.min.js", null, false)}}"></script>
    <script src="{{asset("/addons/jquery.form.min.js", null, false)}}"></script>
    <script src="{{asset("/addons/bootstrap/js/bootstrap.min.js", null, false)}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    <script src="{{asset("/addons/formstone/core.js", null, false)}}"></script>
    <script src="{{asset("/addons/formstone/touch.js", null, false)}}"></script>
    <script src="{{asset("/addons/formstone/dropdown.js", null, false)}}"></script>
    <script src="{{asset("/addons/modernizr.custom.js", null, false)}}"></script>
    <script src="{{asset("/addons/classie.js", null, false)}}"></script>
    <script src="{{asset("/addons/bootstrap-datepicker/bootstrap-datepicker.min.js", null, false)}}"></script>
    <script src="{{asset("/addons/bootstrap/js/bootstrap-select.min.js", null, false)}}"></script>

    <!-- App JS -->
    <script src="{{asset(file_exists('/js/functions.min.js') ? '/js/functions.min.js' : '/js/functions.js')}}"></script>
    <script src="{{asset(file_exists('/js/constants.min.js') ? '/js/constants.min.js' : '/js/constants.js')}}"></script>
    @yield('js')
    @yield('css')
		@include('widgets.google_tagmanager')
</head>

@php
    $currentUser = Auth::user();
    $isArtist = $currentUser->hasRole('ARTIST');
    $isAdmin = $currentUser->hasRole('ADMIN');
    $isArtistManager = $currentUser->hasRole('ARTIST_MANAGER');
@endphp

<body>
<div class="wrapper wrapper-logged wrapper-dark">
    <section class="sidebar">
        <div class="sidebar-container clear">
            <a class="navbar-brand" href="/">Evamore</a>

            <div class="sidebar-inner visible-xs">
                <div class="user">
			<span class="user-avatar">
				<img src="<?=Image::url('uploads/avatars/' . $currentUser->avatar, 39, 39, array('crop'))?>" height="39"
                     width="39" alt="">
			</span><!-- /.user-avatar -->

                    <ul class="user-dropdown">
                        <li>
                            <a href="{{ ($isArtist) ? route('editartist', ['id'=>$currentUser->id]) : route('edit_account', ['id'=>$currentUser->id]) }}">SETTINGS</a>
                        </li>

                        <li>
                            <a href="{{ route('logout') }}">LOGOUT</a>
                        </li>
                    </ul>
                    <!-- /.user-dropdown -->
                </div>
                <!-- /.user -->

                <button type="button" class="navbar-toggle collapsed" id="trigger-overlay" data-toggle="collapse"
                        data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <!-- /.visible-xs -->

            <div class="collapse navbar-collapse navbar" id="bs-example-navbar-collapse-1">
                <?php $left_menu_selected = isset($left_menu_selected) ? $left_menu_selected : '' ?>
                <ul class="sidebar-nav">
		   @if(!$isArtistManager && !$isArtist && !$currentUser->hasRole('ADMIN'))
			
                <li class="{{ ($left_menu_selected == 'dashboard')? 'active' : '' }}">
                        <a href="/dashboard">
                            <span class="sidebar-nav-ico">
<i class="fa fa-arrows ico-arrows" aria-hidden="true"></i>
                            </span>
                            <span>Dashboard</span>
                        </a>
                    </li>
		@endif
                    <li class="{{ ($left_menu_selected == 'upcoming')? 'active' : '' }}">
                        <a href="/events/upcoming">
                            <span class="sidebar-nav-ico">
                                <i class="ico-calendar"></i>
                            </span>
                            @if($isAdmin)
                                <span>UPCOMING EVENTS</span>
                            @elseif(!$isAdmin)
                                    <span>MY EVENTS</span>
                                @endif
                            @if(!$isArtist)
                                <span class="sidebar-nav-count">@currentUserEventsCount()</span>
                            @endif
                        </a>
                    </li>
                        <li class="{{ ($left_menu_selected == 'past')? 'active' : '' }}">
                            <a href="/events/past">
                                <span class="sidebar-nav-ico">
                                  <i class="ico-past"></i>
                                </span>
                                <span>PAST EVENTS</span>
                            </a>
                        </li>
                    @if($isArtistManager)
                        <li class="{{ ($left_menu_selected == 'artists')? 'active' : '' }}">
                            <a href="{{ route('manager_artists') }}">
                                <span class="sidebar-nav-ico"><i class="ico-note"></i></span>
                                <span>MY ARTISTS</span>
                            </a>
                        </li>
                    @elseif(!$isArtist)
                        <li class="{{ ($left_menu_selected == 'artists')? 'active' : '' }}">
                            <a href="{{ route('artistsearch') }}">
                                <span class="sidebar-nav-ico"><i class="ico-note"></i></span>
                                <span>ARTISTS</span>
                            </a>
                        </li>
                    @endif

                    <li class="{{ ($left_menu_selected == 'support')? 'active' : '' }}">
                        <a href="{{ route('support') }}">
                    <span class="sidebar-nav-ico">
                        <i class="ico-support"></i>
                    </span>
                            <span>Support</span>
                        </a>
                    </li>

                    @if($currentUser->hasRole('ADMIN'))

                        <li class="{{ ($left_menu_selected == 'user_management')? 'active' : '' }}">
                            <a href="{{route('cpanel_users_index')}}">
                                <span class="glyphicon glyphicon-user" style="font-size:21px;"></span>
                                <span>User Management</span>
                            </a>
                        </li>
                        <li class="{{ ($left_menu_selected == 'artists_payments')? 'active' : '' }}">
                            <a href="{{route('cpanel_payments_page')}}">
                                <span class="glyphicon glyphicon-usd" style="font-size:21px;"></span>
                                <span>PAYMENT SCHEDULING</span>
                            </a>
                        </li>
                    @endif
                </ul>
                <!-- /.sidebar-nav -->
            </div>
        </div>
        <!-- /.sidebar -->

    </section>
    <!-- /.sidebar -->

    <div class="main">
        <div class="container">
            <div class="main-head clear">
                <div class="main-head-inner">
                    @if(!$isArtist)
                        @if($currentUser->hasRole('ADMIN'))
                            <p><span id="upcomingEvents">@upcomingEventsCount()</span> Upcoming events for all users</p>
                        @else
                            <p>You have <span id="upcomingEvents">@currentUserEventsCount()</span> upcoming event(s)</p>
                        @endif
                    @endif
                </div>
                <!-- /.main-head-inner -->

                <div class="user">
                    <span class="arrow-down glyphicon glyphicon-triangle-bottom"></span>
                    <span class="user-avatar">
						<img src="<?=Image::url('uploads/avatars/' . $currentUser->avatar, 39, 39, array('crop'))?>"
                             height="39" width="39" alt="">
					</span><!-- /.user-avatar -->

                    <span class="user-name">{{ $currentUser->username }}</span>

                    <ul class="user-dropdown">
                        <li>
                            <a href="{{ ($isArtist) ? route('editartist', ['id'=>$currentUser->id]) : route('edit_account', ['id'=>$currentUser->id]) }}">SETTINGS</a>
                        </li>

                        <li>
                            <a href="/logout">LOGOUT</a>
                        </li>
                    </ul>
                    <!-- /.user-dropdown -->
                </div>
                <!-- /.user -->
            </div>
            <!-- /.main-head -->

            @yield('content')

        </div>
        <!-- /.container -->
    </div>
    <!-- /.main -->
    <footer class="footer">
        <div class="container">
            <div class="socials">
                <ul>
                    <li>
                        <a target="_blank" href="http://www.facebook.com/evamoreco">
                            <i class="ico-facebook"></i>
                        </a>
                    </li>

                    <li>
                        <a target="_blank" href="http://www.twitter.com/evamoreco">
                            <i class="ico-twitter"></i>
                        </a>
                    </li>

                    <li>
                        <a target="_blank" href="http://www.instagram.com/evamoreco">
                            <i class="ico-instagram"></i>
                        </a>
                    </li>
                </ul>
            </div>
            <!-- /.socials -->

            <p class="footer-links">
                <a href="{{ route('support') }}">Support</a><span>•</span>
                <a href="#" data-toggle="modal" data-target="#popup-terms">Terms &amp; Conditions</a><span>•</span>
                <a href="#" data-toggle="modal" data-target="#popup-privacy-policy">Privacy Policy</a><span>•</span>
                <a href="#" data-toggle="modal" data-target="#popup-dmca">DMCA</a>
            </p><!-- /.footer-links -->

            <p class="copyright">© EVAmore {{date('Y')}}</p><!-- /.copyright -->
        </div>
        <!-- /.container -->
    </footer>
    <!-- /.footer -->

@include('layouts._modal_terms_and_conditions')
@include('layouts._modal_privacy_policy')
@include('layouts._modal_dmca')

<!-- Modal -->
    <div class="modal fade" id="baseModal" >
        <div class="modal-dialog" role="document">
            <button type="button" class="close outer" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <div class="modal-content"></div>
        </div>
    </div>
</div>

<!-- /.wrapper -->
@yield('footerjs')

</body>
</html>

