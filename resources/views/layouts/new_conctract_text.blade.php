
<p>
    2) Both event planner and artist agree and understand that their mutual relationship exists because of EVAmore. As such, you shall not enter into any transactions, agreements or undertakings with any Event Planner or Artist identified or discovered via EVAmore, if doing so would bypass, compete with, avoid, or circumvent EVAmore or seek or attempt to do so. If you breach this clause, you shall immediately pay EVAmore all fees and charges which would customarily be due to EVAmore had the transaction taken place via the EVAmore platform.
</p><p>
    3) ARTIST shall perform under this agreement with the highest standard of care applicable and in full compliance with the highest professional standards applicable. In the event that EVENT PLANNER provides equipment to ARTIST, ARTIST shall promptly return such equipment promptly after rendering the Services.
</p><p>
    4) Each party represents and warrants that it has the right and authority to enter into this Agreement, and that by entering into this Agreement, it will not violate, conflict with, or cause a material default under any other contract, agreement, lien, encumbrance, or undertaking to which it is a party or by which it may become subject. Each party shall, at its own expense, make, obtain, and maintain in force at all times during the term of this Agreement, all applicable filings, registrations, reports, licenses, permits, and authorizations necessary to perform its obligations under this Agreement. Each party shall, at its own expense, comply with all laws, regulations, and other legal requirements that apply to it and this Agreement. Each party shall indemnify and hold the other party harmless, and will defend the other party against any and all loss or liability, arising from a breach or alleged breach of this agreement.
</p><p>
    5) ARTIST grants to EVENT PLANNER, and EVENT PLANNER’s licensees, the right to use ARTIST’s, and/or ARTIST’s members, name(s) (including trademarks), likeness(es), and biography(s), to advertise, market, and promote the Performance and the Event. ARTIST also grants to EVENT PLANNER, and EVENT PLANNER’s licensees, the right to record, photograph, and otherwise reproduce ARTIST’s Performance at the Event.  To the extent that ARTIST owns or controls any musical composition or master recording (each, a “Work”) used in the Performance, ARTIST grants to EVENT PLANNER an irrevocable worldwide gratis license to use the Work(s) in connection for self-promotional purposes or for non-commercial purposes.
</p><p>
    6) In the event ARTIST does not perform the Services by reason of illness or other incapacity, or if the Event or Performance is prevented, rendered impossible or materially frustrated by any act, requirement or regulation or action of any public authority or bureau, strike or labor difficulties, flood, fire, abnormally severe weather conditions, civil tumult, effects of energy use restrictions, emergencies, lockout or other labor dispute, act of God, absence of power or other essential services, failure of technical facilities or failure or delay of transportation facilities, or any other cause beyond EVENT PLANNER’s reasonable control (each a “Force Majeure Event”), then there shall be no claim for damages by any party to this Agreement and each party’s obligations hereunder as to such Event or Performance shall be waived.
</p><p>
    7) EVENT PLANNER shall obtain and maintain its own insurance policies covering its activities at and in connection with the Event, including commercial liability and property damage, with deductibles and limits at coverage amounts in accordance with current industry custom and practice.
</p><p>
    8) The Rider sets forth ARTIST’s minimum requirements to perform. EVENT PLANNER agrees to furnish at EVENT PLANNER’s sole cost and expense all items set forth in such Rider as well as anything else reasonably required for the Performance which is the subject of this Agreement. 
</p><p>
    9) Each party acknowledges that (i) EVAmore, Inc. is merely the online platform through which EVENT PLANNER and ARTIST are transacting business, (ii) that EVAmore not a party to this Agreement, (iii) that EVAmore is not an “agent” for Artist, (iv) that EVAmore, Inc. assumes no liability hereunder, and (iv) that EVENT PLANNER and ARTIST acknowledge nothing herein shall be deemed to override the terms and conditions for use of the EVAmore, Inc. platform set forth at www.evamore.co.
</p><p>
    10) ARTIST may not assign this agreement to any third party. This agreement may not be modified unless agreed to in writing by both parties. ARTIST’s failure to perform any of the terms of this agreement will be considered a breach and ARTIST shall forfeit the Fee. ARTIST is not permitted to seek any injunctive relief hereunder and may only seek monetary damages in the event of a breach by EVENT PLANNER. ARTIST shall perform the Services as an independent contractor. Nothing contained in this Agreement shall be deemed to create an agency, joint venture, partnership, or franchise relationship between the parties. ARTIST will perform the Services on a non-exclusive basis and may accept other engagements during the term of this agreement.
</p>
<p> READ, UNDERSTOOD, ACCEPTED, AND AGREED:</p>
<table style="font-size: {{ isset($isModal) ? "0.6em" : "1em"  }}" border="0" width="100%" cellspacing="0">
    <tr>
        <td style="padding: 5px; width: 40%" valign="top" align="left">
            <table border="0" width="100%" cellpadding="5" cellspacing="0">
                <tr>
                    <td style="padding: 5px; width: 35%" valign="top" align="right">
                        ARTIST:
                    </td>
                    <td style="padding: 5px; border-bottom: 1px solid;" valign="top" align="left">
                        {!!isset($artist->user) ? $artist->user->name : $artist->user->name!!}
                    </td>
                </tr>
                <tr>
                    <td style="padding: 5px; width: 35%" valign="top" align="right">
                        Date:
                    </td>
                    <td style="padding: 5px; border-bottom: 1px solid;" valign="top" align="left">
                        {{ $event->created_at }}
                    </td>
                </tr>
            </table>
        </td>
        <td style="padding: 5px; width: 60%" valign="top" align="left">
            <table border="0" width="100%" cellspacing="0">
                <tr>
                    <td style="padding: 5px; width: 35%" valign="top" align="right">
                        EVENT PLANNER
                    </td>
                    <td style="padding: 5px; border-bottom: 1px solid;" valign="top" align="left">
                        {!!isset($event->user) ? $event->user->name : $event->user->name !!}
                    </td>
                </tr>
                <tr>
                    <td style="padding: 5px; width: 35%" valign="top" align="right">&nbsp;</td>
                    <td style="padding: 5px; border-bottom: 1px solid;" valign="top" align="left">
                        {{ ! isset($isModal) && isset($request_json['signature']) ? $request_json['signature'] : ''}}
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
