{{--<script type="text/template" id="performance-contract-1">--}}

<meta name="csrf-token" content="{{ csrf_token() }}"/>
<div>

    <div class="modal-header modal-head performance-contract">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        <h3>Performance Contract</h3>
        <div>
            <ul class="pay-steps">
                <li class="active step-n">REVIEW<br/><span>1</span></li>
                <li class="active">
                    <div></div>
                </li>
                <li class="active step-n">CONTRACT<br/><span>2</span></li>
                <li>
                    <div></div>
                </li>
                <li class="step-n">PAYMENT<br/><span>3</span></li>
            </ul>
        </div>
    </div><!-- /.modal-head -->
    <form>
        <div class="modal-body text-center performance-contract">
            <div class="sub-title">The following is agreed between <a href="/account/{{ $event->user->id }}/edit"
                                                                      target="_blank">{{ $event->user->name }}</a> and
                <a href="/artists/{{ $artist->user->username }}" target="_blank">{{ $artist->user->name }}</a>, for good
                and
                valuable consideration, as of the dates below:
            </div>

            <div class="contract-content text-left pre-scrollable" style="overflow-x: hidden">
                <div class="row ">
                    <div class="col-xs-12 col-sm-12 col-lg-12 col-md-12 col-xl-12">
                        <h4 class="text-center">
                            1) Performance Details (the "Performance(s)"):
                        </h4>
                    </div>
                </div>
                <div class="row ">
                    <div class="col-xs-12 col-sm-12 col-lg-4 col-md-4 col-xl-4 text-right">
                        <label class="label-form-control" for="form-planer-name">Event Planner:</label>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-lg-8 col-md-8 col-xl-8">
                        <input id="form-planer-name" readonly="readonly" type="text"
                               class="form-control form-control-default" value="{{ $event->user->name }}">
                    </div>
                </div>
                <div class="row ">
                    <div class="col-xs-12 col-sm-12 col-lg-4 col-md-4 col-xl-4 text-right">
                        <label class="label-form-control" for="form-artist-name">Event Artist:</label>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-lg-8 col-md-8 col-xl-8">
                        <input id="form-artist-name" readonly="readonly" type="text"
                               class="form-control form-control-default" value="{{ $artist->user->name }}">
                    </div>
                </div>
                <div class="row ">
                    <div class="col-xs-12 col-sm-12 col-lg-4 col-md-4 col-xl-4 text-right">
                        <label class="label-form-control" for="form-event-name">Event:</label>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-lg-8 col-md-8 col-xl-8">
                        <input id="form-event-name" readonly="readonly" type="text"
                               class="form-control form-control-default" value="{{ $event->name }}">
                    </div>
                </div>
                <div class="row ">
                    <div class="col-xs-12 col-sm-12 col-lg-4 col-md-4 col-xl-4 text-right">
                        <label class="label-form-control" for="form-venue">Venue:</label>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-lg-8 col-md-8 col-xl-8">
                        <input id="form-venue" readonly="readonly" type="text" class="form-control form-control-default"
                               value="{{ $event->venue }}">
                    </div>
                </div>
                <div class="row ">
                    <div class="col-xs-12 col-sm-12 col-lg-4 col-md-4 col-xl-4 text-right">
                        <label class="label-form-control" for="form-city-state">City, State:</label>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-lg-8 col-md-8 col-xl-8">
                        <input id="form-city-state" readonly="readonly" type="text"
                               class="form-control form-control-default" value="{{ $event->sity }}, {{ $event->sity }}">
                    </div>
                </div>
                <div class="row ">
                    <div class="col-xs-12 col-sm-12 col-lg-4 col-md-4 col-xl-4 text-right">
                        <label class="label-form-control" for="form-event-date">Event Date:</label>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-lg-8 col-md-8 col-xl-8">
                        <input id="form-event-date" readonly="readonly" type="text"
                               class="form-control form-control-default" value="{{ $event->starttime }}">
                    </div>
                </div>
                <div class="row ">
                    <div class="col-xs-12 col-sm-12 col-lg-4 col-md-4 col-xl-4 text-right">
                        <label class="label-form-control" for="form-event-price">Total Payment:</label>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-lg-8 col-md-8 col-xl-8">
                        <input id="form-event-price" readonly="readonly" type="text"
                               class="form-control form-control-default" value="${{ $bid->price }}">
                    </div>
                </div>
                <div class="row ">
                    <div class="col-xs-12 col-sm-12 col-lg-4 col-md-4 col-xl-4 text-right">
                        <label class="label-form-control" for="form-duration">Set Duration:</label>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-lg-8 col-md-8 col-xl-8">
                        <input id="form-duration" readonly="readonly" type="text"
                               class="form-control form-control-default"
                               value="{{ $event->duration }} min">
                    </div>
                </div>
                <div class="row ">
                    <div class="col-xs-12 col-sm-12 col-lg-4 col-md-4 col-xl-4 text-right">
                        <label class="label-form-control" for="form-load-in-time">Load In Time:</label>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-lg-8 col-md-8 col-xl-8">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-lg-4 col-md-4 col-xl-4">
                                <label for="">
                                    <input name=load_in_time_hours id="load_in_time_hours"
                                           value="{{ $event->splitStartDate()['load_in_time_hours'] }}" type="text"
                                           class="form-control form-control-default">
                                </label>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-lg-4 col-md-4 col-xl-4">
                                <label for="">
                                    <input name="load_in_time_minutes" id="load_in_time_minutes"
                                           value="{{ $event->splitStartDate()['load_in_time_minutes'] }}" type="text"
                                           class="form-control form-control-default">
                                </label>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-lg-4 col-md-4 col-xl-4">
                                <select name="load_in_time_ampm" id="" class="form-control form-control-default">
                                    <option {{ $event->splitStartDate()['load_in_time_ampm'] == 'AM' ? 'selected' : '' }} value="AM">
                                        AM
                                    </option>
                                    <option {{ $event->splitStartDate()['load_in_time_ampm'] == 'PM' ? 'selected' : '' }} value="PM">
                                        PM
                                    </option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row ">
                    <div class="col-xs-12 col-sm-12 col-lg-4 col-md-4 col-xl-4 text-right">
                        <label class="label-form-control" for="form-load-in-time">Load Out Time:</label>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-lg-8 col-md-8 col-xl-8">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-lg-4 col-md-4 col-xl-4">
                                <label for="">
                                    <input name=load_out_time_hours id="load_out_time_hours"
                                           value="{{ $event->splitStartDate()['load_out_time_hours'] }}" type="text"
                                           class="form-control form-control-default">
                                </label>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-lg-4 col-md-4 col-xl-4">
                                <label for="">
                                    <input name="load_out_time_minutes" id="load_out_time_minutes"
                                           value="{{ $event->splitStartDate()['load_out_time_minutes'] }}" type="text"
                                           class="form-control form-control-default">
                                </label>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-lg-4 col-md-4 col-xl-4">
                                <select name="load_out_time_ampm" id="" class="form-control form-control-default">
                                    <option {{ $event->splitStartDate()['load_in_time_ampm'] == 'AM' ? 'selected' : '' }} value="AM">
                                        AM
                                    </option>
                                    <option {{ $event->splitStartDate()['load_in_time_ampm'] == 'PM' ? 'selected' : '' }} value="PM">
                                        PM
                                    </option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                @include('layouts.new_conctract_text')
            </div>
            <br>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-lg-12 col-md-12 col-xl-12 text-muted text-left">
                    <small>
                        Please Note! By typing your name in the field below, you agree to the contract as stated
                        above.
                    </small>
                </div>
            </div>

            <div class="form-horizontal">
                {{csrf_field()}}
                <input type="hidden" name="bid_id" value="{{ $bid->id }}"/>
                <div class="form-group">
                    <label for="signatureName" class="col-sm-2 control-label"></label>
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-lg-12 col-md-12 col-xl-12">
                            <input type="text" class="form-control" id="signatureName" name="signature"
                                   placeholder="Name">
                        </div>
                    </div>
                    <div class="text-danger error-message error-signature"></div>
                    <div class="text-danger error-message error-global"></div>
                </div>
            </div>


        </div><!-- /.modal-body -->

        <div class="modal-foot" style="background-color: #f5f5f5; border: none">
            <button type="button" class="btn btn-danger btn-cancel" data-dismiss="modal">Cancel</button>
            <button id="contractAgree" class="btn btn-fill form-btn btn-agree">I agree</button>
        </div><!-- /.modal-foot -->
</div>
</form>
</div>

{{--</script>--}}
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('#contractAgree').on('click', function (e) {
        e.preventDefault()
        $.ajax({
            url: '{{ route('api_forward_confirm_contract') }}',
            method: 'post',
            data: {
                bid_id: '{{ $bid->id }}',
                signature: $('#signatureName').val()
            },
            success: function (response) {
                if ($('#signatureName').val() != '') {

                    $('#secondStepContract').fadeOut(800)

                    $('#thirdStepPay').fadeIn(1000)
                }

            }
        })
    })
</script>
@section('js')
    @parent
@endsection
@section('css')
    @parent
@endsection
