    <div class="modal-bid-payment-form">
            {{ csrf_field() }}
            <input type="hidden" name="bid_id" value="{{ $bid->id }}">
            <div class="modal-header">
                <h3>Payment & Confirmation</h3>
                <div>
                    <ul class="pay-steps">
                        <li class="active step-n">REVIEW<br/><span>1</span></li>
                        <li class="active">
                            <div></div>
                        </li>
                        <li class="active step-n">CONTRACT<br/><span>2</span></li>
                        <li class="active">
                            <div></div>
                        </li>
                        <li class="active step-n">PAYMENT<br/><span>3</span></li>
                    </ul>
                    Good News! {{ $artist->user->name }} has offered to play in {{ $event->name }}
                </div>
            </div>
            <div class="modal-body">
                <div class="error-message error-global"></div>

                <div class="for-support">Prefer to pay check? Please contact us at <br/><a
                            href="mailto:{{config('mail.support_email')}}">{{config('mail.support_email')}}</a></div>
                <div>
                    <table>
                        <tr>
                            <td colspan="2">
                                <div class="row pay-amount-selector">
                                    <div class="col-12 col-xs-12">
                                        <span class="error-message error-payAmount"></span>
                                    </div>
                                    <div class="@if(!$bid->order || $bid->order->status == 0) col-6 col-xs-6 @else col-12 col-xs-12 @endif">
                                        <input type="radio" name="payAmount"
                                               value="{{ App\ModelType\BidType::HALF_AMOUNT }}" id="payAmount-1"
                                               checked/> <label for="payAmount-1">50%</label>
                                    </div>

                                    @if(!$bid->order || $bid->order->status == 0)
                                        <div class="col-6 col-xs-6">
                                            <input type="radio" name="payAmount"
                                                   value="{{ App\ModelType\BidType::FULL_AMOUNT }}" id="payAmount-2"/>
                                            <label for="payAmount-2">100%</label>
                                        </div>
                                    @endif
                                </div>
                            </td>
                        </tr>

                        {{--<% if(planerUser.payscapeVault !== null) { %>--}}



                        <tr>
                            <td><input type="radio" name="payMethod"
                                       value="{{ App\ModelType\PaymentType::PAY_BY_CARD }}" id="payMethod-2"
                                       checked  /></td>
                            <td><label for="payMethod-2">Enter new credit card</label></td>
                        </tr>
                        {{--<script>--}}
                        {{--$('#payMethod-2').on('click',function () {--}}
                        {{--$('.payment_method').css('display','table-row')--}}
                        {{--});--}}
                        {{--$('#payMethod-1').on('click',function () {--}}
                        {{--$('.payment_method').css('display','none')--}}
                        {{--})--}}
                        {{--$('#amex').on('click',function () {--}}
                        {{--$('.amex').css('display','block')--}}
                        {{--})--}}
                        {{--</script>--}}
                        <tr class="payment_method" style="display: table-row">
                            <td colspan="2" style="padding: 5px 10px">
                                <div class="row">
                                    <div class="col-12 col-xs-12 text-center" style="padding: unset !important">
                                        <select id="payment-type-change" class="form-control">
                                            <option value="default" selected disabled>Select payment method</option>
                                            <option class="amex" value="amex">AMEX</option>
                                            <option class="other" value="other">VISA</option>
                                            <option class="other" value="other">MasterCard</option>
                                        </select>
                                    </div>
                                </div>
                            </td>
                        </tr>

                        <tr class="amex" style="display: none">
                            <td>
                                AMEX Card Number
                                <span class="error-message error-cardNumber"></span>
                            </td>
                            <td>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <input type="text" class="form-control card-info" id="amexCardNumber"
                                               name="amexCardNumber"/>
                                    </div>
                                </div>
                            </td>
                        </tr>

                        <tr class="other" style="display: none">
                            <td>
                                Card Number
                                <span class="error-message error-cardNumber"></span>
                            </td>
                            <td>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <input type="text" class="form-control card-info" id="cardNumber"
                                               name="cardNumber"/>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr class="exp-date" style="display: none">
                            <td>
                                Expiration date
                                <span class="error-message error-expirationYear error-expirationMonth"></span>
                            </td>
                            <td>
                                <div class="row">
                                    <div class="col-xs-4">
                                        <select class="form-control card-info" id="expirationMonth"
                                                name="expirationMonth" size="1">
                                            @for ($i=1; $i<=12; $i++)
                                                <option value="{{$i}}">{{ ($i<10) ? "0":"" }}{{ $i }}</option>
                                            @endfor
                                        </select>
                                    </div>

                                    <div class="col-xs-8">
                                        <select class="form-control card-info" id="expirationYear" name="expirationYear"
                                                size="1">
                                            @php $now = Carbon\Carbon::now() ; $yearFrom = $now->format('Y');@endphp
                                            @for ($i=$yearFrom; $i<=($yearFrom+10); $i++)
                                                <option value="{{ $i }}">{{ $i }}</option>
                                            @endfor
                                        </select>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr class="other" style="display: none">
                            <td>
                                CVC
                                <span class="error-message error-cvc"></span>
                            </td>
                            <td>
                                <div class="row">
                                    <div class="col-xs-4">
                                        <input type="text" class="form-control card-info" id="cvc" name="cvc" placeholder=""
                                               maxlength="3"/>
                                    </div>
                                </div>
                            </td>

                        </tr>
                        <tr class="amex" style="display: none">
                            <td>
                                CVV
                                <span class="error-message error-cvv"></span>
                            </td>
                            <td>
                                <div class="row">
                                    <div class="col-xs-4">
                                        <input type="text" class="form-control card-info" id="cvv" name="cvv" placeholder=""
                                               maxlength="4"/>
                                        <input type="hidden" name="paymentToken" value="{{ $token }}">
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>

                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary btn-invert" id="thirdStepPayButton">
                    SUBMIT PAYMENT
                </button>
            </div>
    </div>

