<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />

	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<title>EVAmore</title>

	<link rel="shortcut icon" type="image/x-icon" href="/images/favicon.ico" />
	<link href='//fonts.googleapis.com/css?family=Lato:400,300,300italic,700' rel='stylesheet' type='text/css'>
	<link rel="Stylesheet" type="text/css" href="{{asset('croppie.css')}}" />
    <link rel="Stylesheet" type="text/css" href="{{asset('demo/demo.css')}}" />

	<!-- Vendor Styles -->
	<link rel="stylesheet" href="{{asset("/addons/bootstrap/css/bootstrap.min.css", null, false)}}" type="text/css" media="all" />
    <link rel="stylesheet" href="{{asset("/addons/bootstrap/css/bootstrap-select.min.css", null, false)}}" type="text/css" media="all" />
	<link rel="stylesheet" href="{{asset("/addons/formstone/dropdown.css", null, false)}}" type="text/css" media="all" />
	<link rel="stylesheet" href="{{asset("/addons/bootstrap-datepicker/bootstrap-datepicker.min.css", null, false)}}" type="text/css" media="all" />
	<link rel="stylesheet" href="{{asset("/css/jquery-ui-bootstrap-master/jquery.ui.theme.css", null, false)}}" type="text/css" media="all"/>

	<!-- App Styles -->
	<link rel="stylesheet" href="{{asset( '/css/style.css' )}}"/>
	<link rel="stylesheet" href="{{asset("/bower_components/components-font-awesome/css/font-awesome.min.css", null, false)}}" type="text/css" media="all"/>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

	<!-- Vendor JS -->
	<script src="{{asset("/addons/moment-with-locales.js", null, false)}}"></script>
	<script src="{{asset("/addons/jquery-1.11.3.min.js", null, false)}}"></script>
    <script src="{{asset("/addons/jquery.form.min.js", null, false)}}"></script>
	{{-- <script src="{{asset("/addons/bootstrap/js/bootstrap.min.js", null, false)}}"></script> --}}
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	<script src="{{asset("/addons/formstone/core.js", null, false)}}"></script>
	<script src="{{asset("/addons/formstone/touch.js", null, false)}}"></script>
	<script src="{{asset("/addons/formstone/dropdown.js", null, false)}}"></script>
	<script src="{{asset("/addons/modernizr.custom.js", null, false)}}"></script>
	<script src="{{asset("/addons/classie.js", null, false)}}"></script>
	<script src="{{asset("/addons/bootstrap-datepicker/bootstrap-datepicker.min.js", null, false)}}"></script>
    <script src="{{asset("/addons/bootstrap/js/bootstrap-select.min.js", null, false)}}"></script>

	<!-- App JS -->
	<script src="{{asset(file_exists('/js/functions.min.js') ? '/js/functions.min.js' : '/js/functions.js')}}"></script>
    <script src="{{asset(file_exists('/js/constants.min.js') ? '/js/constants.min.js' : '/js/constants.js')}}"></script>
@include('widgets.google_tagmanager')
</head>
<body>

<div class="wrapper layout-secondary">
	<header class="header header-inside">
		
	<nav class="navbar">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" id="trigger-overlay" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="/">Evamore</a>
			</div>

			<div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav nav-top-menu">
					<li>
						<a href="/artists">ARTISTS</a>
					</li>

					<li class="active">
						<a href="/about">About</a>
					</li>

					<li>
                        <a href="{{ route('support') }}">Support</a>
					</li>
					<li>
						<a href="/events">& MORE</a>
					</li>
				</ul>

				<ul class="nav navbar-nav nav-access">
                    @if (!Auth::check())
                        <li>
                            <a href="/#register" class="link-login">SIGN UP</a>
                        </li>
                    @endif

					<li>
                        @if (Auth::check())
                            <a href="#" class="link-join" onclick="window.location.href='{{ route('dashboard')}}'">DASHBOARD</a>
                        @else
                            <a href="#" class="link-join" data-toggle="modal" data-target="#popup-login">LOGIN</a>
                        @endif
					</li>
				</ul>
			</div><!-- /.navbar-collapse -->
		</div><!-- /.container-fluid -->
	</nav>

	<div class="overlay overlay-hugeinc">
		<button type="button" class="overlay-close">Close</button>
		<nav>
			<ul>
				<li>
					<a href="/artists">ARTISTS</a>
				</li>

				<li>
					<a href="/about">About</a>
				</li>
				<li>
                    <a href="{{ route('support') }}">Support</a>
				</li>
				<li>
					<a href="/events">& More</a>
				</li>

				<li>
					<a href="#" data-toggle="modal" data-target="#popup-login">LOGIN</a>
				</li>

				<li>
					<a href="/#register" >JOIN</a>
				</li>
			</ul>
		</nav>
	</div>
	</header><!-- /.header -->

	@yield('content')
	<footer class="footer">
		<div class="container">
			<div class="socials">
				<ul>
					<li>
						<a target="_blank" href="http://www.facebook.com/evamoreco">
							<i class="ico-facebook"></i>
						</a>
					</li>

					<li>
						<a target="_blank" href="http://www.twitter.com/evamoreco">
							<i class="ico-twitter"></i>
						</a>
					</li>

					<li>
						<a target="_blank" href="http://www.instagram.com/evamoreco">
							<i class="ico-instagram"></i>
						</a>
					</li>
				</ul>
			</div><!-- /.socials -->

            <p class="footer-links">
                <a href="{{ route('support') }}">Support</a><span>•</span>
                <a href="#" data-toggle="modal" data-target="#popup-terms">Terms &amp; Conditions</a><span>•</span>
                <a href="#" data-toggle="modal" data-target="#popup-privacy-policy">Privacy Policy</a><span>•</span>
                <a href="#" data-toggle="modal" data-target="#popup-dmca">DMCA</a>
            </p><!-- /.footer-links -->

            <p class="copyright">© EVAmore {{date('Y')}}</p><!-- /.copyright -->
        </div><!-- /.container -->
    </footer><!-- /.footer -->

    @include('layouts._modal_terms_and_conditions')
    @include('layouts._modal_privacy_policy')
    @include('layouts._modal_dmca')
    @include('layouts._modal_login_form')

    <!-- Modal -->
    <div class="modal fade" id="baseModal" >
        <div class="modal-dialog" role="document">
            <button type="button" class="close outer" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <div class="modal-content"></div>
        </div>
    </div>
</div><!-- /.wrapper -->

<script src="{{asset("/addons/jquery-ui-1.12.1/jquery-ui.min.js", null, false)}}"></script>
<script src="{{asset("/addons/picture-cut-master/src/jquery.picture.cut.js", null, false)}}"></script>
<script src="{{asset("/bower_components/jquery.cookie/jquery.cookie.js", null, false)}}"></script>
<script src="{{asset("/addons/jstz.main.js", null, false)}}"></script>
</body>
</html>
