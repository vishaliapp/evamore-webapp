
@extends('layouts.dash')

@section('js')
@stop

<!-- Give past events link active class-->


@section('content')
	<div class="main-body dashboard">
		<div class="fluid container">
  <div class="row">
    <div class="col-12 dash-padding"><h1>Let’s Get This Party Started.</h1>
</div>
</div>
  <div class="row">
    <div style="height: 450px; min-height: 450px;" class="col-sm-12 col-md-6 col-lg-6 dash-create-event bg-white dash-margin dash-padding"><h2>Let the artists know you’re looking.</h2>
<p>Creating an event listing on the EVAmore platform is fast and easy – just click the button below. You’ll hear back from the performers in our trusted network who can work with your date, location and budget. </p>
<p>Check out their profiles and music right here on our site. Once you make your decision, you’re just a couple of clicks away from safely locking in the music you want. Simple, but oh so effective.</p>
<p style="position: absolute; bottom: 0; text-align: center;"><a href="/events/create" class="btn btn-fill form-btn">Create Your Event</a></p>

</div>
    <div style="height: 450px; min-height: 450px;" class="col-sm-12 col-md-5 col-lg-5 dash-browse-artists bg-white dash-margin dash-padding"><h2>Find your new favorite band.</h2>
<p>Want to explore the EVAmore artist roster now? Click below to see what’s new, what’s trending and let us introduce you to the Next Big Thing.  </p>

<p style="position: absolute; bottom: 0; text-align: center"><a href="/artistsearch" class="btn btn-fill form-btn">Browse Artists</a></p>
</div>
</div>
		</div><!-- /.container -->
	</div><!-- /.main-body -->
@stop
