@extends('layouts.dash')

@section('content')
    <div class="main-body">
        <div class="content">

            <div class="content-head">
                <h2>Payment and Confirmation</h2>
            </div><!-- /.content-head -->

            <div class="content-body">
                @if(Session::has('message'))
                    <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
                @endif

                <h4>To secure this artist, make a down payment of 50% (<?php echo $bid->price/2 ?>)</h4>

                <div>Prefer to pay check? Please contact us at <a href="mailto:{{config('mail.support_email')}}">{{config('mail.support_email')}}</a></div>
                <div class="row">
                    <div class="col-6 col-md-6">
                        <div class="form-group">
                            <label for="nameOnCard">Name On Card</label>
                            <input type="text" class="form-control" id="nameOnCard" name="nameOnCard" placeholder="As it appears on your card"/>
                        </div>

                        <div class="form-group">
                            <label for="cardNumber">Card Number</label>
                            <input type="text" class="form-control" id="cardNumber" name="cardNumber" placeholder="1234 5678 1234 5678"/>
                        </div>

                        <div class="form-group">
                            <label for="cvc">CVC</label>
                            <input type="text" class="form-control" id="cvc" name="cvc" placeholder="just to make sure"/>
                        </div>

                    </div>

                    <div class="col-6 col-md-6">
                        <div class="form-group">
                            <label for="cardType">Card Type</label>
                            <select class="form-control" id="cardType" name="cardType" size="1">
                                <option>Visa</option>
                                <option>Master Card</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Expiration</label>
                            <div>
                                <div class="col-6 col-sm-6">
                                    <input type="text" class="form-control" id="mm" placeholder="MM" size="2"/>
                                </div>
                                <div class="col-6 col-sm-6">
                                    <input type="text" class="form-control" id="yyyy" placeholder="YYYY" size="4"/>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="zipCode">Card Type</label>
                            <input type="text" class="form-control" id="zipCode" placeholder="another eazy one" size="5"/>
                        </div>
                    </div>
                </div>

                <p>
                    <b>Bid: </b> {{ $bid->id }}
                </p>

                <p>
                    <b>Event: </b> {{ $bid->event->name }} at {{ date('d/m/Y H:i', strtotime($bid->event->starttime)) }}
                </p>

                <p>
                    <b>Artist: </b> {{ $artistUser->username }}
                </p>
            </div><!-- /.content-body -->
        </div><!-- /.content -->
    </div><!-- /.main-body -->

@stop