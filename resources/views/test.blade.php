@if($canReadComments || $canWriteComments)
    <div class="content">
        <div class="row">
            <div class="event-comments">
                <div class="form-head">
                    <h2>Comments</h2>
                </div>
                <div class="comments-container"></div>
                @if($canWriteComments)
                    <div class="comment-form">
                        <form class="form-horisontal" method="POST" action="{{route('api_create_comment')}}">
                            {{csrf_field()}}
                            <input type="hidden" name="event_id" value="{{$event->id}}"/>
                            <div class="form-group">
                                {{--<label class="sr-only" for="exampleInputAmount">Amount (in dollars)</label>--}}
                                <div class="input-group">
                                    <input type="text" class="form-control" id="comment-content"
                                           name="content" placeholder="Add a comment" autocomplete="off">
                                    <div class="input-group-addon">
                                <span class="glyphicon glyphicon-file file-form-btn">
                                    <span class="files-amount-wrapper">
                                        (<span class="files-amount"></span>/<span class="amount-allowed">{{\App\Comment::ALLOWED_FILES_AMOUNT}}</span>)
                                    </span>
                                </span>
                                    </div>
                                </div>
                                <div class="input-group">
                                    <input type="hidden" value="" name="files" autocomplete="off"/>
                                </div>
                            </div>
                            <div class="comment-files"></div>
                            <div class="form-group alignright">
                                <button type="submit" class="btn btn-primary btn-invert">Post Comment</button>
                            </div>
                        </form>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endif

<script type="text/template" id="comment-in-list">
    <div>
        <div class="comment-content row">
            <div class="side col-md-3 col-sm-3"><span class="time"><%= comment.created_at %></span></div>
            <% if (comment.can_edit) {%>
                <div class="data col-md-9 col-sm-9">
                    <a href="#" data-id="<%= comment.id %>" class="comment-edit-btn"><span class="glyphicon glyphicon-pencil" style="margin-right: 10px"></span></a>
                    <a href="#" data-id="<%= comment.id %>" class="comment-delete-btn"><span class="glyphicon glyphicon-trash"></span></a>
                </div>
            <% } %>
        </div>
        <div class="comment-content row">
            <div class="side col-md-3 col-sm-3"><%= comment.user.name %></div>
            <div class="data col-md-9 col-sm-9"><div class="comment-body"><%= comment.content %></div></div>
        </div>
        <% if(comment.files.length > 0) { %>
        <div class="comment-content files row">
            <div class="side hidden-att col-md-3 col-sm-3">
                <span class="hide-att">Hide Att.</span>
                <span class="show-att">Show Att.</span>
                <span class="glyphicon glyphicon-triangle-right"></span>
            </div>
            <div class="data col-md-9 col-sm-9" style="display:none">
                <% _.each(comment.files, function(f){ %>
                    <span id="comment-file-<%= f.id %>"><a href="<%= f.url %>"><%= f.name %></a>  <i class="<%= f.downloaded_by_artist ? 'text-success fa fa-eye' : '' %>"></i> </span>
                <% }); %>
            </div>
        </div>
        <% } %>
    </div>
</script>

@include('events._modal_upload_files')

<script type="text/template" id="edit-comment-form-tpl">
    <div class="comment-form">
        <form class="form-horisontal" method="POST" action="{{route('api_update_comment')}}">
            {{csrf_field()}}
            <input type="hidden" name="comment_id" value="<%= comment.id %>"/>
            <div class="form-group">
                <div class="input-group">
                    <input type="text" class="form-control" name="content"  autocomplete="off" value="<%= comment.content %>">
                    <div class="input-group-addon">
                        <span class="glyphicon glyphicon-file file-form-btn"></span>
                    </div>
                </div>
                <div class="input-group">
                    <% var filesIds = [] %>
                    <% for(var i=0; i<comment.files.length; i++){%>
                        <% filesIds.push(comment.files[i].id) %>
                    <% } %>
                    <input type="hidden" value="<%= filesIds.join(",") %>" name="origin_files"/>
                    <input type="hidden" value="<%= filesIds.join(",") %>" name="files"/>
                </div>
            </div>
            <div class="comment-files">
              <% _.each(comment.files, function(f){ %>
                <span class="comment-file-wrapper">
                  <a href="<%= f.url %>" id="comment-file-<%= f.id %>"><%= f.name %></a>
                  <a href="#<%= f.id %>" class="comment-file-edit-remove-link">[x]</a>
                </span>
              <% }); %>
            </div>
            <div class="form-group alignright">
                <button type="button" class="btn btn-danger edit-comment-cancel-button">Cancel</button>
                <button type="submit" class="btn btn-primary btn-invert">Save</button>
            </div>
        </form>
    </div>
</script>

<script type="text/javascript">
    (function($){
        $.fn.eventComments = function(options){
            var self = this;

            self.settings = {
                event_id: null
            };
            $.extend(self.settings, options);

            var $form = $("form", self);

            var loadComments = function(callback){
                //get last comment

                var data = {};
                var $allComments = $('.comment-item', self);
                if ($allComments.length > 0) {
                    data['after_id'] = $allComments.last().attr('data-id');
                }

                $.ajax({
                    url: '/api/comments/' + self.settings.event_id,
                    type: "GET",
                    data: data,
                    dataType: "json",
                    success: function (response) {
                        render(response.comments);
                        if (callback !== undefined){
                            callback(response.comments)
                        }
                        FilesAmount.setFileAmount(0);
                    },
                    error: function (response) {
                    }
                })
            };

            var loadComment = function(id, callback){
                //get last comment
                $.ajax({
                    url: '/api/comment/' + id,
                    type: "GET",
                    dataType: "json",
                    success: function (response) {
                        if (callback !== undefined){
                            callback(response.comment)
                        }

                    },
                    error: function (response) {
                    }
                })
            };

            var renderComment = function(comment, container) {
                var commentTemplate = _.template($('#comment-in-list').text());
                var $content = $(commentTemplate({
                    comment: comment
                }));

                container.html($content);
                $content.on('click', '.edit-comment-cancel-button', function(){
                  loadComment(comment.id, function(comment){
                    var $comment = renderComment(comment, $("#comment-" + comment.id));
                  });

                });

                $('.comment-edit-btn', $content).on('click', function(){
                    var $this = $(this);
                    var id = $this.attr('data-id');
                    loadComment(id, function(comment){
                        var commentFormTpl = _.template($("#edit-comment-form-tpl").text());
                        var $editFormContainer = $('.comment-body', $('#comment-'+id));
                        $editFormContainer.html(commentFormTpl({comment:comment}));

                        var options = {
                            beforeSubmit: function(formData, jqForm, options){
                                for(var i=0; i<formData.length; i++) {
                                    var field = formData[i];
                                    if (field.name == 'content' && field.value == '') {
                                        return false;
                                    }
                                }
                                return true;
                            },  // pre-submit callback
                            success: function(response, statusText, xhr, $form){
                                if (response.success) {

                                    var $comment = renderComment(response.comment, $("#comment-"+response.comment.id));

                                }
                            },
                            dataType:  'json'        // 'xml', 'script', or 'json' (expected server response type)
//                                clearForm: true,        // clear all form fields after successful submit
//                                resetForm: true       // reset the form after successful submit
                        };

                        // bind form using 'ajaxForm'
                        var $form = $('form', $editFormContainer);
                        $form.ajaxForm(options);

                        $('.file-form-btn', $form).on('click', function(){
                            $(".file-form-error-message").remove();
                            var $files = $("input[name=files]", $form)
                            var ar = ($files.val() == "") ? [] : $files.val().split(",");
                            if (ar.length >= {{\App\Comment::ALLOWED_FILES_AMOUNT}}) {
                              $form.prepend('<div class="file-form-error-message errors message-danger" style="margin: 10px; color:#ff6666; font-size: 14px">You can\'t upload more than {{\App\Comment::ALLOWED_FILES_AMOUNT}} documents at a time in a single comment.</div>');
                              return;
                            }

                            var $modal = $("#baseModal").clone().modal({show: false});

                            var tpl = _.template($("#upload-file-form-tpl").text());
                            $(".modal-content", $modal).html(tpl({}));

                            $modal.modal('show');
                            $modal.fileForm({
                                entity: 'none',
                                entity_id: 0,
                                successUploadCallback: function(file) {
                                    $(".file-form-error-message").remove();
                                    var $files = $("input[name=files]", $form)

                                    var ar = ($files.val() == "") ? [] : $files.val().split(",");
                                    ar.push(file.id)

                                    $files.val(ar.join(","));
                                    $modal.modal('hide');

                                    var remove_link = $('<a href="" class="comment-file-edit-remove-link"></a>').attr("href", "#" + file.id).text("[x]");
                                    var $file = $('<a href=""></a>').attr("href", file.url).text(file.name).attr("id", "comment-file-" + file.id);
                                    var wrapper = $('<span class="comment-file-wrapper"></span>').append($file).append(remove_link);
                                    $form.find('.comment-files').append(wrapper);
                                },
                                errorUploadCallback: function(errors) {
                                    $(".file-form-error-message").remove();
                                    var keys = Object.keys(errors);
                                    _.each(keys, function(k){
                                        $('.errors', $modal).append(errors[k]);
                                    });
                                },
                                beforeUpload:function(){
                                    $('.errors', $modal).html('');
                                }
                            });
                        });

                        // Remove already choosed file.
                        $(".comment-files").on("click", ".comment-file-edit-remove-link", function(e) {
                          e.preventDefault();
                          $(".file-form-error-message").remove();

                          var id = $(this).attr("href");
                          id = id.replace("#", "");
                          var $files = $("input[name=files]", $form);
                          var ar = ($files.val() == "") ? [] : $files.val().split(",");
                          var index = ar.indexOf(id);
                          if(index!=-1){
                            ar.splice(index, 1);
                            $files.val(ar.join(","));
                            $('.comment-files', self).find("a#comment-file-" + id).parent(".comment-file-wrapper").remove();
                          }
                        });

                    });
                    return false;
                });

                $('.comment-delete-btn', $content).on('click', function(){
                    var $this = $(this);
                    var id = $this.attr('data-id');
                    $.post('/api/comment/delete', {
                        id:id,
                        _token: '{{csrf_token()}}'
                    }, function(response){
                        if (response.success){
                            $('#comment-'+id).remove();
                        }
                    });
                    return false;
                });

                $(".files .side", $content).on('click', function (e) {
                    e.preventDefault();
                    var $this = $(this);
                    if ($this.hasClass("hidden-att")){
                        $this.next().show();
                        $this.removeClass("hidden-att")
                    } else {
                        $this.next().hide();
                        $this.addClass("hidden-att")
                    }
                });

                return $content;
            };

            var render = function(comments){

                for(var i=0; i<comments.length; i++) {
                    var comment = comments[i];
                    var $commentContainer = $('<div class="comment-item" data-id="'+comment.id+'" id="comment-'+comment.id+'"></div>');
                    renderComment(comment, $commentContainer);

                    $('.comments-container', self).append($commentContainer);
                }
            };

            var addListeners = function (){
                var options = {
//                    target:        '#output1',   // target element(s) to be updated with server response
                    beforeSubmit: function(formData, jqForm, options){
                        for(var i=0; i<formData.length; i++) {
                            var field = formData[i];
                            if (field.name == 'content' && field.value == '') {
                                return false;
                            }
                        }
                        return true;
                    },  // pre-submit callback
                    success: function(response, statusText, xhr, $form){
                        if (response.success) {
                            loadComments();
                            $(".file-form-error-message").remove();
                            $(".comment-files").html("");
                            $("input[name=files]", ".comment-form").val("");
                        }
                    },

                    // other available options:
                    //url:       url         // override for form's 'action' attribute
                    //type:      type        // 'get' or 'post', override for form's 'method' attribute
                    dataType:  'json',        // 'xml', 'script', or 'json' (expected server response type)
                    clearForm: true,        // clear all form fields after successful submit
                    resetForm: true       // reset the form after successful submit

                    // $.ajax options can be used here too, for example:
                    //timeout:   3000
                };

                // bind form using 'ajaxForm'
                $form.ajaxForm(options);

                $('.file-form-btn', $form).on('click', function(){
                  $(".file-form-error-message").remove();
                  var $files = $("input[name=files]", $form)
                  var ar = ($files.val() == "") ? [] : $files.val().split(",");
                  if (ar.length >= {{\App\Comment::ALLOWED_FILES_AMOUNT}}) {
                    $form.prepend('<div class="file-form-error-message errors message-danger" style="margin: 10px; color:#ff6666; font-size: 14px">You can\'t upload more than {{\App\Comment::ALLOWED_FILES_AMOUNT}} documents at a time in a single comment.</div>');
                    return;
                  }

                    var $modal = $("#baseModal").clone().modal({show: false});

                    var tpl = _.template($("#upload-file-form-tpl").text());
                    $(".modal-content", $modal).html(tpl({}));

                    $modal.modal('show');
                    $modal.fileForm({
                        entity: 'none',
                        entity_id: 0,
                        successUploadCallback: function(file) {
                            $(".file-form-error-message").remove();
                            var $files = $("input[name=files]", $form)

                            var ar = ($files.val() == "") ? [] : $files.val().split(",");
                            ar.push(file.id)

                            $files.val(ar.join(","));
                            $modal.modal('hide');

                            var remove_link = $('<a href="" class="comment-file-remove-link"></a>').attr("href", "#" + file.id).text("[x]");
                            var $file = $('<a href=""></a>').attr("href", file.url).text(file.name).attr("id", "comment-file-" + file.id);
                            var wrapper = $('<span class="comment-file-wrapper"></span>').append($file).append(remove_link);
                            $form.find('.comment-files').append(wrapper);
                            FilesAmount.setFileAmount( $('.comment-files > .comment-file-wrapper ').length );
                        },
                        errorUploadCallback: function(errors) {
                            $(".file-form-error-message").remove();
                            var keys = Object.keys(errors);
                            _.each(keys, function(k){
                                $('.errors', $modal).append(errors[k]);
                            });
                        },
                        beforeUpload:function(){
                            $('.errors', $modal).html('');
                        }
                    });
                });

                // Remove already choosed file.
                $(".comment-files").on("click", ".comment-file-remove-link", function(e) {
                  e.preventDefault();
                  $(".file-form-error-message").remove();

                  var id = $(this).attr("href");
                  id = id.replace("#", "");
                  var $files = $("input[name=files]", $form);
                  var ar = ($files.val() == "") ? [] : $files.val().split(",");
                  var index = ar.indexOf(id);
                  if(index!=-1){
                    ar.splice(index, 1);
                    $files.val(ar.join(","));
                    $('.comment-files', self).find("a#comment-file-" + id).parent(".comment-file-wrapper").remove();
                      FilesAmount.setFileAmount( $('.comment-files > .comment-file-wrapper ').length );
                  }
                });
            };

            var init = function() {
                loadComments(function(comments){
                    var idHash = window.location.hash.substr(1);
                    if (idHash != '' && $('#'+idHash).length) {
                        $("html, body").animate({ scrollTop: $('#'+idHash).offset().top }, 1000);
                    }
                });
                addListeners();
            };

            init();
        };

        $(".event-comments").eventComments({
            'event_id': '{{$event->id}}'
        })

    })(jQuery);

    $.fn.fileForm = function(options) {
        var self = this;

        self.settings = {
            entity: null,
            entity_id: null,
            successUploadCallback: function(file){},
            errorUploadCallback: function(error){},
            beforeUpload: function(){}
        };
        $.extend(self.settings, options);

        // Grab the files and set them to our variable
        function prepareUpload(event)
        {
            files = event.target.files;
        }

        // Catch the form submit and upload the files
        function uploadFiles(event)
        {
            event.stopPropagation(); // Stop stuff happening
            event.preventDefault(); // Totally stop stuff happening
            self.settings.beforeUpload();

            let target = event.target || event.srcElement;
            let submit_button = $(target).find("button.btn-invert");

            if (submit_button.length) {
              submit_button.prop("disabled", true);
              let event_html_origin = submit_button.html();
              submit_button.html(event_html_origin + ' <span class="eva-loading"></span>');
              submit_button.attr('origin_text', event_html_origin);
            }

            // START A LOADING SPINNER HERE

            // Create a formdata object and add the files
            var data = new FormData();
            data.append('entity', self.settings.entity);
            data.append('entity_id', self.settings.entity_id);
            $.each(files, function(key, value) {
                data.append('file', value);
            });

            //Add CSRF token
            var _token = $("input[name=_token]", self).val()

            data.append('_token', _token);

            $.ajax({
                url: '/api/file/upload?proceed=1',
                type: 'POST',
                data: data,
                cache: false,
                dataType: 'json',
                processData: false, // Don't process the files
                contentType: false, // Set content type to false as jQuery will tell the server its a query string request
                success: function(data, textStatus, jqXHR)
                {
                    if (submit_button.length) {
                      submit_button.prop("disabled", false);
                      submit_button.html(submit_button.attr('origin_text'));
                    }
                    if(data.success) {
                        // Success so call function to process the form
                        //submitForm(event, data);
                        self.settings.successUploadCallback(data.file);
                        //console.log(data);
                    }
                    else
                    {
                        // Handle errors here
//                        console.log('ERRORS: ' + data.error);
                        self.settings.errorUploadCallback(data.errors);
                    }
                },
                error: function(jqXHR, textStatus, errorThrown)
                {
                    if (submit_button.length) {
                      submit_button.prop("disabled", false);
                      submit_button.html(submit_button.attr('origin_text'));
                      submit_button.html(event_html_origin);
                    }
                    self.settings.errorUploadCallback(textStatus);
                    // Handle errors here
//                    console.log('ERRORS: ' + textStatus);
                    // STOP LOADING SPINNER
                }
            });
        }

        var init = function() {
            // Add events
            $('input[type=file]', self).on('change', prepareUpload);

            $('form', self).on('submit', uploadFiles);
        };

        init();
    }
</script>