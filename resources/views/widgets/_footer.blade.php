<footer class="footer">
    <div class="container">
        <div class="socials">
            <ul>
                <li>
                    <a target="_blank" href="http://www.facebook.com/evamoreco">
                        <i class="ico-facebook"></i>
                    </a>
                </li>

                <li>
                    <a target="_blank" href="http://www.twitter.com/evamoreco">
                        <i class="ico-twitter"></i>
                    </a>
                </li>

                <li>
                    <a target="_blank" href="http://www.instagram.com/evamoreco">
                        <i class="ico-instagram"></i>
                    </a>
                </li>
            </ul>
        </div><!-- /.socials -->

        <p class="footer-links">
            <a href="{{ route('support') }}">Support</a><span>•</span>
            <a href="#" data-toggle="modal" data-target="#popup-terms">Terms &amp; Conditions</a><span>•</span>
            <a href="#" data-toggle="modal" data-target="#popup-privacy-policy">Privacy Policy</a><span>•</span>
            <a href="#" data-toggle="modal" data-target="#popup-dmca">DMCA</a>
        </p><!-- /.footer-links -->

        <p class="copyright">© EVAmore {{date('Y')}}</p><!-- /.copyright -->
    </div><!-- /.container -->
</footer><!-- /.footer -->
@include('layouts._modal_terms_and_conditions')
@include('layouts._modal_privacy_policy')
@include('layouts._modal_dmca')
@include('widgets._base_modal')
@include('layouts._modal_login_form')