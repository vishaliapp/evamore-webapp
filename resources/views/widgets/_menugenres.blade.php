@foreach ($genres as $g)
    <div class="dark-genres-link-wrapper">
        <a data-genre_name="{{$g->name}}" class="artist-filter-genre {{in_array($g->name, $request_genres) !== false ? 'active' : ''}}" href="#">
            <span>{{$g->name}}</span>
        </a>
    </div>
@endforeach