<header class="header header-inside dark-header-inside">
    <nav class="navbar">
        <div class="fluid-container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" id="trigger-overlay" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand dark-navbar-brand" href="/">Evamore</a>
            </div>

            <div class="collapse navbar-collapse navbar-right dark-navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav nav-top-menu">
                    <li class="{{url()->current() == route('home_artists') ? "active" : ""}}">
                        <a href="{{route('home_artists')}}">Artists</a>
                    </li>

                    <li class="{{url()->current() == route('home_about') ? "active" : ""}}">
                        <a href="{{route('home_about')}}">About</a>
                    </li>

                    <li class="{{url()->current() == route('support') ? "active" : ""}}">
                        <a href="{{ route('support') }}">Support</a>
                    </li>

                    <li class="{{url()->current() == route('home_events') ? "active" : ""}}">
                        <a href="{{route('home_events')}}">& More</a>
                    </li>
                </ul>

                <ul class="nav navbar-nav nav-access">
                    @if (!Auth::check())
                        <li>
                            <a href="/#register" class="link-login">SIGN UP</a>
                        </li>
                    @endif

                    <li>
                        @if (Auth::check())
                            <a href="#" class="link-join" onclick="window.location.href='{{ route('dashboard')}}'">DASHBOARD</a>
                        @else
                            <a href="#" class="link-join" data-toggle="modal" data-target="#popup-login">LOGIN</a>
                        @endif
                    </li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>

    <div class="overlay overlay-hugeinc">
        <button type="button" class="overlay-close">Close</button>
        <nav>
            <ul>
                <li class="{{url()->current() == route('home_artists') ? "active" : ""}}">
                    <a href="{{route('home_artists')}}">Artists</a>
                </li>

                <li class="{{url()->current() == route('home_about') ? "active" : ""}}">
                    <a href="{{route('home_about')}}">About</a>
                </li>

                <li class="{{url()->current() == route('support') ? "active" : ""}}">
                    <a href="{{ route('support') }}">Support</a>
                </li>

                <li class="{{url()->current() == route('home_events') ? "active" : ""}}">
                    <a href="{{route('home_events')}}">& More</a>
                </li>

                <li>
                    <a href="#" data-toggle="modal" data-target="#popup-login">LOGIN</a>
                </li>

                <li>
                    <a href="/#register" >JOIN</a>
                </li>
            </ul>
        </nav>
    </div>
</header><!-- /.header -->
